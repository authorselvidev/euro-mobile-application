import React, { Component } from 'react';
import { Dimensions,Image,View,Text, Keyboard, AppState  } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import { createStackNavigator } from '@react-navigation/stack';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import SplashScreen from 'react-native-splash-screen'
import AsyncStorage from '@react-native-async-storage/async-storage';

import images from './images/index'
import {Colors} from '../src/screens/Styles/colors';


import IntroScreens from './screens/IntroScreens';
import HomeScreen from './screens/HomeScreen';
import SearchScreen from './screens/SearchScreen';
import OrderScreen from './screens/OrderScreen';
import CartScreen from './screens/CartScreen';
import CustomeSideMenu from './screens/CustomeSideMenu'; 
import Signin from './screens/Signin';
import PostCode from './screens/PostCode';
import Signup from './screens/Signup';
import CategoryList from './screens/CategoryList';
import CompleteOrder from './screens/CompleteOrder';
import PaymentScreen from './screens/PaymentScreen';
import OrderPlaced from './screens/OrderPlaced';
import CreateNewSite from './screens/CreateNewSite';
import MyOrderDetails from './screens/MyOrderDetails';
import Notification from './screens/Notification';
import SingleProductScreen from './screens/SingleProductScreen';
import WishList from './screens/WishList';
import ManageSiteAddress from './screens/ManageSiteAddress';
import MyProfile from './screens/MyProfile';
import ChangePassword from './screens/ChangePassword';
import Help from './screens/Help';
import ShopByCategory from './screens/ShopByCategory';
import OrdersAvailable from './screens/OrdersAvailable';
import PasswordChanged from './screens/PasswordChanged';
import ChangePostCode from './screens/ChangePostCode';
import ForgetPassword from './screens/ForgetPassword';
import NewpasswordUpdated from './screens/NewPasswordUpdated';
import CartCount from './screens/CartCount';
import OrderCancellingReturning from './screens/OrderCancellingReturning';
import EditAddress from './screens/EditAddress';
import TrackOrder from './screens/TrackOrder';
import NewOrder from './screens/NewOrder';
import messaging from '@react-native-firebase/messaging'
import {connect} from 'react-redux'
import PushNotification from 'react-native-push-notification';


// const tabBarListeners = ({ navigation, route }) => ({
//   tabPress: () => console.error("tab press")
// });

 class Navigation extends Component {
    
  constructor(props) {
    super(props);
    this.StartWith;
    this.state = {
      userId:null,
      startWith:null,
      startWithIntro:'introScreens',
      startWithSignin:'signin',
      startWithHomeScreen:'homeScreen',
      notificationArrived:false,
      appState: AppState.currentState
    };
  }

  componentDidMount(){
    this.getNotify()
  }

  componentDidUpdate(){
    if(this.props.initialRouteInHome=="homeScreen"){
      this.props.setinitialRouteInHome(null)
       this.getmethod()
    }
  }

 async getNotify(){
   await messaging().onMessage(async remoteMessage => {
      console.warn("index  screen "+ remoteMessage.notification.title,remoteMessage.notification.body)
      PushNotification.localNotification({
            /* Android Only Properties */
            channelId: "123456", // (required) channelId, if the channel doesn't exist, notification will not trigger.
            // largeIcon: "ic_launcher", // (optional) default: "ic_launcher". Use "" for no large icon.
            // smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
            title: remoteMessage.notification.title, // (optional)
            message:remoteMessage.notification.body, // (required)
            playSound:true, // (optional) default: true
            soundName:'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
          });   
      this.props.setOrderCountChanged(true)
     })
  
   await messaging().onNotificationOpenedApp(remoteMessage => {
     if (remoteMessage) {
       var remote=remoteMessage.notification.body
       console.log('Notification from background state:'+JSON.stringify(remoteMessage.notification));
       this.setState({notificationArrived:true})
       setTimeout(() => {
        if(this.state.notificationArrived==false){
           this.getmethod()
        }else{
          this.props.setorderId(remoteMessage.notification.body.slice(15,17))
          this.props.setinitialRoute("myOrderDetails")
          SplashScreen.hide(); 
         }
      }, 2000);
      }
     });
 
   await messaging().getInitialNotification().then(remoteMessage => {
         if (remoteMessage) {
           console.log('Notification from quit state:'+remoteMessage.notification);
           this.setState({notificationArrived:true})
           setTimeout(() => {
            if(this.state.notificationArrived==false){
               this.getmethod()
            }else{
              this.props.setorderId(remoteMessage.notification.body.slice(15,17))
              this.props.setinitialRoute("myOrderDetails")
              SplashScreen.hide(); 
             }
          }, 2000);
         }
       }); 
    console.log("notify arrive "+this.state.notificationArrived)
     setTimeout(() => {
        if(this.state.notificationArrived==false){
           this.getmethod()
        }
      }, 2000);
    }

async getmethod(){ 
  console.warn("navigation update")
  await AsyncStorage.getItem('IntroScreenVisited').then((value) => {
     if(value!=null){
       AsyncStorage.getItem('userId').then((value) => {
         if(value!=null){
          this.props.setinitialRoute(this.state.startWithHomeScreen)
         }else{
            AsyncStorage.getItem('guestId').then((value) => {
              if(value!=null){
              this.props.setinitialRoute(this.state.startWithHomeScreen)
              }else{
              this.props.setinitialRoute(this.state.startWithSignin)
              }
            })
         }
       })

     }else{
      this.props.setinitialRoute(this.state.startWithIntro)
     }
   })
  console.log("props -- "+ this.props.initialRoute) 
  if(this.props.initialRoute!=null){
    SplashScreen.hide();
  }
  }
  
  render() {
   
    const Stack = createStackNavigator();

    const TabStack=createStackNavigator();

    const Tab = createBottomTabNavigator();

    const Drawer=createDrawerNavigator();

    const TabStackNavigator=()=>(
      <TabStack.Navigator initialRouteName={this.props.initialRouteInHome}>
        <Stack.Screen options={{headerShown:false,drawerLockMode:'locked-closed'}} name="homeStack" component={HomeScreen}/>
        <Stack.Screen options={{headerShown:false}} name="categoryList" component={CategoryList}/>
      </TabStack.Navigator>
    )

    const TabsScreen=()=>(
        <Tab.Navigator 
          initialRouteName={"home"}
          tabBarOptions={{
            activeTintColor: Colors.themeColor,
          }}>
        <Tab.Screen name="home" component={TabStackNavigator} 
        options={{
          headerShown:false,
          tabBarLabel: 'Home',
          tabBarIcon: ({ color,focused }) => {
          if(focused==true){
            return(        
               <Image source={images.homeIconWithColor} style={{width:25,height:25,resizeMode:'contain'}} color={color}/>
            )
           }else{
            return(
            <Image source={images.homeIconWithoutColor} style={{width:25,height:25,resizeMode:'contain'}} color={color}/>
           )
          }
          }
        }}/>
        <Tab.Screen name="search" component={SearchScreen}
            options={{
              headerShown:false,
              tabBarLabel: 'Search',
              tabBarIcon: ({ color,focused }) => {
              if(focused==true){
                return(          
                <Image source={images.searchIconWithColor} style={{width:25,height:25,resizeMode:'contain'}} color={color}/>
                )
              }else{
                return(
                <Image source={images.searchIconWithoutColor} style={{width:25,height:25,resizeMode:'contain'}} color={color}/>
              )
              }
              }
            }}
            />
        <Tab.Screen name="myOrder" component={OrderScreen}
          
          options={{
            headerShown:false,
            tabBarLabel: 'My orders',
            tabBarIcon: ({ color,focused }) => {
              if(focused==true){
                return(          
                <Image source={images.ordersIconWithColor} style={{width:25,height:25,resizeMode:'contain'}} color={color}/>
                )
              }else{
                return(
                <Image source={images.ordersIconWithoutColor} style={{width:25,height:25,resizeMode:'contain'}} color={color}/>
              )
              }
              }
          }}
          // listeners={tabBarListeners}
          />

        <Tab.Screen name="newOrder" component={NewOrder}
          options={{
            headerShown:false,
            tabBarLabel: 'New order',
            tabBarIcon: ({ color,focused }) => {
              if(focused==true){
                return(          
                <Image source={images.newOrderWithColor} style={{width:20,height:20,resizeMode:'contain'}} color={color}/>
                )
              }else{
                return(
                <Image source={images.newOrderWithoutColor} style={{width:20,height:20,resizeMode:'contain'}} color={color}/>
              )
              }
              }
          }}
          />

        <Tab.Screen name="myCart" component={CartScreen}
          options={{
            headerShown:false,
            tabBarVisible: false,
            tabBarLabel: 'My cart',
            tabBarIcon: ({ color,focused }) => {
              return(
                <View>
                    <CartCount/>              
                </View>
              )}
          }}
          />
     </Tab.Navigator>
  )

  const DrawerScreen=()=>(
    <Drawer.Navigator drawerPosition="right" drawerStyle={{backgroundColor: '#c6cbef',width:'70%'}} drawerContent={(props)=><CustomeSideMenu {...props}/>}>
        <Drawer.Screen options={{headerShown:false}} name="drawer" component={TabsScreen}/>
    </Drawer.Navigator>
  )
 
  const AppContainer=()=>(
    <NavigationContainer>
           <Stack.Navigator initialRouteName={this.props.initialRoute}>
                <Stack.Screen options={{headerShown:false}}  name="homeScreen" component={DrawerScreen}/>
                <Stack.Screen options={{headerShown:false}} name="introScreens" component={IntroScreens}/>
                <Stack.Screen options={{headerBackTitle:null,headerTransparent:true,title:''}} name="postCode" component={PostCode}/>
                <Stack.Screen options={{headerBackTitle:null,headerTransparent:true,title:''}} name="ordersAvailable" component={OrdersAvailable}/>
                <Stack.Screen options={{headerShown:false}} name="signin" component={Signin} />
                <Stack.Screen options={{headerBackTitle:null,headerTransparent:true,title:''}} name="signup" component={Signup}/>
                
                <Stack.Screen 
                        options={{ title: 'Forgot password',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}                 
                 name="forgetPassword" component={ForgetPassword}/>
                
                <Stack.Screen options={{headerShown:false}} name="newPasswordUpdated"  component={NewpasswordUpdated}/> 
                
                <Stack.Screen 
                      options={{headerShown:false}}
                      //   options={{ title: 'Create New Job Site',
                      //   headerTintColor:Colors.whiteBg,
                      //   headerTitleAlign:'center',
                      //   headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                      //   headerStyle: {backgroundColor:Colors.themeColor},
                      //   headerBackTitle:null
                      // }}                 
                name="createNewSite" component={CreateNewSite}/>
                
                <Stack.Screen options={{headerShown:false}}  name="notification" component={Notification}/>

               <Stack.Screen 
               options={{ title: 'My profile',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}       
                name="myProfile" component={MyProfile}/>

               <Stack.Screen 
               options={{ title: 'Wishlist',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}       
                name="wishList" component={WishList}/>

               <Stack.Screen 
               options={{ title: 'Change password',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}       
                name="changePassword" component={ChangePassword}/>

               <Stack.Screen
               options={{ title: 'Change post code',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}   
                name="changePostcode" component={ChangePostCode}/>

               <Stack.Screen 
                options={{headerShown:false}}
                name="passwordChanged" component={PasswordChanged}/>

               <Stack.Screen 
               options={{ title: 'Help',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}   
                name="help" component={Help}/>

              <Stack.Screen options={{ headerShown:false}} name="shopByCategory" component={ShopByCategory}/>


               <Stack.Screen 
              //  options={{ title: 'Manage site address',
              //           headerTintColor:Colors.whiteBg,
              //           headerTitleAlign:'center',
              //           headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
              //           headerStyle: {backgroundColor:Colors.themeColor},
              //           headerBackTitle:null
              //         }} 
                options={{headerShown:false}}  
                name="manageSiteAddress" component={ManageSiteAddress}/>

               <Stack.Screen 
               options={{ title: 'Edit address',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}   
                name="editAddress" component={EditAddress}/>

                <Stack.Screen
                // options={{ title: 'Order details',
                //         headerTintColor:Colors.whiteBg,
                //         headerTitleAlign:'center',
                //         headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                //         headerStyle: {backgroundColor:Colors.themeColor},
                //         headerBackTitle:null
                //       }}   
                options={{headerShown:false}}
                 name="myOrderDetails" component={MyOrderDetails}/>

               <Stack.Screen
                options={{ title: 'Track order',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}   
                 name="trackOrder" component={TrackOrder}/>

                <Stack.Screen
                options={{ title: 'Edit placed order',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor},
                        headerBackTitle:null
                      }}   
                 name="orderCancellingReturning" component={OrderCancellingReturning}/>

                

                <Stack.Screen 
                options={{ title: 'Selected product',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor,},
                        headerBackTitle:null
                      }}   
                 name="singleProductScreen" component={SingleProductScreen}/>

                {/* <Stack.Screen options={{headerShown:false}} name="categoryList" component={CategoryList}/> */}
                
                <Stack.Screen 
                options={{headerShown:false}}   
                 name="completeOrder" component={CompleteOrder}/>
                 
                <Stack.Screen 
                options={{ title: 'Payment options',
                        headerTintColor:Colors.whiteBg,
                        headerTitleAlign:'center',
                        headerTitleStyle:{fontSize:17,color:Colors.whiteBg,fontFamily:'Montserrat-SemiBold'},
                        headerStyle: {backgroundColor:Colors.themeColor,}
                      }}   
                 name="paymentScreen" component={PaymentScreen}/>

                <Stack.Screen options={{headerShown:false}} name="orderPlaced" component={OrderPlaced}/>
                
            </Stack.Navigator>
        </NavigationContainer>
  )

    return <AppContainer/>
    }
}

function mapStateToProps(state){
  return{
    initialRouteInHome:state.initialRouteInHome,
    initialRoute:state.initialRoute
  }
}

function mapDispatchToProps(dispatch){
  return{
   // setnavigationScreenUpdate:(value)=>dispatch({type:"setnavigationScreenUpdate",value})
   setinitialRouteInHome:(value)=>dispatch({type:"setinitialRouteInHome",value}),
   setinitialRoute:(value)=>dispatch({type:"setinitialRoute",value}),
   setOrderCountChanged:(value)=>dispatch({type:"setOrderCountChanged",value}),
   setorderId:(value)=>dispatch({type:"setorderId",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Navigation);
