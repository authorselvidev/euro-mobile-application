
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification'


//  messaging().onMessage(async remoteMessage => {
//     console.warn("index  screen "+ remoteMessage.notification.title,remoteMessage.notification.body)
//     PushNotification.localNotification({
//           /* Android Only Properties */
//           channelId: "123456", // (required) channelId, if the channel doesn't exist, notification will not trigger.
//           largeIcon: "ic_launcher", // (optional) default: "ic_launcher". Use "" for no large icon.
//           smallIcon: "ic_launcher", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
//           title: remoteMessage.notification.title, // (optional)
//           message:remoteMessage.notification.body, // (required)
//           playSound:true, // (optional) default: true
//           soundName:'tone', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
//         });   
//      })    
// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message handled in the background!', remoteMessage);
});

AppRegistry.registerComponent(appName, () => App);
