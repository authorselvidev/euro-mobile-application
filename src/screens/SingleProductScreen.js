import React, { Component } from 'react';
import { View, Text ,Dimensions,ScrollView,TouchableOpacity,Modal,Image,KeyboardAvoidingView, TextInput,ActivityIndicator, FlatList} from 'react-native';
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,SingleProductScreenStyle,ProductViewStyle,CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle'
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import WebView from 'react-native-webview';
import CustomNotification from './CustomNotification';
import SingleProductRelatedProducts from './singleProductRelatedProducts';
import { Colors } from './Styles/colors';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {Thumbnail} from 'native-base'
import Swiper from 'react-native-swiper';
import { Button, Snackbar,Avatar } from 'react-native-paper';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
  contentSize.height - paddingToBottom;
};

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

class SingleProductScreen extends Component {
  
state = {
    loading:false,
    selectedProdIndex:null,
    selectedCategoryName:'',
    choosedCategoryIndex:0,
    selectedProductIndex:0,
    selectedProduct:{},
    selectedVariant:{},
    relatedProductSelectedVariant:{},
    modalSelectedVariants:{},
    modalVisible:false,
    relatedProducts:[],
    loadType:[{name:"Loose",status:false},{name:"1TON bag",status:true},{name:'40kg bag',status:false}],
    id:'',
    name:'',
    price:null,
    quantity:1,
    description:'',
    postcode:'',
    isFavourite:false,
    images:[],
    choosedMainImageIndex:0,
    variants:[],
    variants1:[],
    response:[],
    isLoading:false,
    isLoadingBuynow:false,
    webViewHeight:null,
    page:1,
    nextPageAvailable:true,
    nextPageLoading:false,
    
    productOptions:[],
    productOptionCount:null,
    dummyArray:[],
    availableCombinations:[],
    combinationStatus:null,
    variantId:null,
    rowId:null,

    prodPrice:null,

    isediting:false,
    iseditingValue:null,
    snackBarVisible:false
};
  
componentDidMount(){
    console.error("mount")
    //consle.log("prod id "+this.props.productId)
    this.getSingleProductDetails() 
}

componentWillUnmount(){
  // this.props.setProductCountChanged(true)
  //consle.log("unmound")
}

componentDidUpdate(prevProps){
    
    // if(prevProps.postCode!==this.props.postCode){
    //     //consle.log("update")
    //     this.getSingleProductDetails()
    //     console.error("post code mount")
    // }else
     if(this.props.singleProductPageUpdate!==prevProps.singleProductPageUpdate){
      console.error("signle page mount")
      // this.getSingleProductDetailsWithoutLoading()
      this.getSingleProductDetails()
    }
}
   getSingleProductDetailsWithoutLoading(){
     fetch(baseUrl+this.props.userId+'/product/'+this.props.productId)
        .then((response) => response.json())
        .then((responseJson) => {
         //console.warn(responseJson.images)
            this.setState({
                response:responseJson,
                name:responseJson.name,
                price:responseJson.product_variants[0].price.toFixed(2),
                quantity:responseJson.quantity,
                description:responseJson.description,
                postcode:responseJson.postcode,
                isFavourite:responseJson.is_favourite,
                images:responseJson.images,
                relatedProducts:responseJson.related_products,
                productOptions:responseJson.product_options,
                productOptionCount:responseJson.product_option_count,
            })
         })  
         
         this.props.setSingleProductPageUpdate(false)
         this.props.setCategoryProductCountChanged(false) 
      }

async getSingleProductDetails() {
  this.setState({loading:true})
  console.warn(baseUrl+this.props.userId+'/product/'+this.props.productId)
  await fetch(baseUrl+this.props.userId+'/product/'+this.props.productId)
        .then((response) => response.json())
        .then((responseJson) => {
        //  console.error(responseJson.images)
          this.props.setRelatedProducts(responseJson.related_products)
          
          var dummyArray=new Array(responseJson.product_option_count)
          var variantOption=responseJson.product_options
          variantOption.map((item,key)=>{            
              item.option.map((item,index)=>{
                //if(responseJson.default_variant_id==item.variant_id){
                  if(responseJson.product_variants[0].id==item.variant_id){
                  item.selected=true
                  dummyArray.splice(0,0,item.option_value_id)
                }
              })
          })
          var filtered = dummyArray.filter(item=> {
            return item != null;
          });
          
          var reverced=filtered.reverse()
          
          // var img=responseJson.images
          // var imgarr=[]
          // img.map((i,ind)=>{
          //   imgarr.push(i)
          // })

          // console.warn(imgarr)
          
            this.setState({
                dummyArray:reverced,
                response:responseJson,
                price:responseJson.product_variants[0].price.toFixed(2),
                name:responseJson.name,
                description:responseJson.description,
                postcode:responseJson.postcode,
                isFavourite:responseJson.is_favourite,
                images:responseJson.images,
                relatedProducts:responseJson.related_products,
                productOptions:variantOption,
                productOptionCount:responseJson.product_option_count,
                availableCombinations:responseJson.product_variants,
                loading:false,
                page:1,
                nextPageAvailable:true
            })
            if(this.state.productOptions.length!=0){
              this.compareArrayValues()
            }else{
              this.setState({combinationStatus:true,variantId:this.state.availableCombinations[0].id})
            }
            
        })  
        await this.props.setSingleProductPageUpdate(false)
        await this.props.setCategoryProductCountChanged(false) 
    
}

changeChoosedMainImageIndex(index){
    this.setState({choosedMainImageIndex:index})
}

  
 async buyNow(){  
  if(this.state.quantity==0 || this.state.quantity==""){
    alert("Please add quantity!")
  }else{  
   //consle.log(this.props.userId,this.props.productId,this.state.variantId,this.state.quantity)
   await this.setState({isLoadingBuynow:true})
    fetch(baseUrl+this.props.userId+'/buynow', {
           method: 'POST',
           headers: {
           'Content-Type': 'application/json',
           },
           body: JSON.stringify({
              "customer_id":this.props.userId,
              "product_id":this.props.productId,
              "product_variant_id" :this.state.variantId,    
              "quantity":this.state.quantity,
           })
   })
       .then((response) => response.json())
       .then((responseJson) => {
            this.buynowResponse(responseJson)
          //  var arr= Object.values(responseJson.data)
          //  //consle.log((arr[0].rowId))
          //  this.setState({isLoading:false})
       }).catch(e=>{
         //consle.log("buynow catch "+e)
         //this.navigation()
       })
      //  await this.props.setCategoryProductCountChanged(true)
      //  await this.props.setProductCountChanged(true)
       await this.props.setAddressChanged(true)
       let product=this.state.response
       product.quantity=1
      await this.setState({response:product})
  }
}

 async navigation(){
  await this.props.navigation.navigate("completeOrder")
  }

 async buynowResponse(responseJson){
    if(responseJson.success==true){
      // //consle.log("response "+JSON.stringify(responseJson.data))
      var arr= Object.values(responseJson.data)
       await this.setState({rowId:arr[0].rowId})
      //  //consle.log(JSON.stringify(arr[0].rowId))
      //  //consle.log(JSON.stringify(arr))
    //consle.log("state  " + this.state.rowId)
     //this.changeProductValue(indexValue)
    await this.props.setSingleProductId(this.state.rowId)
    //consle.log("props "+this.props.singleProductId)
    // this.props.setSingleProductVariantId(responseJson.variant_id)
     if(this.props.deliveryType==0){
      await this.props.setorderDeliverType(0)
     }else{
      await this.props.setorderDeliverType(1)
     }
    await this.props.navigation.navigate("completeOrder")
     }
     this.setState({isLoadingBuynow:false})
  }

  async addtocart(){     
    console.warn(this.state.quantity)
    if(this.state.quantity==0 || this.state.quantity==""){
      alert("Please add quantity!")
    }else{
   this.setState({isLoading:true})
    //consle.log(this.props.userId,this.props.productId,this.state.variantId,this.state.quantity)
    fetch(baseUrl+this.props.userId+'/addtocart', {
           method: 'POST',
           headers: {
           'Content-Type': 'application/json',
           },
           body: JSON.stringify({
              "customer_id":this.props.userId,
              "product_id":this.props.productId,
              "product_variant_id" :this.state.variantId,    
              "quantity":this.state.quantity,
           })
   }) 
       .then((response) => response.json())
       .then((responseJson) => {
        // //consle.log(JSON.stringify(responseJson))
            this.setState({isLoading:false,snackBarVisible:true})
       }).catch(e=>{
         //consle.log("addtocart catch "+e)
       })  
       await this.props.setCategoryProductCountChanged(true)
       await this.props.setProductCountChanged(true)
      //  Toast.show({
      //   text: "Item added successfully .",
      //   buttonText: "Okay",
      //   position: "bottom",
      //   textStyle: { fontFamily:'Montserrat' },
      //   buttonTextStyle: { fontFamily: 'Montserrat-SemiBold' }
      // })
      //  this.props.navigation.navigate("drawer",{screen:'myCart'})
      }
  }
  
makeSingleProdToFavouriate(value){
    this.setState({isFavourite:!this.state.isFavourite})
    fetch(baseUrl+this.props.userId+'/wishlist', {
           method: 'POST',
           headers: {
           'Content-Type': 'application/json',
           },
           body: JSON.stringify({

            "customer_id": this.props.userId,
            "product_id":this.props.productId,
            "is_wishlist":value
           })
   })
       .then((response) => response.json())
       .then((responseJson) => {
            //consle.log(responseJson)
              // if(responseJson.data.message=="added"){
              // Toast.show({text:"Added to favourites !",duration:1000,buttonText:"Done",textStyle:{fontSize:12}})
              // }else{
              //   Toast.show({text:"Removed from favourites !",duration:1000,buttonText:"Done",textStyle:{fontSize:12}})
              // }
            this.props.setHomeScreenUpdate(true)
       })
}


 async updateSingleProductScreen(id){
  await this.props.setProductId(id)   
    this.getSingleProductDetails()
  }


async singleProductIncrement(){
    this.setState({quantity:parseInt(this.state.quantity)+1,isediting:false})
}

async singleProductDecrement(){
  if(this.state.quantity<=1){
    this.setState({quantity:1,isediting:false})
  }else{
    this.setState({quantity:this.state.quantity-1,isediting:false})
  }
}

async customQty(text){
  await this.setState({quantity:text,modalVisible:false,modalVisible:true,iseditingValue:text})
}

incdecEdit(){
  var qty=this.state.quantity.toString()
  //consle.log(qty)
  this.setState({iseditingValue:qty,isediting:true})
 }


onWebViewMessage = (event) => {
  this.setState({webViewHeight: Number(event.nativeEvent.data)})
}

async getNextPageDetails(){
  if(this.state.nextPageAvailable==true){
  await this.setState({page:this.state.page+1,nextPageLoading:true})
  await fetch(baseUrl+this.props.userId+'/product/'+this.props.productId+'?page='+this.state.page)
  .then((response)=>response.json())
  .then((responseJson)=>{
    this.setState({
      relatedProducts:this.state.relatedProducts.concat(responseJson.related_products),
      nextPageAvailable:responseJson.is_load,
      nextPageLoading:false
    })
  })
}
}

  changeProductVariant(key,indexPosition){
    var dummyArray=this.state.dummyArray
    var productOptions=this.state.productOptions
    productOptions.map((item,index)=>{
      if(index==key){
        item.option.map((item,index1)=>{
          item.selected=false
          if(index1==indexPosition){
            item.selected=true
            dummyArray[key]=item.option_value_id 
          }
        })
      }
    })
    this.setState({productOptions:productOptions,dummyArray:dummyArray})
    this.compareArrayValues()
  }

  compareArrayValues(){
    let array=this.state.availableCombinations

    for(var i=0;i<array.length;i++){
      if(this.state.productOptionCount==1){
        // if(this.state.dummyArray[0]==array[i].product_option_value_id1){
          if(this.state.dummyArray[0]==array[i].product_option_value_id1){
            this.setState({price:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            break;
          // }else{
          //   this.setState({combinationStatus:false})
          //   break
          // }
        }
      }else if(this.state.productOptionCount==2){
        // if((this.state.dummyArray[0]==array[i].product_option_value_id1) || (this.state.dummyArray[1]==array[i].product_option_value_id2)){
          if((this.state.dummyArray[0]==array[i].product_option_value_id1) && (this.state.dummyArray[1]==array[i].product_option_value_id2)){
            this.setState({price:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            break;
          }
        //   else{
        //     this.setState({combinationStatus:false})
        //     break
        //   } 
        // }
      }else if(this.state.productOptionCount==3){
        // if(this.state.dummyArray[0]==array[i].product_option_value_id1 || this.state.dummyArray[1]==array[i].product_option_value_id2 ||
        //   this.state.dummyArray[2]==array[i].product_option_value_id3){
            if(this.state.dummyArray[0]==array[i].product_option_value_id1 && this.state.dummyArray[1]==array[i].product_option_value_id2 &&
              this.state.dummyArray[2]==array[i].product_option_value_id3){
              this.setState({price:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
              break;
            // }else{
            //   this.setState({combinationStatus:false})
            //   break
            // }
          }
      }
    }
    
  }

  render() {
    var css = `<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"><style type="text/css"> @font-face {font-family: 'Montserrat'; src:url('file:///android_asset/fonts/Montserrat.ttf')}</style></head>`;
    var HTML = css + `<body style='font-family:Montserrat'>`+this.state.description+`</body></html>`
    
    var ABC=`<html><head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
    @font-face {
    font-family: 'Montserrat';
    src: url('Montserrat.ttf')  format('truetype') 
    }
    </style></head><body style="font-family:'Montserrat';">`+this.state.description+`</body></html>`

    var selectedVariant={};

      if(this.state.loading==true){
          return(             
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <ActivityIndicator color="#fa7153" size="large"/>
                  <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
              </View>
          )
      }else{
    return (    
    <View style={OverAllStyle.MainContainerPad0} >
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
      <ScrollView style={{width:'100%',height:'100%'}}
        onScroll={({nativeEvent}) => {
              if (isCloseToBottom(nativeEvent)) {
                this.getNextPageDetails()
             }
            }}
            scrollEventThrottle={0}>
            
            <View style={SingleProductScreenStyle.SubMainView}>
                <View style={SingleProductScreenStyle.MainProductWhiteView}>

                    <View style={SingleProductScreenStyle.MainProductView}> 
                    
                        <View style={SingleProductScreenStyle.MainProductInnerView}>

                    <Swiper 
                            ref='swiper'
                            activeDotColor={"#fa7153"} 
                            // paginationStyle={{}} 
                            // autoplay={true}
                            loop={true}
                            onIndexChanged={(index)=>console.log(index)}
                            >
                              {
                                this.state.images.map((item,index)=>{        
                                  return(
                                   <Image key={index} source={{uri:item}} style={{flex:1,width:null,height:null,resizeMode:"contain"}}/>
                                  )
                                })
                              }
                          </Swiper>

                        </View>
                        
                    </View>

                    <View style={SingleProductScreenStyle.MainProductDetailSubView}>
                                
                            {
                                this.state.isFavourite==true?
                                <TouchableOpacity style={SingleProductScreenStyle.HeartView}
                                onPress={()=>{this.makeSingleProdToFavouriate(1)}}
                                >
                                    <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageCoverStyle} />  
                                </TouchableOpacity>
                                :
                                <TouchableOpacity style={SingleProductScreenStyle.HeartView}
                                onPress={()=>{this.makeSingleProdToFavouriate(0)}}
                                >
                                        <Image source={images.grayHeart} style={StyleContainer.ImageContainStyle} />  
                                </TouchableOpacity>
                            }
                    </View>
                </View>

                {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={SingleProductScreenStyle.ProductScrollMainView}>
                    {
                        this.state.images.map((data,index)=>{
                            return(

                                <TouchableOpacity style={[index==this.state.choosedMainImageIndex?SingleProductScreenStyle.ProductListBorderView:SingleProductScreenStyle.ProductListView]}
                                onPress={()=>{this.changeChoosedMainImageIndex(index)}}
                                >
                                    <View style={SingleProductScreenStyle.ProductListInnerView}>
                                        <Image source={{uri:data}} style={StyleContainer.ImageContainStyle}/>
                                    </View>
                                </TouchableOpacity>
                            )
                        })
                    }
                </ScrollView> */}
                
                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:25}}> 
                  <View style={{width:'70%'}}>
                    <Text numberOfLines={5} ellipsizeMode="tail" style={{fontFamily:'Montserrat-SemiBold',fontSize:18,lineHeight:18,color:'#000'}}>{this.state.name}</Text>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15,color:'#fa7153',paddingTop:5}}>$ {(this.state.price * this.state.quantity).toFixed(2)}</Text>
                  </View>

                  <View style={{justifyContent:'center',alignItems:'flex-end',width:'30%'}}>
                    <View style={CartScreenStyle.IncDecMainContainer}>
                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                      onPress={()=>{this.singleProductDecrement()}}>
                          <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                      </TouchableOpacity>
                      
                      {
                        this.state.isediting==true?
                        <TouchableOpacity style={{width:'40%',justifyContent:'center'}}>
                        <TextInput
                          autoFocus={this.state.isediting}
                          keyboardType={"phone-pad"}
                          style={{width:'100%',height:24,paddingVertical:0,fontSize:12,fontFamily:"Montserrat",textAlign:'center',color:Colors.themeColor}}
                          onChangeText={(text)=>this.customQty(text)}
                          value={this.state.iseditingValue}
                        />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={()=>this.incdecEdit()} style={{width:'40%',alignItems:'center',justifyContent:'center'}}>
                          <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                        </TouchableOpacity>
                      }
                      
                      
                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                      onPress={()=>this.singleProductIncrement()}>
                          <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                      </TouchableOpacity>

                  </View>
                </View>
                </View>
                
            </View>
            
           <View style={{marginHorizontal:20,marginBottom:10}}>
             {
               this.state.productOptions.map((item,key)=>{
                 return(
                    <View key={key} style={{borderWidth:0.5,paddingBottom:10,borderColor:'gray',borderStyle:'dashed',borderRadius:5,paddingHorizontal:5,marginVertical:5,backgroundColor:'#fff'}}>
                      <Text style={{marginBottom:3,paddingLeft:5,paddingTop:5}}>{item.type}</Text>
                      <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                        {
                          item.option.map((item,index)=>{
                            
                            if(item.selected==true){
                              if(this.state.combinationStatus==true){
                                return(
                                  <TouchableOpacity key={index} style={{padding:7,borderWidth:0.3,borderColor:'gray',borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#fa7153',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                    <Text style={{textAlign:'center',color:'#fff',fontFamily:'MontSerrat-SemiBold'}}>{item.value}</Text>
                                  </TouchableOpacity>     
                                )
                              }else{
                                  return(
                                  <TouchableOpacity  key={index} style={{padding:7,borderWidth:1,borderRadius:20,borderColor:'#fa7153',minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#f2f5f6',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                    <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                  </TouchableOpacity>
                                )
                              }
                            }else{
                              return(
                              <TouchableOpacity  key={index} onPress={()=>this.changeProductVariant(key,index)} style={{padding:7,borderWidth:0.5,borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                              </TouchableOpacity>
                            )
                            }
                          })
                        }
                      </View>
                    </View>
                 )
               })
             }
             {
               this.state.combinationStatus==true?
               null
               :
               <View style={{flexDirection:'row',alignItems:'center'}}> 
                  <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                  <Text style={{color:'red',fontSize:12,fontFamily:'MontSerrat',paddingLeft:5}}>Selected combination not available</Text>
               </View>
             }
           </View>
            

            {/* <View style={SingleProductScreenStyle.WhiteContainer}>
                <View style={SingleProductScreenStyle.LocationDeliveryView}>
            
                    <View style={SingleProductScreenStyle.SplitInnerView}>
                        <View style={SingleProductScreenStyle.View50}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:12,color:'gray',marginBottom:2}}>Delivery Location</Text>
                            <Text style={{fontFamily:"Montserrat",fontSize:12}}>Sydney - {this.props.postCode}</Text>
                        </View>
                        <View style={SingleProductScreenStyle.View50}>
                            <View style={SingleProductScreenStyle.SplitInnerView}>
                              
                              <View style={SingleProductScreenStyle.DeliveryView}>
                                  {
                                      this.props.deliveryType==0?
                                      <Text style={{fontFamily:"Montserrat",color:'#1fd75d',fontSize:12}}>Delivery available</Text>
                                      :
                                      <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12}}>Delivery not available</Text>
                                  }
                              </View>

                              <View style={SingleProductScreenStyle.ChangeBtnMainView}>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate("changePostcode")} style={[SingleProductScreenStyle.ChangeBtnStyle,{borderRadius:5}]}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:10,textAlign:'center',margin:3}}>Change</Text>
                                </TouchableOpacity>
                              </View>

                            </View>
                        </View>
                        
        
                    </View>

                </View>
            </View> */}
            
            {
              this.state.description==null?null:
              <View style={SingleProductScreenStyle.DescriptionView}>
                <Text style={{fontFamily:"Montserrat-SemiBold",color:'gray',fontSize:16,marginBottom:10}}>Description</Text>
                <View style={{flex:1,flexDirection:'column'}}>
                  
                  {
                    
                  Platform.OS=="ios"?                
                  <WebView 
                  
                    originWhitelist={['*']}
                   // source={{ html: '<html><head><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body>'+this.state.description+'</body></html>' }}
                    source={{ html: ABC ,baseUrl:''}}   
                    style={{height: this.state.webViewHeight }}
                    onMessage={this.onWebViewMessage}
                    injectedJavaScript='window.ReactNativeWebView.postMessage(document.body.scrollHeight)'
                    renderLoading={() => <View style={{ flex: 1 }} ><ActivityIndicator size='small' color='#fa7153' /></View>}
                    startInLoadingState
                    
                    />
                    :
                    <WebView    
                      style={{ height: this.state.webViewHeight }}
                      onMessage={this.onWebViewMessage}
                      injectedJavaScript='window.ReactNativeWebView.postMessage(document.body.scrollHeight)'
                      source={{ baseUrl: '', html: HTML }}   
                    />
                }

                
                
                </View>
            </View>
            }
            
            <SingleProductRelatedProducts/>
            
      </ScrollView>
      
      </KeyboardAwareScrollView>
      <View>
            {
             this.state.combinationStatus==false?
             <View style={SingleProductScreenStyle.SplitInnerView}>
 
                  <TouchableOpacity onPress={()=>alert("Choosed variant not available . ")} style={SingleProductScreenStyle.AddView}>
                   <Text style={{fontFamily:"Montserrat-SemiBold",color:Colors.themeColor,fontSize:15}}>Buy now</Text>
                  </TouchableOpacity>
 
                  <TouchableOpacity onPress={()=>alert("Choosed variant not available . ")} style={SingleProductScreenStyle.BuyNowView}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:15,}}>Add to cart</Text>
                    </TouchableOpacity>
                    
              </View>
              :
              <View style={SingleProductScreenStyle.SplitInnerView}>
                    
                    {
                      this.state.isLoadingBuynow==true?
                      <TouchableOpacity style={SingleProductScreenStyle.AddView}>
                          <ActivityIndicator color="#fa7105"/>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity onPress={()=>{this.buyNow()}} style={SingleProductScreenStyle.AddView}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fA7105',fontSize:15}}>Buy now</Text>
                      </TouchableOpacity>
                    }

                    {
                      this.state.isLoading==true?
                      <TouchableOpacity style={SingleProductScreenStyle.BuyNowView}>
                          <ActivityIndicator color="#fff"/>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity onPress={()=>this.addtocart()} style={SingleProductScreenStyle.BuyNowView}>
                          <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:15,}}>Add to cart</Text>
                      </TouchableOpacity>
                    }
                    
          
              </View>
           }
           </View>
               {
                  this.state.nextPageLoading?
                  <View style={{alignItems:'center',justifyContent:'center',paddingVertical:5}}>
                    <ActivityIndicator/>
                  </View>
                  :
                  null
                }

            <Snackbar
              visible={this.state.snackBarVisible}
              onDismiss={()=>this.setState({snackBarVisible:false})}
              duration={5000}
              action={{
                label: 'GO TO CART',
                onPress: () => {
                  // this.setState({snackBarVisible:false})
                  this.props.navigation.navigate("drawer",{screen:"myCart"})
                },
              }}>
              Item added successfully!
            </Snackbar>

              <CustomNotification/>
    </View>
    
    );
   }
  }
}
function mapStateToProps(state){
    return{
        userId:state.userId,
        categoryId:state.categoryId,
        productCountChanged:state.productCountChanged,
        categoryProductCountChange:state.categoryProductCountChange,
        productId:state.productId,
        singleProductId:state.singleProductId,
        postCode:state.postCode,
        deliveryType:state.deliveryType,
        singleProductPageUpdate:state.singleProductPageUpdate
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
        setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
        setCartCount:(value)=>dispatch({type:"setCartCount",value}),
        setProductId:(value)=>dispatch({type:"setProductId",value}),
        setSingleProductId:(value)=>dispatch({type:"setSingleProductId",value}), 
        setSingleProductVariantId:(value)=>dispatch({type:"setSingleProductVariantId",value}),
        setIncreaseCartCount:()=>dispatch({type:"IncreaseCartCount"}),
        setDecreaseCartCount:()=>dispatch({type:"DecreaseCartCount"}),
        setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
        //setOrderDetails:(value)=>dispatch({type:"setOrderDetails",value}),
        setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
        setSingleProductPageUpdate:(value)=>dispatch({type:"setSingleProductPageUpdate",value}),
        setorderDeliverType:(value)=>dispatch({type:'setorderDeliverType',value}),
        setRelatedProducts:(value)=>dispatch({type:"setRelatedProducts",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(SingleProductScreen);
  
