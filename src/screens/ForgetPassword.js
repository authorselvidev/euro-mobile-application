
import React, { Component } from 'react'
import { Text, View,TouchableOpacity,TextInput,KeyboardAvoidingView,Image, ActivityIndicator } from 'react-native'
import {Label,Input,Item} from 'native-base'
import images from '../images/index';
import base64 from 'react-native-base64';

import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle} from '../screens/Styles/PrimaryStyle';
import { Colors } from './Styles/colors';
import AuthenticationService from '../Controller/ServiceClass';
import { ForgetEmail, ForgetEmailResponse, SetNewPassword, SetNewPasswordResponse } from '../DataModel/ModelClass';
import CustomNotification from './CustomNotification';
// import { ForgetEmail } from '../DataModel/ModelClass'
import {connect} from 'react-redux'

class ForgetPassword extends Component {
    

    state={
        response:[],
        code:'',
        email:'',
        id:'',
        emailError:'',
        loading:false,
        verify:false, 
        mobile_numberError:'',
        otpError:'',
        isValidMail:false,
        newPassword:'',
        confirmPassword:'',  
        newPasswordError:'',
        confirmPasswordError:'',
        updating:false,
        responseCode:'',
        responseCodeError:'',
        hideNewPassword:true,
        hideConfirmPassword:true
    }

   componentDidMount(){
       console.warn("gestfrompayment "+this.props.gestFromPayment)
   }

  async  verify(){
         this.setState({loading:true})

        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(this.state.email!=='')
             {
                
                 if(reg.test(this.state.email)===true)
                 {
                     this.setState({emailError:''})
                     ForgetEmail.email=this.state.email
       
                    await AuthenticationService.forgetPassword()
                    
                    //console.log(ForgetEmailResponse.email,ForgetEmailResponse.id,ForgetEmailResponse.status)
                    
                    if(ForgetEmailResponse.status==true){
                        this.setState({isValidMail:true})
                    }
                     this.setState({responseCode:'',loading:false,emailError:ForgetEmailResponse.errorMessage})
                 }
                 else{
                     this.setState({emailError:'check this email format ex: test@gmail.com'})
                     this.setState({loading:false})
                 }
             }
             else{
                
                 this.setState({emailError:'email feild is required'})
                 this.setState({loading:false})
             }
        }

        async updatePassword(){
            
             if(this.state.responseCode!=='' && this.state.newPassword!=='' && this.state.confirmPassword!==''){
                 if(this.state.newPassword==this.state.confirmPassword){
                    
                    this.setState({updating:true})

                    SetNewPassword.responseCode=this.state.responseCode
                    SetNewPassword.newPassword=this.state.newPassword
                    SetNewPassword.confirmPassword=this.state.confirmPassword
                     
                    await AuthenticationService.SetNewPassword()
                    //console.log(SetNewPasswordResponse.status)

                   await this.setState({updating:false,responseCodeError:SetNewPasswordResponse.errorMessage})

                           if(SetNewPasswordResponse.status==true){
                            //   this.props.navigation.navigate("newPasswordUpdated")
                            if(this.props.gestFromPayment==true){
                                this.props.setpaymentScreenUpdate(true)
                                this.props.setgestFromPayment(false)
                                this.props.navigation.navigate("paymentScreen")
                            }else{
                              this.props.navigation.navigate("signin")
                            }
                           }
                     
                     
                      }else{
                          this.setState({responseCodeError:'',newPasswordError:'',confirmPasswordError:"Passwords are mismatched"})
                      }
                
                     }else{
                         if(this.state.responseCode==''){
                             this.setState({responseCodeError:"please enter your otp here"})
                         }else{
                             this.setState({responseCodeError:''})
                         }
    
                         if(this.state.newPassword==''){
                             this.setState({newPasswordError:"please enter your new password"})
                         }else{
                             this.setState({newPasswordError:''})
                         }
    
                         if(this.state.confirmPassword==''){
                             this.setState({confirmPasswordError:"Please enter your confirm password"})
                         }else{
                             this.setState({confirmPasswordError:''})
                         }
                     }
                    
            }
    

        
    render() {
        return (
            <KeyboardAvoidingView keyboardVerticalOffset={80} behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
                <View style={StyleContainer.MainContainer}>
                 
                    <View style={OverAllStyle.LogoView}>
                    <Image source={images.logo} style={StyleContainer.ImageContainStyle} />
                    </View>

                    {
                        this.state.isValidMail==false?
                        <View style={{width:'100%',marginVertical:20}}>
                        <View style={StyleContainer.TextInputMainView}>
                        <TextInput style={StyleContainer.TextInputView} placeholder="Email Address*"
                            ref={(input) => { this.mobileNumRef = input; }}
                            onSubmitEditing={() => this.verify()}
                            onChangeText={(value)=>{this.setState({email:value})}} 
                            returnKeyType={"done"}
                            keyboardType='default'
                          />
                          {
                          this.state.emailError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.emailError}</Text>
                            </View>
                            }
                        </View>
                        
                        {
                                this.state.loading?
                                 <View style={StyleContainer.ButtonOrange}>
                                    <ActivityIndicator size="small" color={Colors.whiteBg}/>
                                 </View>
                                 :
                                <TouchableOpacity onPress={()=>this.verify()} style={StyleContainer.ButtonOrange} >
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:18}}>GET CODE</Text>
                                 </TouchableOpacity>
                            }
                    </View>
                    :
                    <View style={{width:'100%',marginVertical:15}}>
                        <View style={StyleContainer.TextInputMainView}>
                            <TextInput style={StyleContainer.TextInputView} placeholder="Response Code*"
                                ref={(input) => { this.responseCodeRef = input; }}
                                value={this.state.responseCode}
                                onSubmitEditing={() => {
                                this.newPasswordRef.focus();}}
                                onChangeText={(value)=>{this.setState({responseCode:value})}} 
                                returnKeyType={"next"}
                                keyboardType='ascii-capable'
                            />
                            {
                                this.state.responseCodeError==''?null:
                                <View style={{width:'100%'}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.responseCodeError}</Text>
                                </View>
                                }
                            </View>
                            
                        
                            <View style={{flexDirection:'row',width:'100%',marginBottom:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr}}>
                              <TextInput style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,
                                 fontSize:14,fontFamily:'Montserrat',paddingLeft:20}}
                                placeholder="New Password*"
                                ref={(input) => { this.newPasswordRef = input; }}
                                secureTextEntry={this.state.hideNewPassword}
                                onSubmitEditing={() => {
                                this.confirmPasswordRef.focus();}}
                                onChangeText={(value)=>{this.setState({newPassword:base64.encode(value)})}} 
                                returnKeyType={"next"}
                                keyboardType='ascii-capable'
                                textContentType="oneTimeCode"
                            />
                            <TouchableOpacity onPress={()=>this.setState({hideNewPassword:!this.state.hideNewPassword})}>
                                {
                                  this.state.hideNewPassword==true?
                                  <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  :
                                  <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                }
                              </TouchableOpacity>
                            </View>
                            <View style={{alignSelf:'flex-start',marginTop:-8}}>
                              {
                                this.state.newPasswordError==''?null:
                                <View style={{width:'100%'}}>
                                    <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.newPasswordError}</Text>
                                </View>
                              }
                              </View>
                            
                            <View style={{flexDirection:'row',width:'100%',marginVertical:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr}}>
                               <TextInput style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,
                                   fontSize:14,fontFamily:'Montserrat',paddingLeft:20}}
                                placeholder="Confirm Password*"
                                ref={(input) => { this.confirmPasswordRef = input; }}
                                secureTextEntry={this.state.hideConfirmPassword}
                                onSubmitEditing={() => this.updatePassword()}
                                onChangeText={(value)=>{this.setState({confirmPassword:base64.encode(value)})}}
                                returnKeyType={"done"}
                                keyboardType='ascii-capable'
                                textContentType="oneTimeCode"
                               />
                              <TouchableOpacity onPress={()=>this.setState({hideConfirmPassword:!this.state.hideConfirmPassword})}>
                                {
                                  this.state.hideConfirmPassword==true?
                                  <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  :
                                  <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                }
                              </TouchableOpacity>
                            </View>
                            <View style={{alignSelf:'flex-start',marginTop:-8}}>
                                {
                                    this.state.confirmPasswordError==''?null:
                                    <View style={{width:'100%'}}>
                                    <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.confirmPasswordError}</Text>
                                    </View>
                                }
                               </View>
                        
                    
                        {
                            this.state.updating?
                            <View style={StyleContainer.ButtonOrange}>
                                    <ActivityIndicator size="small" color={Colors.whiteBg}/>
                                 </View>
                            
                            :
                            <TouchableOpacity onPress={()=>this.updatePassword()} style={StyleContainer.ButtonOrange}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:18}}>UPDATE</Text>
                            </TouchableOpacity>
                        }

                        {/* <View style={{paddingVertical:10,justifyContent:'center'}}>
                                <Text style={{fontFamily:"Montserrat",fontSize:12,color:'#242424'}}>* Password must have minimum 6 characters</Text>
                                <Text style={{fontFamily:"Montserrat",fontSize:12,color:'#242424'}}>* That contains alpha numeric values</Text>
                            </View>                                         */}
                    </View>
   
                    }
                    
                    <CustomNotification/>
                </View>
                </KeyboardAvoidingView>
        )
    }
}


function mapStateToProps(state){
    return{
        userId:state.userId,
        gestFromPayment:state.gestFromPayment
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
      setgestFromPayment:(value)=>dispatch({type:'setgestFromPayment',value}),
      setpaymentScreenUpdate:(value)=>dispatch({type:"setpaymentScreenUpdate",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(ForgetPassword);