
export const Colors ={
    whiteBg:'#FFFFFF',
    grayBg:'#f2f5f6',
    themeColor:'#fa7153',
    inPrgrsClr:'#f7bb2b',
    prgrsCmpltClr:'#19b265',

    redIndicationClr:'#fb2424',
    txtInputClr:'#c3c3c3', // used also text Input border Color
    bdrBtnTxtClr:'#262626',
    categoryTxtClr:'#939393',
    StrongHeadingClr:'#171c1f',
    grnIndicatorClr:'#31bf45',
    singlePrdctScrnDescriptionColor:'#949494', //used also search screen text,notification date text
    notificationTxtClr:'#979797',
    categoryListTabInActiveTxtClr:'#a4a4a4',
    bdrBtnClr:'#242424', 
    searchScrBdrClr:'#dedede', //also used my order screen border
    categoryBdrClr:'#e1e3e4'


    
}
