import React, { Component } from 'react';
import { View, Text,TextInput,ScrollView,Image, ActivityIndicator, Platform ,KeyboardAvoidingView} from 'react-native';
import AuthenticationService from '../Controller/ServiceClass'
import AsyncStorage from '@react-native-async-storage/async-storage';
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles'
import { OverAllStyle,PostCodeStyle } from './Styles/PrimaryStyle';
import { baseUrl } from '../Controller/baseUrl';
import {connect} from 'react-redux'
import { PostCodeModelResponse, GuestLoginModelResponse } from '../DataModel/ModelClass';
import CustomNotification from './CustomNotification';

class PostCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postCode:null,
      loading:false,
      keyboard:true,
      guestId:null
    };
  }

 async componentDidMount(){
    // //console.log(GuestLoginModelResponse.id)
  await AsyncStorage.getItem('guestId').then((value) => {
      if(value!=null){
        this.setState({guestId:value})
      }
    })
  }

  async postCode(value){
    await this.setState({postCode:value})
    
     if(this.state.postCode.toString().length==4){
      this.setState({keyboard:false,loading:true})      

      await fetch(baseUrl+"postcode", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "postcode":this.state.postCode,
          //"user_id":this.props.userId
          "user_id":this.state.guestId,
          //"user_type":"guest"
          "check_from":"postcode"
        })
      })
        .then((response)=>response.json())
        .then((responseJson)=>{
          //console.log(responseJson)
          PostCodeModelResponse.deliveryType=responseJson.delivery_type
        })
      await this.props.setPostCode(this.state.postCode)
      // await this.props.setProductCountChanged(true)
      this.setState({loading:false,keyboard:true})
      this.props.navigation.navigate("ordersAvailable")
     }
    }

  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={80} behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
        <View style={StyleContainer.MainContainer}>
            
            <View style={OverAllStyle.LogoView}>
              <Image source={images.logo} style={StyleContainer.ImageContainStyle}/>
            </View>
            
            <View style={PostCodeStyle.MainQuoteView}>
            <Text numberOfLines={2} style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',fontSize:20}}>Enter delivery destination postcode</Text>
            </View>
            
            
            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:12,textAlign:'center',marginBottom:20,}}>We deliver to Sydney metro and outer suburbs</Text>
                
                <View style={StyleContainer.TextInputMainView}>
                    <TextInput 
                      autoFocus
                      style={{borderWidth:1,fontFamily:'Montserrat',borderColor:'gray',fontSize:18,textAlign:'center',height:Platform.OS=="ios"?40:null}}
                      ref={(input) => { this.emaildRef = input; }}
                      onSubmitEditing={() =>this.postCode()}
                      onChangeText={(value)=>this.postCode(value)}
                      returnKeyType={"next"}
                      keyboardType='numeric'
                      placeholder="Your Post Code"
                      editable={this.state.keyboard}                      
                     />
                    </View>
                    
                    {
                      this.state.loading?
                      <View style={{justifyContent:'center',alignSelf:'center'}}>
                        <ActivityIndicator color="#fa7153"/>
                      </View>
                      :
                      null
                    }
            <CustomNotification/>
        </View>
        </KeyboardAvoidingView>
    );
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
  }
}

function mapDispatchToProps(dispatch){
  return{
    setPostCode:(value)=>dispatch({type:"setPostCode",value}),
    setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(PostCode);

