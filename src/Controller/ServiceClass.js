
import { SigninResponseModel, SliderModel, SigninModel, SignupModel, SignupResponseModel, ForgetEmailResponse, ForgetEmail, SetNewPassword, SetNewPasswordResponse, GuestLoginModel, GuestLoginModelResponse, PostCodeModel, PostCodeModelResponse, SliderModelResponse } from '../DataModel/ModelClass'
import { baseUrl } from './baseUrl';
import NetInfo from '@react-native-community/netinfo'
import base64 from 'react-native-base64';

const AuthenticationService={
 
 async getIntroScreens(){
  // NetInfo.fetch().then(state => {
  //   //console.log("Connection type", state.type);
  //   if(state.isConnected==true){
  //       fetch(baseUrl+"slider")
  //       .then((response)=>response.json())
  //       .then((responseJson)=>{
  //         SliderModelResponse.response=responseJson;
  //       })
  //       .catch((err)=>{
  //         //console.log(err)
  //     }) 
  //   }else{
  //     alert("Network connection failed")
  //   }
  // }) 
 await fetch(baseUrl+"slider")
        .then((response)=>response.json())
        .then((responseJson)=>{
          SliderModelResponse.response=responseJson;
        })
        .catch((err)=>{
          //console.log(err)
      })
 },

  async signin(){
     await fetch(baseUrl+'login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
         "email":SigninModel.email,
         "password":SigninModel.password,
         "device_type":SigninModel.deviceType,
         "device_token":SigninModel.deviceToken,
         "guest_id":null
          })
        })
      .then((response) => response.json())
       .then((responseJson) => {
           console.log(responseJson)
           if(responseJson.success==true){
            SigninResponseModel.status=true
            SigninResponseModel.userId=responseJson.data.user.id
           }else{
            SigninResponseModel.status=false
            SigninResponseModel.errorMessage=responseJson.errorMessage
           }
        })
        .catch((err)=>{
          //console.log(err)
      })      
   },
   

 async signup(){
   
    await fetch(baseUrl+'register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "guest_id":SignupModel.guestId,
          "first_name":SignupModel.firstName,
          "last_name":SignupModel.lastName,
          "email":SignupModel.email,
          "contact":SignupModel.mobileNo,
          "business_name":SignupModel.businessName,
          "password":base64.encode(SignupModel.newPassword),
          "confirm_password":base64.encode(SignupModel.confirmPassword),
          "device_type":SignupModel.deviceType,
          "device_token":SignupModel.deviceToken          
                 })
        })
      .then((response) => response.json())
       .then((responseJson) => {
          console.log(responseJson)
          //console.log("SRM"+responseJson.data.id)
          if(responseJson.success==true){
            SignupResponseModel.userId=responseJson.data.id
            SignupResponseModel.status=true
            this.sendMail(responseJson.data)
          }else{
            SignupResponseModel.status=false
            SignupResponseModel.errorMessage=responseJson.errorMessage
            //console.log("service"+JSON.stringify(SignupResponseModel.errorMessage))
          }
      }).catch((err)=>{
        //console.log(err)
    })  
  },

 async sendMail(value){
  console.log("enter send mail function")
  await fetch(baseUrl+"send-mail", {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      // from:'new-account' or 'new- guest',
      // user_id:,
      // 'frwd_pwd':'',
      "from":"new-account",
      "user_id":value.id, 
      "frwd_pwd":value.frwd_pwd,
     })
   })
  .then((response) => response.json())
  .then((responseJson) => {
      console.log("new account mail "+JSON.stringify(responseJson))
  })
  },

 async forgetPassword(){
   await fetch(baseUrl+"forget-code", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
        "email": ForgetEmail.email,
        })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      if(responseJson.success==true){
        ForgetEmailResponse.status=true
        ForgetEmailResponse.email=responseJson.data.email
        ForgetEmailResponse.id=responseJson.data.id
        this.sendMailForgotPassword(responseJson.data)
      }else{
        ForgetEmailResponse.status=false
        ForgetEmailResponse.errorMessage=responseJson.errorMessage[0]
      }     
    }).catch((err)=>{
      //console.log(err)
  })  
  },
  
  async sendMailForgotPassword(value){
    console.log("enter send mail function")
    await fetch(baseUrl+"send-mail", {
      method: 'POST',
      headers: {
      'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "from":'get-code',
        "user_id":value.id, 
        "random_code":value.random_code
       })
     })
    .then((response) => response.json())
    .then((responseJson) => {
        console.log("forgot mail "+JSON.stringify(responseJson))
    })
    },
   

  async SetNewPassword(){
    await fetch(baseUrl+"forget-change", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
      
        body: JSON.stringify({
          "id":ForgetEmailResponse.id,
            "email":ForgetEmailResponse.email,
            "code":SetNewPassword.responseCode,
            "new_password":SetNewPassword.newPassword,
            "confirm_password":SetNewPassword.confirmPassword
        })
      })
      .then((response) => response.json())
      .then((responseJson) => {
          //console.log(responseJson)
          if(responseJson.success==true){
            SetNewPasswordResponse.status=true
          }else{
            SetNewPasswordResponse.status=false
            SetNewPasswordResponse.errorMessage=responseJson.errorMessage[0]
          }    
      }).catch((err)=>{
        //console.log(err)
    })      
  },
}

export default AuthenticationService;