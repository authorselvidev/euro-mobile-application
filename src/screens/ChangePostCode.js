import React, { Component } from 'react';
import { View, Text,TextInput,TouchableOpacity,KeyboardAvoidingView,Image, StyleSheet, ActivityIndicator } from 'react-native';
import {connect} from 'react-redux'
import {baseUrl} from '../Controller/baseUrl'
import {StyleContainer} from '../screens/Styles/CommonStyles'
import {OverAllStyle,OrderAvailableStyle,PostCodeStyle} from './Styles/PrimaryStyle';
import images from '../images/index'
import CustomNotification from './CustomNotification';

class ChangePostCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postCode:null,
      loading:false,
      keyboard:true,
      checkPostCode:false,
      isPostcodeAvailable:false
    };
  }

  componentDidMount(){
    console.log(this.props.addressChanged)
  }

 async changePostCode(value){    
    await this.setState({postCode:value})
    
     if(this.state.postCode.toString().length==4){
      this.setState({keyboard:false,loading:true})      
      await fetch(baseUrl+"postcode", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "postcode":this.state.postCode,
          "user_id":this.props.userId,
          "check_from":"postcode"       
        })
      })
        .then((response)=>response.json())
        .then((responseJson)=>{
          console.log(responseJson)
          if(responseJson.delivery_type==0){
            this.props.setDeliveryType(0)
            this.props.setorderDeliverType(0)
            this.setState({checkPostCode:true,isPostcodeAvailable:true})
            this.props.setPostCode(this.state.postCode)
          }else{
            this.props.setDeliveryType(1)
            this.props.setorderDeliverType(1)
            this.props.setPostCode(this.state.postCode)
            this.setState({checkPostCode:true,isPostcodeAvailable:false})
          }
        })
        //.catch((err)=>{//console.log(err)})
      this.setState({loading:false,keyboard:true})
     }
  }

 async continueShopping(){
    if(this.props.changePostCodeStatus==true){
      await this.props.setchangePostCodeStatus(false)
      await this.props.setAddressChanged(true)
    }
    this.props.navigation.goBack()
  }

  render() {
    if(this.state.checkPostCode==false){
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={80} behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
      <View style={StyleContainer.MainContainer}>
        <View style={PostCodeStyle.MainQuoteView}>
          <Text style={{fontFamily:'Montserrat-SemiBold',textAlign:'center',fontSize:20}}>Enter delivery destination post code</Text>
       </View>
       
       <Text style={{fontFamily:'Montserrat',fontSize:12,textAlign:'center',marginBottom:20}}>We deliver to Sydney metro and outer suburbs</Text>
          
          <View style={StyleContainer.TextInputMainView}>
              <TextInput style={{borderWidth:1,borderColor:'gray',fontSize:18,textAlign:'center',height:Platform.OS=="ios"?40:null}}
                autoFocus
                ref={(input) => { this.emaildRef = input; }}
                onSubmitEditing={() => this.changePostCode()}
                onChangeText={(value)=>this.changePostCode(value)} 
                returnKeyType={"next"}
                keyboardType='numeric'
                placeholder="Your Post Code"     
                editable={this.state.keyboard}  
               />
              </View>
              <View style={{alignItems:'center'}}>
              {
                this.state.loading?
                <ActivityIndicator color="#fa7153"/>
                :null
              }
              </View>
      </View>
      </KeyboardAvoidingView>
     );
    }else{
      return(
        <View style={StyleContainer.MainContainer}>
          
          {
            this.state.isPostcodeAvailable==true?
            <View style={OrderAvailableStyle.MainView}>
                    <View style={OverAllStyle.LogoView}>
                        <Image source={images.available} style={StyleContainer.ImageContainStyle}/>
                    </View>

                    <View style={OrderAvailableStyle.TxtView}>
                        <Text style={OrderAvailableStyle.MainQuoteGreen}>Thank you.</Text>
                        <Text style={OrderAvailableStyle.MainQuoteGreen}>Delivery Available</Text>
                    </View>
                        

                        <TouchableOpacity onPress={()=>this.continueShopping()} style={StyleContainer.ButtonOrange}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18}}>CONTINUE SHOPPING</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={()=>this.setState({checkPostCode:false})} 
                        style={{ width:'100%',borderWidth:1,borderColor:'gray',paddingVertical:15,borderRadius:3,alignItems:'center',marginBottom:15,marginVertical:10}}>
                          <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:18,textAlign:'center'}}>RE-ENTER POST CODE</Text>
                        </TouchableOpacity> */}
                    </View>
                    :
                    <View style={OrderAvailableStyle.MainView}>
                    <View style={OverAllStyle.LogoView}>
                        <Image source={images.notAvailable} style={StyleContainer.ImageContainStyle}/>
                    </View>
                    <View style={OrderAvailableStyle.TxtView}>
                      <Text style={OrderAvailableStyle.MainQuoteRed}>SORRY!</Text>
                      <Text style={OrderAvailableStyle.SubQuoteRed}>We do not service this area. Please visit us at 90-114 Princes Highway for pick-up</Text>
                    </View>
                        
                        {/* <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={StyleContainer.ButtonOrange}>
                            <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff',fontSize:18}}>PLACE ORDER</Text>
                        </TouchableOpacity> */}
                        <TouchableOpacity onPress={()=>this.continueShopping()}
                         style={{ width:'100%',borderWidth:1,borderColor:'gray',paddingVertical:15,borderRadius:3,alignItems:'center',marginBottom:15,marginVertical:10}}>
                          <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:18,textAlign:'center'}}>CONTINUE SHOPPING</Text>
                        </TouchableOpacity>
                    </View>
          }
          <CustomNotification/>
        </View>
      )
    }
   } 
 }

 function mapStateToProps(state){
  return{
      userId:state.userId,
      deliveryType:state.deliveryType,
      addressChanged:state.addressChanged,
      changePostCodeStatus:state.changePostCodeStatus
  }
}

function mapDispatchToProps(dispatch){
  return{
    setPostCode:(value)=>dispatch({type:"setPostCode",value}),
    setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
    setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
    setchangePostCodeStatus:(value)=>dispatch({type:"setchangePostCodeStatus",value}),
    setorderDeliverType:(value)=>dispatch({type:'setorderDeliverType',value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ChangePostCode);
