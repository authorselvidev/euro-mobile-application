import React, { Component } from 'react';
import { View, Text,ScrollView,TextInput,Image,TouchableOpacity,Modal,AsyncStorage,ActivityIndicator,Dimensions,KeyboardAvoidingView, Platform, SafeAreaView, Touchable } from 'react-native';
import {Textarea, Toast} from 'native-base'
import {baseUrl} from '../Controller/baseUrl'
import images from '../images/index';
import NetInfo from '@react-native-community/netinfo';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {CartScreenStyle,CompleteOrderStyle,MyOrderDetailStyle,OverAllStyle, ModelStyle} from './Styles/PrimaryStyle'
import {Colors} from './Styles/colors';
import {connect} from 'react-redux'
import { Calendar } from 'react-native-calendars';
import CustomNotification from './CustomNotification';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Snackbar } from 'react-native-paper';
const {width,height}=Dimensions.get('window')


// var markedDates=thi
var totalAmount=0
var totalAmountWithoutDeliveryCharge=0
var totalWeight=0

class CompleteOrder extends Component {
          
 state = {
        loading:false,
        shippingAddress:[],
        shopAddress:{},
        timeSlots:[],
        billingAddress:[],
        vehicleLists:[],
        cartProducts:[], 
        vehicleTypes:[],
        subTotal:0,
        totalAmount:0,
        totalWeight:0,
        grandTotal:0,
        selectedVehicleIndex:0,
        selectedVehicleId:null,
        selectedProductIndex:0,
        selectedProduct:{},        
        discription:'',
        openingDatePickerModal:false,
        openingTimePickerModal:false,
        dateValue:null,
        timeValue:null,
        date:new Date(),
        mark:null,
        selectedTimeSlot:null,
        selectedTimeSlotName:null,
        slotLoading:false,
        deliveryType:0,
        pickupPersonName:'',
        pickupPersonNumber:'',
        refresh:false,
        
        checkoutIncDecLoader:true,
        selcetedItemForIncDec:null,
        IncOrDecProductName:null,
        IncOrDecProductCountChanged:false,

        shippingAsBilling:false,
        selectedVehicleArray:[],

        singleProductDetails:[],
        //preselecte vehicle
        preloadVehicleIndex:null,
        preSlotid:null,
        // currentTime:new Date().getHours()+':'+new Date().getMinutes(),
        currentTime:new Date().getHours(),
        currentMinutes:new Date().getMinutes(),
        currentDate:new Date().getDate()+2,
        currentMonth:new Date().getMonth() + 1,
        currentYear : new Date().getFullYear(),

        selectedVehicelList:[],
        userChoosedVehicleList:[],
        userChoosedTotalWeight:0,
        vehicleLoading:false,

        deliveryDate:null,

        isediting:false,
        iseditingValue:null,
        selectedItemForEditing:null,
        addressCount:null,
        timesloterror:null
    }

 async componentDidMount(){

    var someDate = new Date();
    var numberOfDaysToAdd = 2;
    someDate.setDate(someDate.getDate() + numberOfDaysToAdd)

    var dd = someDate.getDate();
    var mm = someDate.getMonth() + 1;
    var y = someDate.getFullYear();
    var someFormattedDate =y + '-'+ mm + '-'+dd;

    await this.setState({currentDate:dd})
        if(this.state.currentDate<=9){
           var currentdate=0+""+this.state.currentDate
           await this.setState({currentDate:currentdate})
         }else{
           await this.setState({currentDate:this.state.currentDate})
         }
         var date=y+"-"+mm+"-"+this.state.currentDate  

     this.setState({date:date})
     //console.warn(someFormattedDate)
     this.callAllMountFunction()
}

 async callAllMountFunction(){
      this.getHolidays()
     if(this.props.singleProductId==null){
       this.getCompleteOrderDetail()
      }else{
      this.getSingleProductDetail()
      }
  }

 async componentDidUpdate(prevProps){   
  if(this.props.addressChanged==true){
    this.callAllUpdateFunction()
    this.props.setAddressChanged(false)
    }
}
 
async callAllUpdateFunction(){
   await this.setState({selectedVehicelList:[]})
    this.getHolidays()
     if(this.props.singleProductId==null){
      this.getRefreshCompleteOrderDetail()
     }else{
      this.getRefreshSingleProductDetail()
     }
}

async componentWillUnmount(){
    if(this.props.singleProductId!=null){
      this.props.setProductCountChanged(true)  
      this.props.setCategoryProductCountChanged(true)
    }
} 

preVehiclePost(shippingId){
    //////console.warn(shippingId)
    //  var a = this.state.currentTime;
    //  var b = this.state.currentMinutes;
    //  var currentTime= a+':'+ b
    //  var slot1Timing=9+':'+59
    //  var slot2Timing=12+':'+59
    //  //console.log(currentTime,slot1Timing,slot2Timing)
    this.setState({selectedVehicelList:[]})

    // if(currentTime<=slot1Timing){
         this.setState({preSlotid:1})
    // }else if(currentTime<=slot2Timing){
    //     this.setState({preSlotid:2})
    // }else if(currentTime>=slot2Timing){
    //     this.setState({preSlotid:3})
    // }

  //  var date=this.state.currentYear+"-"+this.state.currentMonth+"-"+this.state.currentDate    
    var timeSlots=this.state.timeSlots
    timeSlots.map((item,index)=>{
        if(item.id==this.state.preSlotid){
            this.setState({selectedTimeSlotName:item.times})
            item.available=true
        }else{
            item.available=true
        }
    })

    // if(this.state.preSlotid==1){
    //     timeSlots[0].available=true
    //     timeSlots[1].available=true
    //     timeSlots[2].available=true
    // }
    // if(this.state.preSlotid==2){
    //     timeSlots[0].available=false
    //     timeSlots[1].available=true
    //     timeSlots[2].available=true
    // }
    // if(this.state.preSlotid==3){
    //     timeSlots[0].available=false
    //     timeSlots[1].available=false
    //     timeSlots[2].available=true
    // }
 
    this.setState({dateValue:this.state.date,selectedTimeSlot:this.state.preSlotid,timeSlots:timeSlots})

    //////console.warn(this.state.preSlotid,date,this.props.totalWeight,shippingId)
    console.log(JSON.stringify({
        "time_slot_id":this.state.preSlotid,
        "delivery_date":this.state.date,
        "total_weight":this.props.totalWeight,
        //"shipping_address_id":shippingId
        "postcode":shippingId
  }))
    fetch(baseUrl+this.props.userId+"/schedule",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
              "time_slot_id":this.state.preSlotid,
              "delivery_date":this.state.date,
              "total_weight":this.props.totalWeight,
              //"shipping_address_id":shippingId
              "postcode":shippingId
        })
       })
      .then((response)=>response.json())
      .then((responseJson)=>{
          console.warn(JSON.stringify(responseJson))
          this.setState({vehicleLists:responseJson.data,selectedVehicelList:[]})

          if(this.props.orderDeliverType==0){
            for(var i=0;i<this.state.vehicleLists.length;i++){
                if(this.state.vehicleLists[i].vehicle_availability==true){
                    if(this.state.vehicleLists[i].vehicle_max_weight>this.props.totalWeight){
                        this.state.selectedVehicelList.push(i)
                        break
                    }
               }
            }
            
          if(this.state.selectedVehicelList.length==0){
              lastIndex=this.state.vehicleLists.length-1
              var list =this.state.vehicleLists
              list[lastIndex].selected_vehicle=true
              this.props.setDeliveryCharge(list[lastIndex].delivery_fee)
              //console.warn("////console last "+this.props.deliveryCharge)
          }else{

          var list=[...this.state.vehicleLists]
          for(var i=0;i<list.length;i++){
            if(this.state.vehicleLists[i].vehicle_availability==true){
                if(list[i].vehicle_max_weight>this.props.totalWeight){
                    this.setState({preloadVehicleIndex:i})
                    this.props.setDeliveryCharge(list[i].delivery_fee)
                    //console.warn("////console "+ this.props.deliveryCharge)
                    break
                }
            }
          }

          list.map((item,index)=>{
              if(index==this.state.preloadVehicleIndex){
                  item.selected_vehicle=true
              }else{
                item.selected_vehicle=false
              }
          })
          this.setState({vehicleLists:list,loading:false})
        }
        this.setState({loading:false})
        //console.log(JSON.stringify(this.state.vehicleLists))
          this.checkuserChoosedVehicles()
        }else{
            this.setState({loading:false})
        }
      })
}

async getHolidays(){
   await fetch(baseUrl+"holidays")  
    .then((response)=>response.json())
    .then((response)=>{
        var arr=response.data.holidays
        var obj = Object.assign(...arr.map(o => ({[o]: {disabled:true,color: 'red', disableTouchEvent: true}})));
        this.setState({mark:obj})
    }).catch((err)=>{
        ////console.log(err)
    })    
    
    if(this.props.orderDeliverType==1){
        await this.props.setDeliveryCharge(0)
    }

    await this.props.setAddressChanged(false)
}

doOrDontPrepost(){
    if(this.state.shippingAsBilling==true){
        if(this.state.billingAddress.length==0){
            //this.setState({loading:false,refresh:false})
            this.preVehiclePost(this.props.postCode)  
        }else{
            this.preVehiclePost(this.state.billingAddress.postcode)  
        }
    }else{
        if(this.state.billingAddress.length!==0 && this.state.shippingAddress.length!==0){
            this.preVehiclePost(this.state.shippingAddress.postcode)  
        }else{
            this.preVehiclePost(this.props.postCode)  
            //this.setState({loading:false,refresh:false})
        }
    }
}

 async getSingleProductDetail(){
   //console.log("get single productdetails")
   await NetInfo.fetch().then(state => {
        if(state.isConnected==true){
               this.setState({loading:true})
               ////console.log("mount "+baseUrl+this.props.userId+'/checkout?from_buynow=true&&row_id='+this.props.singleProductId)
                fetch(baseUrl+this.props.userId+'/checkout?from_buynow=true&&row_id='+this.props.singleProductId)
                .then((response) => response.json())
                 .then((responseJson) => {        
                     //console.log("signle response")
                     //console.warn(responseJson)  
                      this.setState({singleProductDetails:[]})       
                      var arr=[]
                      arr.push(responseJson.data.cart_products)
                      this.props.setTotalAmount(arr[0].price*arr[0].quantity.toFixed(2))
                      this.props.setTotalWeight(responseJson.data.total_weight)
                        this.props.setOrderDetails(arr)
                       this.setState({
                        timeSlots:responseJson.data.time_slot,
                        shippingAddress:responseJson.data.shipping_address,
                        billingAddress:responseJson.data.billing_address,
                        shopAddress:responseJson.data.shop_address,
                        addressCount:responseJson.data.address_count
                    })
                     this.doOrDontPrepost()
                  })
                  .catch(e=>{
                      ////console.log("check out "+e)
                  })
        }else{
           this.setState({networkFailed:true})
           alert("Check network connection")
        }
    })
    
  }

  async getCompleteOrderDetail(){
    //console.log(baseUrl+this.props.userId+'/checkout')  
    //console.log("get complete productdetails")
   await NetInfo.fetch().then(state => {
      if(state.isConnected==true){
             this.setState({loading:true})
              fetch(baseUrl+this.props.userId+'/checkout')
              .then((response) => response.json())
               .then((responseJson) => {
                //    console.log("complete response")
                   console.log(JSON.stringify(responseJson))
                    this.setState({
                        timeSlots:responseJson.data.time_slot,
                        shippingAddress:responseJson.data.shipping_address,
                        billingAddress:responseJson.data.billing_address,
                        shopAddress:responseJson.data.shop_address,
                        addressCount:responseJson.data.address_count
                    })
                    console.log(this.state.addressCount)
                    this.doOrDontPrepost() 
              }) 
              
      }
      
      else{
        this.setState({networkFailed:true})
        alert("Check network connection")
      }
    })  
         
}

async getRefreshSingleProductDetail(){
    //console.log("update single productdetails")
    await NetInfo.fetch().then(state => {
         if(state.isConnected==true){
                this.setState({refresh:true})
                fetch(baseUrl+this.props.userId+'/checkout?from_buynow=true&&row_id='+this.props.singleProductId)
                 .then((response) => response.json())
                  .then((responseJson) => {
                      ////console.log("update response" +JSON.stringify(responseJson))
                        this.setState({singleProductDetails:[]})
                        var arr1=[]
                        arr1.push(responseJson.data.cart_products)

                        this.setState({
                         singleProductDetails:arr1,
                         timeSlots:responseJson.data.time_slot,
                         shippingAddress:responseJson.data.shipping_address,
                         billingAddress:responseJson.data.billing_address,
                         shopAddress:responseJson.data.shop_address,
                         refresh:false,
                         addressCount:responseJson.data.address_count
                        //  pickupPersonName:responseJson.data.pickup_person_name,
                        //  pickupPersonNumber:responseJson.data.pickup_person_number
                      })

                      this.doOrDontPrepost() 
                  })
         }else{
            this.setState({networkFailed:true})
            alert("Check network connection")
         }
     })
   }
 
   async getRefreshCompleteOrderDetail(){
    //console.log("update complete productdetails")
    await NetInfo.fetch().then(state => {
       if(state.isConnected==true){
              this.setState({refresh:true})
               fetch(baseUrl+this.props.userId+'/checkout')
               .then((response) => response.json())
                .then((responseJson) => {
                     this.setState({
                         timeSlots:responseJson.data.time_slot,
                         shippingAddress:responseJson.data.shipping_address,
                         billingAddress:responseJson.data.billing_address,
                         shopAddress:responseJson.data.shop_address,
                         refresh:false,
                         addressCount:responseJson.data.address_count
                        //  pickupPersonName:responseJson.data.pickup_person_name,
                        //  pickupPersonNumber:responseJson.data.pickup_person_number
                     })
                     this.doOrDontPrepost() 
               }) 
       }
       else{
         this.setState({networkFailed:true})
         alert("Check network connection")
       }
     })            
 }

changeVehicle(indexofVechicle,value){
   
    //this.setState({userChoosedVehicleList:[]})
    // //console.warn(this.state.vehicleLists.length-1)
    //console.warn(this.state.vehicleLists[this.state.vehicleLists.length-1].vehicle_max_weight,totalWeight)

    if(this.state.vehicleLists[this.state.vehicleLists.length-1].vehicle_max_weight>=totalWeight){
        //console.warn("true")
        var list=[...this.state.vehicleLists]
        list.map((item,index)=>{
            if(index==indexofVechicle){
              item.selected_vehicle=true
              this.props.setDeliveryCharge(item.delivery_fee)
            }else{
                item.selected_vehicle=false
            }
        })
    }else{
        //Toast.show({text:"Your total weight reached our maximum total weight, Thank you",duration:1000,textStyle:{fontSize:12},buttonText:"Okay"})
        var list=[...this.state.vehicleLists]
    
    if(value==false){
        list.map((item,index)=>{
            if(index==indexofVechicle){
              item.selected_vehicle=false
              this.props.setDeliveryCharge(this.props.deliveryCharge-item.delivery_fee)
            }
        })
    }else{
        list.map((item,index)=>{
            if(index==indexofVechicle){
              item.selected_vehicle=true
              this.props.setDeliveryCharge(this.props.deliveryCharge+item.delivery_fee)
            }
        })
    }
    this.setState({vehicleLists:list})
    }
    
    
    
    this.checkuserChoosedVehicles()
}

  async checkuserChoosedVehicles(){
    var userChoosedVehicleList=[]
    this.setState({userChoosedVehicleList:userChoosedVehicleList})
    await this.state.vehicleLists.map((item,index)=>{
        if(item.suggestion_vehicle==true && item.vehicle_availability==true){
            userChoosedVehicleList.push(item)
        }else{
            const indexOfObject = userChoosedVehicleList.indexOf(index);
        if (indexOfObject > -1) {
          userChoosedVehicleList.splice(indexOfObject, 1);
        }
        }
    })
   await this.setState({userChoosedVehicleList:userChoosedVehicleList})
  }


async checkVehicles(item){

    var addressId=null;
    if(this.state.shippingAsBilling==true){
        if(this.state.billingAddress.length==0){
            addressId=this.props.postCode
        }else{
            addressId=this.state.billingAddress.postcode
        }
    }else{
        if(this.state.billingAddress.length==0 && this.state.shippingAddress.length==0){
            addressId=this.props.postCode
        }else{
            addressId=this.state.shippingAddress.postcode
        }
    }
    if(this.state.timeSlots.length!=0){
    this.state.timeSlots.map((items,index)=>{
        // items.available=true
        if(index==0){
           this.setState({selectedTimeSlot:items.id,selectedTimeSlotName:items.times,timesloterror:null})
        }
    })
   }else{
       console.log("please select day")
       this.setState({selectedTimeSlot:null,selectedTimeSlotName:null,timesloterror:"Please select another date ."})
   }

     if(item!==null){
     await this.setState({selectedTimeSlot:item.id,selectedTimeSlotName:item.times,openingTimePickerModal:false,selectedVehicelList:[]})
     }
  await this.setState({vehicleLoading:true,selectedVehicleArray:[]})

   //////console.warn(this.state.selectedTimeSlot,this.state.dateValue,this.props.totalWeight,addressId)
    fetch(baseUrl+this.props.userId+"/schedule",{
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
              "time_slot_id":this.state.selectedTimeSlot,
              "delivery_date":this.state.dateValue,
              "total_weight":this.props.totalWeight,
              "postcode":addressId
              //"shipping_address_id":addressId
        })
       })
      .then((response)=>response.json())
      .then((responseJson)=>{
          //////console.warn(responseJson)
          this.setState({vehicleLists:responseJson.data,selectedVehicelList:[],vehicleLoading:false})

          if(this.props.orderDeliverType==0){
            for(var i=0;i<this.state.vehicleLists.length;i++){
                if(this.state.vehicleLists[i].vehicle_availability==true){
                    if(this.state.vehicleLists[i].vehicle_max_weight>this.props.totalWeight){
                        this.state.selectedVehicelList.push(i)
                        break
                    }
               }
            }
      
          if(this.state.selectedVehicelList.length==0){
              lastIndex=this.state.vehicleLists.length-1
              var list =this.state.vehicleLists
              list[lastIndex].selected_vehicle=true
              this.props.setDeliveryCharge(list[lastIndex].delivery_fee)
          }else{

          var list=[...this.state.vehicleLists]
          for(var i=0;i<list.length;i++){
            if(this.state.vehicleLists[i].vehicle_availability==true){
                if(list[i].vehicle_max_weight>this.props.totalWeight){
                    this.setState({preloadVehicleIndex:i})
                    this.props.setDeliveryCharge(list[i].delivery_fee)
                    break
                }
            }
          }

          list.map((item,index)=>{
              if(index==this.state.preloadVehicleIndex){
                  item.selected_vehicle=true
              }else{
                item.selected_vehicle=false
              }
          })
          this.setState({vehicleLists:list,loading:false})
        }
        this.setState({loading:false})

    this.checkuserChoosedVehicles()
   }
})

 }
 
 async customQty(text,indexValue,item){
    if(text="") {
        alert("Please enter a valid number")
    }else{
    let products = [...this.props.orderDetails];
    products[indexValue].quantity=text;
    await this.props.setOrderDetails(products)
    await this.props.setSelectProduct(this.props.orderDetails[indexValue])
    await this.addToCart(indexValue,item)
    }
  }


async increaseQty(indexValue,item){
        let products = [...this.props.orderDetails];
        products[indexValue].quantity=parseInt(products[indexValue].quantity)+1
        await this.props.setOrderDetails(products)
        await this.props.setSelectProduct(this.props.orderDetails[indexValue])
        this.addToCart(indexValue,item)
}

async decreaseQty(indexValue,item){
        let products = [...this.props.orderDetails];
        products[indexValue].quantity-=1;
        if(products[indexValue].quantity<=1){
            products[indexValue].quantity=1
        }
        await this.props.setOrderDetails(products)
        await this.props.setSelectProduct(this.props.orderDetails[indexValue])
        this.addToCart(indexValue,item)
}

async postOrderDetail(){
    var selectedVehicleArray=[]
   await this.setState({userChoosedTotalWeight:0,selectedVehicleArray:[]})
   await this.state.userChoosedVehicleList.map((item,index)=>{
        if(item.selected_vehicle==true){
           selectedVehicleArray.push(item.vehicle_id)
           this.setState({userChoosedTotalWeight:this.state.userChoosedTotalWeight+(item.vehicle_max_weight)})
        }
        
    })
   await this.setState({selectedVehicleArray:selectedVehicleArray})
   
    if(this.props.orderDeliverType==0){
        if(this.state.selectedVehicleArray.length==0){
            alert("Please choose delivery vehicle")
        }else{
            if(this.props.totalWeight<=this.state.userChoosedTotalWeight){
                this.navigatePaymentScreen()
            }else{
                alert("Please choose another one vehicle to accommodate your order. Thank you")
            }
        }
    }else{
        this.navigatePaymentScreen()
    }
 }

 async navigatePaymentScreen(){
    await this.props.setcomments(this.state.discription)
    var qty=0;
    this.props.orderDetails.map((item,index)=>{
       qty+=item.quantity
    })
    this.props.setTotalQty(qty)

    if(this.props.orderDeliverType==1){
        if(this.state.billingAddress.length!==0){
            await this.props.setBillingAddressId(this.state.billingAddress.id)
        if(this.state.pickupPersonName!=='' && this.state.pickupPersonNumber!==''){
           await this.props.setPickupPersonName(this.state.pickupPersonName)
           await this.props.setPickupPersonNumber(this.state.pickupPersonNumber)

           if(this.state.selectedTimeSlot!=null && this.state.dateValue!=null){
            await this.props.setTimeSlot(this.state.selectedTimeSlot)
            await this.props.setDateValue(this.state.dateValue)
            await this.props.setorderDeliverType(1)
            this.props.navigation.navigate("paymentScreen")   
         }else{
                alert("Please select date & time")
         }
        }else{
            alert("please fill pickup person details")
        }
    }else{
        alert("Please add billing address")
    }
    }else if(this.state.shippingAsBilling==true){
            if(this.state.shippingAddress.length!==0){
                  await this.props.setShippingAddressId(this.state.shippingAddress.id)
                  await this.props.setBillingAddressId(this.state.shippingAddress.id)
                   if(this.state.selectedTimeSlot!=null && this.state.dateValue!=null){
                        await this.props.setTimeSlot(this.state.selectedTimeSlot)
                        await this.props.setDateValue(this.state.dateValue)
                        await this.props.setorderDeliverType(0)
                        await this.props.setVehicleId(this.state.selectedVehicleArray)
                        this.props.navigation.navigate("paymentScreen")   
                }else{
                    alert("Please select date & time")
                }                    
            }else {
                alert("Please add shipping address ")
            }
            
        }else if(this.state.shippingAddress.length!==0 && this.state.billingAddress.length!==0){
                  await this.props.setShippingAddressId(this.state.shippingAddress.id)
                  await this.props.setBillingAddressId(this.state.billingAddress.id)
                if(this.state.selectedTimeSlot!=null && this.state.dateValue!=null){
                    await this.props.setTimeSlot(this.state.selectedTimeSlot)
                    await this.props.setDateValue(this.state.dateValue)
                    await this.props.setorderDeliverType(0)
                    await this.props.setVehicleId(this.state.selectedVehicleArray)
                    this.props.navigation.navigate("paymentScreen")  
                }else{
                    alert("Please select date & time")
                }
        }else if(this.state.shippingAddress.length==0){
            alert("Please add shipping address")
        }else if(this.state.billingAddress.length==0){
            alert("Please add billing address")
        } 
  }

 async addToCart(indexValue,item){
     await this.setState({totalAmount:0,totalWeight:0,checkoutIncDecLoader:true,selcetedItemForIncDec:indexValue})
        await this.props.orderDetails.map((item,index)=>{
            this.setState({totalAmount:this.state.totalAmount+(item.price*item.quantity),
                totalWeight:this.state.totalWeight+(item.weight*item.quantity)
               })
         })
     await this.props.setTotalAmount((this.state.totalAmount).toFixed(2))
     ////console.log(this.props.totalAmount)
        if(this.state.totalWeight==0){
            await this.props.setTotalWeight(0)
        }else{
        await this.props.setTotalWeight(this.state.totalWeight.toFixed(2))
        }   

        await fetch(baseUrl+this.props.userId+'/updatecart', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
            "row_id"   :item.row_id,
            "quantity":this.props.selectedProduct.quantity,
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
        }).catch((err)=>{
    })  
       await this.setState({IncOrDecProductName:item.name,isediting:false,IncOrDecProductCountChanged:true})
       //await this.props.setHomeScreenUpdate(true)
      await this.props.setAddressChanged(true)
      await this.setState({checkoutIncDecLoader:false})  
  }

  changeDescriptionData(value){
      let length=value.length
      if(length<=100){
        this.setState({discription:value})
      }
  }

 timeSlot(){
      if(this.state.dateValue==null){
         alert("please select a date")
     }else{
    this.setState({openingTimePickerModal:true})
     }
 }

 openDatePicker(){    
    this.setState({openingDatePickerModal:true})
 }

async selectDate(day){
        console.log(day.dateString)
        await this.setState({dateValue:day.dateString,openingDatePickerModal:false})  
        
        await fetch(baseUrl+this.props.userId+'/check-time-slot', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            body: JSON.stringify({
             "delivery_date":day.dateString
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log("time slots "+JSON.stringify(responseJson))
            if(responseJson.success==true){
              this.setState({timeSlots:responseJson.data})
            }
            console.log("time slots "+JSON.stringify(this.state.timeSlots))
        }).catch((err)=>{
    })  
        
        await this.setState({currentDate:new Date().getDate()})
        if(this.state.currentDate<=9){
           var currentdate=0+""+this.state.currentDate
           await this.setState({currentDate:currentdate})
         }else{
           await this.setState({currentDate:this.state.currentDate})
         }
         var date=this.state.currentYear+"-"+this.state.currentMonth+"-"+this.state.currentDate  

         
         var addressId=null;
            if(this.state.shippingAsBilling==true){
                if(this.state.billingAddress.length==0){
                    addressId=this.props.postCode
                }else{
                    addressId=this.state.billingAddress.postcode
                }
            }else{
                if(this.state.billingAddress.length==0 && this.state.shippingAddress.length==0){
                    addressId=this.props.postCode
                }else{
                    addressId=this.state.shippingAddress.postcode
                }
            }
        //////console.warn(addressId)
        // if(date==this.state.dateValue){
            // this.preVehiclePost(addressId)
        // }else{
            this.checkVehicles(null)
        // }
        
 }

 timeSelectionDone(){
    if(this.state.timesloterror==null) {
      if(this.state.selectedTimeSlot!=null){
       this.setState({openingTimePickerModal:false})
      }
    }else{
        this.setState({openingTimePickerModal:false})
    } 
 }
  async changeAddress(value){
      //console.warn(value)
    if(value=="shipping"){
        await this.props.setisShippingAddress(true)
    }else if(value=="billing"){
        await this.props.setisShippingAddress(false)
    }
    await this.props.setisFromCheckout(true)
    this.props.navigation.navigate("manageSiteAddress")
  }

 async addNewAddress(value){
    await this.props.setisFromCheckout(true)
     if(value=="shipping"){
        await this.props.setisShippingAddress(true)
     }else if(value=="billing"){
        await this.props.setisShippingAddress(false)
     }
    //console.log(this.props.isShippingAddress)
     if(this.state.addressCount==0){
        this.props.navigation.navigate("createNewSite",{isFromSignup:0})
     }else{
        this.props.navigation.navigate("manageSiteAddress")
     }
  }

 async setShippingOrBilling(value){
   await this.setState({shippingAsBilling:!this.state.shippingAsBilling})
   await this.props.setshippingAsBilling(value)
  }

 async incdecEdit(item){
      //console.log(item)
    await this.setState({selectedItemForEditing:item.id})
    var qty=item.quantity.toString()
    //console.log(qty)
    this.setState({iseditingValue:qty,isediting:true})
   }
 
  async changePostCode(){
   await this.props.setchangePostCodeStatus(true)
   this.props.navigation.navigate("changePostcode")
   }

  render() {
    
    if(this.props.totalAmount!=0){
         totalAmount=(parseFloat(this.props.totalAmount)+this.props.deliveryCharge).toFixed(2)
         totalAmountWithoutDeliveryCharge=(parseFloat(this.props.totalAmount)+0).toFixed(2)
         totalWeight=this.props.totalWeight
    }
    ////console.warn(totalAmount)
    // const markedDates=this.state;
    // const totalAmount= (JSON.parse(this.props.totalAmount)+this.props.deliveryCharge).toFixed(2)
    // const totalAmountWithoutDeliveryCharge=(JSON.parse(this.props.totalAmount)+0).toFixed(2)
    // const totalWeight=this.props.totalWeight
    if(this.state.loading==true){
        return(
            
            <View style={{flex:1,backgroundColor:'#fff'}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#fa7153',padding:15}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"myCart"})} style={OverAllStyle.HeaderIcon}>
                    <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                    </TouchableOpacity>

                    <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Checkout</Text>
                    <Text/>
                </View>    
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <ActivityIndicator size="large" color="#fa7153"/>
                    <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
                </View>
            </View>
        )
    }else{
    return (
        <View style={OverAllStyle.MainContainerPad0}>  
          <View style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#fa7153',padding:15}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"myCart"})} style={OverAllStyle.HeaderIcon}>
                  <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>

                  <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Checkout</Text>
                  <Text/>
          </View>       
          <KeyboardAwareScrollView showsVerticalScrollIndicator={false} extraScrollHeight={20}>
            <ScrollView>
                  
                <View style={{paddingHorizontal:20,paddingVertical:15,flexDirection:'row',justifyContent:'space-between',marginBottom:10,backgroundColor:'#fff'}}>
                    <View>
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        <Image source={images.available} style={{width:30,height:30,resizeMode:'contain'}}/>                            
                    <View style={{flexDirection:'column'}}>
                    {
                        this.props.orderDeliverType==0?
                        <Text style={{fontFamily:'Montserrat-SemiBold'}}>  Delivery available</Text>
                        :
                        <Text style={{fontFamily:'Montserrat-SemiBold'}}>  Pick-up only</Text>
                    }
                    <Text style={{fontSize:10,fontFamily:'Montserrat'}}>  Postcode : {this.props.postCode}</Text>
                    </View>
                    </View>
                    </View>
                    <TouchableOpacity onPress={()=>this.changePostCode()} style={{width:'25%',borderRadius:3,backgroundColor:'#fa7153',alignItems:'center',justifyContent:'center'}}>
                        <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',fontSize:14}}>Change</Text>
                    </TouchableOpacity>
                </View>
                
                <View style={CartScreenStyle.WhiteContainer}>
                    <View style={CartScreenStyle.WhiteSubContainer}>
                    {
                       this.props.orderDeliverType==1?
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16}}>Pick-up time</Text>
                        :
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16}}>Delivery time</Text>
                    }
                    
                    
                    <View style={{paddingVertical:10}}>

                        <View style={CompleteOrderStyle.DateTimeMainView}>
                        
                            
                                <TouchableOpacity style={CompleteOrderStyle.DateView} 
                                onPress={()=>this.openDatePicker()}>
                                    <View style={{width:'80%'}}> 
                                        {
                                            this.state.dateValue==null?
                                            <Text style={{fontFamily:"Montserrat"}}>  DD/MM</Text>
                                            :
                                            <Text style={{fontFamily:"Montserrat"}}>  {this.state.dateValue}</Text>
                                        }
                                    </View>
                                    
                                    <View style={CompleteOrderStyle.DateIcon}>
                                        <Image source={images.calander} style={StyleContainer.ImageContainStyle} />
                                    </View>
                                </TouchableOpacity>
                                
                                <TouchableOpacity onPress={()=>this.timeSlot()} style={CompleteOrderStyle.TimeView}>
                                    <View style={{width:'80%'}}> 
                                    {
                                            this.state.selectedTimeSlotName==null?
                                            <Text style={{fontFamily:"Montserrat"}}>  HH:MM</Text>
                                            :
                                            <Text style={{fontFamily:"Montserrat"}}>{this.state.selectedTimeSlotName}</Text>
                                        }
                                    </View>
                                    
                                    <View style={CompleteOrderStyle.DateIcon}>
                                        <Image source={images.clock} style={StyleContainer.ImageContainStyle} />
                                    </View>
                                </TouchableOpacity>

                    </View>

                    </View>
                  </View>
                  {
                      this.state.timesloterror==null?null:
                      <Text style={{color:'#fa7153',fontSize:12,paddingLeft:15,paddingBottom:10,fontFamily:'Montserrat'}}>{this.state.timesloterror}</Text>
                  }
                </View>
                
                {
                    this.props.orderDeliverType==1?
                    null
                    :
                    <View>
                    {
                    this.state.vehicleLists.length==0?
                        <>
                        {
                            this.state.timesloterror==null?
                            <View style={{backgroundColor:'#fff',flexDirection:'row',padding:15,alignItems:'center'}}>
                                <Image source={images.notAvailable} style={{width:20,height:20}}/>
                                <Text style={{fontFamily:'Montserrat',color:'#fa7153',fontSize:12,paddingLeft:5}}>Address field is required for vehicle availability</Text>                            
                            </View>
                            :
                            <View style={{backgroundColor:'#fff',flexDirection:'row',padding:15,alignItems:'center'}}>
                                <Image source={images.notAvailable} style={{width:20,height:20}}/>
                                <Text style={{fontFamily:'Montserrat',color:'#fa7153',fontSize:12,paddingLeft:5}}>Timeslot is required for vehicle availability</Text>                            
                            </View>
                        }
                        </>
                        :
                        <View style={{backgroundColor:'#fff',padding:20,marginBottom:10}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16,paddingBottom:10}}>Upgrade delivery vehicle</Text>
                            {
                                this.state.refresh==true || this.state.vehicleLoading==true?
                                <ActivityIndicator color="#fa7153"/>
                                :
                                null
                            }
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            {
                            this.state.vehicleLists.map((item,index)=>{
                                if(item.selected_vehicle==true && item.vehicle_availability==true){
                                    return(
                                        <TouchableOpacity onPress={()=>this.changeVehicle(index,false)} key={index} style={{width:'32%'}}> 
                                            <View style={{width:20,height:20,position:'absolute',zIndex:1,alignSelf:'flex-end',borderRadius:15,backgroundColor:'#fff',alignItems:'center',justifyContent:'center',right:5,top:7}}>
                                                <Image source={images.complete} style={{width:15,height:15,resizeMode:'cover'}}/>
                                            </View>
                                            <View style={{backgroundColor:'#fa7153',paddingVertical:5,alignItems:'center',justifyContent:'center',marginVertical:3,paddingHorizontal:10,borderRadius:5}}>
                                                <Image source={{uri:item.selected_vehicle_image}} style={{width:50,height:40,resizeMode:'contain'}}></Image>
                                                <View style={{height:40,justifyContent:'center'}}>
                                                <Text numberOfLines={2} ellipsizeMode="tail" style={{fontSize:11,textAlign:'center',paddingVertical:5,fontFamily:'Montserrat-SemiBold',color:'#fff'}}>{item.vehicle_name}</Text>
                                                </View>
                                                <Text numberOfLines={1} ellipsizeMode="middle" style={{fontSize:9,textAlign:'center',fontFamily:'Montserrat-SemiBold',paddingBottom:5,color:'#fff'}}>Up to {item.vehicle_max_weight} ton</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )
                                }else{
                                    if(item.vehicle_availability==true){
                                        return(
                                            <TouchableOpacity onPress={()=>this.changeVehicle(index,true)} key={index} style={{width:'32%'}}> 
                                            <View style={{backgroundColor:'#f2f2f2',paddingVertical:5,alignItems:'center',justifyContent:'center',marginVertical:3,paddingHorizontal:10,borderRadius:5}}>
                                                {/* <View style={{width:20,height:20}}/> */}
                                                <Image source={{uri:item.vehicle_image}} style={{width:50,height:40,resizeMode:'contain'}}></Image>
                                                <View style={{height:40,justifyContent:'center'}}>
                                                <Text numberOfLines={2} ellipsizeMode="tail" style={{fontSize:11,textAlign:'center',paddingVertical:5,fontFamily:'Montserrat-SemiBold'}}>{item.vehicle_name}</Text>
                                                </View>
                                                <Text numberOfLines={1} ellipsizeMode="middle" style={{fontSize:9,textAlign:'center',fontFamily:'Montserrat',paddingBottom:5}}>Up to {item.vehicle_max_weight} ton</Text>
                                            </View>
                                        </TouchableOpacity>
                                        )
                                    }else{
                                    return(
                                        <View key={index} style={{width:'32%'}}> 
                                             <View style={{flex:1,position:'absolute',zIndex:1,marginTop:'50%',alignSelf:'center'}}>
                                               <Text style={{fontSize:10,fontFamily:'Montserrat-SemiBold',color:'#fff',textAlign:'center'}}>Not available</Text>
                                             </View>
                                            <View style={{backgroundColor:'#5e5d5d',paddingVertical:5,alignItems:'center',justifyContent:'center',marginVertical:3,paddingHorizontal:10,borderRadius:5}}>
                                                <Image source={{uri:item.vehicle_image}} style={{width:50,height:40,resizeMode:'contain'}}></Image>
                                                <View style={{height:40,justifyContent:'center'}}>
                                                <Text numberOfLines={2} ellipsizeMode="tail" style={{fontSize:11,textAlign:'center',paddingVertical:5,fontFamily:'Montserrat-SemiBold'}}>{item.vehicle_name}</Text>
                                                </View>
                                                <Text numberOfLines={1} ellipsizeMode="middle" style={{fontSize:9,textAlign:'center',fontFamily:'Montserrat',paddingBottom:5}}>Up to {item.vehicle_max_weight} ton</Text>
                                            </View>
                                        </View>
                                    )
                                  }
                                }
                            })
                            }
                        </View>    
                        <Text style={{fontFamily:'Montserrat',lineHeight:13,fontSize:10,paddingTop:10,textAlign:'center'}}>Vehicles allocated based on product type ordered and weight. If you wish to upgrade, select the type of truck needed.</Text>            
                    </View>
                    }
                    </View>
                }
                
                

                <View style={CartScreenStyle.WhiteContainer} >
                
                <View style={CartScreenStyle.WhiteSubContainer}> 
                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16}}>Order details</Text>
                </View>
              
                {
                    this.props.orderDetails.map((item,index)=>{
                        var productPrice=(item.price * item.quantity).toFixed(2)
                    return(
                            <View style={{width:'100%',paddingHorizontal:20,paddingVertical:10,flexDirection:'row',borderBottomColor:Colors.searchScrBdrClr,borderBottomWidth:1}}>
                                <View style={{width:'50%',flexDirection:'row',justifyContent:'space-between',marginRight:10}}>

                                    <View style={{width:'30%',height:50}}>
                                    <Image source={{uri:item.image_url}} style={[StyleContainer.ImageCoverStyle,{borderRadius:5}]}/>
                                    </View>
                                    

                                    <View style={{width:'65%',justifyContent:'center'}}>
                                        <Text style={CartScreenStyle.prodctTxtStyle} numberOfLines={2} ellipsizeMode="middle">{item.name}</Text>
                                        {/* {
                                            item.option_text==null || item.option_value==null? null:
                                              <Text style={{fontSize:10,color:'gray',fontFamily:'Montserrat'}}>{item.option_text} : {item.option_value} {item.option_unit}</Text>
                                        } */}
                                        {
                                            item.product_variants==""?null:
                                            <Text style={{fontSize:10,color:'gray',fontFamily:'Montserrat'}}>{item.product_variants}</Text>
                                        }
                                    </View>
                                </View>


                            <View style={{flexDirection:'row',width:'50%',justifyContent:'space-between',alignItems:'center'}}>                                
                                <View style={{alignItems:'center'}}>                        
                                    <View style={CartScreenStyle.IncDecMainContainer}>
                                        <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                        onPress={()=>{this.decreaseQty(index,item)}}>
                                            <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                        </TouchableOpacity>
                                        <View style={CartScreenStyle.IncDecQtyContainer}>
                                           <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                        </View>
                                        {/* <TouchableOpacity onPress={()=>this.incdecEdit(item)} style={CartScreenStyle.IncDecQtyContainer}>
                                            {
                                                this.state.isediting==true && item.id==this.state.selectedItemForEditing?
                                                 <TextInput
                                                   autoFocus={this.state.isediting}
                                                   style={{width:'100%',height:24,paddingVertical:0,fontFamily:'Montserrat',fontSize:12,textAlign:'center',color:Colors.themeColor}}
                                                   onChangeText={(text)=>this.customQty(text,index,item)}
                                                   value={this.state.iseditingValue}
                                                   keyboardType={"phone-pad"}
                                                  />
                                                  :
                                                  <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                            }
                                        </TouchableOpacity> */}
                                        
                                        
                                        <TouchableOpacity style={CartScreenStyle.OrangeIncBtn}
                                        onPress={()=>{this.increaseQty(index,item)}}>
                                            <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View style={{width:'30%',alignItems:'flex-start'}}>
                                   <Text numberOfLines={1} style={{fontFamily:"Montserrat-SemiBold",fontSize:11}}>${productPrice}</Text>
                                </View>
                            </View>
                            </View>
                    )
                    })
                }

            <View style={{borderBottomColor:'#EEE',borderBottomWidth:1}}>  
            
            <View style={CartScreenStyle.WhiteSubContainer}>
                  
                  {
                      totalWeight==0?null:
                      <View style={CartScreenStyle.SpacebtwnContainer}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:14}}>Total weight (approx..)</Text>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:14}}>{totalWeight} Ton</Text>
                        </View>
                  }

                  <View style={CartScreenStyle.SpacebtwnContainer}>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:14}}>Sub total</Text>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:14}}>$ {(parseFloat(this.props.totalAmount)+0).toFixed(2)}</Text>
                  </View>

                  

                  </View>
            
            </View>
                

           {
               this.props.orderDeliverType==1?
               null
               :
               <View style={{borderBottomColor:'#EEE',borderBottomWidth:1}}>  
                <View style={CartScreenStyle.WhiteSubContainer}>
                    <View style={CartScreenStyle.SpacebtwnContainer}>
                        <Text style={{fontFamily:"Montserrat", color:'red'}}>Delivery charges</Text>
                        <Text style={{fontFamily:"Montserrat",color:'red'}}>$ {(this.props.deliveryCharge+0).toFixed(2)}</Text>
                    </View>
                </View>
               </View>
           }          
            

            <View style={{borderBottomColor:'#EEE',borderBottomWidth:1}}>  
                {/* <View style={CartScreenStyle.WhiteSubContainer}>
                    <View style={CartScreenStyle.SpacebtwnContainer}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15}}>Total amount</Text>
                        {
                            this.props.orderDeliverType==1?
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15}}>$ {totalAmountWithoutDeliveryCharge}</Text>
                            :
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15}}>$ {totalAmount}</Text>
                        }
                        
                    </View>
                </View>            */}
            </View>
            

                  
            <View style={CartScreenStyle.WhiteSubContainer}>
                <View style={CartScreenStyle.SpacebtwnContainer}>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20,  }}>Grand total</Text>
                    {
                        this.props.orderDeliverType==1?
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20}}>$ {totalAmountWithoutDeliveryCharge}</Text>
                        :
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20}}>$ {totalAmount}</Text>
                    }
                </View>
            </View>
                  

              </View>
            

              <View style={CartScreenStyle.WhiteContainer}>
                        {
                            this.props.orderDeliverType==1?
                            <View>
                            {
                                        this.state.billingAddress.length==0?
                                        <View>
                                        <View style={CartScreenStyle.WhiteSubContainer}>
                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                            <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:3}}>Billing address</Text>
                                            {                                       
                                                this.state.refresh? 
                                                <ActivityIndicator color="#fa7153"/>
                                                :
                                                null
                                            }
                                            </View>
 
                                            <TouchableOpacity onPress={()=>this.addNewAddress("billing")} style={{width:'90%',backgroundColor:'#fa7153',borderRadius:5,padding:10,alignSelf:'center',marginVertical:10}}>
                                                <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',textAlign:'center'}}>Add new address</Text>
                                            </TouchableOpacity>
                                        </View>
                                        
                                        <View style={{height:10,backgroundColor:'#f2f2f2'}}/>
                                        <View style={CartScreenStyle.WhiteSubContainer}>
                                            <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:10}}>Pick-up address</Text>
                                            <View style={MyOrderDetailStyle.DelverAddressSubView}>
                                                
                                                    <View style={MyOrderDetailStyle.locationIconView}>                        
                                                        <Image source={images.location} style={StyleContainer.ImageContainStyle} />
                                                    </View>
            
                                                    <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                                            <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}}>{this.state.shopAddress.jobsite_name}</Text>
                                                            <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.shopAddress.address_line1}</Text>
                                                            <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.shopAddress.address_line2}</Text>
                                                    </View>
                                            </View>
                                        </View>

                                        </View>
                                        :
                                        <View>
                                        <View style={[CartScreenStyle.WhiteSubContainer,{borderBottomWidth:10,borderBottomColor:'#f2f2f2'}]}>
                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                            <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:10}}>Billing address</Text>
                                            {                                       
                                                this.state.refresh? 
                                                <ActivityIndicator color="#fa7153"/>
                                                :
                                                null
                                            }
                                            </View>
                                            {/* <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:15,marginBottom:15}}>Job site address</Text> */}
                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                <View style={MyOrderDetailStyle.DelverAddressSubView}>
                                                    
                                                        <View style={MyOrderDetailStyle.locationIconView}>                        
                                                            <Image source={images.location} style={StyleContainer.ImageContainStyle} />
                                                        </View>

                                                        <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                                                {
                                                                    this.state.billingAddress.job_site_name==null?
                                                                    null
                                                                    :
                                                                    <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}}>{this.state.billingAddress.job_site_name}</Text>
                                                                }
                                                                
                                                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.billingAddress.address_line1}</Text>
                                                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.billingAddress.address_line2}</Text>
                                                        </View>

                                                </View>

                                                <TouchableOpacity onPress={()=>this.changeAddress("billing")} style={{marginBottom:5,alignSelf:'flex-end'}}> 
                                                        <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fa7153',fontSize:12}}>Change</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                           
                                        <View style={CartScreenStyle.WhiteSubContainer}>
                                            <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:10}}>Pick-up address</Text>
                                            <View style={MyOrderDetailStyle.DelverAddressSubView}>
                                                
                                                    <View style={MyOrderDetailStyle.locationIconView}>                        
                                                        <Image source={images.location} style={StyleContainer.ImageContainStyle} />
                                                    </View>
            
                                                    <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                                            <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}}>{this.state.shopAddress.jobsite_name}</Text>
                                                            <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.shopAddress.address_line1}</Text>
                                                            <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.shopAddress.address_line2}</Text>
                                                    </View>
                                            </View>
                                        </View>
                                    </View>    
                            }
                            

                            </View>
                            :
                            
                            <View>
                                        {
                                            this.state.shippingAddress.length==0?
                                            <View style={CartScreenStyle.WhiteSubContainer}>
                                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:3}}>Shipping address</Text>
                                                {                                       
                                                    this.state.refresh? 
                                                    <ActivityIndicator color="#fa7153"/>
                                                    :
                                                    null
                                                }
                                                </View>

                                                <TouchableOpacity onPress={()=>this.addNewAddress("shipping")} style={{width:'90%',backgroundColor:'#fa7153',borderRadius:5,padding:10,alignSelf:'center',marginVertical:10}}>
                                                    <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',textAlign:'center'}}>Add new address</Text>
                                                </TouchableOpacity>
                                            </View>
                                            :
                                            <View style={CartScreenStyle.WhiteSubContainer}>
                                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:10}}>Shipping address</Text>
                                                {                                       
                                                    this.state.refresh? 
                                                    <ActivityIndicator color="#fa7153"/>
                                                    :
                                                    null
                                                }
                                                </View>
                                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                    <View style={MyOrderDetailStyle.DelverAddressSubView}>
                                                        
                                                            <View style={MyOrderDetailStyle.locationIconView}>                        
                                                                <Image source={images.location} style={StyleContainer.ImageContainStyle} />
                                                            </View>

                                                            <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                                                 {
                                                                     this.state.shippingAddress.job_site_name==null?null:
                                                                     <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}}>{this.state.shippingAddress.job_site_name}</Text>
                                                                 }
                                                                    
                                                                    <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.shippingAddress.address_line1}</Text>
                                                                    <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.shippingAddress.address_line2}</Text>
                                                            </View>

                                                    </View>

                                                    <TouchableOpacity onPress={()=>this.changeAddress("shipping")} style={{marginBottom:5,alignSelf:'flex-end'}}> 
                                                            <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fa7153',fontSize:12}}>Change</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                    }
                                        </View>
                        }
                    </View>
                        {
                            this.props.orderDeliverType==1?
                            null
                            :
                            <View>
                                {
                                    this.state.shippingAsBilling==true?
                                    <View style={{backgroundColor:'#fff',marginBottom:10,padding:15,marginLeft:5,flexDirection:'row'}}> 
                                        <TouchableOpacity onPress={()=>this.setShippingOrBilling(false)}>
                                        <Image source={require('../images/rememberLogin.png')} style={{width:20,height:20,resizeMode:'contain'}}/>
                                        </TouchableOpacity>

                                        <Text style={{fontFamily:'Montserrat',paddingLeft:10}}>Billing address same as shipping address</Text>
                                    </View>
                                    :
                                    <View style={{backgroundColor:'#fff',marginBottom:10}}> 
                                        <View style={{flexDirection:'row',borderBottomWidth:10,padding:15,marginLeft:5,borderBottomColor:'#f2f2f2'}}> 
                                            <TouchableOpacity onPress={()=>this.setShippingOrBilling(true)}>
                                            <View style={{width:20,height:20,borderWidth:1.5,borderColor:'gray'}}/>
                                            </TouchableOpacity>

                                            <Text style={{fontFamily:'Montserrat',paddingLeft:10}}>Billing address same as shipping address</Text>
                                        </View>
                                        <View>
                                            {
                                                        this.state.billingAddress.length==0?
                                                        <View style={CartScreenStyle.WhiteSubContainer}>
                                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                            <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:3}}>Billing address</Text>
                                                            {                                       
                                                                this.state.refresh? 
                                                                <ActivityIndicator color="#fa7153"/>
                                                                :
                                                                null
                                                            }
                                                            </View>

                                                            <TouchableOpacity onPress={()=>this.addNewAddress("billing")} style={{width:'90%',backgroundColor:'#fa7153',borderRadius:5,padding:10,alignSelf:'center',marginVertical:10}}>
                                                                <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',textAlign:'center'}}>Add new address</Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                        :
                                                        <View style={CartScreenStyle.WhiteSubContainer}>
                                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                            <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,marginBottom:10}}>Billing address</Text>
                                                            {                                       
                                                                this.state.refresh? 
                                                                <ActivityIndicator color="#fa7153"/>
                                                                :
                                                                null
                                                            }
                                                            </View>
                                                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                                                <View style={MyOrderDetailStyle.DelverAddressSubView}>
                                                                    
                                                                        <View style={MyOrderDetailStyle.locationIconView}>                        
                                                                            <Image source={images.location} style={StyleContainer.ImageContainStyle} />
                                                                        </View>

                                                                        <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                                                            {
                                                                                this.state.billingAddress.job_site_name==null?null:
                                                                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}}>{this.state.billingAddress.job_site_name}</Text>
                                                                            }
                                                                                
                                                                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.billingAddress.address_line1}</Text>
                                                                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5}} >{this.state.billingAddress.address_line2}</Text>
                                                                        </View>

                                                                </View>

                                                                <TouchableOpacity onPress={()=>this.changeAddress("billing")} style={{marginBottom:5,alignSelf:'flex-end'}}> 
                                                                        <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fa7153',fontSize:12}}>Change</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    }
                                            
                                            </View> 
                                        

                                        
                                    </View>
                                }

                            </View>
                        }
                                                
                        
                    {
                        
                        this.props.orderDeliverType==1?
                        <View style={{backgroundColor:'#fff',paddingHorizontal:20,paddingTop:10,marginBottom:10}}>
                            <Text style={{fontFamily:'Montserrat-SemiBold',paddingBottom:5}}>Pick-up person name</Text>
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                              <TextInput style={StyleContainer.TextInputView} 
                                //autoFocus
                                value={this.state.pickupPersonName}
                                ref={(input) => { this.jobSiteNameRef = input; }}
                                onSubmitEditing={() => {this.jobSiteNumberRef.focus();}}
                                onChangeText={(value)=>this.setState({pickupPersonName:value})}
                                returnKeyType={"next"}
                               />
                            </View>

                            <Text style={{fontFamily:"Montserrat-SemiBold",paddingBottom:5}}>Phone number</Text>
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                              <TextInput style={StyleContainer.TextInputView} 
                                //autoFocus
                                value={this.state.pickupPersonNumber}
                                ref={(input) => { this.jobSiteNumberRef = input; }}
                                onChangeText={(value)=>this.setState({pickupPersonNumber:value})}
                                returnKeyType={"next"}
                                keyboardType="number-pad"
                                maxLength={10}
                               />
                            </View>
                        </View>
                        :
                        null
                    }

                        <View style={CartScreenStyle.WhiteContainer} >
                            <View style={CartScreenStyle.WhiteSubContainer}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15}}>Comments</Text>
                                <View style={CompleteOrderStyle.TxtAreaView}>
                                    <Textarea placeholder="Add any special comment regarding the order" numberOfLines={5} style={{width:'100%',height:'100%',fontFamily:'Montserrat'}}
                                    value={this.state.discription}
                                    placeholderTextColor="#bfbfbf"
                                    onChangeText={(value)=>{this.changeDescriptionData(value)}}
                                    />
                                </View>
                                <View style={{alignItems:'flex-end'}}>
                                    <Text style={{fontFamily:"Montserrat"}}>{this.state.discription.length}/100</Text>
                                </View>
                            </View>
                        </View>

                    
                    
            </ScrollView>
           </KeyboardAwareScrollView>

           <TouchableOpacity onPress={()=>this.postOrderDetail()} style={CartScreenStyle.nextButtonContainer}>
                <View>
                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:15}}>Next</Text>
                    <Text style={{fontFamily:"Montserrat",color:'#fff',fontSize:12}}>Payments</Text>
                </View>
                <View style={{justifyContent: 'center'}}>
                    <Image source={images.backArrow} style={{width:25,height:25,transform:[{rotate:'180deg'}]}}/>
                </View>
            </TouchableOpacity>

            <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.openingDatePickerModal}
                    onRequestClose={()=>this.setState({openingDatePickerModal:false})}>
                    
                    <TouchableOpacity style={{flex:1,justifyContent:"center",padding:20,backgroundColor:'rgba(52,52,52,0.3)'}}>
                       
                            <Calendar
                                onDayPress={(day) => this.selectDate(day)}
                                markedDates={this.state.mark}
                                hideExtraDays={true}
                                minDate={Date()}
                                //minDate={this.state.date}
                                //current={Date()}
                                />
                    </TouchableOpacity>
            </Modal>

              <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.openingTimePickerModal}
                    onRequestClose={()=>this.setState({openingTimePickerModal:false})}>
                    
                    <View style={{flex:1,justifyContent:"center",padding:20,backgroundColor:'rgba(52,52,52,0.3)'}}>
                            <View style={{width:'90%',backgroundColor:'#fff',alignSelf:'center'}}>
                            {
                                this.state.slotLoading?
                                <View>
                                <ActivityIndicator/>
                                </View>
                                :
                                <View style={{padding:10}}>
                                    {
                                        this.state.timesloterror==null?
                                        <Text style={{fontFamily:"Montserrat-SemiBold",padding:10,paddingBottom:-10}}>Choose a time slot</Text>
                                        :
                                        <Text style={{fontFamily:"Montserrat-SemiBold",padding:10,paddingBottom:-10}}>Your selected date doesn't have slot for delivery!</Text>
                                    }
                                           <View>
                                                {
                                                    this.state.timeSlots.map((item,index)=>{
                                                        return(
                                                            <View>
                                                                 <TouchableOpacity onPress={()=>this.checkVehicles(item)} style={{flexDirection:'row',padding:15,marginLeft:15}}>
                                                                    {
                                                                        this.state.selectedTimeSlot==item.id?
                                                                        <View style={{width:18,height:18,borderRadius:100,borderWidth:1,borderColor:'#EEE',justifyContent:'center',alignItems:'center',marginRight:10}}>
                                                                        <View style={{width:11,height:11,borderRadius:100,backgroundColor:'#fd7153'}} />
                                                                        </View>
                                                                        :
                                                                        <View style={{width:18,height:18,borderRadius:100,borderWidth:1,borderColor:'#EEE',justifyContent:'center',alignItems:'center',marginRight:10}}/>
                                                                    }
                                                                    <Text style={{fontFamily:"Montserrat"}}>{item.times} </Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                        )
                                                    })
                                                }
                                           </View>
                                           
                                        
                                        <View style={{flexDirection:'row',justifyContent:'flex-end'}}>
                                            <TouchableOpacity style={{padding:15}} onPress={()=>this.setState({openingTimePickerModal:false})}>
                                                <Text style={{fontFamily:"Montserrat"}}>Cancel</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity style={{padding:15}} onPress={()=>this.timeSelectionDone()}>
                                                <Text style={{fontFamily:"Montserrat",color:'#fa7153'}}>Done</Text>
                                            </TouchableOpacity>
                                        </View>
                                </View>
                               }
                            </View>
                      </View>
                    </Modal>

                    {/* <Modal 
                            animationType="fade"
                            transparent={true}
                            visible={this.state.IncOrDecProductCountChanged}
                            onRequestClose={()=>this.setState({IncOrDecProductCountChanged:false})}>
                            <View style={{flex:1,justifyContent:'flex-end'}}>
                                <View style={{backgroundColor:'#f2f5f6',width:'80%',alignSelf:'center',padding:5,marginBottom:30,borderRadius:5}}>
                                    <Text style={{fontFamily:"Montserrat",textAlign:'center',fontSize:13,paddingBottom:5}}>You've changed '{this.state.IncOrDecProductName}' QUANTITY to '{this.props.selectedProduct.quantity}'</Text>
                                </View>   
                        </View>
                    </Modal>    */}
                   
                    <Snackbar
                        visible={this.state.IncOrDecProductCountChanged}
                        onDismiss={()=>this.setState({IncOrDecProductCountChanged:false})}
                        duration={3000}
                        action={{
                        label: 'close',
                        onPress: () => {
                            this.setState({IncOrDecProductCountChanged:false})
                        },
                        }}>
                        Item updated successfully!
                    </Snackbar>
        </View>
    );
   }
  }
}
function mapStateToProps(state){
    return{
        userId:state.userId,
        orderDetails:state.orderDetails,
        selectedProduct:state.selectedProduct,
        totalAmount:state.totalAmount,
        totalWeight:state.totalWeight,
        deliveryCharge:state.deliveryCharge,
        vehicleId:state.vehicleId,
        singleProductId:state.singleProductId,
        singleProductVariantId:state.singleProductVariantId,
        deliveryType:state.deliveryType,
        pickupPersonName:state.pickupPersonName,
        pickupPersonNumber:state.pickupPersonNumber,
        addressChanged:state.addressChanged,
        choosedPickupMethod:state.choosedPickupMethod,
        isFromCheckout:state.isFromCheckout,
        orderDeliverType:state.orderDeliverType,
        shippingAsBilling:state.shippingAsBilling,
        totalQty:state.totalQty,
        postCode:state.postCode,
        isShippingAddress:state.isShippingAddress
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setuserId:(value)=>dispatch({type:'setuserId',value}),
        setOrderDetails:(value)=>dispatch({type:"setOrderDetails",value}),
        setSelectProduct:(value)=>dispatch({type:"setSelectProduct",value}),
        setDeliveryCharge:(value)=>dispatch({type:"setDeliveryCharge",value}),
        setTotalAmount:(value)=>dispatch({type:"setTotalAmount",value}),
        setTotalWeight:(value)=>dispatch({type:"setTotalWeight",value}),
        setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
        setVehicleId:(value)=>dispatch({type:"setVehicleId",value}),
        setDateValue:(value)=>dispatch({type:"setDateValue",value}),
        setTimeSlot:(value)=>dispatch({type:"setTimeSlot",value}),
        setPickupPersonName:(value)=>dispatch({type:"setPickupPersonName",value}),
        setPickupPersonNumber:(value)=>dispatch({type:"setPickupPersonNumber",value}),
        setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
        setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
        setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
        setisFromCheckout:(value)=>dispatch({type:'setisFromCheckout',value}),
        setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
        setisShippingAddress:(value)=>dispatch({type:"setisShippingAddress",value}),
        setguestToUserChanged:(value)=>dispatch({type:"setguestToUserChanged",value}),
        setorderDeliverType:(value)=>dispatch({type:'setorderDeliverType',value}),
        setshippingAsBilling:(value)=>dispatch({type:'setshippingAsBilling',value}),
        setShippingAddressId:(value)=>dispatch({type:"setShippingAddressId",value}),
        setBillingAddressId:(value)=>dispatch({type:"setBillingAddressId",value}),
        setTotalQty:(value)=>dispatch({type:"setTotalQty",value}),
        setchangePostCodeStatus:(value)=>dispatch({type:"setchangePostCodeStatus",value}),
        setcomments:(value)=>dispatch({type:"setcomments",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(CompleteOrder);