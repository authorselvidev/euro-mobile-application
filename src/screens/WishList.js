import React, { Component } from 'react';
import { View, Text ,Dimensions,ScrollView,TouchableOpacity,ActivityIndicator,Image,StyleSheet,Modal} from 'react-native';
import images from '../images/index';
import NetInfo from '@react-native-community/netinfo';
import ContentLoader from 'react-native-easy-content-loader';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {ProductViewStyle,OverAllStyle,CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle'
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';

class WishList extends Component {

    state = {
        favouriteProducts:[],
        loading:false,
        modalVisible:false,
        selectedCategoryName:'',
        choosedCategoryIndex:1,
        selectedProductIndex:0,
        selectedProduct:{},
        selectedVariant:{},
        networkFailed:false,
        products:[],
        variants:[],
        deliveryStatus:true,
        addTocartLoading:false,
        weightChoosed:true,
        colorChoosed:true,
        sizeChoosed:true ,
        variantPrice:null,
        variantQty:null,
        variantWeight:null,
        cart:[],
        availableVariants:[],
        refreshing:false,
        deliveryStatus:true,

        signleProductLoading:false,
        modalResponse:[],
        productOptions:[],
        productOptionCount:null,
        dummyArray:[],
        availableCombinations:[],
        combinationStatus:null,
        variantId:null,
        imageUrl:null,
        updateproductmodal:false,
        modalProductWeight:null,
        quantity:0,

        prodPrice:null
    };

  componentDidMount(){
      this.getFavouriteProducts()
 }

 componentDidUpdate(){
   if(this.props.productCountChanged==true){
    this.getFavouriteProducts()
   }
 }

 async getFavouriteProducts(){  
        this.setState({loading:true})
        NetInfo.fetch().then(state => {
          //console.log("Connection type", state.type);
          if(state.isConnected==true){
                  fetch(baseUrl+this.props.userId+'/wishlist')
                  .then((response) => response.json())
                  .then((responseJson) => {
                      console.log(responseJson)
                      this.setState({favouriteProducts:responseJson.wishlist,loading:false})
                  }).catch((err)=>{
                    console.log(err)
                  })   
            }
          else{
            this.setState({networkFailed:true})
            alert("check network connection")
          }
        })    
        this.props.setProductCountChanged(false)        
  }

  modalDecrement(){
    if(this.state.quantity==1){
      this.setState({quantity:1,modalVisible:false,modalVisible:true})     
    }else{
        this.setState({quantity:this.state.quantity-1,modalVisible:false,modalVisible:true})     
      }
  }
  
  async modalIncrement(){
    this.setState({quantity:this.state.quantity+1,modalVisible:false,modalVisible:true})
  }

async selectedProduct(id,imageUrl){
  console.log(id,imageUrl)
  this.setState({modalVisible:true,signleProductLoading:true})
  await fetch(baseUrl+this.props.userId+'/product/'+id)
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          var dummyArray=new Array(responseJson.product_option_count)
          var variantOption=responseJson.product_options
          variantOption.map((item,key)=>{            
              item.option.map((item,index)=>{
                //if(responseJson.default_variant_id==item.variant_id){
                  if(responseJson.product_variants[0].id==item.variant_id){
                  item.selected=true
                  dummyArray.splice(0,0,item.option_value_id)
                }
              })
          })
          var filtered = dummyArray.filter(item=> {
            return item != null;
          });
          
          var reverced=filtered.reverse()
          
          if(responseJson.quantity==0){
            this.setState({quantity:1})
          }else{
            this.setState({quantity:responseJson.quantity})
          }
          
            this.setState({
                dummyArray:reverced,
                modalResponse:responseJson,
                productOptions:variantOption,
                productOptionCount:responseJson.product_option_count,
                availableCombinations:responseJson.product_variants,
                imageUrl:imageUrl,
                signleProductLoading:false
            })

            if(this.state.productOptions.length!=0){
              this.compareArrayValues()
            }else{
              this.setState({combinationStatus:true,variantId:this.state.availableCombinations[0].id,prodPrice:this.state.availableCombinations[0].price.toFixed(2)})
            }
        })  
        console.log(this.state.dummyArray)
}

changeProductVariant(key,indexPosition){
  console.log(key,indexPosition)
  var dummyArray=this.state.dummyArray
  var productOptions=this.state.productOptions
  productOptions.map((item,index)=>{
    if(index==key){
      item.option.map((item,index1)=>{
        item.selected=false
        if(index1==indexPosition){
          item.selected=true
          dummyArray[key]=item.option_value_id 
        }
      })
    }
  })
  this.setState({productOptions:productOptions,dummyArray:dummyArray})
  console.log(this.state.dummyArray)
  this.compareArrayValues()
}

// compareArrayValues(){
//   let array=this.state.availableCombinations

//   for(var i=0;i<array.length;i++){
//     if(this.state.productOptionCount==1){
//       if(this.state.dummyArray[0]==array[i].product_option_value_id1){
//         if(this.state.dummyArray[0]==array[i].product_option_value_id1){
//           // var modalresponse=this.state.modalResponse
//           // modalresponse.default_variant_price=array[i].price
//           // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
//           this.setState({prodPrice:array[i].price,variantId:array[i].id,combinationStatus:true})
//           //this.getModalProductDetails(this.state.variantId)
//           break;
//         }else{
//           this.setState({combinationStatus:false})
//           break
//         }
//       }
//     }else if(this.state.productOptionCount==2){
//       if((this.state.dummyArray[0]==array[i].product_option_value_id1) || (this.state.dummyArray[1]==array[i].product_option_value_id2)){
//         if((this.state.dummyArray[0]==array[i].product_option_value_id1) && (this.state.dummyArray[1]==array[i].product_option_value_id2)){
//           // var modalresponse=this.state.modalResponse
//           // modalresponse.default_variant_price=array[i].price
//           // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
//           this.setState({prodPrice:array[i].price,variantId:array[i].id,combinationStatus:true})
//           break;
//         }
//         else{
//           this.setState({combinationStatus:false})
//           break
//         } 
//       }
//     }else if(this.state.productOptionCount==3){
//       console.log("third")
//       if(this.state.dummyArray[0]==array[i].product_option_value_id1 || this.state.dummyArray[1]==array[i].product_option_value_id2 ||
//         this.state.dummyArray[2]==array[i].product_option_value_id3){
//           if(this.state.dummyArray[0]==array[i].product_option_value_id1 && this.state.dummyArray[1]==array[i].product_option_value_id2 &&
//             this.state.dummyArray[2]==array[i].product_option_value_id3){
//               // var modalresponse=this.state.modalResponse
//               // modalresponse.default_variant_price=array[i].price
//               // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
//               this.setState({prodPrice:array[i].price,variantId:array[i].id,combinationStatus:true})
//             break;
//           }else{
//             console.log("combination false")
//             this.setState({combinationStatus:false})
//             break
//           }
//         }
//     }
//   } 
// }

compareArrayValues(){
  //console.warn("enter comparision")
  let array=this.state.availableCombinations

  for(var i=0;i<array.length;i++){
    if(this.state.productOptionCount==1){
     // if(this.state.dummyArray[0]==array[i].product_option_value_id1){
        if(this.state.dummyArray[0]==array[i].product_option_value_id1){
          // var modalresponse=this.state.modalResponse
          // modalresponse.default_variant_price=array[i].price
          // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
          this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
          //this.getModalProductDetails(this.state.variantId)
          break;
        // }else{
        //   this.setState({combinationStatus:false})
        //   break
        // }
      }
    }else if(this.state.productOptionCount==2){
      // if((this.state.dummyArray[0]==array[i].product_option_value_id1) || (this.state.dummyArray[1]==array[i].product_option_value_id2)){
        if((this.state.dummyArray[0]==array[i].product_option_value_id1) && (this.state.dummyArray[1]==array[i].product_option_value_id2)){
          // var modalresponse=this.state.modalResponse
          // modalresponse.default_variant_price=array[i].price
          // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
          this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
          break;
        }
        // else{
        //   this.setState({combinationStatus:false})
        //   break
        // } 
      //}
    }else if(this.state.productOptionCount==3){
      // if(this.state.dummyArray[0]==array[i].product_option_value_id1 || this.state.dummyArray[1]==array[i].product_option_value_id2 ||
      //   this.state.dummyArray[2]==array[i].product_option_value_id3){
          if(this.state.dummyArray[0]==array[i].product_option_value_id1 && this.state.dummyArray[1]==array[i].product_option_value_id2 &&
            this.state.dummyArray[2]==array[i].product_option_value_id3){
              // var modalresponse=this.state.modalResponse
              // modalresponse.default_variant_price=array[i].price
              // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
              this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            break;
          // }else{
          //   this.setState({combinationStatus:false})
          //   break
          // }
        }
    }
  } 
}

async addToCartWithVariants(){
  console.log(this.props.userId,this.state.modalResponse.id,this.state.variantId,this.state.quantity)
  this.setState({addTocartLoading:true})
  await fetch(baseUrl+this.props.userId+'/addtocart', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "customer_id":this.props.userId,
      "product_id": this.state.modalResponse.id,
      "product_variant_id" :this.state.variantId,
      "quantity":this.state.quantity,
    })
  })
    .then((response) => response.json())
    .then((responseJson) => {
        console.log("response"+responseJson)
        if(responseJson.success==true){
        this.setState({addTocartLoading:false,modalVisible:false})
        this.props.setIncreaseCartCount()
        }                  
    })
    .catch((err)=>{
      //console.log(err)
  })  
  await this.props.setCategoryProductCountChanged(true)
  await this.props.setProductCountChanged(false)
  this.props.navigation.navigate("drawer",{screen:"myCart"})
}


navigateSingleProductScreen(id){
  this.props.setProductId(id)
  this.props.navigation.navigate("singleProductScreen")
}

 _onRefresh = () => {
   //console.log("onrefress")
   this.setState({refreshing: true});
   this.getTabDetail()
 }
 
 retry(){
   //console.log("retry")
   this.setState({networkFailed:false})
   this.getTabDetail()
 }

async addFavourite(value,index){

   console.log(this.props.userId,this.state.favouriteProducts[index].id,this.state.favouriteProducts[index].default_variant_id)
   await this.props.setProductCountChanged(true)
  await fetch(baseUrl+this.props.userId+'/wishlist', {
     method: 'POST',
     headers: {
     'Content-Type': 'application/json',
     },
     body: JSON.stringify({

      "customer_id": this.props.userId,
      "product_id":this.state.favouriteProducts[index].id,
      "is_wishlist":1
     })
   })
  .then((response) => response.json())
  .then((responseJson) => {
      console.log(responseJson)
 })
 
 }
            

  render() {
    var selectedVariant={};

    if(this.state.loading==true){
      return(
        <View>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
        </View>
      )
    }else if(this.state.favouriteProducts.length==0){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <Text>No products available in wishlist</Text>
        </View>
      )
    }
    else{
      return (
        <View style={{flex:1}}>
            <ScrollView>
                <View style={{flex:1,margin:10}}>
                      {
                        this.state.favouriteProducts.map((item,index)=>{
                            return(
                              <View key={index} style={ProductViewStyle.ProductMainContainer}>      

                                <View style={ProductViewStyle.ProductView}>

                                    <View style={ProductViewStyle.ProductImageView}>
                                        <Image source={{uri:item.image_url}} style={ProductViewStyle.ImageCoverStyle} />
                                        <View style={ProductViewStyle.ZindexView}>
                                            <View style={ProductViewStyle.HeartView}>
                                                <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("remove",index)}>
                                                    <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageContainStyle} />  
                                                </TouchableOpacity>
                                            {/* {
                                                item.is_favourite==true?
                                                <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("remove",index)}>
                                                    <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageContainStyle} />  
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("add",index)}>
                                                    <Image source={images.favouriteWithoutBackgroud} style={StyleContainer.ImageContainStyle} />
                                                </TouchableOpacity>
                                            } */}
                                            </View>
                                        </View>
                                    </View>

                                    <TouchableOpacity onPress={()=>this.navigateSingleProductScreen(item.id)}
                                      style={ProductViewStyle.ProductDetailView}>
                                            <Text numberOfLines={2}  ellipsizeMode='tail' style={{fontSize:14,marginBottom:3,fontFamily:'Montserrat-SemiBold'}}>{item.name}</Text>
                                            <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:13,marginBottom:3}}>$ {item.default_variant_price.toFixed(2)}</Text>
                                            {/* {
                                                this.props.deliveryType==0?
                                                <Text style={{fontFamily:"Montserrat",color:'#3bcf0a',fontSize:10}}>Delivery available</Text>:
                                                <Text numberOfLines={2} ellipsizeMode='tail' style={StyleContainer.RedIndicationTxtStyle}>Sorry we do not delivery to this area. Please arrange pick-up in store.</Text>
                                            } */}
                                    </TouchableOpacity>
                                </View>

                                  <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                        <TouchableOpacity style={{width:'100%',height:25,marginBottom:5}} 
                                        onPress={()=>{ this.selectedProduct(item.id,item.image_url) }} >
                                            <Image source={images.plusIcon} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}/>
                                        </TouchableOpacity>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:10,textAlign:'center',color:'#fa7153'}}>ADD</Text>
                                    </View> 

                            </View>
                            )
                        })
                    }
                </View>
            </ScrollView>
    
            {/* <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}>
                
                    <View style={ModelStyle.modelContainer}>
                        <TouchableOpacity onPress={()=>this.setState({modalVisible:false})} 
                         style={{marginBottom:10}}>
                            <Image source={images.cancel} style={{width:25,height:25,resizeMode:'cover'}}/>
                        </TouchableOpacity>
                        <View style={{width:'100%',backgroundColor:'#FFF',padding:20,borderRadius:20,marginBottom:-15}}>
                         {
                           this.state.signleProductLoading==true?
                           <View style={{width:'100%',height:'50%',paddingVertical:20}} >
                              <ContentLoader active avatar aShape="square" pRows={2}/>

                              <View style={{marginTop:20,borderWidth:0.5,borderStyle:'dashed',paddingTop:10,marginBottom:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              <View style={{borderWidth:0.5,borderStyle:'dashed',paddingTop:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>
                            </View>
                           :
                           <ScrollView showsVerticalScrollIndicator={false}>
                           
                            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>ADD TO CART</Text>
                               {
                                 this.state.updateproductmodal==true?
                                 <ActivityIndicator/>
                                 :
                                 null
                               }
                            </View> 
                            
                             <View style={ModelStyle.modelProductContainer}>
                                <Image source={{uri:this.state.imageUrl}} style={{width:60,height:60,borderRadius:7}}></Image>
                                <View style={{paddingLeft:20,width:'80%'}}>
                                    <Text numberOfLines={2}  ellipsizeMode="tail" style={[ModelStyle.modelProductName,{fontFamily:"Montserrat-SemiBold"}]}>{this.state.modalResponse.name}</Text>
                                    <Text style={ModelStyle.modelProductPrice}>$ {this.state.prodPrice}</Text>
                                </View>
                            </View>

                           
                            <View>
                            {
                              this.state.productOptions.map((item,key)=>{
                                return(
                                    <View style={{borderWidth:0.5,paddingBottom:5,borderColor:'gray',borderStyle:'dashed',borderRadius:5,paddingHorizontal:5,marginVertical:5,backgroundColor:'#fff'}}>
                                      <Text style={{marginBottom:3,paddingLeft:5,paddingTop:5}}>{item.type}</Text>
                                      <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                                      {
                                          item.option.map((item,index)=>{
                                            
                                            if(item.selected==true){
                                              if(this.state.combinationStatus==true){
                                                return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:0.3,borderColor:'gray',borderRadius:20,width:'25%',alignItems:'center',backgroundColor:'#fa7153',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',color:'#fff',fontFamily:'MontSerrat-SemiBold'}}>{item.value}</Text>
                                                  </TouchableOpacity>     
                                                )
                                              }else{
                                                  return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:1,borderRadius:20,borderColor:'#fa7153',width:'25%',alignItems:'center',backgroundColor:'#f2f5f6',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                                  </TouchableOpacity>
                                                )
                                              }
                                            }else{
                                              return(
                                              <TouchableOpacity onPress={()=>this.changeProductVariant(key,index)} style={{padding:7,borderWidth:0.5,borderRadius:20,width:'25%',alignItems:'center',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                              </TouchableOpacity>
                                            )
                                            }
                                          })
                                        }
                                      </View>
                                    </View>
                                )
                              })
                            }
                            {
                              this.state.combinationStatus==true?
                              null
                              :
                              <View style={{flexDirection:'row',alignItems:'center',paddingBottom:5}}> 
                                  <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  <Text style={{color:'red',fontSize:12,fontFamily:'MontSerrat',paddingLeft:5}}>Selected combination not available</Text>
                              </View>
                            }
                          </View>
                          
                          <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,paddingVertical:15,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:"Montserrat",fontSize:15}}>Price :  </Text>
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                </View>
                                <View style={{justifyContent:'center',alignItems:'flex-end'}}>
                                    <View style={CartScreenStyle.IncDecMainContainer}>
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>{this.modalDecrement()}}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                      </TouchableOpacity>
                                      
                                      <View style={CartScreenStyle.IncDecQtyContainer}>
                                      <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                      </View>
                                      
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>this.modalIncrement()}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                      </TouchableOpacity>
                                  </View>
                                </View>
                            
                          </View>
                            {
                              this.state.combinationStatus==true?
                              <View>
                                {
                                  this.state.addTocartLoading?
                                  <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <ActivityIndicator color="#fff"  />
                                  </TouchableOpacity>
                                  :
                                  <TouchableOpacity onPress={()=>this.addToCartWithVariants()} 
                                  style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                  </TouchableOpacity>
                                }
                              </View>
                              :
                              <View style={{backgroundColor:'#e3b1a6',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                              </View>
                            }
                            </ScrollView> 
                         }
                        </View>
                    </View>
            </Modal>     */}

            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={()=>this.setState({modalVisible:false})}>
                
                    <View style={ModelStyle.modelContainer}>
                        <TouchableOpacity onPress={()=>this.setState({modalVisible:false})} 
                         style={{marginBottom:10}}>
                            <Image source={images.cancel} style={{width:25,height:25,resizeMode:'cover'}}/>
                        </TouchableOpacity>
                        <View style={{width:'100%',backgroundColor:'#FFF',padding:20,borderRadius:20,marginBottom:-15}}>
                         {
                           this.state.signleProductLoading==true?
                           <View style={{width:'100%',height:'50%',paddingVertical:20}} >
                              <ContentLoader active avatar aShape="square" pRows={2}/>

                              <View style={{marginTop:20,borderWidth:0.5,borderStyle:'dashed',paddingTop:10,marginBottom:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              <View style={{borderWidth:0.5,borderStyle:'dashed',paddingTop:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              {/* <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                </TouchableOpacity> */}
                            </View>
                           :
                           <ScrollView showsVerticalScrollIndicator={false}>
                           
                            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>ADD TO CART</Text>
                               {
                                 this.state.updateproductmodal==true?
                                 <ActivityIndicator/>
                                 :
                                 null
                               }
                            </View> 
                            
                             <View style={ModelStyle.modelProductContainer}>
                                <Image source={{uri:this.state.imageUrl}} style={{width:60,height:60,borderRadius:7}}></Image>
                                <View style={{paddingLeft:20,width:'80%'}}>
                                    <Text numberOfLines={2}  ellipsizeMode="tail" style={[ModelStyle.modelProductName,{fontFamily:"Montserrat-SemiBold"}]}>{this.state.modalResponse.name}</Text>
                                    <Text style={ModelStyle.modelProductPrice}>$ {this.state.prodPrice}</Text>
                                </View>
                            </View>
                           {
                             this.state.productOptions.length==0?
                             null
                             :
                             <View>
                            {
                              this.state.productOptions.map((item,key)=>{
                                return(
                                    <View style={{borderWidth:0.5,paddingBottom:5,borderColor:'gray',borderStyle:'dashed',borderRadius:5,paddingHorizontal:5,marginVertical:5,backgroundColor:'#fff'}}>
                                      <Text style={{marginBottom:3,paddingLeft:5,paddingTop:5}}>{item.type}</Text>
                                      <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                                      {
                                          item.option.map((item,index)=>{
                                            
                                            if(item.selected==true){
                                              if(this.state.combinationStatus==true){
                                                return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:0.3,borderColor:'gray',borderRadius:20,width:'25%',alignItems:'center',backgroundColor:'#fa7153',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',color:'#fff',fontFamily:'MontSerrat-SemiBold'}}>{item.value}</Text>
                                                  </TouchableOpacity>     
                                                )
                                              }else{
                                                  return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:1,borderRadius:20,borderColor:'#fa7153',width:'25%',alignItems:'center',backgroundColor:'#f2f5f6',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                                  </TouchableOpacity>
                                                )
                                              }
                                            }else{
                                              return(
                                              <TouchableOpacity onPress={()=>this.changeProductVariant(key,index)} style={{padding:7,borderWidth:0.5,borderRadius:20,width:'25%',alignItems:'center',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                              </TouchableOpacity>
                                            )
                                            }
                                          })
                                        }
                                      </View>
                                    </View>
                                )
                              })
                            }
                            {
                              this.state.combinationStatus==true?
                              null
                              :
                              <View style={{flexDirection:'row',alignItems:'center',paddingBottom:5}}> 
                                  <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  <Text style={{color:'red',fontSize:12,fontFamily:'MontSerrat',paddingLeft:5}}>Selected combination not available</Text>
                              </View>
                            }
                          </View>
                           }
                            
                          
                          <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,paddingVertical:15,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:"Montserrat",fontSize:15}}>Price :  </Text>
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                </View>
                                <View style={{justifyContent:'center',alignItems:'flex-end'}}>
                                    <View style={CartScreenStyle.IncDecMainContainer}>
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>{this.modalDecrement()}}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                      </TouchableOpacity>
                                      
                                      <View style={CartScreenStyle.IncDecQtyContainer}>
                                      <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                      </View>
                                      
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>this.modalIncrement()}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                      </TouchableOpacity>
                                  </View>
                                </View>
                            
                          </View>
                            {
                              this.state.combinationStatus==true?
                              <View>
                                {
                                  this.state.addTocartLoading?
                                  <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <ActivityIndicator color="#fff"  />
                                  </TouchableOpacity>
                                  :
                                  <TouchableOpacity onPress={()=>this.addToCartWithVariants()} 
                                  style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                  </TouchableOpacity>
                                }
                              </View>
                              :
                              <View style={{backgroundColor:'#e3b1a6',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                              </View>
                            }
                            </ScrollView> 
                         }
                        </View>
                    </View>
            </Modal> 
              <CustomNotification/>              
        </View>
    );
   }
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
      categoryId:state.categoryId,
      productCountChanged:state.productCountChanged,
      categoryProductCountChange:state.categoryProductCountChange,
      cartCount:state.cartCount,
      deliveryType:state.deliveryType
  }
}

function mapDispatchToProps(dispatch){
  return{
      setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
      setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
      setCartCount:(value)=>dispatch({type:"setCartCount",value}),
      setProductId:(value)=>dispatch({type:"setProductId",value}),
      setIncreaseCartCount:()=>dispatch({type:"IncreaseCartCount"}),
      setDecreaseCartCount:()=>dispatch({type:"DecreaseCartCount"}),
      setCategoryId:(value)=>dispatch({type:"setCategoryId",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(WishList);

