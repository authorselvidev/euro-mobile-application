import React, { Component } from 'react';
import { View, Text } from 'react-native';

export class SliderModelResponse{
  response=[];
}

export class SigninModel  {
    email;
    password;
    deviceType;
    deviceToken;
    response;
}

export class SigninResponseModel{
   status=Boolean;
   userId;
   errorMessage;
}

export class GuestLoginModel{
  deviceType;
  deviceToken;
}

export class GuestLoginModelResponse{
  status=Boolean;
  id;
}

export class SignupModel{
   guestId=Number;
   firstName=String;
   lastName=String;
   email=String;
   mobileNo=Number;
   businessName=String;
   newPassword=String;
   confirmPassword=String;
   deviceType=String;
   deviceToken=Number;       
}

export class SignupResponseModel{
  status=Boolean;
  userId;
  errorMessage;
}

export class ForgetEmail {
    email=String;
}

export class ForgetEmailResponse{
  status=Boolean;
  email=String;
  id=Number;
  errorMessage;
}

export class SetNewPassword{
 responseCode; 
 newPassword;
 confirmPassword;
}

export class SetNewPasswordResponse{
  status=Boolean;
  errorMessage;
}

export class PostCodeModel{
  postCode;
}

export class PostCodeModelResponse{
  status=Boolean;
  deliveryType;
}

export class PaypalDetails{
  id;
  state;
  type="paypal"
}