import React, { Component } from 'react';
import { View, Text,ScrollView,Image,StyleSheet, ActivityIndicator, TouchableOpacity} from 'react-native';
import images from '../images/index';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,NotificationStyle} from './Styles/PrimaryStyle';
import CustomNotification from './CustomNotification';
import NetInfo from '@react-native-community/netinfo'
import { baseUrl } from '../Controller/baseUrl';
import {connect} from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { BarIndicator } from 'react-native-indicators';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };
  
class Notification extends Component {  
    state = {
        loading:false,
        networkFailed:false,
        notification:[],
        page:1,
        nextPageAvailable:true,
        nextPageLoading:false
    }
  
    componentDidMount(){
        console.log(this.props.userId)
        if(this.props.userId==null){
            this.getUserid()
        }else{
            this.getNotification()
        }

    }

    async getUserid(){
        console.log("enter get user")
        // await this.setState({loading:true})
          await AsyncStorage.getItem('userId').then((value) => {
              console.log(value)
            if(value!=null){
              this.setUserID(value)
              //consle.log("user")
            //consle.log("customer"+ this.props.userId)
            }
          })
         await AsyncStorage.getItem('guestId').then((value) => {
            if(value!=null){
              this.setUserID(value)
              //consle.log("guest")
            //consle.log("guest"+ this.props.userId)
            }
          })
          
        }
      
       async setUserID(value){
         await this.props.setuserId(JSON.parse(value))
         this.getNotification()
        }

    componentWillUnmount(){
        this.props.setNotificationCount(0)
        this.props.setinitialRoute("homeScreen")
    }

    getNotification(){
        NetInfo.fetch().then(state => {
            //console.log("Connection type", state.type);
            this.setState({loading:true})
            if(state.isConnected==true){
                console.log(baseUrl+this.props.userId+"/notification")
                fetch(baseUrl+this.props.userId+"/notification")
                .then((response)=>response.json())
                .then((responseJson)=>{
                    console.log(JSON.stringify(responseJson))
                     let notifications=responseJson.data.notification
                    // notifications.map((item,index)=>{
                    //     item.read_status=1
                    // })
                  this.setState({
                      notification:notifications,
                      loading:false,
                      networkFailed:false,
                      page:1,     
                      nextPageAvailable:true
                    })    
                })
                .catch((err)=>{
              }) 
            }else{
              this.setState({networkFailed:true,loading:false})
            }
          }) 
    }

   async onClick(item){
       
       if(item.order_id!=null){
       await this.props.setorderId(item.order_id)
       if(item.read_status==0){
        NetInfo.fetch().then(state => {
            //console.log("Connection type", state.type);
            if(state.isConnected==true){
                fetch(baseUrl+this.props.userId+"/read-notify/"+item.id)
                .then((response)=>response.json())
                .then((responseJson)=>{
                    console.log(JSON.stringify(responseJson))
                    let notifications=this.state.notification
                    notifications.map((item1,index)=>{
                        if(item1.id==item.id){
                            item1.read_status=1
                        }
                    })
                    this.setState({notification:notifications})
                })
            }
        })     
      }
        this.props.navigation.navigate("myOrderDetails")
        // this.props.navigation.navigate("drawer",{screen:"myOrder"})  
    }
    }

    async getNextPageDetails(){
        if(this.state.nextPageAvailable==true){
        await this.setState({page:this.state.page+1,nextPageLoading:true})
        await fetch(baseUrl+this.props.userId+'/notification?page='+this.state.page)
        .then((response)=>response.json())
        .then((responseJson)=>{
          this.setState({
            notification:this.state.notification.concat(responseJson.data.notification),
            nextPageAvailable:responseJson.is_load,
            nextPageLoading:false
          })
        })
      }
      }


  render() {
      if(this.state.loading==true){
          return(
            <View style={{flex:1}}>
                <View style={{flexDirection:'row',paddingVertical:15,justifyContent:'space-between',alignItems:'center',backgroundColor:'#fa7150',paddingHorizontal:10}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("homeScreen")} style={{width:25,height:25}}>
                   <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>

                <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Notification</Text>
                <Text/>
            </View>
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <BarIndicator color={'#fa7153'}/>
              </View>
            </View>
          )
      }else if(this.state.notification.length==0){
        return(
            <View style={{flex:1}}>
                <View style={{flexDirection:'row',paddingVertical:15,justifyContent:'space-between',alignItems:'center',backgroundColor:'#fa7150',paddingHorizontal:10}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("homeScreen")} style={{width:'10%',height:20}}>
                   <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>

                <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Notification</Text>
                <Text/>
            </View>
            
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <Image source={require('../images/notificationWithBg.png')} style={{width:80,height:80,transform: [{ rotate: '20deg' }],}}/>
                <Text style={{paddingVertical:20,fontFamily:'Montserrat'}}>Temporarily , no order</Text>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"home"})} style={{width:'70%',paddingVertical:10,backgroundColor:'#fa7153',alignItems:'center',justifyContent:'center',borderRadius:5}}>
                    <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>Add an order</Text>
                </TouchableOpacity>
            </View>
            </View>
        )
      }else{
    return (
      
        <View style={NotificationStyle.MainContainer}>
            <View style={{flexDirection:'row',paddingVertical:15,justifyContent:'space-between',alignItems:'center',backgroundColor:'#fa7150',paddingHorizontal:10}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("homeScreen")} style={{width:'10%',height:20}}>
                   <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>

                <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Notification</Text>
                <Text/>
            </View>
            <ScrollView style={OverAllStyle.MainContainerPad0}
             onScroll={({nativeEvent}) => {
              if (isCloseToBottom(nativeEvent)) {
                this.getNextPageDetails()
             }
            }}
            scrollEventThrottle={0}>
                <View>
                {
                    this.state.notification.map((item,index)=>{
                        return(
                            <View style={{backgroundColor:'#fff'}}>
                                {
                                    item.read_status==1?
                                        <TouchableOpacity onPress={()=>this.onClick(item)} key={index} style={[NotificationStyle.ItemMainView,{backgroundColor:'#fff'}]}>
                                            <View style={NotificationStyle.ImgMainView}>
                                                <View style={NotificationStyle.ImgInnerView}>
                                                    <Image source={images.grayBell} style={StyleContainer.ImageContainStyle}/>
                                                </View>
                                            </View>

                                            <View style={NotificationStyle.TxtView}>
                                                {/* <Text numberOfLines={3} ellipsizeMode="tail" style={{lineHeight:16,fontSize:11,color:'#9b9b9b'}}>{item.title}</Text> */}
                                                <Text numberOfLines={3} ellipsizeMode="tail" style={{lineHeight:18,fontSize:11,fontFamily:'Montserrat',color:'gray'}}>{item.body}</Text>
                                                <Text numberOfLines={3} ellipsizeMode="tail" style={{lineHeight:16,fontSize:11,fontFamily:'Montserrat',color:'gray'}}>{item.message}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity key={index} onPress={()=>this.onClick(item)} style={[NotificationStyle.ItemMainView,{backgroundColor:'#f0f7fc'}]}>
                                            <View style={NotificationStyle.ImgMainView}>
                                                <View style={NotificationStyle.ImgInnerView}>
                                                    <Image source={require('../images/notificationWithBg.png')} style={StyleContainer.ImageContainStyle}/>
                                                </View>
                                            </View>

                                            <View style={NotificationStyle.TxtView}>
                                                {/* <Text numberOfLines={3} ellipsizeMode="tail" style={{lineHeight:16,fontSize:11,color:'#9b9b9b'}}>{item.title}</Text> */}
                                                <Text numberOfLines={3} ellipsizeMode="tail" style={{lineHeight:18,fontFamily:'Montserrat',fontSize:11}}>{item.body}</Text>
                                                <Text numberOfLines={3} ellipsizeMode="tail" style={{lineHeight:16,fontSize:11,fontFamily:'Montserrat'}}>{item.message}</Text>
                                            </View>
                                        </TouchableOpacity>
                                }
                            </View>
                        )
                    })
                }
                </View>

                {
                  this.state.nextPageLoading?
                  <View style={{alignItems:'center',justifyContent:'center',paddingVertical:5}}>
                    <ActivityIndicator/>
                  </View>
                  :
                  null
                }

            </ScrollView>
        </View>
    );
   }
  }
}
function mapStateToProps(state){
    return{
        userId:state.userId,
        // guestLogin:state.guestLogin,
        // cartCount:state.cartCount
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
    //   setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
    //   setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
    setorderId:(value)=>dispatch({type:"setorderId",value}),
    setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
    setNotificationCount:(value)=>dispatch({type:"setNotificationCount",value}),
    setuserId:(value)=>dispatch({type:'setuserId',value}),
    setinitialRoute:(value)=>dispatch({type:"setinitialRoute",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(Notification);


