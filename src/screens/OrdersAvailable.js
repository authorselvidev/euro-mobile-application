import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity,StyleSheet } from 'react-native'
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,OrderAvailableStyle} from './Styles/PrimaryStyle';
import { PostCodeModelResponse } from '../DataModel/ModelClass';
import { connect } from 'react-redux';
import CustomNotification from './CustomNotification';

class OrdersAvailable extends Component {

   async componentDidMount(){
    //   console.log(PostCodeModelResponse.deliveryType)
       if(PostCodeModelResponse.deliveryType==1){
          await this.setState({isAvailable:false})
       }else{
          await this.setState({isAvailable:true})
       }
    }

    state={
        isAvailable:true
    }

   async navigateHome(){
      await this.props.setProductCountChanged(true)
      this.props.setinitialRoute("homeScreen")
      this.props.navigation.navigate("homeScreen")  
    }

    render() {
        return (
            <View style={StyleContainer.MainContainer}>
                
                {
                    this.state.isAvailable==true?
                    
                    <View style={OrderAvailableStyle.MainView}>
                    <View style={OverAllStyle.LogoView}>
                        <Image source={images.available} style={StyleContainer.ImageContainStyle}/>
                    </View>
                    <View style={OrderAvailableStyle.TxtView}>
                        <Text style={OrderAvailableStyle.MainQuoteGreen}>Thank you.</Text>
                        <Text style={OrderAvailableStyle.MainQuoteGreen}>Delivery Available</Text>
                    </View>
                        

                        <TouchableOpacity onPress={()=>this.navigateHome()} style={StyleContainer.ButtonOrange}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18}}>PLACE ORDER</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    <View style={OrderAvailableStyle.MainView}>
                    <View style={OverAllStyle.LogoView}>
                        <Image source={images.notAvailable} style={StyleContainer.ImageContainStyle}/>
                    </View>
                    <View style={OrderAvailableStyle.TxtView}>
                      <Text style={OrderAvailableStyle.MainQuoteRed}>SORRY!</Text>
                      <Text style={OrderAvailableStyle.SubQuoteRed}>We do not service this area. Please visit us at 90-114 Princes Highway for pick-up</Text>
                    </View>
                        
                    <TouchableOpacity onPress={()=>this.navigateHome()} style={StyleContainer.ButtonOrange}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18}}>CONTINUE SHOPPING</Text>
                        </TouchableOpacity>
                    </View>
                }
                
                <CustomNotification/>
            </View>
        )
    }
}

function mapStateToProps(state){
    return{
        userId:state.userId,
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
      setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
      setinitialRoute:(value)=>dispatch({type:"setinitialRoute",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(OrdersAvailable);
  
  