
import React, { Component } from 'react'
import { Text, View ,Image,TouchableOpacity} from 'react-native'
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,} from '../screens/Styles/PrimaryStyle';
import {Colors} from '../screens/Styles/colors';
import CustomNotification from './CustomNotification';

export default class NewpasswordUpdated extends Component {
    static navigationOptions={
        title: 'Password Updated',
        headerStyle: { backgroundColor:'#fa7153'},
        headerTitleStyle: { color: '#FFF',fontSize:17,},
        headerTintColor: 'white',
    }
   
    render() {
        return (
            <View style={StyleContainer.MainContainer}>

                    <View style={OverAllStyle.LogoView}>
                        <Image source={images.available} style={StyleContainer.ImageContainStyle} />
                    </View>
                   
                    <View style={{width:'100%',marginVertical:15,justifyContent:'center',alignItems:'center'}}>

                        <View style={{width:'80%'}}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:22,color:Colors.grnIndicatorClr,letterSpacing:1,marginBottom:10,textAlign:'center'}}>Password Updated Successfully</Text>
                        </View>

                    
            
            <View style={{flexDirection:'row',padding:10,alignItems:'center'}}>
            <Text style={{fontFamily:"Montserrat",fontSize:14,color:'#242424'}}>Now,</Text>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('signin')}>
                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:16}}> Sign In </Text>
                </TouchableOpacity>
                <Text style={{fontFamily:"Montserrat",fontSize:14,color:'#242424'}}> to enjoy the service</Text>
            </View>
        </View>
                    <CustomNotification/>
            </View>
            
      
      

      
        )
    }
}