import { StyleSheet,Dimensions} from 'react-native';
import {Colors} from './colors';

const Width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const OverAllStyle = StyleSheet.create({

    MainContainer:{
        flex:1,
        backgroundColor:Colors.whiteBg,
        padding:30,
      },
      MainContainerPad20:{
        flex:1,
        backgroundColor:Colors.grayBg,
        padding:20,
      },
      MainContainerPad0:{
        flex:1,
        backgroundColor:Colors.grayBg,
      },
      PasswordChangedMainContainer:{
        flex:1,
        backgroundColor:Colors.grayBg,
        padding:20,
        alignItems:'center',
        justifyContent:'center'
      },
    TextInputView:{
        borderRadius:3,
        //color:Colors.txtInputClr,
        borderWidth:1,
        borderColor:Colors.txtInputClr,
        backgroundColor:Colors.whiteBg,
        width:'100%',
        height:45,
        paddingHorizontal:10,
        fontSize:14,
        fontFamily:'Montserrat'
        },
    TextInputWhiteMainView:{
        width:'100%',
        marginBottom:10,
        backgroundColor:Colors.whiteBg,
    },
    SearchBarStyle:{
        width:'90%',
        alignSelf:'center',
        height:40,
        marginTop:-20,
        backgroundColor:Colors.whiteBg,
        borderRadius:4,
        borderColor:Colors.searchScrBdrClr,
        borderWidth:1,
        flexDirection:'row',
        justifyContent:'center'
      },
      LogoView:{
        width:'50%',
        height:70
        },
        HeaderMainView:{
            width:'100%',
            justifyContent:'center',
            alignItems:'center',
            backgroundColor:Colors.themeColor
          },
        HeaderStyle:{
            width:'90%',
            alignSelf:'center',
            paddingVertical:15,
            flexDirection:'row',
            justifyContent:'space-between'
          },
        HeaderIcon:{
            width:25,
            height:25
        },
        IncDecTxtStyle:{
            color:'#fff',
            fontFamily:'Montserrat-SemiBold'
        },
        QtyTxtStyle:{
            color:'#fa7153',
            fontSize:12,
            fontFamily:'Montserrat'
        },
        BtnOrangeTxtStyle:{
            color:'#FFF',
            fontSize:17,
            fontFamily:'Montserrat-SemiBold'
        }
})

export const ModelStyle=StyleSheet.create({
    modelContainer:{
        //marginTop:'20%',
        paddingTop:'20%',
        flex:1,
        // width:'100%',
        // height:'80%',
        justifyContent:'flex-end',
        alignItems:'center',
        backgroundColor:'rgba(52,52,52,0.5)',
      },
      modelProductContainer:{
        flexDirection:'row',
        paddingTop:20,
        paddingBottom:10
      },
      modelProductName:{
        fontSize:16,
        fontFamily:'Montserrat-SemiBold'
      },
      modelProductPrice:{
        color:'#fa7153',
        paddingTop:7,
        fontFamily:'Montserrat-SemiBold'
      },
      modalSelectedType:{
          paddingVertical:5,
          paddingHorizontal:15,
          backgroundColor:"#fa7153",
          marginLeft:15,
          borderRadius:7
        },
        modalLoadType:{
          paddingVertical:5,
          paddingHorizontal:15,
          backgroundColor:Colors.grayBg,
          borderRadius:5,
          marginLeft:15
        }
})



export const ProductViewStyle=StyleSheet.create({
    ProductMainContainer:{
        paddingVertical:10,
        borderBottomColor:Colors.categoryBdrClr,
        borderBottomWidth:1,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    ProductView:{
        width:'80%',
        borderRightColor:Colors.categoryBdrClr,
        borderRightWidth:1,
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    ProductImageView:{
        width:70,
        height:70,
        borderRadius:7
    },
    ProductDetailView:{
        width:'70%',
        paddingHorizontal:10
    },
    ImageCoverStyle:{
        width:undefined,
        height:undefined,
        flex:1,
        resizeMode:'contain',
        borderRadius:7
    },
    ZindexView:{
        width:'100%',
        height:'100%',
        position:'absolute',
        zIndex:1,
        justifyContent:'flex-end',
        alignItems:'flex-end'
    },
    HeartView:{
        width:'20%',
        height:'20%',
        margin:5
    },
    AddButtonView:{
        width:'20%',
        justifyContent:'center'
    },
    ButtonImageView:{
        width:'100%',
        height:25,
        marginBottom:5
    }
})



export const MyProfileStyle=StyleSheet.create({
    MainContainer:{
        flex:1,
        backgroundColor:Colors.grayBg,
        padding:30,
      },
    TextShowView:{
        padding:10,
        borderRadius:3,
        borderWidth:1,
        borderColor:Colors.txtInputClr,
        backgroundColor:Colors.whiteBg,
        width:'100%',
        height:45,
    }
})


export const ManageSiteAddressStyle=StyleSheet.create({
    ButtonOrange:{
        width:'100%',
        paddingVertical:12,
        backgroundColor:Colors.themeColor,
        borderRadius:3,
        alignItems:'center',
        marginBottom:15,
        marginBottom:10,
        justifyContent:'center',
        alignItems:'center'
      },
      ButtonSubView:{
        flexDirection:'row',
        width:'80%',
        justifyContent:'center'
      },
      AddImageMainView:{
        width:30,
        height:30,
        justifyContent:'center',
        marginRight:5
      },
      AddImageInnerView:{
        width:'100%',
        height:'60%'
      },
      NewSiteAddressTextView:{
        justifyContent:'center',
        alignItems:'center'
      },
})






export const ShopByCategoryStyle=StyleSheet.create({
    HeaderStyle:{
        backgroundColor:Colors.themeColor,
        paddingVertical:10,
        paddingHorizontal:10,
        flexDirection:'row',    
      },
    HeaderImgMainView:{
        width:'10%',
        height:25,
        justifyContent:'center'
    },
    HeaderImgInnerView:{
        width:'90%',
        height:'90%'
    },
    HeaderTitleView:{
        width:'80%',
        justifyContent:'center'
    },
    SearchBarImgMainView:{
        width:'10%',
        height:'100%',
        justifyContent:'center'
    },
    SearchBarImgInnerView:{
        width:'100%',
        height:'40%'
    },
    SearchTxtInputView:{
        width:'85%',
        height:'100%',
        justifyContent:'center'
    },
    CategoryheadingView:{
        padding:20
    },
    CategoryItemMainView:{
        backgroundColor:Colors.whiteBg
    },
    ItemViewStyle:{
        backgroundColor:Colors.whiteBg,
        paddingHorizontal:20,
        paddingVertical:10,
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1,
      flexDirection:'row'
      },
      ImgArrow:{
        width:'10%',
        height:15
      },
      ItemTxtView:{
          width:'90%'
      }

    
})



export const PasswordChangedStyle=StyleSheet.create({
    ImgViewStyle:{
        width:'85%',
        height:'40%',
        },
    IntractTxtView2:{
        width:'70%',
        alignSelf:'center'
    }
})


export const NotificationStyle=StyleSheet.create({
    MainContainer:{
        flex:1,
        backgroundColor:'#fff'
    },
    ItemMainView:{
        padding:15,
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    ImgMainView:{
        width:40,
        height:40,
        //backgroundColor:,
        borderRadius:100,
        justifyContent:'center',
        alignItems:'center'
    },
    ImgInnerView:{
        width:'50%',
        height:'50%'
    },
    TxtView:{
        width:'85%'
    }
})




export const HelpStyles=StyleSheet.create({
    MainContainer:{
        flex:1,
        backgroundColor:Colors.grayBg,
        padding:20
    },
    ItemMainView:{
      backgroundColor:Colors.whiteBg,
      padding:10,
      borderRadius:3,
      marginBottom:10
    },
    ItemHeadView:{
        flexDirection:'row'
    },
    ItemHeadTxtView:{
        width:'90%',
        justifyContent:'center',
        alignItems:'flex-start'
    },
    ItemHeadImgView:{
        width:'10%',
        height:15,
        justifyContent:'center'
    },
    ItemHeadImgInnerView:{
        width:'100%',
        height:'80%'
    },
    ItemHiddenView:{
        marginTop:10,
        marginHorizontal:15
    }
  
  })



export const IntroScreenStyles=StyleSheet.create({
    MainContainer:{
        flex:1,
        backgroundColor:Colors.whiteBg,
        padding:20,
    },
    SubView1:{
        alignItems:'center',
    },
    SkipButtonStyle:{
        paddingVertical:3,
        borderRadius:15,
        paddingHorizontal:25,
        backgroundColor:'rgba(150,150,150,0.9)',
        position:'absolute',
        zIndex:1,
        top:20,
        right:20
    },
    ImgMainView:{
        width:'100%',
        height:'70%',
        justifyContent:'flex-end',
    },
    ImgInnerView:{
      width:'100%',
      height:'90%',
    },
    DotView:{
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row'
    },
    OrangeDot:{
        width:10,
        height:10,
        borderRadius:100,
        backgroundColor:Colors.themeColor,
        marginRight:5
    },
    GrayDot:{
        width:10,
        height:10,
        borderRadius:100,
        backgroundColor:Colors.grayBg,
        marginRight:5
    },
    BtnOrange:{
            paddingVertical:8,
            borderRadius:3,
            width:'70%',
            backgroundColor:Colors.themeColor,
            alignSelf:'center',
            alignItems:'center',
    }

})



export const SignInStyles=StyleSheet.create({
    MainContainer:{
        width:'100%',
        height:height/1.5,
        borderWidth:1,
    },

    ImgView:{
        width:'50%',
        height:'15%'
    },
    MainContainer:{
        flex:1,
        backgroundColor:Colors.whiteBg,
        padding:20,

    },
    SubView:{
        width:'100%',
        height:'100%',
    },
    InnerSubView:{
        justifyContent: 'center',
        alignItems:'center',
        borderWidth:1
    },
    

})




export const PostCodeStyle=StyleSheet.create({
MainQuoteView:{
    width:'70%',
    marginVertical:15
}

})

export const OrderAvailableStyle=StyleSheet.create({
    MainView:{
        width:'100%',
        alignItems:'center'
    },
    TxtView:{
        width:'80%',
        marginVertical:30
    },
    MainQuoteGreen:{
        fontSize:23,
        fontFamily:'Montserrat-SemiBold',
        color:Colors.grnIndicatorClr,
        marginBottom:5,
        textAlign:'center',
        letterSpacing:0.5
    
      },
      MainQuoteRed:{
        fontSize:19,
        fontFamily:'Montserrat-SemiBold',
        color:Colors.redIndicationClr,
        marginBottom:5,
        textAlign:'center',
        letterSpacing:0.5
    
      },
      SubQuoteRed:{
        fontSize:16,
        fontFamily:'Montserrat-SemiBold',
        color:Colors.redIndicationClr,
        marginBottom:5,
        textAlign:'center',
        //letterSpacing:0.5
    
      },
      
})


export const HomeScreenStyle = StyleSheet.create({
  SubContainer:{
      flex:1
  },
  headerStyle:{
    height:height/4,
},
headerContentStyle:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'space-between',
    paddingHorizontal:15,
    paddingVertical:10,
    
},
MenuIcon:{
    width:20,
    height:20,
    marginRight:10,
    
},
LocationIcon:{
    width:20,
    height:20,
    marginRight:5,
},
LogoView:{
    width:'100%',
    height:50,
},
LogoMainView:{
    marginVertical:20
},
CategoryMainView:{
    backgroundColor:Colors.whiteBg,
    marginHorizontal:15,
    marginTop:-20,
    borderRadius:10,

},
ViewAllButtonStyle:{
    width:'100%',
    backgroundColor:Colors.themeColor,
    padding:5,
    borderBottomEndRadius:10,
    borderBottomLeftRadius:10
},
CategorySubView:{
    flexDirection:'row',
    flexWrap:'wrap',
    justifyContent:'space-between'
},
CatecoryContainer:{
    width:'50%',
    height:height/5.2,
    alignItems:'center',
    justifyContent:'center'
},
CategoryBorderRight:{
    borderRightColor:Colors.searchScrBdrClr,
    borderRightWidth:1
},
CategoryBorderBottom:{
    borderBottomColor:Colors.searchScrBdrClr,
    borderBottomWidth:1
},
CategoryImgView:{
    width:'50%',
    height:'45%'
},
ContainerPading:{
    paddingHorizontal:20,
    paddingVertical:10
},
PopularTxtView:{
    paddingVertical:10,
    borderBottomColor:Colors.categoryBdrClr,
    borderBottomWidth:1
}

})




export const SearchScreenStyle=StyleSheet.create({
    MainContainerPad0:{
        flex:1,
        backgroundColor:Colors.whiteBg,
      },
      ItemViewStyle:{
        width:'90%',
        flexDirection:'row',
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1
      },
      ItemTextStyle:{
        // color:Colors.singlePrdctScrnDescriptionColor,
        fontSize:12,
        fontFamily:'Montserrat'
      },
      EmptyView:{
        width:'100%',
        backgroundColor:Colors.themeColor,
        height:30
      },
      SearchBarImgMainView:{
        width:'10%',
        height:'100%',
        justifyContent:'center'
      },
      SearchBarImgInnerView:{
        width:'100%',
        height:'40%'
      },
      SearchBarTextView:{
        width:'85%',
        height:'100%',
        justifyContent:'center'
      },
      ItemMainView:{
        width:'100%',
        alignItems:'center',
        marginBottom:10
      },
      ItemSearchImgMainView:{
        width:'15%',
        height:40,
        justifyContent:'center',
        alignItems:'center'
      },
      ItemSearchImgInnerView:{
        width:'40%',
        height:'40%'
      },
      ItemTxtView:{
        width:'75%',
        justifyContent:'center'
      },
      ItemArrowImgMainView:{
        width:'10%',
        height:40,
        justifyContent:'center',
        alignItems:'center'
      },
      ItemArrowImgInnerView:{
        width:'50%',
        height:'50%'
      }   
    
})

export const MyOrders=StyleSheet.create({
    InProgressBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:Colors.inPrgrsClr,
        justifyContent:'center'
    },

    completedWithBlackBg:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:'black',
        justifyContent:'center'
    },

    CanceledBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:'#eb5850',
        justifyContent:'center'
    },
    completed:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:'green',
        justifyContent:'center'
    },
    InsideProgress:{
        width:'100%',
        justifyContent:'space-between',
        flexDirection:'row'
    },
    CompleteOrderBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:Colors.prgrsCmpltClr,
    },
    ReturnOrderBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:Colors.prgrsCmpltClr,
    },
    OrderDetailView:{
        backgroundColor:Colors.whiteBg,
        paddingVertical:10
    },
    ItemDetailView:{
        marginBottom:20,paddingHorizontal:20,
        // paddingBottom:10,
        // borderBottomColor:Colors.searchScrBdrClr,
        // borderBottomWidth:1
    },
    ItemNameView:{
        marginBottom:10,
        flexDirection:'row',
        paddingHorizontal:20,
        justifyContent:'space-between'
    }

})



export const MyOrderDetailStyle=StyleSheet.create({
    InsideProgress:{
        width:'100%',
        justifyContent:'space-between',
        flexDirection:'row',
        paddingVertical:2
    },
    InProgressBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:'#f3c800',
        justifyContent:'center'
    },
    CompleteOrderBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:'#00c268'
    },
    ReturnOrderBanner:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:Colors.prgrsCmpltClr
    },
    BillDetailView:{
        paddingHorizontal:20,
        paddingVertical:10,
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1,
        backgroundColor:Colors.whiteBg
    },
    InsideBillView:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginVertical:5
    },
    ProductDisplayView:{
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        marginBottom:10
    },
    DeliveryAddressMainView:{
        paddingHorizontal:20,
        paddingVertical:10,
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1,
        backgroundColor:Colors.whiteBg
    },
    DelverAddressSubView:{
        flexDirection:'row'
    },
    locationIconView:{
        width:15,
        height:15
    },
    JobSiteDetailView:{
        marginLeft:5,
        bottom:4,
    },
    ProductDisplayMainView:{
        paddingHorizontal:20,paddingVertical:10,borderBottomColor:Colors.searchScrBdrClr,borderBottomWidth:1,backgroundColor:Colors.whiteBg
    },
    ProductView:{
        width:'75%',
        flexDirection:'row',
        justifyContent:'flex-start'
    },
    ProductImageView:{
        width:'25%', 
        height:50,
    },
    ImgContainerStyle:{
        width:undefined,
        height:undefined,
        flex:1,
        resizeMode:'contain',
        borderRadius:3
    },
    ImgCoverStyle:{
        width:undefined,
        height:undefined,
        flex:1,
        resizeMode:'cover',
        borderRadius:3
    },
    ProductDetailView:{
        width:'70%',
        justifyContent:'center',
        paddingLeft:10       
    },
    PriceView:{
        width:'15%',
        justifyContent:'center',
        alignItems:'flex-end'
    }

})




export const CartScreenStyle = StyleSheet.create({

    WhiteContainer:{
        backgroundColor:Colors.whiteBg,
        marginBottom:10
    },
    ProductMainContainer:{
        width:'100%',
        paddingHorizontal:5,
        paddingVertical:10,
        flexDirection:'row',
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1
    },
    WhiteSubContainer:{
        paddingHorizontal:20,
        paddingVertical:10
    },
    ProductContainer:{
        width:'50%',
        flexDirection:'row',
        justifyContent:'flex-start',
        marginRight:10
    },
    DeleteImgContainer:{
        width:'10%',
        justifyContent:'center',
        alignItems:'center'
    },
    DeleteImgSubContainer:{
        width:'100%',
        height:20,
    },
    ProductImgContainer:{
        width:'30%',
        height:50,
        marginHorizontal:5
    },
    ProductNameContainer:{
        width:'60%',
        justifyContent:'center'
    },
    ProductQtyPriceContainer:{
        flexDirection:'row',
        width:'50%',
        alignItems:'center',
      },
    IncDecMainContainer:{
        borderWidth:1,
        borderColor:Colors.themeColor,
        flexDirection:'row',
        borderRadius:3
    },
    IncDecQtyContainer:{
        width:'40%',
        alignItems:'center',
        justifyContent:'center'
    },
    OrangeIncBtn:{
        paddingVertical:2,
        paddingHorizontal:4,
        backgroundColor:Colors.themeColor
    },
    SpacebtwnContainer:{
        justifyContent:'space-between',
        flexDirection:'row',
        marginVertical:5
    },
    OfferIconMainView:{
        width:'10%',
        justifyContent:'center'
    },
    OfferIconSubView:{
        width:30,
        height:30
    },
    TxtInputMainView:{
        width:'55%',
        height:35,
        justifyContent:'center'
    },
    TxtInputView:{
        fontSize:12,
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1,
        fontFamily:'Montserrat'
    },
    ApplyBtnMainView:{
        width:'35%',
        justifyContent:'center',
        alignItems:'flex-end'
    },
    ApplyBtnSubView:{
        width:'80%',
        backgroundColor:'#fd7153',
        padding:5,
        borderRadius:3
    },
    OrderTypeMainView:{
        flexDirection:'row',
        paddingVertical:10
    },
    OrderTypeSubView:{
        flexDirection:'row',
        marginRight:10
    },
    WhiteSubBtmBdrContainer:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        borderBottomColor:'#E2e2e2',
        borderBottomWidth:1      
    },
    nextButtonContainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:Colors.themeColor,
        paddingHorizontal:20,
        paddingVertical:10
      },
      prodctTxtStyle:{
        fontSize:12,
        fontFamily:'Montserrat-SemiBold'
      },
      prodctPriceTxtStyle:{
        fontSize:10,
        fontFamily:'Montserrat'
      },
      productTypeTxtStyle:{
        fontSize:10,
        color:'gray',
        fontFamily:'Montserrat'
      },
      blackBoldHeadTxtStyle:{
        fontSize:14,
        fontFamily:'Montserrat-SemiBold'
      },
      applyTxtStyle:{
        textAlign:'center',
        color:'#FFF',
        fontFamily:'Montserrat'
      },
      amtTxtStyle:{
        fontSize:13,
        fontFamily:'Montserrat-SemiBold'
      },
      offerTxtStyle:{
        fontSize:13,
        color:Colors.grnIndicatorClr,
        fontFamily:'Montserrat'
      },
      DeliveryTxtStyle:{
        color:Colors.redIndicationClr,
        fontFamily:'Montserrat-SemiBold'
      },
      PayTxtStyle:{
          fontSize:20,
          fontFamily:'Montserrat-SemiBold'
      }


})





export const CompleteOrderStyle=StyleSheet.create({
    
    LoadTypeMainView:{
        flexDirection:'row',
        justifyContent:'flex-start',
        marginVertical:20
    },
    SelectedLoadTypeView:{
        paddingVertical:5,
        paddingHorizontal:15,
        backgroundColor:Colors.themeColor,
        borderRadius:4,
        marginRight:5
    },
    NonSelectLoadTypeView:{
        paddingVertical:5,
        paddingHorizontal:15,
        backgroundColor:Colors.grayBg,
        borderRadius:4,
        marginRight:5
    },
    VechileMainView:{
        flexDirection:'row',
        alignSelf:'flex-start',
        //marginBottom:10
    },
    SelectedVechileView:{
        backgroundColor:Colors.themeColor,
        width:60,
        height:60,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        //marginRight:15,
        
    },
    NonSelectedVechileView:{
        width:60,
        height:60,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        marginRight:15,
        backgroundColor:Colors.grayBg
    },
    ProductMainContainer:{
        width:'100%',
        marginBottom:10,
        flexDirection:'row',
        borderBottomColor:Colors.searchScrBdrClr,
        borderBottomWidth:1,
        paddingHorizontal:20,
        paddingVertical:10
    },
    
    ProductNameContainer:{
        width:'70%',
        justifyContent:'center',
    },
    DateTimeMainView:{
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        //padding:10
    },
    DateView:{
        width:'55%',
        justifyContent:'space-between',
        borderWidth:0.3,
        borderRadius:3,
        borderColor:Colors.txtInputClr,
        flexDirection:'row',
        padding:10
    },
    TimeView:{
        width:'40%',
        justifyContent:'space-between',
        borderWidth:0.3,
        borderRadius:3,
        borderColor:Colors.txtInputClr,
        flexDirection:'row',
        padding:10
    },
    DateIcon:{
        width:20,
        height:20,
        resizeMode:'contain'
    },
    TxtAreaView:{
        height:100,
        backgroundColor:Colors.grayBg,
        marginVertical:10,
        borderRadius:3,
        padding:10
    }

    
    
})


export const OrderPlacedScreenStyle=StyleSheet.create({
    ImgMainView:{
        width:'40%',
        height:'16%',
        marginVertical:15
    },
    SubQuoteView:{
        width:'90%',
        marginBottom:15
    }
})

export const SingleProductScreenStyle=StyleSheet.create({
    SubMainView:{
        padding:20
    },  
    MainProductWhiteView:{
        width:'100%',
        height:height/2.5,
        backgroundColor:Colors.whiteBg,
        //paddingVertical:10
    },
    MainProductView:{
        width:'100%',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        //padding:10,
    },
    MainProductInnerView:{
        // width:'100%',
        // height:'100%',
        flex:1,justifyContent:'center',
        alignItems:'center'
    },
    DetailShowView:{
            width:'100%',
            height:'20%',
            paddingBottom:10,
            justifyContent:'center',
            alignItems:'center'
    },
    MainProductDetailContainer:{
        width:'100%',
        alignItems:'flex-end'
    },
    MainProductDetailSubView:{
        // width:'100%',
        // height:'100%',
        alignItems:'flex-end',
        justifyContent:'flex-end',
        position:'absolute',
        zIndex:1,
        bottom:15,
        right:15
    },
    MainProductDetailView:{
        width:'70%',justifyContent:'center'
    },
    HeartView:{
        width:30,
        height:30,
        justifyContent:'center'
    },
    ProductScrollMainView:{
        marginVertical:15,
        flexDirection:'row'
    },
    ProductListView:{
        width:70,
        height:60,
        backgroundColor:Colors.whiteBg,
        marginRight:10,
        justifyContent:'center',
        alignItems:'center'
    },
    ProductListBorderView:{
        width:70,
        height:60,
        backgroundColor:Colors.whiteBg,
        marginRight:10,
        justifyContent:'center',
        alignItems:'center',
        borderColor:Colors.themeColor,
        borderWidth:1
    },
    ProductListInnerView:{
        width:'80%',
        height:'80%',
    },
    AdditionalDetailView:{
        width:'90%',marginBottom:5
    },
    SplitInnerView:{
        backgroundColor:Colors.whiteBg,
        width:'100%',
        flexDirection:'row',
    },
    QtyTypeStyle:{
        width:'50%',
        borderRightWidth:1,
        borderRightColor:Colors.searchScrBdrClr,
        padding:10,
        justifyContent:'center',
        alignItems:'center'
    },
    QtyTypeInnerView:{
        width:'80%'
    },
    WhiteContainer:{
        backgroundColor:Colors.whiteBg,
        marginBottom:10,
        paddingVertical:10
    },
    LocationDeliveryView:{
        width:'100%',
        paddingHorizontal:20,
        paddingVertical:10,
        backgroundColor:Colors.whiteBg,
    },
    View50:{
        width:'50%'
    },
    DeliveryView:{
        width:'60%',
        justifyContent:'center'
    },
    ChangeBtnMainView:{
        width:'35%',justifyContent:'center',marginHorizontal:10
    },
    ChangeBtnStyle:{
        backgroundColor:Colors.themeColor,justifyContent:'center',padding:3
    },
    DescriptionView:{
        backgroundColor:Colors.whiteBg,
        width:'100%',
        padding:20
    },
    AddView:{
        flex:1,padding:15,justifyContent: 'center',alignItems:'center',borderWidth:0.5,borderColor:'#fa7150'
    },
    BuyNowView:{
        flex:1,padding:15,justifyContent:'center',alignItems:'center',backgroundColor:Colors.themeColor
    }
    
})