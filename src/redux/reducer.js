
const initialState={
  initialRoute:null,
  userId:null,
  productId:null,
  deliveryType:0,
  orderDeliverType:0,
  networkFailed:false,
  categoryId:1,
  categoryUrl:null,
  categoryName:null,
  cartCount:0,
  notificationCount:0,
  postCode:null,
  orderDetails:[],
  totalAmount:0,
  totalWeight:null,
  deliveryCharge:0,
  selectedProduct:{},
  productCountChanged:false,
  categoryProductCountChange:false,
  orderCountChanged:false,
  singleProductId:null,
  vehicleId:null,
  orderId:null,
  timeSlot:null,
  dateValue:null,
  paypalPaymentId:null,
  paypalPaymentState:null,
  guestLogin:false,
  tradeAccount:false,
  pickupPersonName:null,
  pickupPersonNumber:null,
  addressChanged:true,
  choosedPickupMethod:false,
  addAddressRefershing:false,
  addressId:null,
  orderReturn:false,
  isFromCheckout:false,
  homeScreenUpdate:false,
  singleProductPageUpdate:false,
  singleProductVariantId:null,
  isShippingAddress:false,
  guestToUserChanged:false,
  shippingAsBilling:true,
  billingAddressId:null,
  shippingAddressId:null,
  totalQty:null,
  relatedProducts:[],
  orderPlacedTime:null,
  initialRouteInHome:"homeScreen",
  gestFromPayment:false,
  paymentScreenUpdate:false,
  changePostCodeStatus:false,
  isFromOrderScreen:false,
  comments:null
}
  const reducer=(state=initialState,action)=>{

    if(action.type=="DecreaseCartCount"){
      return{
        ...state,
        cartCount:state.cartCount-1
      }
    }else if(action.type=="IncreaseCartCount"){
      return{
        ...state,
        cartCount:state.cartCount+1
      }
    }

    switch(action.type){
      case 'setinitialRoute':
        return{
          ...state,
          initialRoute:action.value
        }

      case 'setuserId':
        return{
          ...state,
          userId:action.value
        }
      case 'setGuestLogin':
        return{
          ...state,
          guestLogin:action.value
        } 

        case 'setProductId':
        return{
          ...state,
          productId:action.value
        }

        case 'setDeliveryType':
        return{
          ...state,
          deliveryType:action.value
        }

        case 'setorderDeliverType':
        return{
          ...state,
          orderDeliverType:action.value
        }

        case 'setChoosedPickupMethod':
        return{
          ...state,
          choosedPickupMethod:action.value
        }
      
        case 'setaddAddressRefershing':
        return{
          ...state,
          addAddressRefershing:action.value
        }

        case 'setPostCode':
        return{
          ...state,
          postCode:action.value
        }

        case 'setNotificationCount':
        return{
          ...state,
          notificationCount:action.value
        }

        
        case 'setSingleProductId':
        return{
          ...state,
          singleProductId:action.value
        }
       
        case 'setSingleProductVariantId':
          return{
            ...state,
            singleProductVariantId:action.value
          }

        case 'setNetworkFailed':
        return{
          ...state,
          networkFailed:action.value
        }

        case 'setCategoryId':
        return{
          ...state,
          categoryId:action.value
        }

        case 'setCategoryUrl':
        return{
          ...state,
          categoryUrl:action.value
        }

        case 'setCategoryName':
        return{
          ...state,
          categoryName:action.value
        }

        case 'setCartCount':
        return{
          ...state,
          cartCount:action.value
        }

        case 'setProductCountChanged':
        return{
          ...state,
          productCountChanged:action.value
        }

        case 'setCategoryProductCountChange':
        return{
          ...state,
          categoryProductCountChange:action.value
        }
      
        case 'setOrderCountChanged':
        return{
          ...state,
          orderCountChanged:action.value
        }

        case 'setRelatedProducts':
        return{
          ...state,
          relatedProducts:action.value
        }

        case 'setOrderDetails':
        return{
          ...state,
          orderDetails:action.value
        }

        case 'setSelectProduct':
        return{
          ...state,
          selectedProduct:action.value
        }

        case 'setDeliveryCharge':
        return{
          ...state,
          deliveryCharge:action.value
        }

        case 'setTotalAmount':
        return{
          ...state,
          totalAmount:action.value
        }
        
        case 'setTotalWeight':
        return{
          ...state,
          totalWeight:action.value
        }

        case 'setorderId':
        return{
          ...state,
          orderId:action.value
        }

        case 'setVehicleId':
        return{
          ...state,
          vehicleId:action.value
        }

        case 'setTimeSlot':
        return{
          ...state,
          timeSlot:action.value
        }

        case 'setDateValue':
          return{
            ...state,
            dateValue:action.value
          }

          case 'setPaypalPaymentId':
            return{
              ...state,
              paypalPaymentId:action.value
            }

          case 'setPaypalPaymentState':
            return{
              ...state,
              paypalPaymentState:action.value
            }

         case 'setTradeAccount':
           return{
             ...state,
             tradeAccount:action.value
           }

          case 'setPickupPersonName':
          return{
            ...state,
            pickupPersonName:action.value
          }

          case 'setPickupPersonNumber':
            return{
              ...state,
              pickupPersonNumber:action.value
            }

          case 'setAddressChanged':
            return{
              ...state,
              addressChanged:action.value
            }
          
          case 'setAddressId':
            return{
              ...state,
              addressId:action.value
            }

         case 'setOrderReturn':
          return{
            ...state,
            orderReturn:action.value
          }

          case 'setisFromCheckout':
            return{
              ...state,
              isFromCheckout:action.value
            }

            case 'setisShippingAddress':
            return{
              ...state,
              isShippingAddress:action.value
            }

            case 'setHomeScreenUpdate':
              return{
                ...state,
                homeScreenUpdate:action.value
              }
              
            case 'setSingleProductPageUpdate':
              return{
                ...state,
                singleProductPageUpdate:action.value
              }
          
              case 'setguestToUserChanged':
              return{
                ...state,
                guestToUserChanged:action.value
              }

              case 'setshippingAsBilling':
              return{
                ...state,
                shippingAsBilling:action.value
              }
              
              case 'setBillingAddressId':
              return{
                ...state,
                billingAddressId:action.value
              }

              case 'setShippingAddressId':
              return{
                ...state,
                shippingAddressId:action.value
              }

              case 'setTotalQty':
                return{
                  ...state,
                  totalQty:action.value
                }
              
              case 'setOrderPlacedTime':
                return{
                  ...state,
                  orderPlacedTime:action.value
                }
             
             case 'setinitialRouteInHome':
               return{
                 ...state,
                 initialRouteInHome:action.value
               }

             case 'setgestFromPayment':
              return{
                ...state,
                gestFromPayment:action.value
              }
              
              case 'setpaymentScreenUpdate':
              return{
                ...state,
                paymentScreenUpdate:action.value
              }

              case 'setchangePostCodeStatus':
              return{
                ...state,
                changePostCodeStatus:action.value
              }

              case 'setisFromOrderScreen':
              return{
                ...state,
                isFromOrderScreen:action.value
              }
              case 'setcomments':
              return{
                ...state,
                comments:action.value
              }
              
    }
    return state
  }
 
  export default reducer;
  