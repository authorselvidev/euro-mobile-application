import React, { Component } from 'react';
import { View, Text,Image,ScrollView,TouchableOpacity,TextInput,StyleSheet,ToastAndroid, ActivityIndicator } from 'react-native';
import {OverAllStyle,MyProfileStyle} from './Styles/PrimaryStyle';
import NetInfo from '@react-native-community/netinfo';
import ContentLoader from 'react-native-easy-content-loader';
import { StyleContainer } from './Styles/CommonStyles';
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';

class MyProfile extends Component {
  
    state = {
        isEdit:false,
        networkFailed:false,
        firstName:"",
        lastName:'',
        email:'',
        contact:'',
        businessName:'',
        firstnameError:'',
        emailError:'',
        mobileNumError:'',
        businessNameError:'',
        loading:false,
        saveLoading:false
    };
    
    changeIsEdit(value){
        this.setState({isEdit:value})
    }

  componentDidMount(){
      this.getProfileDetail()
  }

  async getProfileDetail(){
  
   this.setState({loading:true})
        NetInfo.fetch().then(state => {
          //console.log("Connection type", state.type);
          if(state.isConnected==true){ 
                  fetch(baseUrl+this.props.userId+'/profile')
                  .then((response) => response.json())
                   .then((responseJson) => {
                        console.log(responseJson)
                        this.setState({
                            firstName:responseJson.first_name,
                            lastName:responseJson.last_name,
                            email:responseJson.email,
                            contact:responseJson.contact,
                            businessName:responseJson.business_name,
                            loading:false
                        })
                  }).catch((err)=>{
                    //console.log(err)
                  })
          }
          else{
            this.setState({networkFailed:true})
            alert("Check network connection")
          }
        })            
    }
    
    changeFirstname(value) {
        this.setState({firstName:value})
      }
    
    changeLastName(value){
       this.setState({lastName:value})
    }
    
    changeEmailAddress(value){
    this.setState({email:value})
    }
    
    changeBusinessName(value){
      this.setState({businessName:value})
    }
    
    changeMobileNumber(value){
      this.setState({contact:value})
    }
    
    checkIsEmpty(value){
        if(value!==''){
          return true
        }else{
          return false
        }
      }
      
      async validateForm(){     
        let numPattern=/^0[0-8]\d{8}$/g;
        let namePattern=/^[a-zA-Z0-8]+$/;
        let emailPattern= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        let firstName,lastName,email,mobileNum,businessName=true;
      
                // if(this.checkIsEmpty(this.state.firstName)){
                //     if(namePattern.test(this.state.firstName)){
                //       this.setState({firstnameError:''})
                //       firstName=true
                //     }
                //     else{
                //       this.setState({firstnameError:'Only lettes are allowed'})
                //       firstName=false
                //     }
                // }
                // else
                // {
                //   this.setState({firstnameError:'First name should not be empty'})
                //   firstName=false
                // }

                if(this.state.firstName==""){
                  this.setState({firstnameError:"First name feild is required"})
                }else{
                  firstName=true
                  this.setState({firstnameError:""})
                }
      
                if(this.checkIsEmpty(this.state.lastName)){
                  this.setState({lastNameError:''})
                 }
                else{
                  this.setState({lastNameError:''})
                  lastName=true
                }
                
                if(this.checkIsEmpty(this.state.email))
                {
                  if(emailPattern.test(this.state.email))
                  {
                    this.setState({emailError:''})
                    email=true
                  }
                  else{
                    this.setState({emailError:'Check the Email format '})
                    email=false
                  }
                }
                else
                {
                  this.setState({emailError:'Email Address should not be empty'})
                  email=false
                }
      
                if(this.checkIsEmpty(this.state.contact)){
                  this.setState({mobileNumError:''})
                }
                else{
                  this.setState({mobileNumError:'Mobile should not be empty'})
                }
      
                if(this.checkIsEmpty(this.state.businessName)){
                  this.setState({businessNameError:''})
                }
                else{
                  this.setState({businessNameError:'Business name should not be empty'})
                }
      
                if(firstName==true&&email==true)
                {
                  
                  //console.log('ALL OK')
                  this.postProfileDetail()
      
                }
                else{
                  //console.log('check the whole form')
                  ToastAndroid.show('Check the form details' , ToastAndroid.SHORT);
                }
      
              }

        async postProfileDetail(){
        this.setState({saveLoading:true})
        await fetch(baseUrl+this.props.userId+"/profile", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "first_name":this.state.firstName,
                "last_name":this.state.lastName,
                "email":this.state.email,
                "contact":this.state.contact,
                "business_name":this.state.businessName
            })
    
            })
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({saveLoading:false})
              console.log(responseJson)
              if(responseJson.success==true){
                this.changeIsEdit(false)
              }else{
                this.changeIsEdit(true)
              }
            })    
    }

    render() {
    return (
        
            <View style={MyProfileStyle.MainContainer}>

            {
                this.state.loading==true?
                <View>
                    <ContentLoader primaryColor='#FFF' active pRows={5} pWidth={['100%','100%','100%','100%','100%']} pHeight={[45,45,45,45,45]}/>
                </View>
                :
                <View>
                {
                    this.state.isEdit==false?
                <View style={{width:'100%'}}>
                
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                                <View style={MyProfileStyle.TextShowView}>
                                    {
                                      this.state.firstName==""?
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >First name</Text>
                                      :
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.firstName}</Text>  
                                    }
                                </View>
                        </View>

                        <View style={OverAllStyle.TextInputWhiteMainView}>
                                <View style={MyProfileStyle.TextShowView}>
                                {
                                      this.state.lastName==null?
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >last name</Text>
                                      :
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.lastName}</Text>  
                                    }
                                </View>
                        </View>
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                                <View style={MyProfileStyle.TextShowView}>
                                    <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.email}</Text>
                                </View>
                        </View>
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                                <View style={MyProfileStyle.TextShowView}>
                                    {
                                      this.state.contact==null?
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >Contact</Text>
                                      :
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.contact}</Text>  
                                    }
                                </View>
                        </View>
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                                <View style={MyProfileStyle.TextShowView}>
                                   {
                                      this.state.businessName==null?
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >Business name</Text>
                                      :
                                      <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.businessName}</Text>  
                                    }
                                </View>
                        </View>
                        


                        <TouchableOpacity style={{width:'100%',marginVertical:50}} onPress={()=>{this.changeIsEdit(true)}}>
                                <View style={StyleContainer.ButtonBorder}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#000',fontSize:17}} >EDIT</Text>
                                </View>

                        </TouchableOpacity>
                </View>
                    :
                    <View style={{width:'100%'}}>
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                        <TextInput style={OverAllStyle.TextInputView} placeholder="First Name"
                            value={this.state.firstName}
                            ref={(input) => { this.firstNameRef = input; }}
                            onSubmitEditing={() => {
                            this.lastNameRef.focus();}}
                            onChangeText={(value)=>{this.changeFirstname(value)}} 
                            returnKeyType={"next"}
                            />
                                            
                                
                        </View>
                        {
                                this.state.firstnameError==''?null:
                                <View style={{width:'100%',marginBottom:5}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.firstnameError}</Text>
                                </View> 
                              }

                        <View style={OverAllStyle.TextInputWhiteMainView}>
                        <TextInput style={OverAllStyle.TextInputView} placeholder="Last Name"
                            value={this.state.lastName}
                            ref={(input) => { this.lastNameRef = input; }}
                            onSubmitEditing={() => {
                            this.emailRef.focus();}}
                            onChangeText={(value)=>{this.changeLastName(value)}} 
                                returnKeyType={"next"}
                        />
                        
                        </View>
                        {
                                this.state.lastNameError==''?null:
                                <View style={{width:'100%',marginBottom:5}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.lastNameError}</Text>
                                </View> 
                              }
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                        <TextInput style={OverAllStyle.TextInputView} placeholder="Email"
                              value={this.state.email}
                              ref={(input) => { this.emailRef = input; }}
                              onSubmitEditing={() => {
                              this.mobileNumRef.focus();}}
                              onChangeText={(value)=>{this.changeEmailAddress(value)}} 
                              returnKeyType={"next"}
                              editable={false}
                          />
                            
                        </View>
                        {
                                this.state.emailError==''?null:
                                <View style={{width:'100%',marginBottom:5}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.emailError}</Text>
                                </View> 
                              }
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                        <TextInput style={OverAllStyle.TextInputView} placeholder="Contact"
                            value={this.state.contact}
                            ref={(input) => { this.mobileNumRef = input; }}
                            onSubmitEditing={() => {
                            this.siteRef.focus();}}
                            onChangeText={(value)=>{this.changeMobileNumber(value)}} 
                            returnKeyType={"next"}
                            keyboardType='number-pad'
                            maxLength={10}
                        />
                        </View>
                        {
                                this.state.mobileNumError==''?null:
                                <View style={{width:'100%',marginBottom:5}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.mobileNumError}</Text>
                                </View> 
                              }

                        
                        <View style={OverAllStyle.TextInputWhiteMainView}>
                        <TextInput style={OverAllStyle.TextInputView} placeholder="Business Name"
                          value={this.state.businessName}
                          ref={(input) => { this.siteRef = input; }}
                          onSubmitEditing={() => {
                              this.validateForm()
                          }}
                          onChangeText={(value)=>{this.changeBusinessName(value)}} 
                              returnKeyType={"done"}
                            />
                        </View>
                        {
                                this.state.businessNameError==''?null:
                                <View style={{width:'100%',marginBottom:5}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.businessNameError}</Text>
                                </View> 
                              }
                        
                        {
                          this.state.saveLoading?
                          <TouchableOpacity style={{width:'100%',marginVertical:50}}>
                            <View style={StyleContainer.ButtonOrange}>
                                <ActivityIndicator color="#fff"/>
                            </View>
                          </TouchableOpacity>
                        :
                        <TouchableOpacity style={{width:'100%',marginVertical:50}} onPress={()=>{this.validateForm()}}>
                          <View style={StyleContainer.ButtonOrange}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:17}} >SAVE</Text>
                          </View>
                        </TouchableOpacity>
                        }

                </View>

                }
                </View>
            }
            
                <CustomNotification/>
            
            </View>  
    );
  }
}


function mapStateToProps(state){
  return{
      userId:state.userId,
  }
}

function mapDispatchToProps(dispatch){
  return{
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(MyProfile);


