
import React, {Component, useState,useEffect } from 'react';
import {Text,View,StyleSheet,TextInput,ActivityIndicator,SafeAreaView,KeyboardAvoidingView,Platform,TouchableOpacity,Image,Modal} from 'react-native';
import {OverAllStyle,ProductViewStyle,CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle';
import { WebView } from 'react-native-webview';
import axios from "axios";
import qs, { parse } from "qs";
import { decode, encode } from 'base-64'
import base64 from 'react-native-base64'
import images from '../images/index'
import {connect} from 'react-redux'
import { PaypalDetails } from '../DataModel/ModelClass';
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';
import stripe from 'tipsi-stripe'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import NetInfo from '@react-native-community/netinfo';
//import firebase from "react-native-firebase";
import { Colors } from './Styles/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator,
} from 'react-native-indicators';

stripe.setOptions({
  publishableKey:"pk_test_sqs8P7KlOfi3FX7nfkvrXb6O00ZBhKmctr"
})

class PaymentScreen extends Component{
  state={
    loading:false,
    paypalUrl:"",
    accessToken:"",
    isWebViewLoading:false,
    shouldShowWebViewLoading:true,
    response:null,
    getDetailLoading:false,
    responseData:[],
    tradeAccount:false,
    confirmationLoading:false,
    stripeloading: false,
    token: null,  
    modalVisible:false,
    billingAddress:null,

    //guest login
    isguest:false,
    postcodeAvailability:false,
    postcode:null,

    guestEmail:null,
    guestEmailError:null,
    guestLoginLoading:false,
    deliveryAvailable:false,
    
    fcmToken:null,
    permissionRejected:false,
    devicetype:null,

    //user login
    password:null,
    passwordError:null,
    userPasswordVisible:false,
    hidePassword:true
  }

 async componentDidMount(){
    this.callPaymentScreen()
  }

  componentDidUpdate(){
    if(this.props.paymentScreenUpdate==true){
      this.props.setpaymentScreenUpdate(false)
      this.callPaymentScreen()
    }
  }

 async callPaymentScreen(){
    this.getToken()
    this.deviceType()
    await AsyncStorage.getItem('guestId').then((value) => {
      if(value!=null){
          //this.props.setuserId(JSON.parse(value))
          this.setState({isguest:true})
         //  this.checkPermission() 
         //  this.getToken()

      }else{
          console.log("else")
          this.getMethod()
      }
   })  
  }

  async checkPermission() {
    // const enabled = await firebase.messaging().hasPermission();
    // // If Premission granted proceed towards token fetch
    // //console.log("check permission")
    // if (enabled) {
    //   this.getToken();
    // } else {
    //   // If permission hasn’t been granted to our app, request user in requestPermission method. 
    //   this.requestPermission();
    // }
    this.getToken()
  }

  async getToken() {
    const authStatus=await messaging().requestPermission();
    const enabled=
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

   if(enabled){
     console.log('Authorization status : ', authStatus);
     }
     
    if (!this.state.fcmToken) {
      // Register the device with FCM
      await messaging().registerDeviceForRemoteMessages();

      // Get the token
      const token = await messaging().getToken(); 
      await this.setState({fcmToken:token})
      console.log(this.state.fcmToken)
    }
  }

  async requestPermission() {
    // try {
    //   await firebase.messaging().requestPermission();
    //     this.getToken();
    // } catch (error) {
    //   // User has rejected permissions
    //   //alert("permission rejected")
    //   await this.setState({permissionRejected:true})
    //   //console.log(this.state.permissionRejected)
    // }
  }


  async deviceType(){
   if(Platform.OS==="ios"){
     await this.setState({devicetype:"ios"})
    }else{
     await this.setState({devicetype:"android"})
    }
    console.log(this.state.devicetype)
  }


  getMethod(){   
    console.log("order delivery type "+ this.props.orderDeliverType)
    if (!global.btoa) {
        global.btoa = encode;
      }
  
      if (!global.atob) {
        global.atob = decode;
      }
      this.setState({getDetailLoading:true})
      // fetch(baseUrl+this.props.userId+"/payment")
      console.log(baseUrl+this.props.userId+"/payment")
      fetch(baseUrl+this.props.userId+"/payment?shipping="+this.props.deliveryCharge)
      .then((response)=>response.json())
      .then((responseJson)=>{
        console.log(JSON.stringify(responseJson))
        this.setState({responseData:responseJson,billingAddress:responseJson.billing_address,getDetailLoading:false})
      })
  }
  

  /*---Paypal checkout section---*/
  buyBook() {
    console.log("press")
    this.setState({loading:true})
    //Check out https://developer.paypal.com/docs/integration/direct/payments/paypal-payments/# for more detail paypal checkout

    const url = `https://api.sandbox.paypal.com/v1/oauth2/token`;

    const data = {
      grant_type: 'client_credentials'

    };

    const auth = {
      username: "AVl0GkkUMCHdPYXHswzr-G0_Bbw9tngpWteYeuZn3cZ0MyOGLoHgnPA-V2IPxZNreSJDvfxZXAtirfHS",  //"your_paypal-app-client-ID",
      password: "EMU9kk0oN4KWVKr9nSKQPKRC7A9OU0m63WGvRorpi8y3ynAzJL23IVdNndWmW7ZgcVkzL2IcSv1fKi-7"   //"your-paypal-app-secret-ID

    };

    const options = {

      method: 'post',
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Credentials': true
      },

      //Make sure you use the qs.stringify for data
      data: qs.stringify(data),
      auth: auth,
      url,
    };

    // Authorise with seller app information (clientId and secret key)
    axios(options).then(response => {console.log("access tocken"+response.data.access_token)
      //setAccessToken(response.data.access_token)
      this.setState({accessToken:response.data.access_token})

      //Resquest payal payment (It will load login page payment detail on the way)
      // axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, dataDetail,
      axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, this.state.responseData.data,
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${response.data.access_token}`
          }
        }
      )
        .then(response => {
          const { id, links } = response.data
          const approvalUrl = links.find(data => data.rel == "approval_url").href

          console.log("response", links)
          console.log("approvalurl  ",approvalUrl)
          this.setState({paypalUrl:approvalUrl,loading:false})
          //setPaypalUrl(approvalUrl)
          //SetLoading(false)
        }).catch(err => {
          console.log({ ...err })
        })
    }).catch(err => {
      console.log(err)
    })
  };

  /*---End Paypal checkout section---*/

  onWebviewLoadStart = () => {
    console.log("onwebviewloading")
    if (this.state.shouldShowWebViewLoading) {
      this.setState({isWebViewLoading:true})
    }
  }

  _onNavigationStateChange = (webViewState) => {
    console.log("onnavigationstatechange", webViewState)

    //When the webViewState.title is empty this mean it's in process loading the first paypal page so there is no paypal's loading icon
    //We show our loading icon then. After that we don't want to show our icon we need to set setShouldShowWebviewLoading to limit it
    if (webViewState.title == "") {
      //When the webview get here Don't need our loading anymore because there is one from paypal
      // setShouldShowWebviewLoading(false)
      this.setState({shouldShowWebViewLoading:false})
    }

    if (webViewState.url.includes('https://example.com/')) {
       this.setState({paypalUrl:null})
      //setPaypalUrl(null)
      const urlArr = webViewState.url.split(/(=|&)/);

      const paymentId = urlArr[2];
      const payerId = urlArr[10];
      console.log("before payer id")

      this.setState({confirmationLoading:true})
      axios.post(`https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute`, { payer_id: payerId },
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${this.state.accessToken}`
          }
        }
      )
        .then(response => {
          console.log("after payer id")
          // setShouldShowWebviewLoading(true)
          this.setState({shouldShowWebViewLoading:true})
          console.log("payid  "+JSON.stringify(response))
          // SetLoading(false)
          // PaypalDetails.id=response.data.id
          // PaypalDetails.state=response.data.state
          this.navigateFinalScreen(response)
        }).catch(err => {
          console.log("cancel")
          this.setState({loading:false,confirmationLoading:false})
          alert("Transaction failed")
          // setShouldShowWebviewLoading(true)
          this.setState({shouldShowWebViewLoading:true})
          console.log({ ...err })
        })

    }
  }

 async navigateFinalScreen(response){
  this.setState({loading:true})
   await this.props.setPaypalPaymentId(response.data.id)
   await this.props.setPaypalPaymentState(response.data.state)
   await this.props.navigation.navigate("orderPlaced")
   this.setState({loading:false,confirmationLoading:false})
  }

 async payLater(){
   await this.props.setTradeAccountStatus(true)
   this.props.navigation.navigate("orderPlaced")
  }

  handleCardPayPress = async () => {
    // try {
      this.setState({ stripeloading: true, token: null })
      const token = await stripe.paymentRequestWithCardForm({
        // Only iOS support this options
        // smsAutofillDisabled: true,
        // requiredBillingAddressFields: 'full',
        // prefilledInformation: {

        //   billingAddress: {
        //     name: this.state.billingAddress.name,
        //     line1:this.state.billingAddress.line1,
        //     line2:this.state.billingAddress.line2,
        //     city:this.state.billingAddress.city,
        //     state:this.state.billingAddress.state,
        //     country:this.state.billingAddress.country,
        //     postalCode:this.state.billingAddress.postalCode,
        //     email:this.state.billingAddress.email,
        //   },
        // },
      })
      console.log(token)
      this.setState({ stripeloading: false, token:token,modalVisible:true })
      this.stripePay(token)
    // } catch (error) {
    //   console.log(error)
    //   this.setState({ stripeloading: false })
    // }
  }

 stripePay(token){
    console.warn(token.tokenId)
    var payAmount=(JSON.parse(this.props.totalAmount)+this.props.deliveryCharge).toFixed(2)
    console.warn(payAmount)
    fetch(baseUrl+this.props.userId+'/stripepayment', {
      method: 'POST',
      headers: {
      'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "stripe_token":token.tokenId,
        // "payment_amount": "1.00"
        "payment_amount":payAmount
      })
   })
  .then((response) => response.json())
  .then((responseJson) => {
      console.log(responseJson)    
      this.navigateFinalScreenUsingStripe(responseJson)
  }).catch((error)=>{
    console.log(error)
    this.setState({modalVisible:false})
    alert("Payment failed")
  })
  }

 async navigateFinalScreenUsingStripe(response){
    await this.props.setPaypalPaymentId(response.transaction_id)
    await this.props.setPaypalPaymentState(response.payment_status)
    await this.setState({modalVisible:false})
    await this.props.navigation.navigate("orderPlaced")
  }

  guestEmailSubmit(){
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    if(this.state.guestEmail==null){
        this.setState({guestEmailError:'The email field is required'}) 
    }else{
        this.setState({guestEmailError:null})
        if(reg.test(this.state.guestEmail) === true){
            this.setState({guestEmailError:null})
            this.guestSignUp()
        }else{
            this.setState({guestEmailError:'check the email format'})
        }
    } 
}

async guestSignUp(){
    await NetInfo.fetch().then(state => {
        //console.log("Connection type", state.type);
        if(state.isConnected==true){
          this.setState({guestLoginLoading:true})
          fetch(baseUrl+this.props.userId+"/guest-signup", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            }, 
            body: JSON.stringify({
                 "email":this.state.guestEmail
              })
            })
          .then((response) => response.json())
           .then((responseJson) => {
             console.log(responseJson)
             if(responseJson.success==false){
              if(responseJson.data!=""){
                this.setState({guestLoginLoading:false,userPasswordVisible:true})
               }
             }else if(responseJson.success==true){
               AsyncStorage.removeItem('guestId')
               AsyncStorage.setItem('userId',JSON.stringify(responseJson.data.id))
               this.props.setguestToUserChanged(true)
               this.setState({guestLoginLoading:false,isguest:false})
               this.getMethod()
               this.sendMail(responseJson.data)
             }
           }).catch((err)=>{
            console.log(err)
        })  
    }
})
}

async sendMail(value){
  console.log("enter send mail function")
  await fetch(baseUrl+"send-mail", {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "from":"new-guest",
      "user_id":value.id, 
      "frwd_pwd":value.frwd_pwd,
     })
   })
  .then((response) => response.json())
  .then((responseJson) => {
      console.log("guest to user mail "+JSON.stringify(responseJson))
  })
}

async signin(){
    console.log(this.state.guestEmail,this.state.password,this.state.devicetype,this.state.fcmToken)
    if(this.state.password==null){
      this.setState({passwordError:"Password feild is required"})
      console.log(this.state.passwordError)
    }else{
      this.setState({passwordError:null,guestLoginLoading:true})
      await fetch(baseUrl+'login', {
        method: 'POST',
        headers: {
          Accecpt:'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
         "email":this.state.guestEmail,
         "password":this.state.password,
         "device_type":this.state.devicetype,
         "device_token":this.state.fcmToken,
         "guest_id":this.props.userId
          })
        })
      .then((response) => response.json())
       .then((responseJson) => {
          console.log("signin "+ JSON.stringify(responseJson))
          if(responseJson.success==true){
            this.setState({passwordError:null})
            console.log(responseJson.data.user.id)
            AsyncStorage.removeItem('guestId')
            AsyncStorage.setItem('userId',JSON.stringify(responseJson.data.user.id))
            this.props.setuserId(responseJson.data.user.id)
            this.props.setguestToUserChanged(true)
            this.setState({guestLoginLoading:false,isguest:false})
            this.getMethod()
          }else if(responseJson.success==false){
            this.setState({passwordError:responseJson.errorMessage[0],guestLoginLoading:false})
          }
      })
    }
  }

cancelGuestLogin(){
  this.setState({isguest:false})
  this.props.navigation.navigate("completeOrder")
}

async navigateToForgetPassword(){
 await this.props.setgestFromPayment(true)
 this.setState({isguest:false})
 this.props.navigation.navigate("forgetPassword")
}

  render(){
    const totalAmountWithoutDeliveryCharge=(JSON.parse(this.props.totalAmount+0).toFixed(2))
    const totalAmount= (JSON.parse(this.props.totalAmount)+this.props.deliveryCharge).toFixed(2)
    if(this.state.getDetailLoading==true){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
          <ActivityIndicator color="#fa7153" size="large"/>
          <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
        </View>
      )
    }else{
    return(
      <View style={{flex:1,backgroundColor:'#fff'}}>
        <View style={{paddingHorizontal:20,paddingVertical:10,borderBottomWidth:5,borderBottomColor:'#f2f2f2'}}>
          <Text style={{fontFamily:"Montserrat",fontSize:16,paddingVertical:5}}>Pay</Text>
          {
            this.props.orderDeliverType==true?
              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20,color:'#fa7153',}}>$ {totalAmountWithoutDeliveryCharge}</Text>
              :
              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20,color:'#fa7153',}}>$ {totalAmount}</Text>
          }
          
        </View>

          {
            this.state.responseData.trade_account==true?
            <View>
            {
              this.state.tradeAccount==true?
              <TouchableOpacity onPress={()=>this.setState({tradeAccount:false})} style={{flexDirection:'row',padding:20,borderBottomWidth:5,borderBottomColor:'#f2f2f2'}}>            
                <View style={{width:20,height:20,borderWidth:0.5,borderRadius:10,justifyContent:'center',alignItems:'center'}}>
                  <View style={{width:10,height:10,backgroundColor:'#fa7153',borderRadius:10}}></View>
                </View>
                <View style={{marginLeft:15}}>
                  <Text style={{fontFamily:"Montserrat",fontSize:17}}>Trade account</Text>
                </View>
              </TouchableOpacity>
              :
              <View>
                <TouchableOpacity onPress={()=>this.setState({tradeAccount:true})} style={{flexDirection:'row',padding:20,borderBottomWidth:5,borderBottomColor:'#f2f2f2'}}>            
                  <View style={{width:20,height:20,borderWidth:0.5,borderRadius:10}}></View>
                  <View style={{marginLeft:15}}>
                    <Text style={{fontFamily:"Montserrat",fontSize:17}}>Trade account</Text>
                  </View>
                </TouchableOpacity>
          
                
              </View>
            }
            </View>
            :
            null
          }

        {
          this.state.tradeAccount==true?
          <TouchableOpacity onPress={()=>this.payLater()} style={{width:'90%',height:50,borderRadius:5,justifyContent:'center',alignSelf:'center',backgroundColor:'#fa7153',marginVertical:20}}>
            <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',color:'#fff',fontSize:17}}>ADD TO ACCOUNT</Text>
          </TouchableOpacity>
          :
          <View style={{paddingHorizontal:20,paddingVertical:10,borderBottomWidth:7,borderBottomColor:'#f2f2f2',paddingBottom:10}}>
            <Text style={{fontFamily:"Montserrat",fontSize:17}}>Express checkout</Text>
            {
              this.state.loading?
              <TouchableOpacity style={{marginVertical:20}}>
                <ActivityIndicator color="#fa7153"/>
              </TouchableOpacity>
              :
              <TouchableOpacity onPress={()=>this.buyBook()}  style={{marginVertical:10}}>
                <Image source={images.paypalBtn} style={{width:'100%',height:50,resizeMode:'contain'}}/>
              </TouchableOpacity>
            }

            <TouchableOpacity onPress={this.handleCardPayPress} style={{width:'100%',height:45,borderWidth:0.5,borderRadius:5,borderColor:'#5469d4',justifyContent:'center',alignItems:'center',backgroundColor:'#5469d4'}}>
              <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>Pay with stripe</Text>
            </TouchableOpacity>
          </View>
        }

      {
        this.state.paypalUrl ?
        <View style={styles.webview}>
          <WebView
            style={{ height: "100%", width: "100%" }}
            source={{ uri: this.state.paypalUrl }}
            onNavigationStateChange={this._onNavigationStateChange}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={false}
            onLoadStart={()=>this.onWebviewLoadStart()}
            onLoadEnd={() => this.setState({isWebViewLoading:false})}
          />
        </View>
       :
        null
        }

      {
        this.state.isWebViewLoading? 
        <View style={{ ...StyleSheet.absoluteFill, justifyContent: "center", alignItems: "center", backgroundColor: "#ffffff" }}>
          <DotIndicator color="#fa7153"/>
          <Text style={{fontFamily:'MontSerrat-SemiBold',paddingBottom:30,fontSize:16}}>Redirect to Paypal login page</Text>
        </View>
       :
        null
      }

      {
        this.state.confirmationLoading? 
        <View style={{ ...StyleSheet.absoluteFill, justifyContent: "center", alignItems: "center", backgroundColor: "#ffffff" }}>
          <DotIndicator color="#fa7153"/>
          <Text style={{fontFamily:'MontSerrat-SemiBold',paddingBottom:30,fontSize:16}}>Payment processing</Text>
        </View>
       :
        null
      }

      <CustomNotification/>

      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.loading}>
          <View style={ModelStyle.modelContainer}>
             <View style={{width:'100%',height:'20%',backgroundColor:'#FFF',borderRadius:20,marginBottom:-15,justifyContent:'center',alignItems:'center'}}>             
               <Text style={{textAlign:'left',marginTop:20,paddingLeft:10}}>Paypal payment loading</Text>
                <SkypeIndicator color="#fa7153"/>
              </View>
          </View>
        </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}>
            <View style={ModelStyle.modelContainer}>
                <View style={{width:'100%',height:'20%',backgroundColor:'#FFF',borderRadius:20,marginBottom:-15,justifyContent:'center',alignItems:'center'}}>
                   <Text style={{fontFamily:'Montserrat',marginTop:20}}>Payment processing</Text>
                    <DotIndicator color="#fa7153"/>
                    
                </View>
            </View>
      </Modal>

      <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.isguest}>
            <KeyboardAvoidingView keyboardVerticalOffset={20}  behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
          <SafeAreaView style={{flex:1,justifyContent:'flex-end',backgroundColor:'rgba(52,52,52,0.5)'}}>
              <View style={{width:'100%',backgroundColor:'#FFF',borderRadius:15}}>
                  <View style={{backgroundColor:'#fa7153',flexDirection:'row',justifyContent:'space-between',alignItems:'center',padding:15,borderTopLeftRadius:15,borderTopRightRadius:15}}>
                      <TouchableOpacity onPress={()=>this.cancelGuestLogin()}>
                      <Image source={images.cancel} style={{width:22,height:22,resizeMode:'contain'}}></Image>
                      </TouchableOpacity>
                      <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,color:'#fff'}}>Euro Building Supplies</Text>
                      <Text/>
                  </View>

                  {
                    this.state.userPasswordVisible==false?
                    <View style={{padding:20}}>
                      <Text style={{fontFamily:'Montserrat',paddingBottom:15}}>Enter your email address :</Text>
                      <View style={{width:'100%'}}>
                          <TextInput style={StyleContainer.TextInputView} placeholder="Email id"                        
                              ref={(input) => { this.suburbRef = input; }}
                              onSubmitEditing={() => {this.guestEmailSubmit()}}
                              onChangeText={(value)=>this.setState({guestEmail:value})} 
                              value={this.state.guestEmail}
                              keyboardType="default"
                              returnKeyType={"next"}/>

                                  {
                                      this.state.guestEmailError==null?null
                                      :
                                      <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.guestEmailError}</Text>
                                  }
                          </View>
                      </View>
                      :
                      <View style={{padding:20}}>
                        <Text style={{fontFamily:'Montserrat',paddingBottom:15}}>Enter your password :</Text>
                        <View style={{flexDirection:'row',width:'100%',marginVertical:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3}}>
                            <TextInput 
                            style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
                              fontSize:14,fontFamily:'Montserrat'}} 
                              
                              placeholder="Password"
                              ref={(input) => { this.passwordRef = input; }}                         
                              onChangeText={(value)=>{this.setState({password:base64.encode(value)})}} 
                              onSubmitEditing={() => {this.signin()}}
                              returnKeyType={"done"}        
                              secureTextEntry={this.state.hidePassword}
                              textContentType="oneTimeCode"        
                                />
                                <TouchableOpacity onPress={()=>this.setState({hidePassword:!this.state.hidePassword})}>
                                  {
                                    this.state.hidePassword==true?
                                    <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                    :
                                    <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  }
                                  
                                </TouchableOpacity>
                          </View>
                          {
                            this.state.passwordError==null?null:
                             <Text style={{fontFamily:'Montserrat',color:'red',fontSize:10,paddingTop:-3}}>{this.state.passwordError}</Text>
                          }
                          <TouchableOpacity style={{alignSelf:'flex-end'}} onPress={()=>this.navigateToForgetPassword()}>
                            <Text style={{fontFamily:"Montserrat",fontSize:12}}>Forgot password ?</Text>
                          </TouchableOpacity>
                        </View>
                  }

                  {
                      this.state.guestLoginLoading==true?
                      <TouchableOpacity style={{backgroundColor:'#fa7153',padding:15,marginBottom:20,width:'90%',alignSelf:'center',borderRadius:5,alignItems:'center',justifyContent:'center'}}>
                          <ActivityIndicator color="#fff"/>
                      </TouchableOpacity>
                      :
                      <View>
                       {
                         this.state.userPasswordVisible==true?
                         <TouchableOpacity onPress={()=>this.signin()} style={{backgroundColor:'#fa7153',padding:15,marginBottom:20,width:'90%',alignSelf:'center',borderRadius:5,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>NEXT</Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={()=>this.guestEmailSubmit()} style={{backgroundColor:'#fa7153',padding:15,marginBottom:20,width:'90%',alignSelf:'center',borderRadius:5,alignItems:'center',justifyContent:'center'}}>
                            <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>NEXT</Text>
                        </TouchableOpacity>
                       }
                      </View>
                  }
              </View>
          </SafeAreaView>
          </KeyboardAvoidingView>  
      </Modal>

      </View>
    )
   }
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
      paypalPaymentId:state.paypalPaymentId,
      paypalPaymentState:state.paypalPaymentState,
      totalAmount:state.totalAmount,
      deliveryCharge:state.deliveryCharge,
      choosedPickupMethod:state.choosedPickupMethod,
      orderDetails:state.orderDetails,
      orderDeliverType:state.orderDeliverType,
      paymentScreenUpdate:state.paymentScreenUpdate
  }
}

function mapDispatchToProps(dispatch){
  return{
    setPaypalPaymentId:(value)=>dispatch({type:"setPaypalPaymentId",value}),
    setPaypalPaymentState:(value)=>dispatch({type:"setPaypalPaymentState",value}),
    setTradeAccountStatus:(value)=>dispatch({type:"setTradeAccount",value}),
    setguestToUserChanged:(value)=>dispatch({type:"setguestToUserChanged",value}),
    setuserId:(value)=>dispatch({type:'setuserId',value}),
    setgestFromPayment:(value)=>dispatch({type:'setgestFromPayment',value}),
    setpaymentScreenUpdate:(value)=>dispatch({type:"setpaymentScreenUpdate",value})
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(PaymentScreen);


const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    justifyContent: "center",
    alignItems: "center"
  },
  webview: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  btn: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 10,
    backgroundColor: '#61E786',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
});


// import React, {Component, useState,useEffect } from 'react';
// import {Text,View,StyleSheet,TextInput,ActivityIndicator,SafeAreaView,KeyboardAvoidingView,Platform,TouchableOpacity,Image,Modal} from 'react-native';
// import {OverAllStyle,ProductViewStyle,CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle';
// import { WebView } from 'react-native-webview';
// import axios from "axios";
// import qs, { parse } from "qs";
// import { decode, encode } from 'base-64'
// import base64 from 'react-native-base64'
// import images from '../images/index'
// import {connect} from 'react-redux'
// import { PaypalDetails } from '../DataModel/ModelClass';
// import { baseUrl } from '../Controller/baseUrl';
// import CustomNotification from './CustomNotification';
// import stripe from 'tipsi-stripe'
// import {StyleContainer} from '../screens/Styles/CommonStyles';
// import NetInfo from '@react-native-community/netinfo';
// import firebase from "react-native-firebase";
// import { Colors } from './Styles/colors';
// import AsyncStorage from '@react-native-async-storage/async-storage';
// import {
//   BallIndicator,
//   BarIndicator,
//   DotIndicator,
//   MaterialIndicator,
//   PacmanIndicator,
//   PulseIndicator,
//   SkypeIndicator,
//   UIActivityIndicator,
//   WaveIndicator,
// } from 'react-native-indicators';

// stripe.setOptions({
//   publishableKey:"pk_test_sqs8P7KlOfi3FX7nfkvrXb6O00ZBhKmctr"
// })

// class PaymentScreen extends Component{
//   state={
//     loading:false,
//     paypalUrl:"",
//     accessToken:"",
//     isWebViewLoading:false,
//     shouldShowWebViewLoading:true,
//     response:null,
//     getDetailLoading:false,
//     responseData:[],
//     tradeAccount:false,
//     confirmationLoading:false,
//     stripeloading: false,
//     token: null,  
//     modalVisible:false,
//     billingAddress:null,

//     //guest login
//     isguest:false,
//     postcodeAvailability:false,
//     postcode:null,

//     guestEmail:null,
//     guestEmailError:null,
//     guestLoginLoading:false,
//     deliveryAvailable:false,
    
//     fcmToken:null,
//     permissionRejected:false,
//     devicetype:null,

//     //user login
//     password:null,
//     passwordError:null,
//     userPasswordVisible:false,
//     hidePassword:true
//   }

//  async componentDidMount(){
//        await AsyncStorage.getItem('guestId').then((value) => {
//        if(value!=null){
//            //this.props.setuserId(JSON.parse(value))
//            this.setState({isguest:true})
//            this.checkPermission() 
//            this.getToken()

//        }else{
//            console.log("else")
//            this.getMethod()
//        }
//     })  
//   }

//   async checkPermission() {
//     const enabled = await firebase.messaging().hasPermission();
//     // If Premission granted proceed towards token fetch
//     //console.log("check permission")
//     if (enabled) {
//       this.getToken();
//     } else {
//       // If permission hasn’t been granted to our app, request user in requestPermission method. 
//       this.requestPermission();
//     }
//   }

//   async getToken() {
//     if (!this.state.fcmToken) {
//     let fcmToken = await firebase.messaging().getToken();
//       if (fcmToken) {
//         // user has a device token
//         //await AsyncStorage.setItem('fcmToken', fcmToken);
//         await this.setState({fcmToken:fcmToken})
//         console.log(this.state.fcmToken)
//         this.deviceType()
//       }
//     }
//     //let fcmToken = await AsyncStorage.getItem('fcmToken');
//     //console.log("state"+ this.state.fcmToken)
//   }

//   async requestPermission() {
//     try {
//       await firebase.messaging().requestPermission();
//         this.getToken();
//     } catch (error) {
//       // User has rejected permissions
//       //alert("permission rejected")
//       await this.setState({permissionRejected:true})
//       //console.log(this.state.permissionRejected)
//     }
//   }


//   async deviceType(){
//    if(Platform.OS==="ios"){
//      await this.setState({devicetype:"ios"})
//     }else{
//      await this.setState({devicetype:"android"})
//     }
//     console.log(this.state.devicetype)
//   }


//   getMethod(){   
//     console.log("order delivery type "+ this.props.orderDeliverType)
//     if (!global.btoa) {
//         global.btoa = encode;
//       }
  
//       if (!global.atob) {
//         global.atob = decode;
//       }
//       this.setState({getDetailLoading:true})
//       // fetch(baseUrl+this.props.userId+"/payment")
//       // fetch(baseUrl+this.props.userId+"/payment?shipping="+this.props.deliveryCharge)
//       fetch(baseUrl+"3/payment?shipping=35")
//       .then((response)=>response.json())
//       .then((responseJson)=>{
//         console.log(JSON.stringify(responseJson))
//         this.setState({responseData:responseJson,billingAddress:responseJson.billing_address,getDetailLoading:false})
//       })
//   }
  

//   /*---Paypal checkout section---*/
//   buyBook() {
//     console.log("press")
//     this.setState({loading:true})
//     //Check out https://developer.paypal.com/docs/integration/direct/payments/paypal-payments/# for more detail paypal checkout

//     const url = `https://api.sandbox.paypal.com/v1/oauth2/token`;

//     const data = {
//       grant_type: 'client_credentials'

//     };

//     const auth = {
//       username: "AVl0GkkUMCHdPYXHswzr-G0_Bbw9tngpWteYeuZn3cZ0MyOGLoHgnPA-V2IPxZNreSJDvfxZXAtirfHS",  //"your_paypal-app-client-ID",
//       password: "EMU9kk0oN4KWVKr9nSKQPKRC7A9OU0m63WGvRorpi8y3ynAzJL23IVdNndWmW7ZgcVkzL2IcSv1fKi-7"   //"your-paypal-app-secret-ID

//     };

//     const options = {

//       method: 'post',
//       headers: {
//         'content-type': 'application/x-www-form-urlencoded',
//         'Access-Control-Allow-Credentials': true
//       },

//       //Make sure you use the qs.stringify for data
//       data: qs.stringify(data),
//       auth: auth,
//       url,
//     };

//     // Authorise with seller app information (clientId and secret key)
//     axios(options).then(response => {console.log("access tocken"+response.data.access_token)
//       //setAccessToken(response.data.access_token)
//       this.setState({accessToken:response.data.access_token})

//       //Resquest payal payment (It will load login page payment detail on the way)
//       // axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, dataDetail,
//       axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, this.state.responseData.data,
//         {
//           headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${response.data.access_token}`
//           }
//         }
//       )
//         .then(response => {
//           const { id, links } = response.data
//           const approvalUrl = links.find(data => data.rel == "approval_url").href

//           console.log("response", links)
//           console.log("approvalurl  ",approvalUrl)
//           this.setState({paypalUrl:approvalUrl,loading:false})
//           //setPaypalUrl(approvalUrl)
//           //SetLoading(false)
//         }).catch(err => {
//           console.log({ ...err })
//         })
//     }).catch(err => {
//       console.log(err)
//     })
//   };

//   /*---End Paypal checkout section---*/

//   onWebviewLoadStart = () => {
//     console.log("onwebviewloading")
//     if (this.state.shouldShowWebViewLoading) {
//       this.setState({isWebViewLoading:true})
//     }
//   }

//   _onNavigationStateChange = (webViewState) => {
//     console.log("onnavigationstatechange", webViewState)

//     //When the webViewState.title is empty this mean it's in process loading the first paypal page so there is no paypal's loading icon
//     //We show our loading icon then. After that we don't want to show our icon we need to set setShouldShowWebviewLoading to limit it
//     if (webViewState.title == "") {
//       //When the webview get here Don't need our loading anymore because there is one from paypal
//       // setShouldShowWebviewLoading(false)
//       this.setState({shouldShowWebViewLoading:false})
//     }

//     if (webViewState.url.includes('https://example.com/')) {
//        this.setState({paypalUrl:null})
//       //setPaypalUrl(null)
//       const urlArr = webViewState.url.split(/(=|&)/);

//       const paymentId = urlArr[2];
//       const payerId = urlArr[10];
//       console.log("before payer id")

//       this.setState({confirmationLoading:true})
//       axios.post(`https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute`, { payer_id: payerId },
//         {
//           headers: {
//             'Content-Type': 'application/json',
//             'Authorization': `Bearer ${this.state.accessToken}`
//           }
//         }
//       )
//         .then(response => {
//           console.log("after payer id")
//           // setShouldShowWebviewLoading(true)
//           this.setState({shouldShowWebViewLoading:true})
//           console.log("payid  "+JSON.stringify(response))
//           // SetLoading(false)
//           // PaypalDetails.id=response.data.id
//           // PaypalDetails.state=response.data.state
//           this.navigateFinalScreen(response)
//         }).catch(err => {
//           console.log("cancel")
//           this.setState({loading:false,confirmationLoading:false})
//           alert("Transaction failed")
//           // setShouldShowWebviewLoading(true)
//           this.setState({shouldShowWebViewLoading:true})
//           console.log({ ...err })
//         })

//     }
//   }

//  async navigateFinalScreen(response){
//   this.setState({loading:true})
//    await this.props.setPaypalPaymentId(response.data.id)
//    await this.props.setPaypalPaymentState(response.data.state)
//    await this.props.navigation.navigate("orderPlaced")
//    this.setState({loading:false,confirmationLoading:false})
//   }

//  async payLater(){
//    await this.props.setTradeAccountStatus(true)
//    this.props.navigation.navigate("orderPlaced")
//   }

//   handleCardPayPress = async () => {
//     // try {
//       this.setState({ stripeloading: true, token: null })
//       const token = await stripe.paymentRequestWithCardForm({
//         // Only iOS support this options
//         // smsAutofillDisabled: true,
//         // requiredBillingAddressFields: 'full',
//         // prefilledInformation: {

//         //   billingAddress: {
//         //     name: this.state.billingAddress.name,
//         //     line1:this.state.billingAddress.line1,
//         //     line2:this.state.billingAddress.line2,
//         //     city:this.state.billingAddress.city,
//         //     state:this.state.billingAddress.state,
//         //     country:this.state.billingAddress.country,
//         //     postalCode:this.state.billingAddress.postalCode,
//         //     email:this.state.billingAddress.email,
//         //   },
//         // },
//       })
//       console.log(token)
//       this.setState({ stripeloading: false, token:token,modalVisible:true })
//       this.stripePay(token)
//     // } catch (error) {
//     //   console.log(error)
//       // this.setState({ stripeloading: false })
//     // }
//   }

//  stripePay(token){
//     console.warn(token.tokenId)
//     // fetch(baseUrl+this.props.userId+'/stripepayment', {
//       fetch(baseUrl+'3/stripepayment', {
//       method: 'POST',
//       headers: {
//       'Content-Type': 'application/json',
//       },
//       body: JSON.stringify({
//         "stripe_token":token.tokenId,
//         "payment_amount": "1.00"
//       })
//    })
//   .then((response) => response.json())
//   .then((responseJson) => {
//       this.setState({modalVisible:false})
//       alert(responseJson)    
//       // this.navigateFinalScreenUsingStripe(responseJson)
//   }).catch((error)=>{
//     console.log(error)
//     this.setState({modalVisible:false})
//     alert("Payment failed")
//   })
//   }

//  async navigateFinalScreenUsingStripe(response){
//     await this.props.setPaypalPaymentId(response.transaction_id)
//     await this.props.setPaypalPaymentState(response.payment_status)
//     await this.setState({modalVisible:false})
//     await this.props.navigation.navigate("orderPlaced")
//   }

//   guestEmailSubmit(){
//     let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
//     if(this.state.guestEmail==null){
//         this.setState({guestEmailError:'The email field is required'}) 
//     }else{
//         this.setState({guestEmailError:null})
//         if(reg.test(this.state.guestEmail) === true){
//             this.setState({guestEmailError:null})
//             this.guestSignUp()
//         }else{
//             this.setState({guestEmailError:'check the email format'})
//         }
//     } 
// }

// async guestSignUp(){
//     await NetInfo.fetch().then(state => {
//         //console.log("Connection type", state.type);
//         if(state.isConnected==true){
//           this.setState({guestLoginLoading:true})
//           fetch(baseUrl+this.props.userId+"/guest-signup", {
//             method: 'POST',
//             headers: {
//               'Content-Type': 'application/json',
//             }, 
//             body: JSON.stringify({
//                  "email":this.state.guestEmail
//               })
//             })
//           .then((response) => response.json())
//            .then((responseJson) => {
//              console.log(responseJson)
//              if(responseJson.success==false){
//               if(responseJson.data!=""){
//                 this.setState({guestLoginLoading:false,userPasswordVisible:true})
//                }
//              }else if(responseJson.success==true){
//                AsyncStorage.removeItem('guestId')
//                AsyncStorage.setItem('userId',JSON.stringify(responseJson.data.id))
//                this.props.setguestToUserChanged(true)
//                this.setState({guestLoginLoading:false,isguest:false})
//                this.getMethod()
//              }
//            }).catch((err)=>{
//             console.log(err)
//         })  
//     }
// })
// }

// async signin(){
//    await this.deviceType()
//     console.log(this.state.guestEmail,this.state.password,this.state.devicetype,this.state.fcmToken)
//     if(this.state.password==null){
//       this.setState({passwordError:"Password feild is required"})
//     }else{
//       this.setState({passwordError:null,guestLoginLoading:true})
//       await fetch(baseUrl+'login', {
//         method: 'POST',
//         headers: {
//           Accecpt:'application/json',
//           'Content-Type': 'application/json',
//         },
//         body: JSON.stringify({
//          "email":this.state.guestEmail,
//          "password":this.state.password,
//          "device_type":this.state.devicetype,
//          "device_token":this.state.fcmToken,
//          "guest_id":this.props.userId
//           })
//         })
//       .then((response) => response.json())
//        .then((responseJson) => {
//           console.log("signin "+ JSON.stringify(responseJson))
//           if(responseJson.success==true){
//             this.setState({passwordError:null})
//             console.log(responseJson.data.user.id)
//             AsyncStorage.removeItem('guestId')
//             AsyncStorage.setItem('userId',JSON.stringify(responseJson.data.user.id))
//             this.props.setuserId(responseJson.data.user.id)
//             this.props.setguestToUserChanged(true)
//             this.setState({guestLoginLoading:false,isguest:false})
//             this.getMethod()
//           }else if(responseJson.success==false){
//             this.setState({passwordError:responseJson.errorMessage[0]})
//           }
//       })
//     }
//   }

// cancelGuestLogin(){
//   this.setState({isguest:false})
//   this.props.navigation.navigate("completeOrder")
// }

//   render(){
//     const totalAmountWithoutDeliveryCharge=(JSON.parse(this.props.totalAmount+0).toFixed(2))
//     const totalAmount= (JSON.parse(this.props.totalAmount)+this.props.deliveryCharge).toFixed(2)
//     if(this.state.getDetailLoading==true){
//       return(
//         <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
//           <ActivityIndicator color="#fa7153" size="large"/>
//           <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
//         </View>
//       )
//     }else{
//     return(
//       <View style={{flex:1,backgroundColor:'#fff'}}>
//         <View style={{paddingHorizontal:20,paddingVertical:10,borderBottomWidth:5,borderBottomColor:'#f2f2f2'}}>
//           <Text style={{fontFamily:"Montserrat",fontSize:16,paddingVertical:5}}>Pay</Text>
//           {
//             this.props.orderDeliverType==true?
//               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20,color:'#fa7153',}}>$ {totalAmountWithoutDeliveryCharge}</Text>
//               :
//               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20,color:'#fa7153',}}>$ {totalAmount}</Text>
//           }
          
//         </View>

//           {
//             this.state.responseData.trade_account==true?
//             <View>
//             {
//               this.state.tradeAccount==true?
//               <TouchableOpacity onPress={()=>this.setState({tradeAccount:false})} style={{flexDirection:'row',padding:20,borderBottomWidth:5,borderBottomColor:'#f2f2f2'}}>            
//                 <View style={{width:20,height:20,borderWidth:0.5,borderRadius:10,justifyContent:'center',alignItems:'center'}}>
//                   <View style={{width:10,height:10,backgroundColor:'#fa7153',borderRadius:10}}></View>
//                 </View>
//                 <View style={{marginLeft:15}}>
//                   <Text style={{fontFamily:"Montserrat",fontSize:17}}>Trade account</Text>
//                 </View>
//               </TouchableOpacity>
//               :
//               <View>
//                 <TouchableOpacity onPress={()=>this.setState({tradeAccount:true})} style={{flexDirection:'row',padding:20,borderBottomWidth:5,borderBottomColor:'#f2f2f2'}}>            
//                   <View style={{width:20,height:20,borderWidth:0.5,borderRadius:10}}></View>
//                   <View style={{marginLeft:15}}>
//                     <Text style={{fontFamily:"Montserrat",fontSize:17}}>Trade account</Text>
//                   </View>
//                 </TouchableOpacity>
          
                
//               </View>
//             }
//             </View>
//             :
//             null
//           }

//         {
//           this.state.tradeAccount==true?
//           <TouchableOpacity onPress={()=>this.payLater()} style={{width:'90%',height:50,borderRadius:5,justifyContent:'center',alignSelf:'center',backgroundColor:'#fa7153',marginVertical:20}}>
//             <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',color:'#fff',fontSize:17}}>ADD TO ACCOUNT</Text>
//           </TouchableOpacity>
//           :
//           <View style={{paddingHorizontal:20,paddingVertical:10,borderBottomWidth:7,borderBottomColor:'#f2f2f2',paddingBottom:10}}>
//             <Text style={{fontFamily:"Montserrat",fontSize:17}}>Express checkout</Text>
//             {
//               this.state.loading?
//               <TouchableOpacity style={{marginVertical:20}}>
//                 <ActivityIndicator color="#fa7153"/>
//               </TouchableOpacity>
//               :
//               <TouchableOpacity onPress={()=>this.buyBook()}  style={{marginVertical:10}}>
//                 <Image source={images.paypalBtn} style={{width:'100%',height:50,resizeMode:'contain'}}/>
//               </TouchableOpacity>
//             }

//             <TouchableOpacity onPress={this.handleCardPayPress} style={{width:'100%',height:45,borderWidth:0.5,borderRadius:5,borderColor:'#5469d4',justifyContent:'center',alignItems:'center',backgroundColor:'#5469d4'}}>
//               <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>Pay with stripe</Text>
//             </TouchableOpacity>
//           </View>
//         }

//       {
//         this.state.paypalUrl ?
//         <View style={styles.webview}>
//           <WebView
//             style={{ height: "100%", width: "100%" }}
//             source={{ uri: this.state.paypalUrl }}
//             onNavigationStateChange={this._onNavigationStateChange}
//             javaScriptEnabled={true}
//             domStorageEnabled={true}
//             startInLoadingState={false}
//             onLoadStart={()=>this.onWebviewLoadStart()}
//             onLoadEnd={() => this.setState({isWebViewLoading:false})}
//           />
//         </View>
//        :
//         null
//         }

//       {
//         this.state.isWebViewLoading? 
//         <View style={{ ...StyleSheet.absoluteFill, justifyContent: "center", alignItems: "center", backgroundColor: "#ffffff" }}>
//           <DotIndicator color="#fa7153"/>
//           <Text style={{fontFamily:'MontSerrat-SemiBold',paddingBottom:30,fontSize:16}}>Redirect to Paypal login page</Text>
//         </View>
//        :
//         null
//       }

//       {
//         this.state.confirmationLoading? 
//         <View style={{ ...StyleSheet.absoluteFill, justifyContent: "center", alignItems: "center", backgroundColor: "#ffffff" }}>
//           <DotIndicator color="#fa7153"/>
//           <Text style={{fontFamily:'MontSerrat-SemiBold',paddingBottom:30,fontSize:16}}>Payment processing</Text>
//         </View>
//        :
//         null
//       }

//       <CustomNotification/>

//       <Modal
//         animationType="slide"
//         transparent={true}
//         visible={this.state.loading}>
//           <View style={ModelStyle.modelContainer}>
//              <View style={{width:'100%',height:'20%',backgroundColor:'#FFF',borderRadius:20,marginBottom:-15,justifyContent:'center',alignItems:'center'}}>             
//                <Text style={{textAlign:'left',marginTop:20,paddingLeft:10}}>Paypal payment loading</Text>
//                 <SkypeIndicator color="#fa7153"/>
//               </View>
//           </View>
//         </Modal>
//       <Modal
//         animationType="slide"
//         transparent={true}
//         visible={this.state.modalVisible}>
//             <View style={ModelStyle.modelContainer}>
//                 <View style={{width:'100%',height:'20%',backgroundColor:'#FFF',borderRadius:20,marginBottom:-15,justifyContent:'center',alignItems:'center'}}>
//                    <Text style={{fontFamily:'Montserrat',marginTop:20}}>Payment processing</Text>
//                     <DotIndicator color="#fa7153"/>
                    
//                 </View>
//             </View>
//       </Modal>

//       <Modal
//           animationType="slide"
//           transparent={true}
//           visible={this.state.isguest}>
//             <KeyboardAvoidingView
//               behavior="padding"
//               enabled
//               style={{flex:1}}
//           >
//           <SafeAreaView style={{flex:1,justifyContent:'flex-end',backgroundColor:'rgba(52,52,52,0.5)'}}>
//               <View style={{width:'100%',backgroundColor:'#FFF',borderRadius:15}}>
//                   <View style={{backgroundColor:'#fa7153',flexDirection:'row',justifyContent:'space-between',alignItems:'center',padding:15,borderTopLeftRadius:15,borderTopRightRadius:15}}>
//                       <TouchableOpacity onPress={()=>this.cancelGuestLogin()}>
//                       <Image source={images.cancel} style={{width:22,height:22,resizeMode:'contain'}}></Image>
//                       </TouchableOpacity>
//                       <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16,color:'#fff'}}>Euro Building Supplies</Text>
//                       <Text/>
//                   </View>

//                   {
//                     this.state.userPasswordVisible==false?
//                     <View style={{padding:20}}>
//                       <Text style={{fontFamily:'Montserrat',paddingBottom:15}}>Enter your email address :</Text>
//                       <View style={{width:'100%'}}>
//                           <TextInput style={StyleContainer.TextInputView} placeholder="Email id"                        
//                               ref={(input) => { this.suburbRef = input; }}
//                               onSubmitEditing={() => {this.guestEmailSubmit()}}
//                               onChangeText={(value)=>this.setState({guestEmail:value})} 
//                               value={this.state.guestEmail}
//                               keyboardType="email-address"
//                               returnKeyType={"next"}/>

//                                   {
//                                       this.state.guestEmailError==null?null
//                                       :
//                                       <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.guestEmailError}</Text>
//                                   }
//                           </View>
//                       </View>
//                       :
//                       <View style={{padding:20}}>
//                         <Text style={{fontFamily:'Montserrat',paddingBottom:15}}>Enter your password :</Text>
//                         <View style={{flexDirection:'row',width:'100%',marginVertical:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3}}>
//                             <TextInput 
//                             style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
//                                   fontSize:14,fontFamily:'Montserrat'}} 
//                               secureTextEntry={this.state.hidePassword}
//                               placeholder="Password"
//                               ref={(input) => { this.passwordRef = input; }}                         
//                               onChangeText={(value)=>{this.setState({password:base64.encode(value)})}} 
//                               onSubmitEditing={() => {this.signin()}}
//                               returnKeyType={"done"}        
//                               textContentType="oneTimeCode"        
//                                 />
//                                 <TouchableOpacity onPress={()=>this.setState({hidePassword:!this.state.hidePassword})}>
//                                   {
//                                     this.state.hidePassword==true?
//                                     <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
//                                     :
//                                     <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
//                                   }
                                  
//                                 </TouchableOpacity>
//                             </View>
//                         </View>
//                   }

//                   {
//                       this.state.guestLoginLoading==true?
//                       <TouchableOpacity style={{backgroundColor:'#fa7153',padding:15,marginBottom:20,width:'90%',alignSelf:'center',borderRadius:5,alignItems:'center',justifyContent:'center'}}>
//                           <ActivityIndicator color="#fff"/>
//                       </TouchableOpacity>
//                       :
//                       <View>
//                        {
//                          this.state.userPasswordVisible==true?
//                          <TouchableOpacity onPress={()=>this.signin()} style={{backgroundColor:'#fa7153',padding:15,marginBottom:20,width:'90%',alignSelf:'center',borderRadius:5,alignItems:'center',justifyContent:'center'}}>
//                             <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>NEXT</Text>
//                         </TouchableOpacity>
//                         :
//                         <TouchableOpacity onPress={()=>this.guestEmailSubmit()} style={{backgroundColor:'#fa7153',padding:15,marginBottom:20,width:'90%',alignSelf:'center',borderRadius:5,alignItems:'center',justifyContent:'center'}}>
//                             <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff'}}>NEXT</Text>
//                         </TouchableOpacity>
//                        }
//                       </View>
//                   }
//               </View>
//           </SafeAreaView>
//           </KeyboardAvoidingView>  
//       </Modal>

//       </View>
//     )
//    }
//   }
// }

// function mapStateToProps(state){
//   return{
//       userId:state.userId,
//       paypalPaymentId:state.paypalPaymentId,
//       paypalPaymentState:state.paypalPaymentState,
//       totalAmount:state.totalAmount,
//       deliveryCharge:state.deliveryCharge,
//       choosedPickupMethod:state.choosedPickupMethod,
//       orderDetails:state.orderDetails,
//       orderDeliverType:state.orderDeliverType,
//   }
// }

// function mapDispatchToProps(dispatch){
//   return{
//     setPaypalPaymentId:(value)=>dispatch({type:"setPaypalPaymentId",value}),
//     setPaypalPaymentState:(value)=>dispatch({type:"setPaypalPaymentState",value}),
//     setTradeAccountStatus:(value)=>dispatch({type:"setTradeAccount",value}),
//     setguestToUserChanged:(value)=>dispatch({type:"setguestToUserChanged",value}),
//     setuserId:(value)=>dispatch({type:'setuserId',value}),
//   }
// }
// export default connect(mapStateToProps,mapDispatchToProps)(PaymentScreen);


// const styles = StyleSheet.create({
//   container: {
//     width: '100%',
//     height: '100%',
//     justifyContent: "center",
//     alignItems: "center"
//   },
//   webview: {
//     width: '100%',
//     height: '100%',
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     right: 0,
//     bottom: 0,
//   },
//   btn: {
//     paddingVertical: 5,
//     paddingHorizontal: 15,
//     borderRadius: 10,
//     backgroundColor: '#61E786',
//     justifyContent: 'center',
//     alignItems: 'center',
//     alignContent: 'center',
//   },
// });



