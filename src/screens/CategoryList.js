
import React, { Component } from 'react';
import { View,Text,ScrollView, Image,TouchableOpacity,Modal,StyleSheet,RefreshControl,KeyboardAvoidingView, ActivityIndicator,TextInput } from 'react-native';
import {Tab,Tabs,ScrollableTab,Container, Header, Toast} from 'native-base'
import NetInfo from '@react-native-community/netinfo';
import images from '../images/index';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,ProductViewStyle,CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle';
import {connect} from 'react-redux'
import ContentLoader from 'react-native-easy-content-loader';
import {baseUrl} from '../Controller/baseUrl'
import CustomNotification from './CustomNotification';
import { Colors } from './Styles/colors';
import { Button, Snackbar } from 'react-native-paper';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

class CategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
        loading:false,
        selectedProdIndex:null,
        modalVisible:false,
        selectedCategoryName:'',
        choosedCategoryIndex:1,
        selectedProductIndex:0,
        selectedProduct:{},
        selectedVariant:{},
        networkFailed:false,
        loadType:[{name:"Loose",status:false},{name:"1TON bag",status:true},{name:'40kg bag',status:false}],
        categories:[],
        products:[],
        variants:[],
        deliveryStatus:true,
        addTocartLoading:false,
        weightChoosed:true,
        colorChoosed:true,
        sizeChoosed:true ,
        variantPrice:null,
        variantQty:null,
        variantWeight:null,
        cart:[],
        availableVariants:[],
        refreshing:false,
        page:1,
        nextPageAvailable:true,
        nextPageLoading:false,

        signleProductLoading:false,
        modalResponse:[],
        productOptions:[],
        productOptionCount:null,
        dummyArray:[],
        availableCombinations:[],
        combinationStatus:null,
        variantId:null,
        imageUrl:null,
        updateproductmodal:false,
        modalProductWeight:null,
        addToCartStatus:false,

        prodPrice:null,
        isediting:false,
        iseditingValue:null,
        snackBarVisible:false
    }
  }

componentDidMount(){
  
  this.setState({choosedCategoryIndex:this.props.categoryId})  
  this.getTabDetail()
}

async  componentDidUpdate(){
  if(this.props.productCountChanged==true){
    await this.props.setProductCountChanged(false)
    await this.getTabDetail()
  }
}

async getTabDetail(){  
  
     await NetInfo.fetch().then(state => {
        ////consle.log("Connection type", state.type);
        if(state.isConnected==true){
            if(this.props.productCountChanged==false){
              this.setState({refreshing:false,loading:true})
            }
            console.warn(this.props.categoryUrl)
            // fetch(baseUrl+this.props.userId+'/category/'+this.props.categoryId)
            fetch(this.props.categoryUrl)
            .then((response) => response.json())
              .then((responseJson) => {
                console.warn(JSON.stringify(responseJson))
                 this.setResponse(responseJson)
                  //this.props.setCartCount(responseJson.cart_count)
                  if(this.props.productCountChanged==false){
                    this.setState({loading:false})
                  }
            })  
            .catch((err)=>{
              consle.log(err)
          })   
        }
        else{ 
          this.setState({networkFailed:true})
            //alert("Check network connection")
        }
      })   
     await this.props.setCategoryProductCountChanged(true)
    }

  async setResponse(responseJson){
    //consle.log(responseJson)

     await responseJson.categories.map((item,index)=>{
      if(item.current_category==true){
        this.setState({choosedCategoryIndex:index})
      }
    })

       await this.setState({
        categories:responseJson.categories,
        products:responseJson.categories[this.state.choosedCategoryIndex].products,
        page:1,
        nextPageAvailable:true
      })
    
  }

  async increaseQty(indexValue){
       let products = [...this.state.products];
       if(products[indexValue].quantity<products[indexValue].stock_quantity){
       products[indexValue].quantity+=1;
       await this.setState({products:products})
       await this.setState({
        selectedProductIndex:indexValue,
        selectedProduct:this.state.products[indexValue]
        })
        await this.addToCart(indexValue)
      }else{
        alert("Stock unavailable")
      }
       
  }
 
 async decreaseQty(indexValue,selectedVariant){
    ////consle.log('dec qty')
    let products = [...this.state.products];
    products[indexValue].quantity-=1;

    let setSelectedVariant=selectedVariant
    setSelectedVariant.quantity -=1
    selectedVariant=setSelectedVariant

    if(products[indexValue].quantity<1){
      this.props.setDecreaseCartCount()

      let setSelectedVariant=selectedVariant
      setSelectedVariant.quantity=0
      selectedVariant=setSelectedVariant

      products[indexValue].seleted=false
      this.setState({products:products})
    }
    await this.setState({products:products})
    await this.setState({
    selectedProductIndex:indexValue,
    selectedProduct:this.state.products[indexValue]
    })
    await this.addToCart(indexValue)
  }

  modalDecrement(){
    if(this.state.quantity==1){
      this.setState({quantity:1,isediting:false,modalVisible:false,modalVisible:true})     
    }else{
        this.setState({quantity:this.state.quantity-1,isediting:false,modalVisible:false,modalVisible:true})     
      }
  }
  
  async modalIncrement(){
    this.setState({quantity:parseInt(this.state.quantity)+1,isediting:false,modalVisible:false,modalVisible:true})
  }

  async customQty(text){
    await this.setState({quantity:text,modalVisible:false,modalVisible:true,iseditingValue:text})
  }

  incdecEdit(){
    var qty=this.state.quantity.toString()
    //consle.log(qty)
    this.setState({iseditingValue:qty,isediting:true})
   }

  async changeTabIndex(index){
    ////consle.log(index)
     const categories=[...this.state.categories]
     //await this.setState({selectedCategoryName:categories[index].name})
     await this.props.setCategoryId(categories[index].id)
     await this.props.setCategoryUrl(categories[index].url)
     this.getTabDetail()
  }


  async selectedProduct(id,imageUrl){
    //consle.log("prodid  "+ id)
    this.setState({modalVisible:true,signleProductLoading:true})
    await fetch(baseUrl+this.props.userId+'/product/'+id)
          .then((response) => response.json())
          .then((responseJson) => {
            //consle.log(responseJson.product_variants[0].id)
            var dummyArray=new Array(responseJson.product_option_count)
            var variantOption=responseJson.product_options
            variantOption.map((item,key)=>{            
                item.option.map((item,index)=>{
                  //if(responseJson.default_variant_id==item.variant_id){
                    if(responseJson.product_variants[0].id==item.variant_id){
                    item.selected=true
                    dummyArray.splice(0,0,item.option_value_id)
                  }else{
                    //consle.log("not matching")
                  }
                })
            })
            var filtered = dummyArray.filter(item=> {
              return item != null;
            });
            
            var reverced=filtered.reverse()
            
            if(responseJson.quantity==0){
              this.setState({quantity:1})
            }else{
              this.setState({quantity:responseJson.quantity})
            }
            
              this.setState({
                  dummyArray:reverced,
                  modalResponse:responseJson,
                  productOptions:variantOption,
                  productOptionCount:responseJson.product_option_count,
                  availableCombinations:responseJson.product_variants,
                  imageUrl:imageUrl,
                  signleProductLoading:false
              })
              //consle.log(JSON.stringify(dummyArray))
              if(this.state.productOptions.length!=0){
                this.compareArrayValues()
              }else{
                //consle.log(this.state.availableCombinations[0].id)
                this.setState({combinationStatus:true,variantId:this.state.availableCombinations[0].id,prodPrice:this.state.availableCombinations[0].price.toFixed(2)})
              }
              
          })  
  }
  
  changeProductVariant(key,indexPosition){
  
    var dummyArray=this.state.dummyArray
    var productOptions=this.state.productOptions
    productOptions.map((item,index)=>{
      if(index==key){
        item.option.map((item,index1)=>{
          item.selected=false
          if(index1==indexPosition){
            item.selected=true
            dummyArray[key]=item.option_value_id 
          }
        })
      }
    })
    this.setState({productOptions:productOptions,dummyArray:dummyArray})
    this.compareArrayValues()
  }
  
  compareArrayValues(){
    let array=this.state.availableCombinations
  
    for(var i=0;i<array.length;i++){
      if(this.state.productOptionCount==1){
        //if(this.state.dummyArray[0]==array[i].product_option_value_id1){
          if(this.state.dummyArray[0]==array[i].product_option_value_id1){
            //var modalresponse=this.state.modalResponse
            //modalresponse.default_variant_price=array[i].price
           
            this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            //this.getModalProductDetails(this.state.variantId)
            break;
        //   }else{
        //     this.setState({combinationStatus:false})
        //     break
        //   }
         }
      }else if(this.state.productOptionCount==2){
        //if((this.state.dummyArray[0]==array[i].product_option_value_id1) || (this.state.dummyArray[1]==array[i].product_option_value_id2)){
          if((this.state.dummyArray[0]==array[i].product_option_value_id1) && (this.state.dummyArray[1]==array[i].product_option_value_id2)){
            // var modalresponse=this.state.modalResponse
            // modalresponse.default_variant_price=array[i].price
            this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            break;
          }
          // else{
          //   this.setState({combinationStatus:false})
          //   break
          // } 
        //}
      }else if(this.state.productOptionCount==3){
        // if(this.state.dummyArray[0]==array[i].product_option_value_id1 || this.state.dummyArray[1]==array[i].product_option_value_id2 ||
        //   this.state.dummyArray[2]==array[i].product_option_value_id3){
            if(this.state.dummyArray[0]==array[i].product_option_value_id1 && this.state.dummyArray[1]==array[i].product_option_value_id2 &&
              this.state.dummyArray[2]==array[i].product_option_value_id3){
                // var modalresponse=this.state.modalResponse
                // modalresponse.default_variant_price=array[i].price
                this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
              break;
            // }else{
            //   this.setState({combinationStatus:false})
            //   break
            // }
          }
      }
    } 
  }

  async addToCartWithVariants(){
   if(this.state.quantity==0 || this.state.quantity==""){
     alert("Please add quantity!")
   }else{
   this.setState({addTocartLoading:true,isediting:false})
   await fetch(baseUrl+this.props.userId+'/addtocart', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "customer_id":this.props.userId,
      "product_id": this.state.modalResponse.id,
      "product_variant_id" :this.state.variantId,
      "quantity":this.state.quantity,
    })
   })
     .then((response) => response.json())
     .then((responseJson) => {
        //consle.log("response"+responseJson)
        if(responseJson.success==true){
        this.setState({addTocartLoading:false,modalVisible:false,snackBarVisible:true})
        this.props.setIncreaseCartCount()
        }                  
     })
     .catch((err)=>{
      ////consle.log(err)
  })  
  await this.props.setCategoryProductCountChanged(true)
  await this.props.setProductCountChanged(false)
  }
  }

   navigateSingleProductScreen(id){
     this.props.setProductId(id)
     this.props.navigation.navigate("singleProductScreen")
   }

   _onRefresh = () => {
      ////consle.log("onrefress")
      this.setState({refreshing: true});
      this.getTabDetail()
    }
    
    retry(){
      ////consle.log("retry")
      this.setState({networkFailed:false})
      this.getTabDetail()
    }

    addFavourite(value,index){
      var type;
      if(this.state.products[index].is_favourite==true){
        let products=[...this.state.products]
        products[index].is_favourite=false
        type=1
        this.setState({products:products})
      }else{
        let products=[...this.state.products]
        products[index].is_favourite=true
        type=0
        this.setState({products:products})
      }
      ////consle.log(this.props.userId,this.state.products[index].id,this.state.products[index].variant_id,type)
      fetch(baseUrl+this.props.userId+'/wishlist', {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({

         "customer_id": this.props.userId,
         "product_id":this.state.products[index].id,
         "is_wishlist":type
        })
      })
     .then((response) => response.json())
     .then((responseJson) => {
         ////consle.log(responseJson)
        //  if(responseJson.data.message=="added"){
        //   Toast.show({text:"Added to favourites !",duration:1000,buttonText:"Done",textStyle:{fontSize:12}})
        //   }else{
        //     Toast.show({text:"Removed from favourites !",duration:1000,buttonText:"Done",textStyle:{fontSize:12}})
        //   }
         this.props.setHomeScreenUpdate(true)
    })
    }

    async getNextPageDetails(){
      if(this.state.nextPageAvailable==true){
      await this.setState({page:this.state.page+1,nextPageLoading:true})
      await fetch(baseUrl+this.props.userId+'/category/'+this.props.categoryId+'?page='+this.state.page)
      .then((response)=>response.json())
      .then((responseJson)=>{
        console.log(responseJson.categories[this.state.choosedCategoryIndex].products)
        this.setState({
          products:this.state.products.concat(responseJson.categories[this.state.choosedCategoryIndex].products),
          nextPageAvailable:responseJson.categories[this.state.choosedCategoryIndex].is_load,
          nextPageLoading:false
        })
      })
      .catch(e=>{
        //console.error(e)
        this.setState({nextPageLoading:false})
      })
    }
    console.warn(JSON.stringify(this.state.products))
    }

    async customQty(text){
      await this.setState({quantity:text,modalVisible:false,modalVisible:true,iseditingValue:text})
    }

  render() {
    var selectedVariant={};
    if(this.state.networkFailed==true){
      return(              
             <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                 <Image source={images.networkFailed} style={{width:100,height:100,resizeMode:'contain'}}/>
                 <Text style={{fontFamily:"Montserrat",fontSize:17,color:'black',paddingBottom:10}}>Connection Error</Text>
                 <Text>Please check your network and try again.</Text>
                 <TouchableOpacity onPress={()=>this.retry()} style={{marginVertical:20,borderColor:'#fa7153',borderWidth:1,paddingHorizontal:30,paddingVertical:5,borderRadius:5}}>
                   <Text style={{fontFamily:"Montserrat",color:'#fa7153',fontSize:16}}>Retry</Text>
                 </TouchableOpacity>
             </View>
      )
    }else
    if(this.state.loading==true){
      return(
        <View style={{flex:1}} >
        <View style={styles.headerContainer}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('homeStack')} style={OverAllStyle.HeaderIcon}>
              <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
              </TouchableOpacity>
              <Text style={styles.headerTitle}>{this.props.categoryName}</Text>
              <Text/>
          </View>
          <View style={{margin:20}}>
          <View style={{width:'100%',flexDirection:'row',justifyContent:'space-around'}}>
            <ContentLoader active  pRows={0} containerStyles={{width:100,height:50}}/>
            <ContentLoader active  pRows={0} containerStyles={{width:100,height:50}}/>
            <ContentLoader active  pRows={0} containerStyles={{width:100,height:50}}/>
            <ContentLoader active  pRows={0} containerStyles={{width:100,height:50}}/>
          </View>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
          <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30}}/>
        </View>
        </View>
      )
    }else{
    return (
      
      <View style={{flex:1}}>
          <View style={styles.headerContainer}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('homeStack')} style={OverAllStyle.HeaderIcon}>
              <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
              </TouchableOpacity>
                <Text style={styles.headerTitle}>{this.props.categoryName}</Text>
              <Text/>
          </View>
          <Container>
          {
            this.state.categories.length==1?
            <View>
              <ScrollView 
                  refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
                  onScroll={({nativeEvent}) => {
                      if (isCloseToBottom(nativeEvent)) {
                        this.getNextPageDetails()
                    }
                    }}
                    scrollEventThrottle={0}> 
                    {
                      this.state.products.length==0?
                      <View style={{flex:1,alignItems:'center',marginVertical:30}}>
                        <Text>No products available, added soon.</Text>
                      </View>
                      :
                      null
                    } 
                  <View style={{paddingHorizontal:20,paddingVertical:10}}>
                  {
                      this.state.products.map((item,index)=>{
                          return(
                              <View key={index} style={ProductViewStyle.ProductMainContainer}>
                                  

                                  <View style={ProductViewStyle.ProductView}>

                                      <View style={ProductViewStyle.ProductImageView}>
                                          <Image source={{uri:item.image_url}} style={ProductViewStyle.ImageCoverStyle} />
                                          <View style={ProductViewStyle.ZindexView}>
                                              <View style={ProductViewStyle.HeartView}>
                                              {
                                                  item.is_favourite==true?
                                                  <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("remove",index)}>
                                                      <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageContainStyle} />  
                                                  </TouchableOpacity>
                                                  :
                                                  <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("add",index)}>
                                                      <Image source={images.favouriteWithoutBackgroud} style={StyleContainer.ImageContainStyle} />
                                                  </TouchableOpacity>
                                              }
                                              </View>
                                          </View>
                                      </View>

                                      <TouchableOpacity onPress={()=>this.navigateSingleProductScreen(item.id)}
                                        style={ProductViewStyle.ProductDetailView}>
                                              <Text numberOfLines={2}  ellipsizeMode='tail' style={{fontFamily:'Montserrat-SemiBold',fontSize:14,marginBottom:3}}>{item.name}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:13,marginBottom:3}}>$ {item.default_variant_price.toFixed(2)}</Text>
                                      </TouchableOpacity>
                                  </View>

                                    {
                                      item.quantity==0?
                                      <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                          <TouchableOpacity style={{width:'100%',height:25,marginBottom:5}} 
                                          onPress={()=>{ this.selectedProduct(item.id,item.image_url) }} >
                                              <Image source={images.plusIcon} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}/>
                                          </TouchableOpacity>
                                          <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:10,textAlign:'center',color:'#fa7153'}}>ADD</Text>
                                      </View> 
                                      :
                                      <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                          <View style={CartScreenStyle.IncDecMainContainer}>
                                                <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} onPress={()=>{this.decreaseQty(index,selectedVariant)}}>
                                                    <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                                </TouchableOpacity>
                                                
                                                <View style={CartScreenStyle.IncDecQtyContainer}>
                                                <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                                </View>
                                                
                                                
                                                <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} onPress={()=>{this.increaseQty(index)}}>
                                                    <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                                </TouchableOpacity>
                                            </View>
                                      </View> 
                                    }
                              </View>
                          )
                      })
                  }
                  </View>
                  </ScrollView>
                  {
                    this.state.nextPageLoading?
                    <View style={{alignItems:'center',justifyContent:'center',paddingVertical:5}}>
                      <ActivityIndicator/>
                    </View>
                    :
                    null
                  }
            </View>
            :
           <Tabs 
                tabBarUnderlineStyle={styles.TabsStyle} 
                renderTabBar={()=><ScrollableTab/>}
                initialPage={this.state.choosedCategoryIndex}
                //page={this.state.choosedCategoryIndex}
                onChangeTab={(tab)=>{
                    this.changeTabIndex(tab.i)                  
                }}>
                {
                    this.state.categories.map((item,index)=>{
                        return(
                            <Tab heading={item.name} tabStyle={styles.tabStyle} activeTabStyle={styles.tabStyle}  textStyle={styles.tabTextStyle} activeTextStyle={styles.activeTabStyle} style={{backgroundColor:'transparent'}}>        
                              <ScrollView 
                              refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
                              onScroll={({nativeEvent}) => {
                                  if (isCloseToBottom(nativeEvent)) {
                                    this.getNextPageDetails()
                                }
                                }}
                                scrollEventThrottle={0}> 
                               {
                                 this.state.products.length==0?
                                 <View style={{flex:1,alignItems:'center',marginVertical:30}}>
                                   <Text>No products available, added soon.</Text>
                                 </View>
                                 :
                                 null
                               } 
                              <View style={{paddingHorizontal:20,paddingVertical:10}}>
                              {
                                  this.state.products.map((item,index)=>{
                                      return(
                                          <View key={index} style={ProductViewStyle.ProductMainContainer}>
                                              

                                              <View style={ProductViewStyle.ProductView}>

                                                  <View style={ProductViewStyle.ProductImageView}>
                                                      <Image source={{uri:item.image_url}} style={ProductViewStyle.ImageCoverStyle} />
                                                      <View style={ProductViewStyle.ZindexView}>
                                                          <View style={ProductViewStyle.HeartView}>
                                                          {
                                                              item.is_favourite==true?
                                                              <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("remove",index)}>
                                                                 <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageContainStyle} />  
                                                              </TouchableOpacity>
                                                              :
                                                              <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("add",index)}>
                                                                 <Image source={images.favouriteWithoutBackgroud} style={StyleContainer.ImageContainStyle} />
                                                              </TouchableOpacity>
                                                          }
                                                          </View>
                                                      </View>
                                                  </View>

                                                  <TouchableOpacity onPress={()=>this.navigateSingleProductScreen(item.id)}
                                                   style={ProductViewStyle.ProductDetailView}>
                                                          <Text numberOfLines={2}  ellipsizeMode='tail' style={{fontFamily:'Montserrat-SemiBold',fontSize:14,marginBottom:3}}>{item.name}</Text>
                                                          <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:13,marginBottom:3}}>$ {item.default_variant_price.toFixed(2)}</Text>
                                                          {/* {
                                                              this.props.deliveryType==0?
                                                              <Text style={{fontFamily:"Montserrat",color:'#3bcf0a',fontSize:10}}>Delivery available</Text>:
                                                              <Text numberOfLines={2} ellipsizeMode='tail' style={StyleContainer.RedIndicationTxtStyle}>Sorry we do not delivery to this area. Please arrange pick-up in store.</Text>
                                                          } */}
                                                  </TouchableOpacity>
                                              </View>

                                                {
                                                  item.quantity==0?
                                                  <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                                      <TouchableOpacity style={{width:'100%',height:25,marginBottom:5}} 
                                                      onPress={()=>{ this.selectedProduct(item.id,item.image_url) }} >
                                                          <Image source={images.plusIcon} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}/>
                                                      </TouchableOpacity>
                                                      <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:10,textAlign:'center',color:'#fa7153'}}>ADD</Text>
                                                  </View> 
                                                  :
                                                  <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                                      <View style={CartScreenStyle.IncDecMainContainer}>
                                                            <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} onPress={()=>{this.decreaseQty(index,selectedVariant)}}>
                                                                <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                                            </TouchableOpacity>
                                                            
                                                            <View style={CartScreenStyle.IncDecQtyContainer}>
                                                            <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                                            </View>
                                                            
                                                            
                                                            <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} onPress={()=>{this.increaseQty(index)}}>
                                                                <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                  </View> 
                                                }
                                          </View>
                                      )
                                  })
                              }
                              </View>
                              </ScrollView>
                              {
                                this.state.nextPageLoading?
                                <View style={{alignItems:'center',justifyContent:'center',paddingVertical:5}}>
                                  <ActivityIndicator/>
                                </View>
                                :
                                null
                              }
                            </Tab>
                        )
                    })
                }
            </Tabs>
          }
           </Container>
            
           <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={()=>this.setState({modalVisible:false})}>
                <KeyboardAvoidingView keyboardVerticalOffset={20}  behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
                    <View style={ModelStyle.modelContainer}>
                        <TouchableOpacity onPress={()=>this.setState({isediting:false,iseditingValue:0,modalVisible:false})} 
                         style={{marginBottom:10}}>
                            <Image source={images.cancel} style={{width:25,height:25,resizeMode:'cover'}}/>
                        </TouchableOpacity>
                        <View style={{width:'100%',backgroundColor:'#FFF',padding:20,borderRadius:20,marginBottom:-15}}>
                         {
                           this.state.signleProductLoading==true?
                           <View style={{width:'100%',height:'50%',paddingVertical:20}} >
                              <ContentLoader active avatar aShape="square" pRows={2}/>

                              <View style={{marginTop:20,borderWidth:0.5,borderStyle:'dashed',paddingTop:10,marginBottom:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              <View style={{borderWidth:0.5,borderStyle:'dashed',paddingTop:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              {/* <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                </TouchableOpacity> */}
                            </View>
                           :
                           <ScrollView showsVerticalScrollIndicator={false}>
                           
                            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>ADD TO CART</Text>
                               {
                                 this.state.updateproductmodal==true?
                                 <ActivityIndicator/>
                                 :
                                 null
                               }
                            </View> 
                            
                             <View style={ModelStyle.modelProductContainer}>
                                <Image source={{uri:this.state.imageUrl}} style={{width:60,height:60,borderRadius:7}}></Image>
                                <View style={{paddingLeft:20,width:'80%'}}>
                                    <Text numberOfLines={2}  ellipsizeMode="tail" style={[ModelStyle.modelProductName,{fontFamily:"Montserrat-SemiBold"}]}>{this.state.modalResponse.name}</Text>
                                    <Text style={ModelStyle.modelProductPrice}>$ {this.state.prodPrice}</Text>
                                </View>
                            </View>
                           {
                             this.state.productOptions.length==0?
                             null
                             :
                             <View>
                            {
                              this.state.productOptions.map((item,key)=>{
                                return(
                                    <View style={{borderWidth:0.5,paddingBottom:5,borderColor:'gray',borderStyle:'dashed',borderRadius:5,paddingHorizontal:5,marginVertical:5,backgroundColor:'#fff'}}>
                                      <Text style={{marginBottom:3,paddingLeft:5,paddingTop:5}}>{item.type}</Text>
                                      <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                                      {
                                          item.option.map((item,index)=>{
                                            
                                            if(item.selected==true){
                                              if(this.state.combinationStatus==true){
                                                return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:0.3,borderColor:'gray',borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#fa7153',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',color:'#fff',fontFamily:'MontSerrat-SemiBold'}}>{item.value}</Text>
                                                  </TouchableOpacity>     
                                                )
                                              }else{
                                                  return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:1,borderRadius:20,borderColor:'#fa7153',minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#f2f5f6',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                                  </TouchableOpacity>
                                                )
                                              }
                                            }else{
                                              return(
                                              <TouchableOpacity onPress={()=>this.changeProductVariant(key,index)} style={{padding:7,borderWidth:0.5,borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                              </TouchableOpacity>
                                            )
                                            }
                                          })
                                        }
                                      </View>
                                    </View>
                                )
                              })
                            }
                            {
                              this.state.combinationStatus==true?
                              null
                              :
                              <View style={{flexDirection:'row',alignItems:'center',paddingBottom:5}}> 
                                  <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  <Text style={{color:'red',fontSize:12,fontFamily:'MontSerrat',paddingLeft:5}}>Selected combination not available</Text>
                              </View>
                            }
                          </View>
                           }
                            
                          
                          <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,paddingVertical:15,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:"Montserrat",fontSize:15}}>Price :  </Text>
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                </View>
                                <View style={{justifyContent:'center',alignItems:'flex-end'}}>
                                    <View style={CartScreenStyle.IncDecMainContainer}>
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>{this.modalDecrement()}}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                      </TouchableOpacity>
                                      
                                      {
                                        this.state.isediting==true?
                                        <TouchableOpacity style={{width:'30%',justifyContent:'center'}}>
                                        <TextInput
                                          autoFocus={this.state.isediting}
                                          keyboardType={"phone-pad"}
                                          style={{width:'100%',height:24,paddingVertical:0,fontSize:12,fontFamily:"Montserrat",textAlign:'center',color:Colors.themeColor}}
                                          onChangeText={(text)=>this.customQty(text)}
                                          value={this.state.iseditingValue}
                                        />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={()=>this.incdecEdit()} style={{width:'30%',alignItems:'center',justifyContent:'center'}}>
                                          <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                        </TouchableOpacity>
                                      }
                                      
                                      
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>this.modalIncrement()}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                      </TouchableOpacity>
                                  </View>
                                </View>
                            
                          </View>
                            {
                              this.state.combinationStatus==true?
                              <View>
                                {
                                  this.state.addTocartLoading?
                                  <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <ActivityIndicator color="#fff"  />
                                  </TouchableOpacity>
                                  :
                                  <TouchableOpacity onPress={()=>this.addToCartWithVariants()} 
                                  style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                  </TouchableOpacity>
                                }
                              </View>
                              :
                              <View style={{backgroundColor:'#e3b1a6',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                              </View>
                            }
                            </ScrollView> 
                         }
                        </View>
                    </View>
                    </KeyboardAvoidingView>
            </Modal>    
                      
            <Modal 
                animationType="fade"
                transparent={true}
                visible={this.state.addToCartStatus}
                onRequestClose={()=>this.setState({addToCartStatus:false})}>
                <View style={{flex:1,justifyContent:'flex-end'}}>
                    <View style={{backgroundColor:'gray',width:'80%',alignSelf:'center',padding:8,marginBottom:"20%",borderRadius:5}}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',color:'#fff',fontSize:14,paddingBottom:5}}>Item added successfully .</Text>
                    </View>   
                </View>
            </Modal>   

            <Snackbar
              visible={this.state.snackBarVisible}
              onDismiss={()=>this.setState({snackBarVisible:false})}
              duration={5000}
              action={{
                label: 'GO TO CART',
                onPress: () => {
                  //this.setState({snackBarVisible:false})
                  this.props.navigation.navigate("drawer",{screen:"myCart"})
                },
              }}>
              Item added successfully!
            </Snackbar>

            <CustomNotification/>
      </View>
    );
   }
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
      categoryId:state.categoryId,
      productCountChanged:state.productCountChanged,
      categoryProductCountChange:state.categoryProductCountChange,
      cartCount:state.cartCount,
      deliveryType:state.deliveryType,
      categoryUrl:state.categoryUrl,
      categoryName:state.categoryName
  }
}

function mapDispatchToProps(dispatch){
  return{
      setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
      setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
      setIncreaseCartCount:()=>dispatch({type:"IncreaseCartCount"}),
      setDecreaseCartCount:()=>dispatch({type:"DecreaseCartCount"}),
      setProductId:(value)=>dispatch({type:"setProductId",value}),
      setCategoryId:(value)=>dispatch({type:"setCategoryId",value}),
      setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
      setCategoryUrl:(value)=>dispatch({type:"setCategoryUrl",value})
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(CategoryList);


const styles=StyleSheet.create({
  headerContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingHorizontal:20,
    backgroundColor:'#fa7153',
    paddingVertical:15
  },
  headerTitle:{
    fontSize:18,
    color:'#fff',
    fontFamily:'Montserrat-SemiBold'
  },
  TabsStyle:{
    backgroundColor:'#fa7153',
    height:5,
  },
  tabStyle:{
    backgroundColor: '#fff',
  },
  activeTabStyle:{
    color: '#fa7153',
    fontFamily:'Montserrat-SemiBold',
    fontSize:18
  },
  tabTextStyle:{
    color:'#A7A7A7'
  },
})
