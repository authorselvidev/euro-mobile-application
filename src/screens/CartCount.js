import React, { Component } from 'react';
import { View, Text ,Image} from 'react-native';
import images from '../images/index'
import {Colors} from './Styles/colors';
import {connect} from 'react-redux'

class CartCount extends Component {
  render() {
    return (
        <View>
            <Image source={images.trukIconWithoutColor} style={{width:30,height:25,resizeMode:'cover'}}/>
            {
                this.props.cartCount<=0?
                    null
                    :
                    <View style={{position: 'absolute',right: -6,top: -3,backgroundColor: Colors.themeColor,borderRadius: 7,width: 16,height: 15,justifyContent: 'center',alignItems: 'center'}}>
                     {
                         this.props.cartCount<9?
                         <Text style={{fontFamily:'Montserrat-SemiBold', color: Colors.whiteBg, fontSize: 10 }}>{this.props.cartCount}</Text>
                         :
                         <View style={{flexDirection:'row',justifyContent:'center'}}>
                            <Text style={{fontFamily:'Montserrat-SemiBold', color: Colors.whiteBg, fontSize: 10 }}>9</Text>
                            <Text style={{fontFamily:'Montserrat-SemiBold', color: Colors.whiteBg, fontSize: 10,bottom:2 }}>+</Text>
                         </View>
                     }
                    </View>
            }
        </View>
    );
  }
}

function mapStateToProps(state){
    return{
        baseUrl:state.baseUrl,
        cartCount:state.cartCount
    }
}

function mapDispatchToProps(dispatch){
    return{

    }
}
export default connect(mapStateToProps,mapDispatchToProps)(CartCount);

