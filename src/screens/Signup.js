import React, { Component } from 'react';
import { View,Platform,Text,TextInput,ScrollView,Image,TouchableOpacity,KeyboardAvoidingView, ActivityIndicator, SafeAreaView} from 'react-native';
import {Item,Input} from 'native-base'
import images from '../images/index'
import base64 from 'react-native-base64';
//import firebase from "react-native-firebase";
import AsyncStorage from '@react-native-async-storage/async-storage';
import {StyleContainer} from '../screens/Styles/CommonStyles'
import {OverAllStyle,SignUpStyle}from './Styles/PrimaryStyle'
import { SignupModel, SignupResponseModel } from '../DataModel/ModelClass';
import AuthenticationService from '../Controller/ServiceClass';
import {connect} from 'react-redux'
import { Colors } from './Styles/colors';
import CustomNotification from './CustomNotification';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import messaging from '@react-native-firebase/messaging';

class Signup extends Component {
  state = {
  loading:false,
  firstName:'',
  lastName:'',
  businessName:'',
  mobileNumber:'',
  newPassword:'',
  confirmPassword:'',
  emailAddress:'',
  deviceType:'',
  fcmToken:null,

  firstnameError:'',
  lastNameError:'',
  businessNameError:'',
  mobileNumError:'',
  emailError:'',
  newPasswordError:'',
  confirmPasswordError:'',
  guestId:null,
  hideNewPassword:true,
  hideConfirmPassword:true
  };

 async componentDidMount(){
   await this.checkPermission()
    await AsyncStorage.getItem('guestId').then((value) => {
      if(value!=null){
         this.setState({guestId:JSON.parse(value)})
      }
    })
   }

   async checkPermission() {
    const authStatus=await messaging().requestPermission();
       const enabled=
         authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
         authStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if(enabled){
        console.log('Authorization status : ', authStatus);
        }
    this.getToken()
  }

  async getToken() {
    if (!this.state.fcmToken) {
      // Register the device with FCM
      await messaging().registerDeviceForRemoteMessages();

      // Get the token
      const token = await messaging().getToken();

      console.log(token)
      await this.setState({fcmToken:token})
    }
  }

  async requestPermission() {
    // try {
    //   await firebase.messaging().requestPermission();
    //     this.getToken();
    // } catch (error) {
    //   // User has rejected permissions
    //   alert("permission rejected")
    // }
  }

  changeFirstName(value) {
    this.setState({firstName:value})
  }

changeLastName(value){
   this.setState({lastName:value})
}

changeEmailAddress(value){
this.setState({emailAddress:value})
}

changeBusinessName(value){
  this.setState({businessName:value})
}

changeMobileNumber(value){
  this.setState({mobileNumber:value})
}


changeNewPassword(value){
  this.setState({newPassword:value})
}

changeConfirmPassword(value){
  this.setState({confirmPassword:value})
}

checkIsEmpty(value){
  if(value!==''){
    return true
  }else{
    return false
  }
}


async validateForm(){

  {Platform.OS === 'ios' ? await this.setState({deviceType:'ios'}) : await this.setState({deviceType:'android'})}
  //console.log(this.state.deviceType)
  let numPattern=/^0[0-8]\d{8}$/g;
  let namePattern=/^[a-zA-Z]+$/;
  let emailPattern= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
  let firstName,lastName,email,mobileNum,passsword,businessName=true;
          
          if(this.state.firstName==""){
            this.setState({firstnameError:'First name should not be empty'})
            firstName=false
          }else{
            this.setState({firstnameError:''})
            firstName=true
          }
          
          // if(this.checkIsEmpty(this.state.firstName)){
          //     if(namePattern.test(this.state.firstName)){
          //       this.setState({firstnameError:''})
          //       firstName=true
          //     }
          //     else{
          //       this.setState({firstnameError:'Only lettes are allowed'})
          //       firstName=false
          //     }
          // }
          // else
          // {
          //   this.setState({firstnameError:'First name should not be empty'})
          //   firstName=false
          // }

          // if(this.checkIsEmpty(this.state.lastName))
          // {
          //   if(namePattern.test(this.state.lastName))
          //   {
          //     this.setState({lastNameError:''})
          //     lastName=true
          //   }
          //   else{
          //     this.setState({lastNameError:'Only lettes are allowed'})
          //     lastName=false
          //   }
          // }
          // else{
          //   this.setState({lastNameError:''})
          //   lastName=true
          // }
          
          if(this.checkIsEmpty(this.state.emailAddress))
          {
            if(emailPattern.test(this.state.emailAddress))
            {
              this.setState({emailError:''})
              email=true
            }
            else{
              this.setState({emailError:'Check the Email format '})
              email=false
            }
          }
          else
          {
            this.setState({emailError:'Email Address should not be empty'})
            email=false
          }

          if(this.checkIsEmpty(this.state.mobileNumber)){
            if(this.state.mobileNumber.length<10){  
              this.setState({mobileNumError:'Looks like the entered mobile number is invalid'}) 
              mobileNum=true 
            }
            else { 
              this.setState({mobileNumError:''})
              mobileNum=true 
            }

            // if(numPattern.test(this.state.mobileNumber)){             
            //   this.setState({mobileNumError:''})
            //   mobileNum=true
            // }
            // else{
            //   this.setState({mobileNumError:'Looks like the entered mobile number is invalid'})
            //   mobileNum=false
            // }
          }
          else{
            this.setState({mobileNumError:'Mobile number should not be empty'})
            mobileNum=false
          }

          if(this.checkIsEmpty(this.state.businessName)){
            this.setState({businessNameError:''})
            businessName=true
          }
          else{
            this.setState({businessNameError:'Business name should not be empty'})
            businessName=false
          }

          
          if(this.checkIsEmpty(this.state.newPassword)){
            if(this.state.newPassword.length>=6){
              this.setState({newPasswordError:''})
              passsword=true
            }
            else{
              this.setState({newPasswordError:'Password should be 6 digits'})
              passsword=false
            }
          }
          else{
            this.setState({newPasswordError:'Password should not be empty'}),
            passsword=false
          }

          if(this.checkIsEmpty(this.state.confirmPassword)){
            this.setState({confirmPasswordError:''})
            passsword=true
          }
         else
          {
            this.setState({confirmPasswordError:'Confirm password should not be empty'})
            passsword=false
          }

          if(this.state.confirmPassword==this.state.newPassword){
            passsword=true
          }
          else{
            this.setState({confirmPasswordError:'Both passwords should be same'})
            passsword=false
          }

          if(firstName==true&&email==true&&mobileNum==true&&passsword==true&&businessName==true)
          {
            console.log('All oK')
            // var newPassword=base64.encode(this.state.newPassword);
            // var confirmPassword=base64.encode(this.state.confirmPassword);
            var newPassword=this.state.newPassword;
            var confirmPassword=this.state.confirmPassword;
            await this.setState({
              newPassword:newPassword,
              confirmPassword:confirmPassword
            })
            //console.log(this.state.deviceType)
            this.postData()

          }
          else{
            console.log('check the whole form')
          }

        }

 
 async postData(){
   //console.log(this.state.fcmToken)
   SignupModel.guestId=this.state.guestId
   SignupModel.firstName=this.state.firstName
   SignupModel.lastName=this.state.lastName
   SignupModel.email=this.state.emailAddress
   SignupModel.mobileNo=this.state.mobileNumber
   SignupModel.businessName=this.state.businessName
   SignupModel.newPassword=this.state.newPassword
   SignupModel.confirmPassword=this.state.confirmPassword
   SignupModel.deviceType=this.state.deviceType
   SignupModel.deviceToken=this.state.fcmToken

   await this.setState({loading:true})
   await AuthenticationService.signup()
   await this.setState({loading:false})
   //console.log("userId"+SignupResponseModel.userId)

   if(SignupResponseModel.status){
    await AsyncStorage.setItem('userId',JSON.stringify(SignupResponseModel.userId)) 
    await this.props.setuserId(SignupResponseModel.userId)
    //console.log("Signup ID"+ this.props.userId)
     this.props.navigation.navigate("createNewSite",{isFromSignup:1})
     this.props.setguestToUserChanged(true)
     this.props.setisShippingAddress(true)
     //this.props.navigation.navigate("signupPostcode")
   }else{
    await this.setState({emailError:SignupResponseModel.errorMessage.email})
    //console.log(SignupResponseModel.errorMessage.email)
   }
  }

  render() {
    return (
      
          <SafeAreaView style={{flex:1,backgroundColor:'#fff',paddingTop:'10%'}}>
            {/* <KeyboardAvoidingView keyboardVerticalOffset={80} behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}> */}
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
            <ScrollView showsVerticalScrollIndicator={false} style={{flex:1}}>
            
                <View style={StyleContainer.MainContainer}>           
                    <View style={OverAllStyle.LogoView}>        
                        <Image source={images.logo} style={StyleContainer.ImageContainStyle}/>
                    </View>

                    
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:20,textAlign:'justify',marginVertical:10}}>New customer - Sign up</Text>
                                      

                        <View style={StyleContainer.TextInputMainView}>
                        <TextInput style={StyleContainer.TextInputView} placeholder="First Name *"                      
                            ref={(input) => { this.firstNameRef = input; }}
                            onSubmitEditing={() => {
                            this.lastNameRef.focus();
                            }}
                            onChangeText={(value)=>{this.changeFirstName(value)}} 
                            returnKeyType={"next"}
                          />
                          {
                          this.state.firstnameError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.firstnameError}</Text>
                            </View>
                          }
                        </View>
                        
                        
                        <View style={StyleContainer.TextInputMainView}>
                        <TextInput style={StyleContainer.TextInputView} placeholder="Last Name"
                            ref={(input) => { this.lastNameRef = input; }}
                            onSubmitEditing={() => {
                            this.emailRef.focus();}}
                            onChangeText={(value)=>{this.changeLastName(value)}} 
                            returnKeyType={"next"}
                          />
                          {
                            this.state.lastNameError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.lastNameError}</Text>
                            </View>
                          }
                        </View>
                        

                        <View style={StyleContainer.TextInputMainView}>
                        <TextInput style={StyleContainer.TextInputView} placeholder="Email Address *"
                            ref={(input) => { this.emailRef = input; }}
                            onSubmitEditing={() => {
                            this.mobileNumRef.focus();}}
                            onChangeText={(value)=>{this.changeEmailAddress(value)}} 
                            returnKeyType={"next"}
                            keyboardType="default"
                          />
                          {
                            this.state.emailError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.emailError}</Text>
                            </View>
                          }
                        </View>
                        
                        <View style={StyleContainer.TextInputMainView}>
                        <TextInput style={StyleContainer.TextInputView} placeholder="Mobile Number *"
                            ref={(input) => { this.mobileNumRef = input; }}
                            onSubmitEditing={() => {
                            this.businessNameRef.focus();}}
                            onChangeText={(value)=>{this.changeMobileNumber(value)}} 
                            returnKeyType={"next"}
                            maxLength={10}
                            keyboardType="number-pad"
                          />
                          {
                            this.state.mobileNumError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.mobileNumError}</Text>
                            </View>
                          }
                        </View>
                        
                        <View style={StyleContainer.TextInputMainView}>
                        <TextInput style={StyleContainer.TextInputView} placeholder="Business Name *"
                            ref={(input) => { this.businessNameRef = input; }}
                            onSubmitEditing={() => {
                            this.newPasswordRef.focus();}}
                            onChangeText={(value)=>{this.changeBusinessName(value)}} 
                            returnKeyType={"next"}
                          />
                          {
                          this.state.businessNameError ==''?null:
                          <View style={{width:'100%'}}>
                          <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.businessNameError}</Text>
                          </View>
                        }
                        </View>


                <View style={{width:'100%',marginVertical:20}}>
                            
                <View style={{flexDirection:'row',width:'100%',marginBottom:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr}}>
                    <TextInput 
                    style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,
                          fontSize:14,fontFamily:'Montserrat',paddingLeft:20}} 
                                placeholder="New Password *"
                                ref={(input) => { this.newPasswordRef = input; }}
                                onSubmitEditing={() => {
                                this.confirmPasswordRef.focus();}}
                                onChangeText={(value)=>{this.changeNewPassword(value)}} 
                                value={this.state.newPassword}
                                returnKeyType={"next"}
                                secureTextEntry={this.state.hideNewPassword}
                                textContentType="oneTimeCode"
                              />
                              <TouchableOpacity onPress={()=>this.setState({hideNewPassword:!this.state.hideNewPassword})}>
                                {
                                  this.state.hideNewPassword==true?
                                  <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  :
                                  <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                }
                                
                              </TouchableOpacity>
                          </View>
                          <View style={{alignSelf:'flex-start',marginTop:-8}}>
                              {
                                this.state.newPasswordError==''?null:
                                <View style={{width:'100%'}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.newPasswordError}</Text>
                                </View> 
                              }
                          </View>

                          <View style={{flexDirection:'row',width:'100%',marginBottom:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,marginTop:10}}>
                            <TextInput 
                            style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,
                                  fontSize:14,fontFamily:'Montserrat',paddingLeft:20}}     
                                placeholder="Confirm Password *"
                                secureTextEntry={this.state.hideConfirmPassword}    
                                value={this.state.confirmPassword}                
                                ref={(input) => { this.confirmPasswordRef = input; }}
                                onSubmitEditing={() => {
                                this.validateForm()
                                }}
                                onChangeText={(value)=>{this.changeConfirmPassword(value)}} 
                                returnKeyType={"done"}
                                textContentType="oneTimeCode"
                              />
                              <TouchableOpacity onPress={()=>this.setState({hideConfirmPassword:!this.state.hideConfirmPassword})}>
                                {
                                  this.state.hideConfirmPassword==true?
                                  <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  :
                                  <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                                }
                                
                              </TouchableOpacity>
                            </View>
                            <View style={{alignSelf:'flex-start',marginTop:-8}}>
                              {
                                this.state.confirmPasswordError==''?null:
                                <View style={{width:'100%'}}>
                                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.confirmPasswordError}</Text>
                                </View>
                              }
                            </View>

                </View>

                {
                  this.state.loading?
                  <TouchableOpacity style={StyleContainer.ButtonOrange}>
                      <ActivityIndicator color="#fff"/>
                  </TouchableOpacity>      
                  :
                  <TouchableOpacity onPress={()=>{this.validateForm()}} style={StyleContainer.ButtonOrange}>
                      <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:17,}}>NEXT</Text>
                  </TouchableOpacity>                              
                }

                </View>

                
           </ScrollView>
           {/* </KeyboardAvoidingView> */}
           </KeyboardAwareScrollView>
           <CustomNotification/>
           </SafeAreaView>
  
    );
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
  }
}

function mapDispatchToProps(dispatch){
  return{
      setuserId:(value)=>dispatch({type:'setuserId',value}),
      setisShippingAddress:(value)=>dispatch({type:"setisShippingAddress",value}),
      setguestToUserChanged:(value)=>dispatch({type:"setguestToUserChanged",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Signup);
