import React, { Component } from 'react';
import { View, Text , Image ,TextInput ,TouchableOpacity,ActivityIndicator,ScrollView,StyleSheet} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import images from '../images/index'
import {OverAllStyle,ShopByCategoryStyle} from './Styles/PrimaryStyle';
import {StyleContainer} from '../screens/Styles/CommonStyles'
import {connect} from 'react-redux'
import {baseUrl} from '../Controller/baseUrl'
import CustomNotification from './CustomNotification';

class ShopByCategory extends Component {
  
  state = {
    loading:false,
    popularCategories:[],
    allCategoriesArrHolder:[],
    allCategoriesArr:[],
    searchValueText:'',
    showTwoPart:true,
    networkFailed:false,
    };
  

    componentDidMount(){
      this.getCategoryDetail()
    }

    async getCategoryDetail(){
  
      NetInfo.fetch().then(state => {
        //console.log("Connection type", state.type);
        if(state.isConnected==true){
                this.setState({loading:true})
                fetch(baseUrl+this.props.userId+'/allcategories')
                .then((response) => response.json())
                 .then((responseJson) => {
                      console.log(responseJson)
                      this.setState({
                        popularCategories:responseJson.popular_categories,
                        allCategoriesArr:responseJson.all_categories,
                        allCategoriesArrHolder:responseJson.all_categories
                      })
                      this.setState({loading:false})
                }) 
                .catch((err)=>{
                  //console.log(err)
              })  
          }
        else{
          this.setState({networkFailed:true})
          alert("Check network connection")
        }
      })            
   }

    SearchFilterFunction(text) {
      if(text.length>0){
        this.setState({showTwoPart:false})
      }
      else{
        this.setState({showTwoPart:true})
      }
      const newData = this.state.allCategoriesArrHolder.filter(item=> {
        const itemData =item.name.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) >= 0;
      });
      this.setState({allCategoriesArr : newData,searchValueText:text});
    }

    setCategoryId(url){
      //console.log(categoryId)
      console.warn(url)
      this.props.setCategoryUrl(url)
      this.props.navigation.navigate("categoryList")
    }

  render() {
    if(this.state.loading==true){
      return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
          <ActivityIndicator color="#fa7153" size="large"/>
          <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
      </View>
      )
     }else{
    return (
      <View style={OverAllStyle.MainContainerPad0}>

        <View style={ShopByCategoryStyle.HeaderStyle}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("homeScreen")} style={ShopByCategoryStyle.HeaderImgMainView}>
              <View style={ShopByCategoryStyle.HeaderImgInnerView}>
              <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
              </View>  
            </TouchableOpacity>
            <View style={ShopByCategoryStyle.HeaderTitleView}>
              <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:17,textAlign:'center'}} >Shop by Category</Text>
            </View>
            
        </View>

        <View>

            <View style={{width:'100%',backgroundColor:'#fa7153',height:30}}> 
            </View>
            
            
              <View style={OverAllStyle.SearchBarStyle}>
                  
                  <View style={ShopByCategoryStyle.SearchBarImgMainView}>
                      <View style={ShopByCategoryStyle.SearchBarImgInnerView}>
                          <Image source={images.searchIconWithoutColor} style={StyleContainer.ImageContainStyle} />
                      </View>
                  </View>

                  <View style={ShopByCategoryStyle.SearchTxtInputView}>
                      <TextInput placeholder='Search' placeholderTextColor='#BBB' style={{fontSize:15,fontFamily:'Montserrat'}}
                      onChangeText={(text)=>{this.SearchFilterFunction(text)}}
                      value={this.state.searchValueText}
                      />
                  </View>

              </View>
            

            
            
        </View>

      <ScrollView>

      {
        this.state.showTwoPart==true?
        
        <View>
                  {
                    this.state.popularCategories.length==0?null:
                    <View>
                        <View style={ShopByCategoryStyle.CategoryheadingView}>
                            <Text style={Styles.CategoryHeadingStyle}>Most popular categories</Text>
                        </View>
                        <View>
                        {
                        this.state.popularCategories.map((item,index)=>{
                          return(
                            <TouchableOpacity style={ShopByCategoryStyle.ItemViewStyle}
                            onPress={()=>this.setCategoryId(item.url)}>
                              
                              <View style={ShopByCategoryStyle.ItemTxtView}>
                                  <Text style={Styles.ItemTextStyle}>{item.name}</Text>
                              </View>
    
                              <View style={{width:'10%',height:15}}>
                                  
                                      <Image source={images.nextButton} style={StyleContainer.ImageContainStyle}/>
                                  
                              </View>
    
                                
                            </TouchableOpacity>
                          )
                        })}
                      </View>
                    </View>
                  }


                  <View style={ShopByCategoryStyle.CategoryheadingView}>
                          <Text style={Styles.CategoryHeadingStyle}>All categories</Text>
                  </View>

                  <View>
                    {this.state.allCategoriesArr.map((item,index)=>{
                      return(
                        <TouchableOpacity style={ShopByCategoryStyle.ItemViewStyle}
                        onPress={()=>this.setCategoryId(item.url)}>
                          
                        <View style={ShopByCategoryStyle.ItemTxtView}>
                            <Text style={Styles.ItemTextStyle}>{item.name}</Text>
                        </View>

                        <View style={ShopByCategoryStyle.ImgArrow}>
                            
                                <Image source={images.nextButton} style={StyleContainer.ImageContainStyle}/>
                            
                        </View>

                          
                      </TouchableOpacity>
                      )
                    })}
                  </View>
        </View>:
        
        <View>
                <View style={ShopByCategoryStyle.CategoryheadingView}>
                          <Text style={Styles.CategoryHeadingStyle}>Categories</Text>
                  </View>

                  <View>
                    {this.state.allCategoriesArr.map((item,index)=>{
                      return(
                        <TouchableOpacity style={ShopByCategoryStyle.ItemViewStyle}
                        onPress={()=>this.setCategoryId(item.url)}>
                          
                        <View style={ShopByCategoryStyle.ItemTxtView}>
                            <Text style={Styles.ItemTextStyle}>{item.name}</Text>
                        </View>

                        <View style={{width:'10%',height:15}}>
                            
                                <Image source={images.nextButton} style={StyleContainer.ImageContainStyle}/>
                            
                        </View>

                          
                      </TouchableOpacity>
                      )
                    })}
                  </View>
           
        </View>
      }           
                  
      </ScrollView>
        
        <CustomNotification/>
      </View>
    );
   }
  }
}
function mapStateToProps(state){
  return{
      userId:state.userId
  }
}

function mapDispatchToProps(dispatch){
  return{
      setFetchLoading:(value)=>dispatch({type:'setFetchLoading',value}),
      setCategoryUrl:(value)=>dispatch({type:"setCategoryUrl",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ShopByCategory);


const Styles=StyleSheet.create({
   
  CategoryHeadingStyle:{
    fontSize:17,
    fontFamily:'Montserrat-SemiBold'
  },
  ItemTextStyle:{
    fontSize:13,
    color:'#acacac',
    fontFamily:'Montserrat'
  }
})
