import React, { Component } from 'react';
import { View, Text,SafeAreaView,TouchableOpacity,Image, TextInput,ScrollView,StyleSheet, ActivityIndicator} from 'react-native';
import images from '../images/index';
import {StyleContainer} from '../screens/Styles/CommonStyles'
import {HomeScreenStyle, OverAllStyle,SearchScreenStyle} from './Styles/PrimaryStyle';
import {baseUrl} from '../Controller/baseUrl'
import {connect} from 'react-redux'
import CustomNotification from './CustomNotification';
import { AndroidBackHandler } from "react-navigation-backhandler";
import { SkypeIndicator } from 'react-native-indicators';
import NetInfo from '@react-native-community/netinfo';

class SearchScreen extends Component {
  
  state = {
    loading:false,
    products:[],
    searchValueText:'',
    showTwoPart:true,
    networkFailed:false
    };

   componentDidMount(){
      this.checkNetwork()
  }

  async checkNetwork(){
    await NetInfo.fetch().then(state => {
      console.log(state)
      if(state.isConnected==true){
        this.setState({networkFailed:false})
      }else{
        this.setState({networkFailed:true})
      }
  })
  // console.warn("network "+ this.state.networkFailed)
   }

   async SearchFilterFunction(text) {
     await this.setState({searchValueText:text})
       this.checkNetwork()
       if(text.toString().length>=2 || text.toString().length==0){
       this.setState({loading:true})
       fetch(baseUrl+this.props.userId+"/search", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "text":this.state.searchValueText            
        })

        })
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({products:responseJson.data,loading:false})
          console.log("response "+ responseJson)
        })
      }
    }

   async searchedProduct(id){
     await this.props.setProductId(id)
      this.props.navigation.navigate("singleProductScreen")
    }

    onBackButtonPressAndroid = async() => {
     await this.setState({searchValueText:"",products:[]})
      this.props.navigation.goBack()
      // return true;
    };

  retry(){
    this.checkNetwork()
  }
    
  render() {
    if(this.state.networkFailed==true){
      return(              
             <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                 <Image source={images.networkFailed} style={{width:70,margin:8,height:100,resizeMode:'contain'}}/>
                 <Text style={{fontFamily:"Montserrat",fontSize:17,color:'black',paddingBottom:10}}>Connection Error</Text>
                 <Text style={{fontFamily:"Montserrat"}}>Please check your network and try again.</Text>
                 <TouchableOpacity onPress={()=>this.retry()} style={{marginVertical:20,borderColor:'#fa7153',borderWidth:1,paddingHorizontal:30,paddingVertical:5,borderRadius:5}}>
                   <Text style={{fontFamily:"Montserrat",color:'#fa7153',fontSize:16}}>Retry</Text>
                 </TouchableOpacity>
             </View>
      )
    }else{
    return (
      <View style={SearchScreenStyle.MainContainerPad0}>
        
        <SafeAreaView style={OverAllStyle.HeaderMainView}>
            <View style={OverAllStyle.HeaderStyle}>

            <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"home"})} style={OverAllStyle.HeaderIcon}>
               <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
            </TouchableOpacity>
                <Text style={{fontFamily:"Montserrat-SemiBold",color: '#FFF',fontSize:17,}}>Search</Text>
                <TouchableOpacity style={{justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("notification")} style={HomeScreenStyle.MenuIcon}>
                      {
                        this.props.notificationCount==0?
                          null
                          :
                          /* <View style={{width:15,height:15,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:10,left:10}}>
                          <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text>
                          </View>                             */
                          <View style={{width:7,height:7,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:12,left:12}}>
                          {/* <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text> */}
                          </View>                            
                      }
                    <Image source={images.bell} style={StyleContainer.ImageContainStyle}></Image>
                  </TouchableOpacity>
            
            </View>
            
        </SafeAreaView>
        
        

                <View style={SearchScreenStyle.EmptyView}/> 

                  <View style={OverAllStyle.SearchBarStyle}>
                      
                      <View style={SearchScreenStyle.SearchBarImgMainView}>
                          <View style={SearchScreenStyle.SearchBarImgInnerView}>
                              <Image source={images.searchIconWithoutColor} style={StyleContainer.ImageContainStyle} />
                          </View>
                      </View>

                      <View style={SearchScreenStyle.SearchBarTextView}>
                          <TextInput placeholder='Search'
                           autoFocus={true}
                            placeholderTextColor='#BBB' style={{fontSize:15,fontFamily:'Montserrat'}}
                            onChangeText={(text)=>{this.SearchFilterFunction(text)}}
                            value={this.state.searchValueText}
                            />
                      </View>

                  </View>

                  {
                    this.state.loading?
                    <View style={{marginTop:30}}>
                       {/* <ActivityIndicator color="#fa7153"/> */}
                       <SkypeIndicator color="#fa7153" />
                    </View>
                    :
                    null
                  }

        <ScrollView keyboardShouldPersistTaps='always' keyboardDismissMode='on-drag'>
        <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
            <View style={SearchScreenStyle.ItemMainView}>
               
                {
                  this.state.products.length==0 || this.state.products=="NO DATA FOUND"?null
                  :
                  <View>
                  {
                    this.state.products.map((data,index)=>{
                      return(
                        <TouchableOpacity onPress={()=>this.searchedProduct(data.id)} style={SearchScreenStyle.ItemViewStyle}>

                        <View style={SearchScreenStyle.ItemSearchImgMainView}>
                            <View style={SearchScreenStyle.ItemSearchImgInnerView}>
                                <Image source={images.searchIconWithoutColor} style={StyleContainer.ImageContainStyle} />
                            </View>
                        </View>

                            <View style={SearchScreenStyle.ItemTxtView}>
                                <Text style={SearchScreenStyle.ItemTextStyle}>{data.name}</Text>
                            </View>

                            <View style={SearchScreenStyle.ItemArrowImgMainView}>
                                <View style={SearchScreenStyle.ItemArrowImgInnerView}>
                                  <Image source={images.nextButton} style={StyleContainer.ImageContainStyle} />
                                </View>
                            </View>
                        

                        </TouchableOpacity>
                      )
                    })
                  }
                  </View>
                }
            </View>

              </AndroidBackHandler>
        </ScrollView>

        <CustomNotification/>
  </View>

    );
   }
  }
}


function mapStateToProps(state){
  return{
      userId:state.userId,
      notificationCount:state.notificationCount
  }
}

function mapDispatchToProps(dispatch){
  return{
    setProductId:(value)=>dispatch({type:"setProductId",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(SearchScreen);
