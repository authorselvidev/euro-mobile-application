import React, { Component } from 'react';
import { View,Image,Text,SafeAreaView,TouchableOpacity,ScrollView,RefreshControl, ActivityIndicator, Platform} from 'react-native';
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,MyOrders, HomeScreenStyle} from './Styles/PrimaryStyle'
import {baseUrl} from '../Controller/baseUrl'
import {connect} from 'react-redux'
import NetInfo from '@react-native-community/netinfo';
import CustomNotification from './CustomNotification';
 
const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
  const paddingToBottom = 20;
  return layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom;
};

class OrderScreen extends Component {
  
    state = {
        loading:false,
        orderDetail:[],
        networkFailed:false,
        refreshing:false,
        page:1,
        nextPageAvailable:true,
        nextPageLoading:false,
        selectedTab:0,
        daysBtPlacedAndCurrent:null,
        daysBtPlacedAndDelivry:null,
        showingActivityIndicator:true,
        showingConfimationAlert:true,
        waitingForUpcomingOrder:false,
        orderHistoryIsEmpty:false,
        confimationalertshowd:false
    };
  
    componentDidMount(){
        this.getOrderDetails()
      }

   async componentDidUpdate(prevProps){
        if(this.props.orderCountChanged==true){
         await this.props.setOrderCountChanged(false)
          this.getOrderDetails()
          console.log("order screen updated")
          this.setState({showingConfimationAlert:false})
        }
    }
    

   async getOrderDetails(){
    // await this.props.setOrderCountChanged(false)
    console.log("order screen count"+ this.props.orderCountChanged)
    console.log("getting order detail")
    await this.setState({selectedTab:0}) 
    await NetInfo.fetch().then(state => {
        //console.log("Connection type", state.type);
        if(state.isConnected==true){
             this.setState({loading:true})
             fetch(baseUrl+this.props.userId+"/my-orders")
                .then((response)=>response.json())
                .then((responseJson)=>{
                    console.log(responseJson.orders)
                   
                    
                    this.setState({
                      orderDetail:responseJson.orders,
                      loading:false,
                      refreshing:false,
                      networkFailed:false,
                      page:1,
                      nextPageAvailable:true
                    })
                    this.response(this.state.orderDetail)
                })
                //.catch((err)=>//console.log(err))
        }else{
            this.setState({networkFailed:true})
        }
      })
      await setTimeout(()=>{
        this.setState({showingActivityIndicator:false})
     },2000)
    }

   async response(orderDetail){
    //  console.log(orderDetail)
    await orderDetail.map((item,index)=>{
        if(item.order_request==0){
          if(this.state.confimationalertshowd==false){
            this.setState({showingConfimationAlert:false,showingActivityIndicator:false})  
          }else{
            this.setState({showingConfimationAlert:false,showingActivityIndicator:false})
          }
        }else if((item.order_request==1 || item.order_request==2 || item.order_request==3 || item.order_request==4 || item.order_request==5 ) && (this.state.selectedTab==1) ){
          this.setState({
            showingConfimationAlert:false,
            showingActivityIndicator:false,
          })
        }else{
          this.setState({
            showingConfimationAlert:false
          })
          //this.setState({waitingForUpcomingOrder:true,orderHistoryIsEmpty:true})
        } 
      })
      
      // await this.props.setOrderCountChanged(false)
      setTimeout(()=>{
        this.setState({showingActivityIndicator:false,confimationalertshowd:true})
      },1500)
    }
   async navigateMyOrderDetails(data){
       await this.props.setorderId(data.id)
       await this.props.setOrderPlacedTime(data.time_different)
       await this.props.setisFromOrderScreen(true)
        //console.log(this.props.orderId)
        this.props.navigation.navigate("myOrderDetails")
    } 

  async navigateOrderCancelling(id,orderReturn){
    await this.props.setOrderReturn(orderReturn)
    await this.props.setorderId(id)
    this.props.navigation.navigate("orderCancellingReturning")
  }

  _onRefresh = () => {
    //console.log("onrefress")
    this.setState({refreshing: true});
    this.getOrderDetails()
  }
  
  retry(){
    //console.log("retry")
    this.setState({networkFailed:false})
    this.getOrderDetails()
  }

  async getNextPageDetails(){
    if(this.state.nextPageAvailable==true){
    await this.setState({page:this.state.page+1,nextPageLoading:true})
    await fetch(baseUrl+this.props.userId+'/my-orders?page='+this.state.page)
    .then((response)=>response.json())
    .then((responseJson)=>{
      this.setState({
        orderDetail:this.state.orderDetail.concat(responseJson.orders),
        nextPageAvailable:responseJson.is_load,
        nextPageLoading:false
      })
      this.response(this.state.orderDetail)
    })
  }
  }
  
  componentWillUnmount(){
    // if(this.state.showingActivityIndicator==false){
      this.setState({showingActivityIndicator:false,showingConfimationAlert:false,selectedTab:0})
      // console.log("unmount")
    // }
   }


  render() {
    if(this.state.networkFailed==true){
        return(              
               <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                   <Image source={images.networkFailed} style={{width:70,margin:8,height:100,resizeMode:'contain'}}/>
                   <Text style={{fontFamily:"Montserrat",fontSize:17,color:'black',paddingBottom:10}}>Connection Error</Text>
                   <Text style={{fontFamily:'Montserrat'}}>Please check your network and try again.</Text>
                   <TouchableOpacity onPress={()=>this.retry()} style={{marginVertical:20,borderColor:'#fa7153',borderWidth:1,paddingHorizontal:30,paddingVertical:5,borderRadius:5}}>
                     <Text style={{fontFamily:"Montserrat",color:'#fa7153',fontSize:16}}>Retry</Text>
                   </TouchableOpacity>
               </View>
        )
      }else if(this.state.loading==true){
          return(
              <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <ActivityIndicator size="large" color="#fa7153"/>
                  <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
              </View>
          )
      }else if(this.state.orderDetail.length==0){
          return(
            <View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}>
              <View style={{width:'80%',height:200}}>
                <Image source={images.emptyCart} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}></Image>
              </View>          
                <Text style={{fontFamily:"Montserrat",textAlign:'center'}}>No orders available!</Text>
            
                <Text style={{fontFamily:"Montserrat",paddingVertical:10,fontSize:12}}>Add items to it now.</Text>
            <View style={{width:'80%'}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:'home'})}
                  style={StyleContainer.ButtonOrange}>
                  <Text style={{fontFamily:"Montserrat",color:'#fff'}}>Shop now</Text>
                </TouchableOpacity>
              </View>
            </View>
          )
      }else{
        return (
            <View style={OverAllStyle.MainContainerPad0}>
                
                <SafeAreaView style={OverAllStyle.HeaderMainView}>
                <View style={OverAllStyle.HeaderStyle}>
    
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"home"})} style={OverAllStyle.HeaderIcon}>
                  <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>
                    <Text style={{fontFamily:"Montserrat-SemiBold",color: '#FFF',fontSize:17,}}>My Orders</Text>
                <TouchableOpacity style={{justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("notification")} style={HomeScreenStyle.MenuIcon}>
                      {
                        this.props.notificationCount==0?
                          null
                          :
                          /* <View style={{width:15,height:15,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:10,left:10}}>
                          <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text>
                          </View>                             */
                          <View style={{width:7,height:7,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:12,left:12}}>
                          {/* <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text> */}
                          </View>                            
                      }
                    <Image source={images.bell} style={StyleContainer.ImageContainStyle}></Image>
                  </TouchableOpacity>
                
                </View>
                
            </SafeAreaView>
            
                
                  {
                    this.state.selectedTab==0?
                      <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
                        <TouchableOpacity onPress={()=>this.setState({selectedTab:0})} style={{alignItems:'center',width:'48%',borderBottomWidth:4,borderBottomColor:'#fa7153',padding:15}}>
                          <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fa7153'}}>UPCOMING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setState({selectedTab:1})} style={{alignItems:'center',width:'48%',padding:15}}>
                          <Text style={{fontFamily:'Montserrat'}}>HISTORY</Text>
                        </TouchableOpacity>
                      </View>
                    :
                    <View style={{flexDirection:'row',backgroundColor:'#fff'}}>
                        <TouchableOpacity onPress={()=>this.setState({selectedTab:0})} style={{alignItems:'center',width:'48%',padding:15}}>
                          <Text style={{fontFamily:'Montserrat'}}>UPCOMING</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setState({selectedTab:1})} style={{alignItems:'center',width:'48%',borderBottomWidth:4,borderBottomColor:'#fa7153',padding:15}}>
                          <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fa7153'}}>HISTORY</Text>
                        </TouchableOpacity>
                      </View>
                  }
                  
                <ScrollView
                    refreshControl={ <RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/>}
                    onScroll={({nativeEvent}) => {
                      if (isCloseToBottom(nativeEvent)) {
                        this.getNextPageDetails()
                    }
                    }}
                    scrollEventThrottle={0}>

                    {
                      this.state.selectedTab==0?
                      <View>
                        {
                            this.state.showingConfimationAlert==true?
                              <View style={{width:'95%',alignSelf:'center',backgroundColor:'#fff',padding:10,margin:10,borderRadius:5,borderWidth:0.5,borderStyle:'dashed'}}>
                                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                  <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16}}>Confirming order with the store</Text> 
                                  
                                  {
                                    this.state.showingActivityIndicator==true?
                                    <ActivityIndicator/>
                                    :
                                    <TouchableOpacity onPress={()=>this.setState({showingConfimationAlert:false})}>
                                      <Text style={{color:'red'}}>close</Text>
                                    </TouchableOpacity>
                                  }
                                  
                                </View>
                                
                                <Text style={{fontFamily:'Montserrat',fontSize:14,paddingLeft:8,paddingTop:10}}>Please wait...your order may take up to 5 minutes to confirm.</Text>
                              </View>
                              :
                              null
                          }

                      </View>
                      :
                      null
                    }
                    

                    {
                      this.state.selectedTab==0?
                      <View>
                        {
                            this.state.orderDetail.map((data,index)=>{
                              if(data.order_request==0 || data.order_request==1 || data.order_request==2){
                                return(
                                    
                                    <View key={index} style={{marginBottom:10}}>
                                      {
                                        data.order_request==0?
                                          <View style={MyOrders.InProgressBanner}>
                                            <View style={MyOrders.InsideProgress}>
                                                <View style={{flexDirection:'row',alignItems:'center'}}>
                                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                                {
                                                  data.order_request==0?null:
                                                    <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',fontSize:10}}> ( {data.order_status_text} )</Text>
                                                }
                                                
                                                </View>
                                                {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text> */}
                                              
                                                  {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.time_different}</Text> */}
                                              
                                            </View>
                                          </View>
                                          :
                                          <View>
                                            {
                                              data.order_request==1?
                                              <View style={MyOrders.completed}>
                                                <View style={MyOrders.InsideProgress}>
                                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                                    {
                                                      data.order_request==0?null:
                                                        <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',fontSize:10}}> ( {data.order_status_text} )</Text>
                                                    }
                                                    
                                                    </View>
                                                    {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text> */}
                                                  
                                                      {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.time_different}</Text> */}
                                                  
                                                </View>
                                              </View>
                                              :
                                              <View>
                                            {
                                              data.order_request==2?
                                              <View style={MyOrders.completed}>
                                                <View style={MyOrders.InsideProgress}>
                                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                                    {
                                                      data.order_request==0?null:
                                                        <Text style={{color:'#fff',fontFamily:'Montserrat-SemiBold',fontSize:10}}> ( {data.order_status_text} )</Text>
                                                    }
                                                    
                                                    </View>
                                                    {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text> */}
                                                  
                                                      {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.time_different}</Text> */}
                                                  
                                                </View>
                                              </View>
                                              :
                                              null
                                                  }
                                            </View>
                                            }
                                          </View>
                                        }

                                    <View style={MyOrders.OrderDetailView}>
            
                                        <View style={MyOrders.ItemDetailView}>
                                            {
                                              data.order_type==0?
                                              <Text style={{fontFamily:"Montserrat",fontSize:13,marginBottom:5,}}>Delivery type : <Text style={{fontFamily:"Montserrat",fontSize:12}}> Delivery</Text> </Text>
                                              :
                                              <Text style={{fontFamily:"Montserrat",fontSize:13,marginBottom:5,}}>Delivery type : <Text style={{fontFamily:"Montserrat",fontSize:12}}> Pickup</Text> </Text>
                                            }
                                            <Text style={{fontFamily:"Montserrat",fontSize:13,marginBottom:5,}}>Placed on : <Text style={{fontFamily:"Montserrat",fontSize:12}}> {data.placed_on}</Text> </Text>
                                          {
                                            data.order_type==0?
                                            <View>
                                              <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Delivery date: <Text style={{fontFamily:"Montserrat",fontSize:12}}>{data.delivery}</Text></Text>
                                              <Text style={{fontFamily:"Montserrat",fontSize:13,marginTop:5}}>Time slot : <Text style={{fontFamily:"Montserrat",fontSize:12}}> {data.time_slot}</Text> </Text>
                                            </View>
                                            :
                                            <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Pick-up date: <Text style={{fontFamily:"Montserrat",fontSize:12}}>{data.pickup}</Text></Text>
                                          }
                                            
                                        </View>
                                        
                                        <View style={{marginBottom:5,paddingHorizontal:20,}}>
                                            <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Item :</Text>
                                        </View>
            
            
                                        <View style={MyOrders.ItemNameView}>
            
                                                <Text numberOfLines={3} ellipsizeMode='tail' style={{width:'100%'}}>
                                                {
                                                    data.item.map((item,index)=>{
                                                      if(data.item.length-1>index){
                                                        return(
                                                          <Text key={index} style={{fontFamily:"Montserrat",fontSize:12,}}>{item.product_name}, </Text>
                                                      )
                                                      }else{
                                                        return(
                                                          <Text key={index} style={{fontFamily:"Montserrat",fontSize:12,}}>{item.product_name} </Text>
                                                      )
                                                      } 
                                                    })
                                                }
                                                </Text>    
                                        
                                        </View>
                                        
                                        <View style={{borderWidth:1,borderColor:'#ededed',marginBottom:-10}}>
                                            
                                            {/* {
                                                data.order_request==1 || data.order_request==3?
                                                <TouchableOpacity onPress={()=>this.navigateMyOrderDetails(data)} 
                                                style={{width:'50%',alignItems:'center',alignSelf:'center',justifyContent:'center',paddingVertical:15}}>
                                                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>View details</Text>
                                                </TouchableOpacity>    
                                                :
                                                <View style={{width:'100%',flexDirection:'row',}}> 
                                                  {
                                                      data.order_request==2?
                                                        <TouchableOpacity onPress={()=>this.navigateOrderCancelling(data.id,true)}
                                                        style={{width:'50%',alignItems:'center',justifyContent:'center',borderRightWidth:1,borderRightColor:'#ededed',paddingVertical:15}}>
                                                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>Return</Text>
                                                        </TouchableOpacity>
                                                        :
                                                        <TouchableOpacity onPress={()=>this.navigateOrderCancelling(data.id,false)}
                                                        style={{width:'50%',alignItems:'center',justifyContent:'center',borderRightWidth:1,borderRightColor:'#ededed',paddingVertical:15}}>
                                                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>Cancel</Text>
                                                        </TouchableOpacity>
                                                  }
                                                    
                                                
                                                    <TouchableOpacity onPress={()=>this.navigateMyOrderDetails(data)} 
                                                    style={{width:'50%',alignItems:'center',alignSelf:'center',justifyContent:'center',paddingVertical:15}}>
                                                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>View details</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            } */}
                                            <View style={{width:'100%',flexDirection:'row',}}> 
                                              <TouchableOpacity onPress={()=>this.navigateOrderCancelling(data.id,false)}
                                              style={{width:'50%',alignItems:'center',justifyContent:'center',borderRightWidth:1,borderRightColor:'#ededed',paddingVertical:15}}>
                                                  <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>Cancel</Text>
                                              </TouchableOpacity>

                                              <TouchableOpacity onPress={()=>this.navigateMyOrderDetails(data)} 
                                                style={{width:'50%',alignItems:'center',alignSelf:'center',justifyContent:'center',paddingVertical:15}}>
                                                  <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>View details</Text>
                                              </TouchableOpacity>
                                            </View>

                                        </View>

                                    </View>

            
                                    </View>
                                )
                              }else{
                                return null
                              }
                            })
                        }
                            
                        </View>
                        :
                        <View>
                      
                        {
                          this.state.orderDetail.map((data,index)=>{
                            if(data.order_request!==0 && data.order_request!==1 && data.order_request!==2){
                              return(
                                  
                                  <View key={index} style={{marginBottom:10}}>
                                    {/* {
                                      data.order_request==0?
                                        <View style={MyOrders.InProgressBanner}>
                                          <View style={MyOrders.InsideProgress}>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text>
                                          </View>
                                        </View>
                                        :
                                        null
                                      } */}

                                      {/* {
                                      data.order_request==1?
                                        <View style={MyOrders.CanceledBanner}>
                                          <View style={MyOrders.InsideProgress}>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text>
                                          </View>
                                        </View>
                                        :
                                        null
                                      }

                                      {
                                      data.order_request==2?
                                        <View style={MyOrders.completedWithBlackBg}>
                                          <View style={MyOrders.InsideProgress}>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text>
                                          </View>
                                        </View>
                                        :
                                        null
                                      } */}

                                      {
                                      data.order_request==3?
                                        <View style={MyOrders.completedWithBlackBg}>
                                          <View style={MyOrders.InsideProgress}>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text>
                                          </View>
                                        </View>
                                        :
                                        null
                                      }

                                      {
                                      data.order_request==5? //refund
                                        <View style={MyOrders.ReturnOrderBanner}>
                                          <View style={MyOrders.InsideProgress}>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text>
                                          </View>
                                        </View>
                                        :
                                        null
                                      }

                                      {
                                      data.order_request==4? //failed
                                        <View style={MyOrders.CanceledBanner}>
                                          <View style={MyOrders.InsideProgress}>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {data.id}</Text>
                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{data.order_status_text}</Text>
                                          </View>
                                        </View>
                                        :
                                        null
                                      }
          
                                  {/* {
                                    this.state.selectedTab==0?


                                  } */}
                                  <View style={MyOrders.OrderDetailView}>
          
                                      <View style={MyOrders.ItemDetailView}>
                                          <Text style={{fontFamily:"Montserrat",fontSize:13,marginBottom:5,}}>Placed On : <Text style={{fontFamily:"Montserrat",fontSize:12}}> {data.placed_on}</Text> </Text>
                                        {
                                          data.order_type==0?
                                          <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Delivery date: <Text style={{fontFamily:"Montserrat",fontSize:12}}>{data.delivery}</Text></Text>
                                          :
                                          <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Pick-up date: <Text style={{fontFamily:"Montserrat",fontSize:12}}>{data.pickup}</Text></Text>
                                        }
                                          
                                      </View>
                                      
                                      <View style={{marginBottom:5,paddingHorizontal:20,}}>
                                          <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Item :</Text>
                                      </View>
          
          
                                      <View style={MyOrders.ItemNameView}>
          
                                              <Text numberOfLines={3} ellipsizeMode='tail' style={{width:'100%'}}>
                                              {
          
                                                  data.item.map((item,index)=>{
                                                    if(data.item.length-1>index){
                                                      return(
                                                        <Text key={index} style={{fontFamily:"Montserrat",fontSize:12,}}>{item.product_name}, </Text>
                                                    )
                                                    }else{
                                                      return(
                                                        <Text key={index} style={{fontFamily:"Montserrat",fontSize:12,}}>{item.product_name} </Text>
                                                    )
                                                    }
                                                  })
                                              }
                                              </Text>    
                                      
                                      </View>
                                      
                                      <View style={{borderWidth:1,borderColor:'#ededed',marginBottom:-10}}>
                                            <TouchableOpacity onPress={()=>this.navigateMyOrderDetails(data)} 
                                                  style={{width:'50%',alignItems:'center',alignSelf:'center',justifyContent:'center',paddingVertical:15}}>
                                                  <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>View details</Text>
                                            </TouchableOpacity>    
                                          {/* {
                                              data.order_request==4?
                                              <TouchableOpacity onPress={()=>this.navigateMyOrderDetails(data)} 
                                              style={{width:'50%',alignItems:'center',alignSelf:'center',justifyContent:'center',paddingVertical:15}}>
                                                          <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>View details</Text>
                                              </TouchableOpacity>    
                                              :
                                              <View style={{width:'100%',flexDirection:'row',}}> 
                                                {
                                                    data.order_request==3?
                                                      <TouchableOpacity onPress={()=>this.navigateOrderCancelling(data.id,true)}
                                                      style={{width:'50%',alignItems:'center',justifyContent:'center',borderRightWidth:1,borderRightColor:'#ededed',paddingVertical:15}}>
                                                          <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>Return</Text>
                                                      </TouchableOpacity>
                                                      :
                                                      null
                                                }
                                                  
                                              
                                                  <TouchableOpacity onPress={()=>this.navigateMyOrderDetails(data)} 
                                                  style={{width:'50%',alignItems:'center',alignSelf:'center',justifyContent:'center',paddingVertical:15}}>
                                                              <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>View details</Text>
                                                  </TouchableOpacity>
                                              </View>
                                          }
                                           */}
                                      </View>

                                  </View>

          
                                  </View>
                              )
                            }
                            else{
                              return null;
                            }
                          })
                      }
                          
                      </View>

                    }

                {
                  this.state.nextPageLoading?
                  <View style={{alignItems:'center',justifyContent:'center',paddingVertical:5}}>
                    <ActivityIndicator/>
                  </View>
                  :
                  null
                }

               </ScrollView>
              
               {/* <View style={{position:'absolute',bottom:10,right:10}}>
                <TouchableOpacity onPress={()=>this.getOrderDetails()} style={{width:40,height:40,borderRadius:25,backgroundColor:'#fa7153',justifyContent:'center',alignItems:'center'}}>
                  <Image source={require('../images/refresh.png')} style={{width:20,height:20,resizeMode:'contain'}}/>
                </TouchableOpacity>
              </View> */}

            </View>
            
        );
      }
    
  }
}


function mapStateToProps(state){
    return{
        userId:state.userId,
        orderId:state.orderId,
        orderCountChanged:state.orderCountChanged,
        notificationCount:state.notificationCount
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setorderId:(value)=>dispatch({type:"setorderId",value}),
        setOrderCountChanged:(value)=>dispatch({type:"setOrderCountChanged",value}),
        setOrderReturn:(value)=>dispatch({type:"setOrderReturn",value}),
        setOrderPlacedTime:(value)=>dispatch({type:"setOrderPlacedTime",value}),
        setisFromOrderScreen:(value)=>dispatch({type:"setisFromOrderScreen",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(OrderScreen);
  







