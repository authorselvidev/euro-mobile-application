import React, { Component } from 'react';
import { View, Text,TouchableOpacity,TextInput,ScrollView,AsyncStorage,Image,Modal,keyboard,ActivityIndicator, Keyboard } from 'react-native';
import {Textarea} from 'native-base'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle} from './Styles/PrimaryStyle';
import { PostCodeModel, PostCodeModelResponse } from '../DataModel/ModelClass';
import AuthenticationService from '../Controller/ServiceClass';
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import images from '../images';
import { Colors } from './Styles/colors';



class createNewJobSites extends Component {
  state = {
        isAvailable:null,
        postCode:null,
        postCodeError:null,
        loading:false,
        siteName:null,
        siteNameError:null,
        addressLine1:null,
        addressLine1Error:null,
        addressLine2:null,
        addressLine2Error:null,
        suburb:null,
        suburbError:null,
        contactPerson:null,
        contactPersonError:null,
        contactPersonNumber:null,
        contactPersonNumberError:null,
        deliveryType:null,
        addressId:null,
        jobSiteNotes:null,
        jobSiteNotesError:null,
        saveAddressLoading:false,
        modalVisible:false,
        showingDeliveryStatus:false,
        isFromSignup:0
    };
  
   async componentDidMount(){
      
       console.log(this.props.isFromCheckout,this.props.isShippingAddress)
        // console.log(this.props.postCode,this.props.deliveryType,this.props.shippingAsBilling)
        // AsyncStorage.getItem('userId').then((value) => {
        //     this.props.setuserId(JSON.parse(value))
        //   })
        if(this.props.isFromCheckout==true){
          this.setState({postCode:this.props.postCode,deliveryType:this.props.deliveryType})
          console.log("create new site user id"+this.props.userId)
        }
    }

   async componentWillUnmount(){
    //  await this.props.setisFromCheckout(false)
     await this.setState({siteName:""})
     //await this.setState({showingDeliveryStatus:false})
    }


    async postCode(value){
      var check_from;
      if(this.props.isFromCheckout==true){
        check_from="address"
      }else{
        check_from="postcode"
      }
     // console.log(check_from)
      if(this.props.isShippingAddress==true){
        console.log('isfrom shipping')
        await this.setState({postCode:value})
         if(this.state.postCode.toString().length==4){
          this.setState({keyboard:false,loading:true}) 
          
          await fetch(baseUrl+"postcode", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              "postcode":this.state.postCode,
              "user_id":this.props.userId,
              "check_from":check_from
            })
          })
            .then((response)=>response.json())
            .then((responseJson)=>{
              console.log(responseJson)
              if(responseJson.success==true){
                this.setState({deliveryType:responseJson.delivery_type,showingDeliveryStatus:true})
                if(this.state.deliveryType==1){
                  this.setState({modalVisible:true})
                }
              }
            }) 
          this.setState({loading:false,keyboard:true})
         }
        }else{
          console.log('isfrom billing')
          await this.setState({postCode:value})
          if(this.state.postCode.toString().length==4){
          this.setState({keyboard:false,deliveryType:0}) 
          }
        }
        } 

     saveSite(){
         Keyboard.dismiss()
         const {addressId,siteName,addressLine1,addressLine2,contactPerson,contactPersonNumber}=this.state;
         //console.log(addressId,siteName,addressLine1,addressLine2,contactPerson,contactPersonNumber)
        var addressType;
         if(this.props.isShippingAddress==true){
            addressType="shipping"
         }else{
            addressType="billing"
         }
         console.log(addressType)

         if(this.state.postCode==null){
            this.setState({postCodeError:"Postcode feild is required"})
         }else if(this.state.postCode.toString().length>=4){
            this.setState({postCodeError:null})
         }else{
          this.setState({postCodeError:"Please enter the valid post code!"})
         }


         if(this.state.siteName==null){
             this.setState({siteNameError:"Site name feild is required"})
         }else{
             this.setState({siteNameError:null})
         }

         if(this.state.addressLine1==null){
             this.setState({addressLine1Error:"Address line 1 feild is required"})
         }else{
             this.setState({addressLine1Error:null})
         }

        //  if(this.state.addressLine2==null){
        //     this.setState({addressLine2Error:"Address line 2 feild is required"})
        //  }else{
        //      this.setState({addressLine2Error:null})
        //  }

         if(this.state.suburb==null){
            this.setState({suburbError:"Suburb feild is required"})
         }else{
             this.setState({suburbError:null})
         }

         if(this.state.contactPerson==null){
            this.setState({contactPersonError:"Contact person feild is required"})
         }else{
             this.setState({contactPersonError:null})
         }
        
         if(this.state.contactPersonNumber==null){
            this.setState({contactPersonNumberError:"Contact person number feild is required"})
         }else{
          if(this.state.contactPersonNumber.length<10){
            this.setState({contactPersonNumberError:"Contact number must be 10 digits"})
          }else{
             this.setState({contactPersonNumberError:null})
          }
         }

         if(this.props.isShippingAddress==true){
            if(this.state.postCode!=null && this.state.postCode.toString().length==4 && this.state.siteName!=null && this.state.addressLine1!=null && this.state.suburb!=null && this.state.contactPerson!=null && this.state.contactPersonNumber!=null && this.state.contactPersonNumber.toString().length==10){
              
              this.setState({saveAddressLoading:true})
              fetch(baseUrl+this.props.userId+"/address", {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                      "postcode":this.state.postCode,
                      "job_site_name": this.state.siteName,
                      "address_line1": this.state.addressLine1,
                      "address_line2" : this.state.addressLine2,
                      "job_site_note" : this.state.jobSiteNotes,
                      "contact_person": this.state.contactPerson,
                      "contact_person_no":this.state.contactPersonNumber,
                      "from_checkout":true,
                      "suburb":this.state.suburb,
                      "delivery_type":this.state.deliveryType,
                      "address_type" :addressType
                  })
                })
                  .then((response)=>response.json())
                  .then((responseJson)=>{
                    console.log(responseJson)
                    this.setState({saveAddressLoading:false})
                    if(responseJson.success==true){
                      this.addAddressRefershing()
                    }
                  })            
            }
         }else{
           console.warn("is billing post")
          if(this.state.postCode!=null && this.state.addressLine1!=null && this.state.suburb!=null && this.state.contactPerson!=null && this.state.contactPersonNumber!=null && this.state.contactPersonNumber.toString().length==10){
             
            this.setState({saveAddressLoading:true})
            console.log(JSON.stringify({
              "postcode":this.state.postCode,
              "job_site_name": this.state.siteName,
              "address_line1": this.state.addressLine1,
              "address_line2" : this.state.addressLine2,
              "job_site_note" : "",
              "contact_person": this.state.contactPerson,
              "contact_person_no":this.state.contactPersonNumber,
              "from_checkout":true,
              "suburb":this.state.suburb,
              "delivery_type":this.state.deliveryType,
              "address_type" :addressType
          }))
           fetch(baseUrl+this.props.userId+"/address", {
               method: 'POST',
               headers: {
                   Accept: 'application/json',
                   'Content-Type': 'application/json',
               },
               body: JSON.stringify({
                   "postcode":this.state.postCode,
                   "job_site_name": this.state.siteName,
                   "address_line1": this.state.addressLine1,
                   "address_line2" : this.state.addressLine2,
                   "job_site_note" : "",
                   "contact_person": this.state.contactPerson,
                   "contact_person_no":this.state.contactPersonNumber,
                   "from_checkout":true,
                   "suburb":this.state.suburb,
                   "delivery_type":this.state.deliveryType,
                   "address_type" :addressType
               })
             })
               .then((response)=>response.json())
               .then((responseJson)=>{
                 console.log(responseJson)
                 this.setState({saveAddressLoading:false})
                 if(responseJson.success==true){
                   this.addAddressRefershing()
                 }
               })            
        }
         }
         
     }   

    async addAddressRefershing(){
       await this.props.setProductCountChanged(true)
       await this.props.setaddAddressRefershing(true)
       await this.props.setAddressChanged(true)
       await this.setState({siteName:null,addressLine1:null,addressLine2:null,suburb:null,jobSiteNotes:null,contactPerson:null,contactPersonNumber:null})
       if(this.props.route.params.isFromSignup==0){
        if(this.props.isFromCheckout==true){
          this.props.navigation.navigate("completeOrder")
          }else{
            this.props.navigation.goBack()
          }
      }else{
        this.props.navigation.navigate("homeScreen")
      }
     }
 
    async continueToPickup(){
      //await this.props.setorderDeliverType(1)
      await this.props.setProductCountChanged(true)
      await this.props.setAddressChanged(true)
      await this.props.setChoosedPickupMethod(true)
      this.setState({modalVisible:false})
      if(this.props.route.params.isFromSignup==0){
        if(this.props.isFromCheckout==true){
          this.props.navigation.navigate("completeOrder")
          }else{
            this.props.navigation.goBack()
          }
      }else{
        this.props.navigation.navigate("homeScreen")
      }
     }

  render() {
    // try {
      // const {isFromSignup}=this.props.route.params;
      console.log("params "+this.props.route.params.isFromSignup)
    //   if(isFromSignup){
    //     this.setState({isFromSignup})
    //     console.log("state "+this.state.isFromSignup)
    //   }
    //  } catch (error) {
    //   console.log("error")
    //  }
        return (
                
                <View style={{flex:1,backgroundColor:'#fff'}}>

                <View style={{padding:12,backgroundColor:'#fa7153',flexDirection:'row',justifyContent:'space-between'}}>

                  <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                     <Image source={images.backArrow} style={{width:20,height:20}}/>
                  </TouchableOpacity>
                  {
                    this.props.isShippingAddress==true?
                    <Text style={{fontSize:17,color:'#fff',fontFamily:'Montserrat-SemiBold'}}>Add address</Text>
                    /* <Text style={{fontSize:17,color:'#fff',fontFamily:'Montserrat-SemiBold'}}>Create New Job Site</Text> */
                    :
                    <Text style={{fontSize:17,color:'#fff',fontFamily:'Montserrat-SemiBold'}}>Add address</Text>
                  }
                  
                  <Text></Text>

                </View>
                
                <KeyboardAwareScrollView showsVerticalScrollIndicator={false} style={{padding:20}}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                     {
                      this.props.isShippingAddress==true?
                      <View style={{flexDirection:'row',width:'100%',alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3}}>
                          <TextInput 
                          style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
                                fontSize:14,fontFamily:'Montserrat'}} 
                                ref={(input) => { this.postcodeRef = input; }}
                                onSubmitEditing={() =>{this.props.isShippingAddress?this.jobSiteNameRef.focus():this.projectManagerRef.focus()}}
                                value={this.state.postCode}
                                onChangeText={(value)=>this.postCode(value)}
                                returnKeyType={"next"}
                                keyboardType='numeric'
                                placeholder="Your Post Code *"
                                // editable={this.state.keyboard}      
                                maxLength={4}                
                                />
                                {
                                  this.state.loading==true?
                                  <ActivityIndicator/>
                                  :
                                  null
                                }
                        </View>
                        :
                        null
                     }
                     
                      
                               {
                                this.state.postCodeError==null?null
                                 :
                                 <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.postCodeError}</Text>
                                }
{/* 
                            {
                              this.props.isFromCheckout==true?
                              <View>
                                {
                                  this.props.isShippingAddress==true?
                                    <TouchableOpacity style={{marginBottom:20,marginTop:3}}>
                                        {
                                            this.state.deliveryType==0?
                                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                              <Image source={images.available} style={{width:20,height:20,resizeMode:'contain'}}/>
                                              <Text style={{paddingLeft:5,fontFamily:'Montserrat',fontSize:12,color:'#31bf45'}}>Delivery available</Text>
                                            </View>
                                            
                                            :

                                            <View style={{flexDirection:'row',alignItems:'center'}}>
                                              <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                                              <Text style={{paddingLeft:5,fontFamily:'Montserrat',fontSize:12,color:'#fb2424'}}>Delivery not available</Text>
                                            </View>
                                        }
                                  </TouchableOpacity>
                                  :
                                  <View style={{marginBottom:25}}></View>
                                }
                              </View>
                              :
                              <View style={{marginBottom:25}}></View>
                            } */}
                            <View style={{marginBottom:25}}></View>

                            <TouchableOpacity >
                            {
                              this.props.isShippingAddress==true?
                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                <TextInput style={StyleContainer.TextInputView} 
                                  //autoFocus
                                  placeholder="Name *"              
                                  ref={(input) => { this.jobSiteNameRef = input; }}
                                  onSubmitEditing={() => {this.addressLine1Ref.focus();}}
                                  onChangeText={(value)=>this.setState({siteName:value})} 
                                  returnKeyType={"next"}
                                  value={this.state.siteName}
                                />
                                {
                                  this.state.siteNameError==null?null
                                  :
                                  <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.siteNameError}</Text>
                                  }
                              </View>
                              :
                              <View style={OverAllStyle.TextInputWhiteMainView}>
                                <TextInput style={StyleContainer.TextInputView} 
                                  //autoFocus
                                  placeholder="Company Name"              
                                  ref={(input) => { this.jobSiteNameRef = input; }}
                                  onSubmitEditing={() => {this.addressLine1Ref.focus();}}
                                  onChangeText={(value)=>this.setState({siteName:value})} 
                                  returnKeyType={"next"}
                                  value={this.state.siteName}
                                />
                                
                              </View>
                            }
                            
    
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={StyleContainer.TextInputView} placeholder="Address Line 1 *"                     
                                ref={(input) => { this.addressLine1Ref = input; }}
                                    onSubmitEditing={() => {this.addressLine2Ref.focus();}}
                                    onChangeText={(value)=>this.setState({addressLine1:value})} 
                                    returnKeyType={"next"}
                                    value={this.state.addressLine1}
                                    />
                                    
                                    {
                                        this.state.addressLine1Error==null?null
                                        :
                                        <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.addressLine1Error}</Text>
                                    }
                            </View>
    
                            <View style={{width:'100%',marginBottom:10}}>
                            <TextInput style={StyleContainer.TextInputView} placeholder="Address Line 2"                        
                                ref={(input) => { this.addressLine2Ref = input; }}
                                onSubmitEditing={() => {this.suburbRef.focus();}}
                                onChangeText={(value)=>this.setState({addressLine2:value})} 
                                returnKeyType={"next"}
                                value={this.state.addressLine2}
                                />
                            </View>

                            <View style={{width:'100%',marginBottom:25}}>
                            <TextInput style={StyleContainer.TextInputView} placeholder="Suburb *"                        
                                ref={(input) => { this.suburbRef = input; }}
                                onSubmitEditing={() => {this.props.isShippingAddress?this.projectManagerRef.focus():this.postcodeRef.focus()}}
                                onChangeText={(value)=>this.setState({suburb:value})} 
                                returnKeyType={"next"}
                                value={this.state.suburb}
                                />

                                    {
                                        this.state.suburbError==null?null
                                        :
                                        <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.suburbError}</Text>
                                    }

                            </View>
                            
                            {
                              this.props.isShippingAddress==false?
                              <View style={{flexDirection:'row',width:'100%',alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3,marginBottom:15}}>
                                  <TextInput 
                                  style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
                                        fontSize:14,fontFamily:'Montserrat'}} 
                                        ref={(input) => { this.postcodeRef = input; }}
                                        onSubmitEditing={() =>{this.projectManagerRef.focus();}}
                                        value={this.state.postCode}
                                        onChangeText={(value)=>this.postCode(value)}
                                        returnKeyType={"next"}
                                        keyboardType='numeric'
                                        placeholder="Your Post Code *"
                                        maxLength={4}
                                        // editable={this.state.keyboard}                      
                                        />
                                        {
                                          this.state.loading==true?
                                          <ActivityIndicator/>
                                          :
                                          null
                                        }
                                </View>
                                :
                                null
                            }

                        <View style={{width:'100%'}}>
    
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                             <TextInput style={StyleContainer.TextInputView} placeholder="Contact Manager *"    
                                ref={(input) => { this.projectManagerRef = input; }}
                                onSubmitEditing={() => {this.projectManagerMobileRef.focus();}}
                                onChangeText={(value)=>this.setState({contactPerson:value})} 
                                returnKeyType={"next"}
                                value={this.state.contactPerson}
                              />

                                    {
                                        this.state.contactPersonError==null?null
                                        :
                                        <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.contactPersonError}</Text>
                                    }

                            </View>
                            
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                              <TextInput style={StyleContainer.TextInputView}
                                placeholder="Contact Manager Mobile Number *"
                                ref={(input) => { this.projectManagerMobileRef = input; }}
                                //onSubmitEditing={() => {this.jobSiteNotesRef.focus();}}
                                onChangeText={(value)=>this.setState({contactPersonNumber:value})} 
                                returnKeyType="next"
                                keyboardType="number-pad"
                                maxLength={10}
                                value={this.state.contactPersonNumber}
                                />

                                    {
                                        this.state.contactPersonNumberError==null?null
                                        :
                                        <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.contactPersonNumberError}</Text>
                                    }

                            </View>
    
                          
    
                        
                        {
                          this.props.isShippingAddress==true?
                          <View>
                              <View style={{marginBottom:10}}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15,}}>Job site notes</Text>
                              </View>
                              <View style={{width:'100%',borderRadius:3,borderWidth:1,borderColor:'#ddd',backgroundColor:'#FFF'}}>
                                    <Textarea  
                                    placeholderTextColor='#acacac' 
                                    value={this.state.jobSiteNotes}
                                    onChangeText={(value)=>this.setState({jobSiteNotes:value})}
                                    //onSubmitEditing={()=>this.saveSite()}
                                    placeholder="Enter your job site notes here" 
                                    returnKeyType={"done"}
                                    />
                              </View>
                            </View>
                            :
                            null
                        }
                          
    
                          
                              {
                                  this.state.saveAddressLoading?
                                  <TouchableOpacity style={{width:'100%',marginVertical:20}}>
                                      <View style={StyleContainer.ButtonOrange}>
                                          <ActivityIndicator color="#fff"/>
                                      </View>
                                  </TouchableOpacity>
                                  :
                                  <TouchableOpacity style={{width:'100%',marginVertical:20}} onPress={()=>this.saveSite()}>
                                      <View style={StyleContainer.ButtonOrange}>
                                          <Text style={{fontFamily:"Montserrat-SemiBold",color:'#FFF',fontSize:17,}} >SAVE</Text>
                                      </View>
                                  </TouchableOpacity>
                              }
                      
                        </View>
                        </TouchableOpacity>
                        </ScrollView>        
                        <CustomNotification/>    
                        </KeyboardAwareScrollView>

                        <Modal
                            animationType="slide"
                            transparent={true}
                           visible={this.state.modalVisible}
                           onRequestClose={()=>this.setState({modalVisible:false})}>
                            <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.5)'}}>
                                
                                <View style={{width:'85%',backgroundColor:'#FFF',borderRadius:5}}>
                                <View>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",paddingHorizontal:20,paddingVertical:20}}>Delivery Not available!!</Text>
                                    <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,fontSize:12}}>Sorry! we did not delivery to this area?</Text>
                                    <View style={{borderWidth:0.3,borderRadius:3,borderColor:'gray',width:'100%',flexDirection:'row',marginTop:15}}>
                                        <TouchableOpacity onPress={()=>this.setState({postCode:null,postCodeError:null,modalVisible:false})}
                                        style={{alignItems:'center',width:'50%',padding:10,borderRightWidth:0.3,borderRightColor:'gray'}}>
                                        <Text style={{fontFamily:'Montserrat'}}>Cancel</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={()=>this.setState({postCode:null,postCodeError:null,modalVisible:false})}
                                        style={{alignItems:'center',width:'50%',padding:10}}>
                                        <Text style={{fontFamily:'Montserrat',color:'#fa7153'}}>Re-enter</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                </View>
                            </View>
                        </Modal>
                </View>
            
        );
      }
 
  }
// }


function mapStateToProps(state){
    return{
        userId:state.userId,
        isFromCheckout:state.isFromCheckout,
        isShippingAddress:state.isShippingAddress,
        postCode:state.postCode,
        deliveryType:state.deliveryType,
        choosedPickupMethod:state.choosedPickupMethod,
        shippingAsBilling:state.shippingAsBilling
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setuserId:(value)=>dispatch({type:'setuserId',value}),
        setPostCode:(value)=>dispatch({type:"setPostCode",value}),
        setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
        setaddAddressRefershing:(value)=>dispatch({type:"setaddAddressRefershing",value}),
        setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
        setisFromCheckout:(value)=>dispatch({type:'setisFromCheckout',value}),
        setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
        setorderDeliverType:(value)=>dispatch({type:'setorderDeliverType',value}),
        setChoosedPickupMethod:(value)=>dispatch({type:"setChoosedPickupMethod",value}),
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(createNewJobSites);
  

