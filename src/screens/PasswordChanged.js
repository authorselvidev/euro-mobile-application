import React, { Component } from 'react'
import { Text, View, Image ,TouchableOpacity,StyleSheet} from 'react-native'
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,PasswordChangedStyle} from './Styles/PrimaryStyle'
import CustomNotification from './CustomNotification';

export default class PasswordChanged extends Component {
    render() {
        return (
            <View style={OverAllStyle.PasswordChangedMainContainer}>
                <View style={PasswordChangedStyle.ImgViewStyle}>
                <Image source={images.passwordChanged} style={StyleContainer.ImageContainStyle}/>
                </View>

                <View>
                <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',marginBottom:10,fontSize:18,letterSpacing:1}}>Successfully password Reset!</Text>
                  
                  <View style={PasswordChangedStyle.IntractTxtView2}>
                  <Text style={{fontFamily:"Montserrat",textAlign:'center',fontSize:12,lineHeight:15,color:'#aaa'}}>You can now use your new password to login to your account</Text>
                  </View>

                </View>
              
                <TouchableOpacity style={{width:'100%',marginVertical:10}} onPress={()=>this.props.navigation.navigate("signin")}>
                                <View style={StyleContainer.ButtonBorder}>
                                    <Text style={{fontFamily:"Montserrat",color:'#000',fontSize:17}} >LOGIN</Text>
                                </View>

                        </TouchableOpacity>              
              
              <CustomNotification/>
            </View>
        )
    }
}


