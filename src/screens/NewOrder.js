import React, { Component } from 'react';
import { View, Text,TextInput,ActivityIndicator,Image, TouchableOpacity,Modal } from 'react-native';
import {baseUrl} from '../Controller/baseUrl'
import {StyleContainer} from '../screens/Styles/CommonStyles'
import {OverAllStyle,OrderAvailableStyle,PostCodeStyle} from './Styles/PrimaryStyle';
import {connect} from 'react-redux'
import images from '../images/index'
import CustomNotification from './CustomNotification';

class NewOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // postCode:null,
      // loading:false,
      // keyboard:true,
      // checkPostCode:false,
      // isPostcodeAvailable:false,
      // modalVisible:false,
      // postCodeError:null,
      // clearcartModal:false
    };
  }

 async componentDidMount(){
    console.log(this.props.cartCount)
   await this.setState({postCode:this.props.postCode})
  }

 async clearcart(){
    this.setState({modalVisible:false})
    await fetch(baseUrl+this.props.userId+'/emptycart')
    .then((response)=>response.json())
    .then((responseJson)=>{
      console.log(responseJson)
      this.setState({checkPostCode:false})
      this.props.navigation.navigate("drawer",{screen:"home"})
    })
   await this.props.setHomeScreenUpdate(true)
  }

  // async changePostCode(value){    
  //   await this.setState({postCode:value})
    
  //    if(this.state.postCode.toString().length==4){
  //     this.setState({keyboard:false,loading:true})      
  //     await fetch(baseUrl+"postcode", {
  //       method: 'POST',
  //       headers: {
  //           Accept: 'application/json',
  //           'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify({
  //         "postcode":this.state.postCode,
  //         "user_id":this.props.userId,
  //         "check_from":"postcode"       
  //       })
  //     })
  //       .then((response)=>response.json())
  //       .then((responseJson)=>{
  //         console.log(responseJson)
  //         if(responseJson.delivery_type==0){
  //           this.props.setDeliveryType(0)
  //           this.setState({checkPostCode:true,isPostcodeAvailable:true})

  //         }else{
  //           this.props.setDeliveryType(1)
  //           this.setState({checkPostCode:true,isPostcodeAvailable:false})
  //         }
  //       })
  //       //.catch((err)=>{//console.log(err)})
  //     await this.props.setPostCode(this.state.postCode)
  //     this.setState({loading:false,keyboard:true,postCodeError:null})
  //    }else{
  //     this.setState({postCodeError:"Please enter a valid postcode"})
  //    }
  // }

  render(){
    if(this.props.cartCount==0){
      return(
        <View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}>
          <View style={{width:'80%',height:200}}>
            <Image source={images.emptyCart} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}></Image>
          </View>          
          <Text style={{fontFamily:"Montserrat",textAlign:'center'}}>Your cart is empty!</Text>
          <Text style={{fontFamily:"Montserrat",paddingVertical:10,fontSize:12}}>Add items to it now.</Text>
          <View style={{width:'80%'}}>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:'home'})}
              style={StyleContainer.ButtonOrange}>
              <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff'}}>Shop now</Text>
            </TouchableOpacity>
           </View>
        </View>
      )
    }else{
    return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.8)'}}>
        <View style={{width:'85%',backgroundColor:'#FFF',borderRadius:5}}>
          <View>
              <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,paddingVertical:10,color:'black'}}>Create a new order !</Text>
              <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,fontSize:12}}>Your existing cart item will be removed.</Text>
              <View style={{borderWidth:0.3,borderColor:'gray',width:'100%',flexDirection:'row',marginTop:10}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"home"})}
                style={{alignItems:'center',width:'50%',padding:10,borderRightWidth:0.3,borderRightColor:'gray'}}>
                  <Text style={{fontFamily:'Montserrat'}}>No</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.clearcart()}
                style={{alignItems:'center',width:'50%',padding:10}}>
                  <Text style={{fontFamily:'Montserrat',color:'#fa7153'}}>Yes</Text>
                </TouchableOpacity>
              </View>
          </View>
        </View>
      </View>
    )
    }
  }
}
  function mapStateToProps(state){
    return{
        userId:state.userId,
        postCode:state.postCode,
        cartCount:state.cartCount,
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setPostCode:(value)=>dispatch({type:"setPostCode",value}),
        setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
        setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(NewOrder);
  
//   render() {
//     if(this.state.checkPostCode==false){
//     return (
//       <View style={StyleContainer.MainContainer}>
//         <View style={PostCodeStyle.MainQuoteView}>
//           <Text style={{fontFamily:'Montserrat-SemiBold',textAlign:'center',fontSize:20}}>Your current post code</Text>
//        </View>
       
//        <Text style={{fontFamily:'Montserrat',fontSize:12,textAlign:'center',marginBottom:20}}>We deliver to Sydney metro and outer suburbs</Text>
          
//           <View style={StyleContainer.TextInputMainView}>
//               <TextInput style={{borderWidth:1,borderColor:'gray',fontSize:18,textAlign:'center',height:Platform.OS=="ios"?40:null}}
//                 // autoFocus
//                 ref={(input) => { this.emaildRef = input; }}
//                 onSubmitEditing={() => this.changePostCode()}
//                 onChangeText={(value)=>this.changePostCode(value)} 
//                 returnKeyType={"next"}
//                 keyboardType='numeric'
//                 placeholder="Your Post Code"     
//                 editable={this.state.keyboard}  
//                 value={this.state.postCode}
//                />
//               </View>

//               {
//                 this.state.postCodeError==null?null:
//                 <Text style={{color:'red',fontSize:13,textAlign:'left',alignSelf:'flex-start'}}>{this.state.postCodeError}</Text>
//               }
//               <View style={{alignItems:'center'}}>
//               {
//                 this.state.loading?
//                 <ActivityIndicator color="#fa7153"/>
//                 :null
//               }
//               </View>
//               <TouchableOpacity onPress={()=>this.changePostCode(this.state.postCode)} style={StyleContainer.ButtonOrange}>
//                   <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff',fontSize:18}}>CONTINUE</Text>
//               </TouchableOpacity>
              
//       </View>
//      );
//     }else{
//       return(
//         <View style={StyleContainer.MainContainer}>
          
//           {
//             this.state.isPostcodeAvailable==true?
//             <View style={OrderAvailableStyle.MainView}>
//                     <View style={OverAllStyle.LogoView}>
//                         <Image source={images.available} style={StyleContainer.ImageContainStyle}/>
//                     </View>

//                     <View style={OrderAvailableStyle.TxtView}>
//                         <Text style={OrderAvailableStyle.MainQuoteGreen}>Thank you.</Text>
//                         <Text style={OrderAvailableStyle.MainQuoteGreen}>Delivery Available</Text>
//                     </View>
                        

//                         <TouchableOpacity onPress={()=>this.setState({modalVisible:true})} style={StyleContainer.ButtonOrange}>
//                             <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18}}>CREATE NEW ORDER</Text>
//                         </TouchableOpacity>
//                         <TouchableOpacity onPress={()=>this.setState({checkPostCode:false})} 
//                         style={{ width:'100%',borderWidth:1,borderColor:'gray',paddingVertical:15,borderRadius:3,alignItems:'center',marginBottom:15,marginVertical:10}}>
//                           <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:18,textAlign:'center'}}>RE-ENTER POST CODE</Text>
//                         </TouchableOpacity>
//                     </View>
//                     :
//                     <View style={OrderAvailableStyle.MainView}>
//                     <View style={OverAllStyle.LogoView}>
//                         <Image source={images.notAvailable} style={StyleContainer.ImageContainStyle}/>
//                     </View>
//                     <View style={OrderAvailableStyle.TxtView}>
//                       <Text style={OrderAvailableStyle.MainQuoteRed}>SORRY!</Text>
//                       <Text style={OrderAvailableStyle.SubQuoteRed}>We do not delivery to this area. Please arrange pick-up in store.</Text>
//                     </View>
                        
//                     <TouchableOpacity onPress={()=>this.setState({modalVisible:true})} style={StyleContainer.ButtonOrange}>
//                             <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff',fontSize:18}}>CREATE NEW ORDER</Text>
//                         </TouchableOpacity>
//                         <TouchableOpacity onPress={()=>this.setState({checkPostCode:false})}
//                          style={{ width:'100%',borderWidth:1,borderColor:'gray',paddingVertical:15,borderRadius:3,alignItems:'center',marginBottom:15,marginVertical:10}}>
//                           <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:18,textAlign:'center'}}>RE-ENTER POST CODE</Text>
//                         </TouchableOpacity>
//                     </View>
//           }

//           <Modal 
//             animationType="fade"
//             transparent={true}
//             visible={this.state.modalVisible}>
//             <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.8)'}}>
            // <View style={{width:'85%',backgroundColor:'#FFF',borderRadius:5}}>
            //   <View>
            //       <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,paddingVertical:10,color:'black'}}>Create a new order !</Text>
            //       <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,fontSize:12}}>Are you sure you want to remove cart items?</Text>
            //       <View style={{borderWidth:0.3,borderColor:'gray',width:'100%',flexDirection:'row',marginTop:10}}>
            //         <TouchableOpacity onPress={()=>this.setState({modalVisible:false})}
            //         style={{alignItems:'center',width:'50%',padding:10,borderRightWidth:0.3,borderRightColor:'gray'}}>
            //           <Text style={{fontFamily:'Montserrat'}}>Cancel</Text>
            //         </TouchableOpacity>

            //         <TouchableOpacity onPress={()=>this.clearcart()}
            //         style={{alignItems:'center',width:'50%',padding:10}}>
            //           <Text style={{fontFamily:'Montserrat',color:'#fa7153'}}>Yes</Text>
            //         </TouchableOpacity>
            //       </View>
            //   </View>
            //   </View>
//             </View>
//         </Modal> 

//         <Modal 
//             animationType="fade"
//             transparent={true}
//             visible={this.state.clearcartModal}>
//             <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.8)'}}>
//               <ActivityIndicator color="#fff" size="large"/>
//               <Text style={{color:'#fff',fontFamily:'Montserrat'}}>Clear old cart items ...</Text>
//             </View>
//         </Modal>
        
//           <CustomNotification/>
//         </View>
//     );
//   }
// }
// }

