import React, { Component } from 'react';
import { View, Text,Image,ScrollView,TouchableOpacity,TextInput,StyleSheet} from 'react-native';
import {OverAllStyle} from './Styles/PrimaryStyle';
import { StyleContainer } from './Styles/CommonStyles';
import base64 from 'react-native-base64';
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';
import { Toast } from 'native-base';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Colors } from './Styles/colors';
import images from '../images/index'
import { Button, Snackbar } from 'react-native-paper';

class ChangePassword extends Component {
  
    state = {
      currentPassword:'',
      newPassword:'',
      confirmPassword:'',
      currentPasswordError:'',
      confirmPasswordError:'',
      newPasswordError:'',
      newPasswordHide:true,
      confirmPasswordHide:true,
      snackBarVisible:false
    };
  

    changeCurrentPassword(value){
      this.setState({currentPassword:value})
    }

    changeNewPassword(value){
      this.setState({newPassword:value})
    }
    
    changeConfirmPassword(value){
      this.setState({confirmPassword:value})
    }
    
    checkIsEmpty(value){
      if(value!==''){
        return true
      }else{
        return false
      }
    }
    
    async validateForm(){
      let currentPassword=true;
      if(this.checkIsEmpty(this.state.currentPassword)){
          this.setState({currentPasswordError:''})
          currentPassword=true
      }
      else{
        this.setState({currentPasswordError:'Password field should not be empty'}),
        currentPassword=false
      }
          if(this.checkIsEmpty(this.state.newPassword)){
                if(this.state.newPassword.length>=6){
                  this.setState({newPasswordError:''})                   
                }
                else{
                  this.setState({newPasswordError:'Password is too short'})
                }
              }
              else{
                this.setState({newPasswordError:'Password field should not be empty'})
              }
              if(this.checkIsEmpty(this.state.confirmPassword)){
                if(this.state.confirmPassword>=6){
                  this.setState({confirmPasswordError:''})
                }
                else{
                  this.setState({confirmPasswordError:'password is too short'}) 
                }
              }
            else{
                this.setState({confirmPasswordError:'Confirm password field should not be empty'})
              }

              if(this.state.confirmPassword!==this.state.newPassword){
                this.setState({confirmPasswordError:'Both passwords should be same'})
              }
              

              if((this.state.newPassword!==null&&this.state.newPassword.length>=6)&&(this.state.confirmPassword!==null&&this.state.confirmPassword.length>=6)&&(this.state.confirmPassword==this.state.newPassword)&&currentPassword==true)
              {
                //console.log('All oK')
                var newPassword=this.state.newPassword;
                var confirmPassword=this.state.confirmPassword;
                await this.setState({
                  newPassword:newPassword,
                  confirmPassword:confirmPassword
                })
                
                this.postPasswordDetail()
    
              }
              else{
                //console.log('check the whole form')
              }
    
            }
    
    
            async postPasswordDetail(){

              await fetch(baseUrl+this.props.userId+"/change-password", {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    "current_password":base64.encode(this.state.currentPassword),
                    "new_password":base64.encode(this.state.newPassword),
                    "confirm_password":base64.encode(this.state.confirmPassword)
                  })
          
                  })
                  .then((response) => response.json())
                  .then((responseJson) => {
                  //console.log(responseJson)
                  if(responseJson.success==true){
                    this.setState({snackBarVisible:true})
                    //console.log('all set')
                    // Toast.show({
                    //   text: "Password changed successfully .",
                    //   buttonText: "Okay",
                    //   position: "bottom",
                    //   textStyle: { fontFamily:'Montserrat' },
                    //   buttonTextStyle: { fontFamily: 'Montserrat-SemiBold' }
                    // })
                    AsyncStorage.removeItem('userId');
                    AsyncStorage.removeItem('guestId');
                    this.props.setuserId(null)
                    this.props.navigation.navigate("signin")
                  }
                  else{
                    this.setState({
                      currentPasswordError:responseJson.errorMessage[0]
                    })
                  }                  
                  })          
          }



  render() {
    return (
        
            <View style={OverAllStyle.MainContainer}>            
                  <View style={{width:'100%',marginBottom:40}}>
                        <TextInput style={OverAllStyle.TextInputView} placeholder="Current Password"
                          onSubmitEditing={() => {
                          this.newPasswordRef.focus();}}
                          onChangeText={(value)=>{this.changeCurrentPassword(value)}} 
                              returnKeyType={"next"}
                        />
                          {
                            this.state.currentPasswordError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.currentPasswordError}</Text>
                            </View>
                          }
                  </View>

                  <View style={OverAllStyle.TextInputWhiteMainView}>

                    <View style={{flexDirection:'row',width:'100%',marginVertical:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3}}>
                    <TextInput 
                        style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
                          fontSize:14,fontFamily:'Montserrat'}} 
                        placeholder="New Password"
                        secureTextEntry={this.state.newPasswordHide}
                        ref={(input) => { this.newPasswordRef = input; }}
                        onSubmitEditing={() => {
                        this.confirmPasswordRef.focus();}}
                        onChangeText={(value)=>{this.changeNewPassword(value)}} 
                        returnKeyType={"next"}
                        textContentType="oneTimeCode"
                       />
                        <TouchableOpacity onPress={()=>this.setState({newPasswordHide:!this.state.newPasswordHide})}>
                           {
                             this.state.newPasswordHide?
                             <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                             :
                             <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                           }
                           
                        </TouchableOpacity>
                    </View>

                          {
                            this.state.newPasswordError==''?null:
                            <View style={{width:'100%'}}>
                            <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.newPasswordError}</Text>
                            </View>
                          }
                  </View>

                  <View style={OverAllStyle.TextInputWhiteMainView}>
                      <View style={{flexDirection:'row',width:'100%',marginVertical:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3}}>
                      <TextInput 
                          style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
                          fontSize:14,fontFamily:'Montserrat'}} 
                          placeholder="Confirm Password"
                          secureTextEntry={this.state.confirmPasswordHide}
                          ref={(input) => { this.confirmPasswordRef = input; }}
                          onSubmitEditing={()=>{this.validateForm()}}
                          onChangeText={(value)=>{this.changeConfirmPassword(value)}} 
                          returnKeyType={"done"}
                          textContentType="oneTimeCode"
                      />
                      <TouchableOpacity onPress={()=>this.setState({confirmPasswordHide:!this.state.confirmPasswordHide})}>
                           {
                             this.state.confirmPasswordHide?
                             <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                             :
                             <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                           }
                           
                        </TouchableOpacity>
                    </View>

                      {
                        this.state.confirmPasswordError==''?null:
                        <View style={{width:'100%'}}>
                        <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.confirmPasswordError}</Text>
                        </View>
                      }
                  </View>


                  <TouchableOpacity onPress={()=>this.props.navigation.navigate("passwordChanged")} style={{width:'100%',marginVertical:20}}
                  onPress={()=>{this.validateForm()}}>
                      <View style={StyleContainer.ButtonOrange}>
                          <Text style={OverAllStyle.BtnOrangeTxtStyle} >SAVE</Text>
                      </View>
                  </TouchableOpacity>
                  <CustomNotification/>

                  <Snackbar
                    visible={this.state.snackBarVisible}
                    onDismiss={()=>this.setState({snackBarVisible:false})}
                    duration={3000}
                    action={{
                      label: 'Close',
                      onPress: () => {
                        this.setState({snackBarVisible:false})
                      },
                    }}>
                    Password changed.
                  </Snackbar>
            </View>        
    );
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
  }
}

function mapDispatchToProps(dispatch){
  return{
      setuserId:(value)=>dispatch({type:'setuserId',value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ChangePassword);
