import React, { Component } from 'react';
import { View, Text ,Dimensions,ScrollView,TouchableOpacity,Modal,ToastAndroid,KeyboardAvoidingView,Image, ActivityIndicator} from 'react-native';
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,SingleProductScreenStyle,ProductViewStyle,CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle'
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import  ContentLoader from 'react-native-easy-content-loader'

class SingleProductRelatedProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
        relatedProducts: [
            // {
            //   "id": 2,
            //   "name": "Sydney Sand Extra charge for truck delivery",
            //   "category_id": 1,
            //   "subcategory_id": 3,
            //   "weight": "1",
            //   "weight_unit": "ton",
            //   "default_variant_id": 3,
            //   "default_variant_price": 8.7,
            //   "quantity": 0,
            //   "is_favourite": true,
            //   "url": "http://selvisoftware.in/euroabrasives/api/80/product/2",
            //   "image_url": "http://selvisoftware.in/euroabrasives/images/products/2020-07-24.png"
            // }
          ],
        modalVisible:false,
        signleProductLoading:false,
        modalResponse:[],
        productOptions:[],
        productOptionCount:null,
        dummyArray:[],
        availableCombinations:[],
        combinationStatus:null,
        variantId:null,
        imageUrl:null,
        updateproductmodal:false,
        modalProductWeight:null,
        quantity:0,
        addToCartStatus:false,
        prodPrice:null,

        isediting:false,
        isdeitingValue:null
    };
  }

  componentDidMount(){
      //consle.log(this.props.relatedProducts)
      this.setState({relatedProducts:this.props.relatedProducts})
  }

  modalDecrement(){
    if(this.state.quantity==1){
      this.setState({quantity:1,isediting:false,modalVisible:false,modalVisible:true})     
    }else{
        this.setState({quantity:this.state.quantity-1,isediting:false,modalVisible:false,modalVisible:true})     
      }
  }
  
  async modalIncrement(){
    this.setState({quantity:parseInt(this.state.quantity)+1,isediting:false,modalVisible:false,modalVisible:true})
  }

  async customQty(text){
    await this.setState({quantity:text,modalVisible:false,modalVisible:true,iseditingValue:text})
  }

  incdecEdit(){
    var qty=this.state.quantity.toString()
    //consle.log(qty)
    this.setState({iseditingValue:qty,isediting:true})
   }

  async selectedProduct(id,imageUrl){
    //consle.log(id,imageUrl)
    this.setState({modalVisible:true,signleProductLoading:true})
    await fetch(baseUrl+this.props.userId+'/product/'+id)
          .then((response) => response.json())
          .then((responseJson) => {
            //consle.log(responseJson)
            var dummyArray=new Array(responseJson.product_option_count)
            var variantOption=responseJson.product_options
            variantOption.map((item,key)=>{            
                item.option.map((item,index)=>{
                  //if(responseJson.default_variant_id==item.variant_id){
                    if(responseJson.product_variants[0].id==item.variant_id){
                    item.selected=true
                    dummyArray.splice(0,0,item.option_value_id)
                  }
                })
            })
            var filtered = dummyArray.filter(item=> {
              return item != null;
            });
            
            var reverced=filtered.reverse()
            
            if(responseJson.quantity==0){
              this.setState({quantity:1})
            }else{
              this.setState({quantity:responseJson.quantity})
            }
            
              this.setState({
                  dummyArray:reverced,
                  modalResponse:responseJson,
                  productOptions:variantOption,
                  productOptionCount:responseJson.product_option_count,
                  availableCombinations:responseJson.product_variants,
                  imageUrl:imageUrl,
                  signleProductLoading:false
              })
  
              if(this.state.productOptions.length!=0){
                this.compareArrayValues()
              }else{
                this.setState({combinationStatus:true,variantId:this.state.availableCombinations[0].id,prodPrice:this.state.availableCombinations[0].price.toFixed(2)})
              }
              
          })  
  }
  
  changeProductVariant(key,indexPosition){
  
    var dummyArray=this.state.dummyArray
    var productOptions=this.state.productOptions
    productOptions.map((item,index)=>{
      if(index==key){
        item.option.map((item,index1)=>{
          item.selected=false
          if(index1==indexPosition){
            item.selected=true
            dummyArray[key]=item.option_value_id 
          }
        })
      }
    })
    this.setState({productOptions:productOptions,dummyArray:dummyArray})
    this.compareArrayValues()
  }
  
  compareArrayValues(){
    let array=this.state.availableCombinations
  
    for(var i=0;i<array.length;i++){
      if(this.state.productOptionCount==1){
        // if(this.state.dummyArray[0]==array[i].product_option_value_id1){
          if(this.state.dummyArray[0]==array[i].product_option_value_id1){
            // var modalresponse=this.state.modalResponse
            // modalresponse.default_variant_price=array[i].price
            // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
            this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            //this.getModalProductDetails(this.state.variantId)
            break;
          // }else{
          //   this.setState({combinationStatus:false})
          //   break
          // }
        }
      }else if(this.state.productOptionCount==2){
        // if((this.state.dummyArray[0]==array[i].product_option_value_id1) || (this.state.dummyArray[1]==array[i].product_option_value_id2)){
          if((this.state.dummyArray[0]==array[i].product_option_value_id1) && (this.state.dummyArray[1]==array[i].product_option_value_id2)){
            // var modalresponse=this.state.modalResponse
            // modalresponse.default_variant_price=array[i].price
            // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
            this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            break;
          }
        //   else{
        //     this.setState({combinationStatus:false})
        //     break
        //   } 
        // }
      }else if(this.state.productOptionCount==3){
        // if(this.state.dummyArray[0]==array[i].product_option_value_id1 || this.state.dummyArray[1]==array[i].product_option_value_id2 ||
        //   this.state.dummyArray[2]==array[i].product_option_value_id3){
            if(this.state.dummyArray[0]==array[i].product_option_value_id1 && this.state.dummyArray[1]==array[i].product_option_value_id2 &&
              this.state.dummyArray[2]==array[i].product_option_value_id3){
                // var modalresponse=this.state.modalResponse
                // modalresponse.default_variant_price=array[i].price
                // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
                this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
              break;
            // }else{
            //   this.setState({combinationStatus:false})
            //   break
            // }
          }
      }
    } 
  }
  
  
  async addToCartWithVariants(){
    //consle.log(this.props.userId,this.state.modalResponse.id,this.state.variantId,this.state.quantity)
   this.setState({addTocartLoading:true,isediting:false})
   await fetch(baseUrl+this.props.userId+'/addtocart', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "customer_id":this.props.userId,
      "product_id": this.state.modalResponse.id,
      "product_variant_id" :this.state.variantId,
      "quantity":this.state.quantity,
    })
   })
     .then((response) => response.json())
     .then((responseJson) => {
        //consle.log("response"+responseJson)
        if(responseJson.success==true){
        this.setState({addTocartLoading:false,modalVisible:false})
        this.props.setIncreaseCartCount()

        }                  
     })
     .catch((err)=>{
      ////consle.log(err)
  })  
  await this.props.setCategoryProductCountChanged(true)
  await this.props.setProductCountChanged(false)
  this.setState({addToCartStatus:true})
  setTimeout(()=>{
    this.setState({addToCartStatus:false})
   },1000)
  }
  
  
  addFavourite(value,index){
    var type;
    if(this.state.relatedProducts[index].is_favourite==true){
      let products=[...this.state.relatedProducts]
      products[index].is_favourite=false
      type=1
      this.setState({products:products})
    }else{
      let products=[...this.state.relatedProducts]
      products[index].is_favourite=true
      type=0
      this.setState({products:products})
    }
    ////consle.log(this.props.userId,this.state.relatedProducts[index].id,this.state.relatedProducts[index].variant_id,type)
    fetch(baseUrl+this.props.userId+'/wishlist', {
      method: 'POST',
      headers: {
      'Content-Type': 'application/json',
      },
      body: JSON.stringify({
  
       "customer_id": this.props.userId,
       "product_id":this.state.relatedProducts[index].id,
       "is_wishlist":type
      })
    })
   .then((response) => response.json())
   .then((responseJson) => {
       //consle.log(responseJson)
  })
  }
 
  render() {
    return (
      <View>
        {
             this.state.relatedProducts.length!=0?
             <View style={{paddingVertical:10,paddingHorizontal:20}}>
     
                <View style={{paddingVertical:10,borderBottomColor:'#e3e6e6',borderBottomWidth:1}}>
                    <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:17}}>Related products</Text>
                </View>
                {
                    this.state.relatedProducts.map((item,index)=>{
                        return(
                            <View key={index} style={ProductViewStyle.ProductMainContainer}>
                                

                                        <View style={ProductViewStyle.ProductView}>

                                            <View style={ProductViewStyle.ProductImageView}>
                                                <Image source={{uri:item.image_url}} style={ProductViewStyle.ImageCoverStyle} />
                                                <View style={ProductViewStyle.ZindexView}>
                                                    <TouchableOpacity style={ProductViewStyle.HeartView}>
                                                    {
                                                        item.is_favourite==true?
                                                        <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("remove",index)}>
                                                            <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageContainStyle} />  
                                                        </TouchableOpacity>
                                                        :
                                                        <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("add",index)}>
                                                            <Image source={images.favouriteWithoutBackgroud} style={StyleContainer.ImageContainStyle} />
                                                        </TouchableOpacity>
                                                    }
                                                    </TouchableOpacity>
                                                </View>
                                            </View>

                                            <View onPress={()=>this.updateSingleProductScreen(item.id)} style={ProductViewStyle.ProductDetailView}>
                                                    <Text numberOfLines={2}  ellipsizeMode='tail' style={{fontFamily:'Montserrat-SemiBold',fontSize:14,marginBottom:3,}}>{item.name}</Text>
                                                    <Text style={{fontFamily:"Montserrat",color:'red',fontSize:13,marginBottom:3}}>$ {item.default_variant_price}</Text>
                                                    {/* {
                                                        this.props.deliveryType==0?
                                                        <Text style={{fontFamily:"Montserrat",color:'#3bcf0a',fontSize:10}}>Delivery available</Text>:
                                                        <Text numberOfLines={2} ellipsizeMode='tail' style={StyleContainer.RedIndicationTxtStyle}>Sorry we do not delivery to this area. Please arrange pick-up in store.</Text>
                                                    } */}
                                            </View>
                                        </View>
                                       
                                        <View style={{width:'25%',flex:1,justifyContent:'center',alignItems:'center'}}>
                                            <TouchableOpacity style={{width:'100%',height:25,marginBottom:5}} 
                                           onPress={()=>{ this.selectedProduct(item.id,item.image_url) }} >
                                                <Image source={images.plusIcon} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}/>
                                            </TouchableOpacity>
                                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:10,textAlign:'center',color:'#fa7153'}}>ADD</Text>
                                        </View> 
                            </View>
                        )
                    })
                 }
                </View>
                :
                null
                }

                <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={()=>this.setState({modalVisible:false})}>
                  <KeyboardAvoidingView  keyboardVerticalOffset={20} behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
                    <View style={ModelStyle.modelContainer}>
                        <TouchableOpacity onPress={()=>this.setState({modalVisible:false})} 
                         style={{marginBottom:10}}>
                            <Image source={images.cancel} style={{width:25,height:25,resizeMode:'cover'}}/>
                        </TouchableOpacity>
                        <View style={{width:'100%',backgroundColor:'#FFF',padding:20,borderRadius:20,marginBottom:-15}}>
                         {
                           this.state.signleProductLoading==true?
                           <View style={{width:'100%',height:'50%',paddingVertical:20}} >
                              <ContentLoader active avatar aShape="square" pRows={2}/>

                              <View style={{marginTop:20,borderWidth:0.5,borderStyle:'dashed',paddingTop:10,marginBottom:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              <View style={{borderWidth:0.5,borderStyle:'dashed',paddingTop:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              {/* <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                </TouchableOpacity> */}
                            </View>
                           :
                           <ScrollView showsVerticalScrollIndicator={false}>
                           
                            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>ADD TO CART</Text>
                               {
                                 this.state.updateproductmodal==true?
                                 <ActivityIndicator/>
                                 :
                                 null
                               }
                            </View> 
                            
                             <View style={ModelStyle.modelProductContainer}>
                                <Image source={{uri:this.state.imageUrl}} style={{width:60,height:60,borderRadius:7}}></Image>
                                <View style={{paddingLeft:20,width:'80%'}}>
                                    <Text numberOfLines={2}  ellipsizeMode="tail" style={[ModelStyle.modelProductName,{fontFamily:"Montserrat-SemiBold"}]}>{this.state.modalResponse.name}</Text>
                                    <Text style={ModelStyle.modelProductPrice}>$ {this.state.prodPrice}</Text>
                                </View>
                            </View>

                           
                            <View>
                            {
                              this.state.productOptions.map((item,key)=>{
                                return(
                                    <View style={{borderWidth:0.5,paddingBottom:5,borderColor:'gray',borderStyle:'dashed',borderRadius:5,paddingHorizontal:5,marginVertical:5,backgroundColor:'#fff'}}>
                                      <Text style={{marginBottom:3,paddingLeft:5,paddingTop:5}}>{item.type}</Text>
                                      <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                                      {
                                          item.option.map((item,index)=>{
                                            
                                            if(item.selected==true){
                                              if(this.state.combinationStatus==true){
                                                return(
                                                  <TouchableOpacity style={{paddingVertical:7,borderWidth:0.3,borderColor:'gray',borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#fa7153',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',color:'#fff',fontFamily:'MontSerrat-SemiBold'}}>{item.value}</Text>
                                                  </TouchableOpacity>     
                                                )
                                              }else{
                                                  return(
                                                  <TouchableOpacity style={{paddingVertical:7,borderWidth:1,borderRadius:20,borderColor:'#fa7153',minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#f2f5f6',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                                  </TouchableOpacity>
                                                )
                                              }
                                            }else{
                                              return(
                                              <TouchableOpacity onPress={()=>this.changeProductVariant(key,index)} style={{paddingVertical:7,borderWidth:0.5,borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                              </TouchableOpacity>
                                            )
                                            }
                                          })
                                        }
                                      </View>
                                    </View>
                                )
                              })
                            }
                            {
                              this.state.combinationStatus==true?
                              null
                              :
                              <View style={{flexDirection:'row',alignItems:'center',paddingBottom:5}}> 
                                  <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                                  <Text style={{color:'red',fontSize:12,fontFamily:'MontSerrat',paddingLeft:5}}>Selected combination not available</Text>
                              </View>
                            }
                          </View>
                          
                          <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,paddingVertical:15,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:"Montserrat",fontSize:15}}>Price :  </Text>
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                </View>
                                <View style={{justifyContent:'center',alignItems:'flex-end'}}>
                                    <View style={CartScreenStyle.IncDecMainContainer}>
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>{this.modalDecrement()}}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                      </TouchableOpacity>
                                      
                                      {
                                        this.state.isediting==true?
                                        <TouchableOpacity style={{width:'30%',justifyContent:'center'}}>
                                        <TextInput
                                          autoFocus={this.state.isediting}
                                          style={{width:'100%',height:24,paddingVertical:0,fontSize:12,fontFamily:"Montserrat",textAlign:'center',color:Colors.themeColor}}
                                          onChangeText={(text)=>this.customQty(text)}
                                          value={this.state.iseditingValue}
                                          keyboardType={"phone-pad"}
                                        />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={()=>this.incdecEdit()} style={{width:'30%',alignItems:'center',justifyContent:'center'}}>
                                          <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                        </TouchableOpacity>
                                      }
                                      
                                      
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>this.modalIncrement()}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                      </TouchableOpacity>
                                  </View>
                                </View>
                            
                          </View>
                            {/* <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom: 10,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{width:'50%',borderRightWidth:0.5,borderColor:'#c3c3c3',paddingVertical:20}}>
                                    
                                     {
                                       this.state.modalResponse.weight==""?
                                       <View>
                                        <Text style={{fontFamily:"Montserrat"}}>Total Weight : </Text>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:15}}>NAN</Text>
                                       </View>
                                       :
                                       <View>
                                        <Text style={{fontFamily:"Montserrat"}}>Total Weight : </Text>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:15}}>{this.state.modalResponse.weight*this.state.quantity} {this.state.modalResponse.weight_unit}</Text>
                                       </View>
                                     }
                                      
                                    
                                </View>
                                <View style={{flexDirection:'row',width:"50%",justifyContent:'space-between',paddingVertical:20,paddingLeft:10}}>
                                    <View style={{width:'60%'}}>
                                        <Text style={{fontFamily:"Montserrat"}}>Price :</Text>
                                        <View style={{width:'100%'}}>
                                            <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                          </View>
                                    </View>

                                    <View style={{justifyContent:'center',alignItems:'flex-end',flex:1,widht:'50%'}}>
                                       <View style={CartScreenStyle.IncDecMainContainer}>
                                          <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                          onPress={()=>{this.modalDecrement()}}>
                                              <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                          </TouchableOpacity>
                                          
                                          <View style={CartScreenStyle.IncDecQtyContainer}>
                                          <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                          </View>
                                          
                                          <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                          onPress={()=>this.modalIncrement()}>
                                              <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                          </TouchableOpacity>
                                      </View>
                                    </View>
                                </View>
                            </View> */}

                            {
                              this.state.combinationStatus==true?
                              <View>
                                {
                                  this.state.addTocartLoading?
                                  <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <ActivityIndicator color="#fff"  />
                                  </TouchableOpacity>
                                  :
                                  <TouchableOpacity onPress={()=>this.addToCartWithVariants()} 
                                  style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                  </TouchableOpacity>
                                }
                              </View>
                              :
                              <View style={{backgroundColor:'#e3b1a6',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                              </View>
                            }
                            </ScrollView> 
                         }
                        </View>
                    </View>
                    </KeyboardAvoidingView>
            </Modal>    
           
            <Modal 
                animationType="fade"
                transparent={true}
                visible={this.state.addToCartStatus}>
                <View style={{flex:1,justifyContent:'flex-end'}}>
                    <View style={{backgroundColor:'gray',width:'80%',alignSelf:'center',padding:8,marginBottom:"20%",borderRadius:5}}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',color:'#fff',fontSize:14,paddingBottom:5}}>Item added successfully .</Text>
                    </View>   
                </View>
            </Modal>     
      </View>
    );
  }
}
function mapStateToProps(state){
    return{
        userId:state.userId,
        categoryId:state.categoryId,
        productCountChanged:state.productCountChanged,
        categoryProductCountChange:state.categoryProductCountChange,
        productId:state.productId,
        singleProductId:state.singleProductId,
        postCode:state.postCode,
        deliveryType:state.deliveryType,
        singleProductPageUpdate:state.singleProductPageUpdate,
        relatedProducts:state.relatedProducts
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
        setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
        setCartCount:(value)=>dispatch({type:"setCartCount",value}),
        setProductId:(value)=>dispatch({type:"setProductId",value}),
        setSingleProductId:(value)=>dispatch({type:"setSingleProductId",value}), 
        setSingleProductVariantId:(value)=>dispatch({type:"setSingleProductVariantId",value}),
        setIncreaseCartCount:()=>dispatch({type:"IncreaseCartCount"}),
        setDecreaseCartCount:()=>dispatch({type:"DecreaseCartCount"}),
        setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
        //setOrderDetails:(value)=>dispatch({type:"setOrderDetails",value}),
        setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
        setSingleProductPageUpdate:(value)=>dispatch({type:"setSingleProductPageUpdate",value}),
        setorderDeliverType:(value)=>dispatch({type:'setorderDeliverType',value}),
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(SingleProductRelatedProducts);
  
