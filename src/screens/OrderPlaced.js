import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity,StyleSheet,Modal,BackHandler, ActivityIndicator} from 'react-native';
import images from '../images/index';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OrderPlacedScreenStyle} from './Styles/PrimaryStyle';
import { AndroidBackHandler } from "react-navigation-backhandler";
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import { PaypalDetails } from '../DataModel/ModelClass';
import CustomNotification from './CustomNotification';
import  ContentLoader from 'react-native-easy-content-loader'
import { BarIndicator } from 'react-native-indicators';

class OrderPlaced extends Component {
 
  state = {
      loading:false,
      modalVisible:false,
      paymentType:null,
      postUrl:"",
      grandTotal:0,
      fromBuyNow:false,
      rowId:'',
      confirmationModal:false
  }
  
   async componentDidMount(){
     await this.postOrder();
    }

   async postOrder(){
      this.setState({loading:true})

      if(this.props.orderDeliverType==0){
        console.log("delivery 0")
      await this.setState({grandTotal:this.props.totalAmount+this.props.deliveryCharge})
      }else{
       console.log("delivery 1")
      await this.setState({grandTotal:this.props.totalAmount})
      }
       console.log(this.state.grandTotal,this.props.totalWeight,this.props.totalQty)

      if(this.props.tradeAccount==true){
      await this.setState({paymentType:"trade-account"})
      }else{
        if(this.props.paypalPaymentState=="succeeded"){
          await  this.setState({paymentType:"stripe"})    
        }else{
          await  this.setState({paymentType:"paypal"})
        }      
      }

      if(this.props.singleProductId==null){
        await this.setState({postUrl:baseUrl+this.props.userId+'/checkout',fromBuyNow:false,rowId:''})
      }else{
        await this.setState({postUrl:baseUrl+this.props.userId+'/checkout',fromBuyNow:true,rowId:this.props.singleProductId})
      }
    
      console.log(
        JSON.stringify({
          "customer_id"   :this.props.userId ,
          "delivery_type" : this.props.orderDeliverType,
          "vehicle_id" : this.props.vehicleId,
          "time_slot_id"     : this.props.timeSlot,
          "delivery_date" :this.props.dateValue,
          "pickup_person_name" :this.props.pickupPersonName,
          "pickup_person_number" : this.props.pickupPersonNumber,
          "transaction_id" :this.props.paypalPaymentId,  
          "payment_type"  :this.state.paymentType,
          "payment_status":this.props.paypalPaymentState,
          "shipping_address_id":this.props.shippingAddressId,
          "billing_address_id":this.props.billingAddressId,
          "delivery_fee":this.props.deliveryCharge,
          "grand_total":this.state.grandTotal,
          "total_quantity":this.props.totalQty,
          "total_weight":this.props.totalWeight,
          "from_buynow":this.state.fromBuyNow,
          "row_id":this.state.rowId,
          "sub_total":this.props.totalAmount,
          "comments":this.props.comments
        }))
        
     await fetch(this.state.postUrl, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "customer_id"   :this.props.userId ,
          "delivery_type" : this.props.orderDeliverType,
          "vehicle_id" : this.props.vehicleId,
          "time_slot_id"     : this.props.timeSlot,
          "delivery_date" :this.props.dateValue,
          "pickup_person_name" :this.props.pickupPersonName,
          "pickup_person_number" : this.props.pickupPersonNumber,
          "transaction_id" :this.props.paypalPaymentId,  
          "payment_type"  :this.state.paymentType,
          "payment_status":this.props.paypalPaymentState,
          "shipping_address_id":this.props.shippingAddressId,
          "billing_address_id":this.props.billingAddressId,
          "delivery_fee":this.props.deliveryCharge,
          "grand_total":this.state.grandTotal,
          "total_quantity":this.props.totalQty,
          "total_weight":this.props.totalWeight,
          "from_buynow":this.state.fromBuyNow,
          "row_id":this.state.rowId,
          "sub_total":this.props.totalAmount,
          "comments":this.props.comments
         })
       })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log(responseJson)    
          if(responseJson.success==true){
             this.setDeliveryType(responseJson)
           }
      }).catch((err)=>{
        console.log(err)
        this.setDeliveryType()
        this.setState({loading:false})
      }) 

    }

   async setDeliveryType(value){
      // if(this.props.choosedPickupMethod==false){
      //   await this.props.setDeliveryType(1)
      //  }else{
      //   await this.props.setDeliveryType(0)
      //  }
       await this.setState({loading:false})
       await this.props.setPaypalPaymentId(null)
       await this.props.setPaypalPaymentState(null)
       await this.props.setTradeAccountStatus(false)
       console.log(this.props.tradeAccount,this.props.paypalPaymentId,this.props.paypalPaymentState)
       await fetch(baseUrl+"send-mail", {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          "from":"new-order",
          "user_id":this.props.userId, 
          "orderid":value.order_id,
         })
       })
      .then((response) => response.json())
      .then((responseJson) => {
          console.log("order placed")
          console.log(responseJson)  
      })
    }

   async navigateOrderScreen(){
      await this.props.setOrderCountChanged(true)
      await this.props.setProductCountChanged(true)
      await this.props.setCategoryProductCountChanged(true)
      this.props.navigation.navigate("drawer",{screen:'myOrder'})
    }

    async navigateHomeScreen(){
      await this.props.setOrderCountChanged(true)
      await this.props.setProductCountChanged(true)
      await this.props.setCategoryProductCountChanged(true)
      await this.props.setinitialRouteInHome("homeScreen")
      // this.props.navigation.navigate("drawer",{screen:'home'})
      this.props.navigation.navigate("homeScreen")
    }

      onBackButtonPressAndroid = () => {
        if (this.state.modalVisible==false) {
          this.setState({modalVisible:true})
          return true;
        }
        return false;
      };

      backpress(){
        this.setState({modalVisible:!this.state.modalVisible})
      }
      
      backHandler=()=>{
        BackHandler.exitApp()
      }

  render() {
    if(this.state.loading==true){
      return(
        <View style={{flex:1,alignItems:'center'}}>
          <BarIndicator color="#fa7153"/>
          <Text style={{fontFamily:'MontSerrat-SemiBold',paddingBottom:30,fontSize:16}}>Placing your order, please wait.</Text>
        </View>
      )
    }else{
    return (
      <View style={StyleContainer.MainContainer}>
         <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:21,letterSpacing:2,marginBottom:3,textAlign:'center'}}>ORDER CONFIRMED !</Text>
        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:11,letterSpacing:1,color:'#000',textAlign:'center'}}>YOUR ORDER HAS BEEN SUCCESSFULLY RECEIVED</Text>
        <View style={OrderPlacedScreenStyle.ImgMainView}>
          <Image source={images.available} style={StyleContainer.ImageContainStyle}/>
        </View>
        <View style={OrderPlacedScreenStyle.SubQuoteView}>
          <Text style={{fontFamily:"Montserrat",fontSize:12,textAlign:'center',color:'#000'}}>Your order is PENDING. It has been sent to EURO team for confirmation. The team will CONFIRM your booking shortly and you will receive a notification.</Text>
          <Text style={{fontFamily:"Montserrat",fontSize:10,textAlign:'center',color:'#000',paddingTop:10}}>ORDERS ARE CHECKED & CONFIRMED BY OUR OFFICE MON - FRI, BETWEEN 8am - 4pm. ENQUIRIES 02 9557 7661</Text>
        </View>
        

        {/* <TouchableOpacity onPress={()=>this.navigateOrderScreen()} style={StyleContainer.ButtonOrange}>
            <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:15}}>VIEW PLACED ORDER</Text>
        </TouchableOpacity> */}

        <TouchableOpacity onPress={()=>this.navigateHomeScreen()} style={StyleContainer.ButtonBorder}>
            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15}}>CONTINUE SHOPPING</Text>
        </TouchableOpacity>

        {/* <TouchableOpacity style={{marginTop:20}} onPress={()=>this.setState({confirmationModal:true})}>
          <Text>touch</Text>
        </TouchableOpacity> */}

         <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={()=>this.setState({modalVisible:false})}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.5)'}}>
                  
                  <View style={{width:300,height:170,backgroundColor:'#FFF',padding:20,borderRadius:20}}>
                  <View>
                      <Text style={{fontFamily:"Montserrat",color:'black',fontSize:17}}>Order placed !</Text>
                      <Text lineBreakMode='clip' numberOfLines={4} style={{fontFamily:"Montserrat",paddingVertical:20,fontSize:15}}>   Your previous order is placed ,if you want to continue or exit?</Text>
                      <View style={{flexDirection:'row',alignItems:'flex-end',alignSelf:'flex-end'}}>
                      <TouchableOpacity onPress={()=>this.backpress()}>
                          <Text style={{fontFamily:"Montserrat",color:'#ae0000',fontSize:17,paddingRight:20}}>Continue</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.backHandler()}>
                          <Text style={{fontFamily:"Montserrat",color:'#ae0000',fontSize:17}}>Exit</Text>
                      </TouchableOpacity>
                      </View>
                  </View>
                  </View>
              </View>
          </Modal>

          {/* <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.confirmationModal}
            onRequestClose={() => {
            Alert.alert('Modal has been closed.');
            }}>
                <View style={{flex:1,justifyContent:'flex-end',backgroundColor:'rgba(52,52,52,0.5)'}}>
                  <View style={{backgroundColor:'#fff',width:'100%',height:'70%',borderTopLeftRadius:20,borderTopRightRadius:20}}>
                      <View style={{flexDirection:'row'}}>
                          <ContentLoader active  pRows={0} containerStyles={{width:800,height:8,marginLeft:-10}}/>
                          <ContentLoader active  pRows={0} containerStyles={{width:'50%',height:30}}/>
                      </View>
                      
                  </View>
                </View>
              </Modal> */}
          </AndroidBackHandler>
          <CustomNotification/>


      </View>
    );
    }
  }
}


function mapStateToProps(state){
  return{
      userId:state.userId,
      orderDeliverType:state.orderDeliverType,
      vehicleId:state.vehicleId,
      timeSlot:state.timeSlot,
      dateValue:state.dateValue,
      paypalPaymentId:state.paypalPaymentId,
      paypalPaymentState:state.paypalPaymentState,
      tradeAccount:state.tradeAccount,
      pickupPersonName:state.pickupPersonName,
      pickupPersonNumber:state.pickupPersonNumber,
      choosedPickupMethod:state.choosedPickupMethod,
      singleProductId:state.singleProductId,
      singleProductVariantId:state.singleProductVariantId,
      billingAddressId:state.billingAddressId,
      shippingAddressId:state.shippingAddressId,
      deliveryCharge:state.deliveryCharge,
      totalAmount:state.totalAmount,
      totalQty:state.totalQty,
      deliveryCharge:state.deliveryCharge,
      totalWeight:state.totalWeight,
      comments:state.comments
  }
}

function mapDispatchToProps(dispatch){
  return{
      setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
      setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
      setOrderCountChanged:(value)=>dispatch({type:"setOrderCountChanged",value}),
      setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
      setPaypalPaymentId:(value)=>dispatch({type:"setPaypalPaymentId",value}),
      setPaypalPaymentState:(value)=>dispatch({type:"setPaypalPaymentState",value}),
      setTradeAccountStatus:(value)=>dispatch({type:"setTradeAccount",value}),
      setinitialRouteInHome:(value)=>dispatch({type:"setinitialRouteInHome",value})
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(OrderPlaced);


