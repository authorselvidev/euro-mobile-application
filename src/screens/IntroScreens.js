
import React, { Component } from 'react';
import { View, Text,TouchableOpacity,Image,StyleSheet,ActivityIndicator, SafeAreaView } from 'react-native';
import Swiper from 'react-native-swiper'
import images from '../images/index'
import AuthenticationService from '../Controller/ServiceClass'
import { SliderModel, SliderModelResponse } from '../DataModel/ModelClass';
import NetInfo from '@react-native-community/netinfo'
import { baseUrl } from '../Controller/baseUrl';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class IntroScreens extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false,
      index:0,
      array:[],
      networkFailed:false
    };
  }

async componentDidMount(){
  // await this.setState({loading:true})
  // await AuthenticationService.getIntroScreens()
  // await this.setState({array:SliderModelResponse.response,loading:false})
  this.getSliders()
 }

 getSliders(){
   NetInfo.fetch().then(state => {
    //console.log("Connection type", state.type);
    this.setState({loading:true})
    if(state.isConnected==true){
        fetch(baseUrl+"slider")
        .then((response)=>response.json())
        .then((responseJson)=>{
          //SliderModelResponse.response=responseJson;
          //console.log(responseJson)
          this.setState({array:responseJson,loading:false,networkFailed:false})    
        })
        .catch((err)=>{
          //console.log(err)
      }) 
    }else{
      this.setState({networkFailed:true,loading:false})
      //alert("Network connection failed")
    }
  }) 
 }

 async setVisited(){
  await AsyncStorage.setItem('IntroScreenVisited',JSON.stringify("visited"))
  this.props.navigation.navigate('signin')
 }

swipped(index){
  this.setState({index:index})
 }

 retry(){
  //console.log("retry")
  this.setState({networkFailed:false})
  this.getSliders()
}

  render() {
    if(this.state.loading==true){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
          <ActivityIndicator size="large" color="#fa7153"/>
        </View>
      )
    }else if(this.state.networkFailed==true){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
            <Image source={images.networkFailed} style={{width:70,margin:8,height:100,resizeMode:'contain'}}/>
            <Text style={{fontFamily:"Montserrat",fontSize:17,color:'black',paddingBottom:10}}>Connection Error</Text>
            <Text style={{fontFamily:"Montserrat"}}>Please check your network and try again.</Text>
            <TouchableOpacity onPress={()=>this.retry()} style={{marginVertical:20,borderColor:'#fa7153',borderWidth:1,paddingHorizontal:30,paddingVertical:5,borderRadius:5}}>
              <Text style={{fontFamily:"Montserrat",color:'#fa7153',fontSize:16}}>Retry</Text>
            </TouchableOpacity>
        </View>
      )
    }else{
      return (
        <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
                <Swiper ref='swiper' activeDotColor={"#fa7153"} 
                paginationStyle={styles.paginationStyle} 
                //autoplay={true}
                loop={false}
                onIndexChanged={(index)=>this.swipped(index)}
                >
                  {
                    this.state.array.map((item,index)=>{
                      
                      return(
                  
                        <View key={index} style={{flex:1}}>
                          <View style={{flex:1}}>
                            <TouchableOpacity onPress={()=>this.setVisited()} style={styles.skipButton}>
                              <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff'}}>SKIP</Text>
                            </TouchableOpacity>
                            <View style={styles.imageContainer}>
                              <Image source={{uri:item.images}} style={styles.imageStyle}></Image>
                            </View>
                          </View>
                          <View style={styles.textContainer}>
                            <Text style={styles.text1}>{item.title}</Text>
                            <Text style={styles.text2}>{item.text1}</Text>
                            <Text style={styles.text3}>{item.text2}</Text>
                            <Text>{item.text4}</Text>
                          </View>
                        </View>        
                      )
                    })
                  }
                </Swiper>
                  {
                      this.state.array.length-1==this.state.index?
                      <TouchableOpacity onPress={() => this.setVisited()} style={styles.nextButton}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,color:'#fff'}}>NEXT</Text>
                      </TouchableOpacity>
                      :
                      <TouchableOpacity onPress={() => this.refs.swiper.scrollBy(1)} style={styles.nextButton}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,color:'#fff'}}>NEXT</Text>
                      </TouchableOpacity>
                    
                    
                    }

          </SafeAreaView>  
      );
    }
  }
}

const styles=StyleSheet.create({
  paginationStyle:{
    position:'absolute',
    bottom: 20
  },
  skipButton:{
    position:'relative',
    alignSelf:'flex-end',
    marginTop:20,
    marginRight:20,
    paddingHorizontal:20,
    paddingVertical:4,
    borderRadius:20,
    backgroundColor:'#8f8e8d'
  },
  imageContainer:{
    flex:1,
    justifyContent:'flex-end',
    alignItems:'center',
  },
  imageStyle:{
    resizeMode:'contain',
    height:'100%',
    width:'90%'
  },
  textContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    paddingHorizontal:20
  },
  text1:{
    fontSize:22,
    color:'#fa7153',
    textAlign:'center',
    fontFamily:"Montserrat-SemiBold"
  },
  text2:{
    fontSize:18,
    paddingTop:25,
    textAlign:'center',
    fontFamily:"Montserrat"
  },
  text3:{
    fontSize:15,
    paddingTop:5,
    textAlign:'center',
    fontFamily:"Montserrat"
  },
  nextButton:{
    marginVertical:30,
    paddingVertical:8,
    borderRadius:3,
    width:'70%',
    backgroundColor:'#fa7153',
    alignSelf:'center',
    alignItems:'center'
  }
})


