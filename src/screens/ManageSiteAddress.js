import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, Image, Modal, ActivityIndicator } from 'react-native';
import images from '../images/index'
import { StyleContainer } from '../screens/Styles/CommonStyles';
import { OverAllStyle, ManageSiteAddressStyle } from './Styles/PrimaryStyle';
import { connect } from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';
import { BarIndicator } from 'react-native-indicators';

class ManageSiteAddress extends Component {

    state = {
        loading: false,
        selectedAddress: null,
        existingAddress: [],
        deleteAddressId: null,
        deleteAddress: false,
        deleteAddressModalShowing: false,
        deleteAddressLoading: false,
        setPrimaryLoading:false
    };

    componentDidMount() {
        this.getExistingSites();
        console.log("isShippingAddress " + this.props.isShippingAddress)

    }

    async componentWillUnmount() {
        //if(this.state.existingAddress.length==0){
        // await this.props.setAddressChanged(true)
        //}
    }

    componentDidUpdate(prevProps) {
        if (this.props.addAddressRefershing != prevProps.addAddressRefershing) {
            this.getExistingSites();
        }
    }

    async getExistingSites() {
        this.setState({ loading: true })
        var url = baseUrl + this.props.userId + "/address"
        if (this.props.isShippingAddress == true) {
            url = baseUrl + this.props.userId + "/address?add_from=shipping"
        }
        console.log(url)
        await fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson))
                this.setState({ existingAddress: responseJson.data, loading: false })
            }).catch((err) => {
            })
        await this.props.setaddAddressRefershing(false)
    }

    async selectedAddress(id) {

        let addressType;
        if (this.props.isShippingAddress == true) {
            addressType = "shipping"
        } else {
            addressType = "billing"
        }
        console.log(id, addressType)
        await this.setState({ selectedAddress: id })
        fetch(baseUrl + this.props.userId + "/change-address", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "address_id": this.state.selectedAddress,
                "address_type": addressType
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
            }).catch((err) => {
            })
        await this.props.setAddressChanged(true)
        if (this.props.isFromCheckout == true) {
            this.props.navigation.goBack()
        }
    }

    async deleteAddress(id) {
        await this.setState({ deleteAddressId: id, deleteAddressModalShowing: true })
        console.log(this.state.deleteAddressId)
    }

    async remove() {
        await this.setState({ deleteAddressLoading: true })
        await fetch(baseUrl + this.props.userId + "/address/" + this.state.deleteAddressId + "/delete")
            .then((response) => response.json())
            .then((responseJson) => {
                this.props.setaddAddressRefershing(true)
                this.setState({ deleteAddressLoading: false, deleteAddressModalShowing: false, deleteAddressId: null })
                this.getExistingSites()
            })
    }

    async editAddress(id) {
        await this.props.setAddressId(id)
        this.props.navigation.navigate("editAddress")
    }

    async navigateCreateNewSite() {
        this.props.navigation.navigate('createNewSite',{isFromSignup:0})
    }

    setAsPrimary(id){
        this.setState({setPrimaryLoading:true})
        fetch(baseUrl + this.props.userId + "/change-address", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "address_id": id,
                "address_type": 'primary'
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson)
                setTimeout(() => {
                    this.setState({setPrimaryLoading:false})
                    this.getExistingSites()    
                },800);
            }).catch((err) => {
            })
    }

    render() {
        if (this.state.loading == true) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff' }}>
                    <ActivityIndicator size="large" color="#fa7153" />
                    <Text style={{ fontFamily: "Montserrat", paddingTop: 10 }}>Loading</Text>
                </View>
            )
        } else {
            return (
                <View style={{flex:1}}>
                <View style={{width:'100%',flexDirection:'row',paddingVertical:15,justifyContent:'space-between',alignItems:'center',backgroundColor:'#fa7150',paddingHorizontal:20}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{width:20,height:20}}>
                        <Image source={images.backArrow} style={{width:25,height:25}}/>
                    </TouchableOpacity>
                    {
                        this.props.isFromCheckout==true?
                        <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Change address</Text>
                        :
                        <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Manage address</Text>
                    }
                    
                    <Text/>
                </View>
                <View style={{ flex: 1, backgroundColor: '#fff', padding: 20 }}>


                    <TouchableOpacity style={ManageSiteAddressStyle.ButtonOrange}
                        onPress={() => this.navigateCreateNewSite()}>
                        <View style={ManageSiteAddressStyle.ButtonSubView}>
                            {/* <View style={ManageSiteAddressStyle.AddImageMainView}>
                                  <View style={ManageSiteAddressStyle.AddImageInnerView}>
                                  <Image source={images.plusIcon} style={{width:20,height:20}} />
                                  </View>
                              </View> */}

                            <View style={ManageSiteAddressStyle.NewSiteAddressTextView}>
                                <Text style={{ fontFamily: "Montserrat-SemiBold", color: '#FFF', fontSize: 16 }}>ADD NEW ADDRESS</Text>
                            </View>
                        </View>
                    </TouchableOpacity>



                    <ScrollView showsVerticalScrollIndicator={false}>

                        {
                            this.state.existingAddress.length == 0 ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text>No Existing sites available</Text>
                                </View>
                                :
                                <View>
                                   {
                                       this.props.isFromCheckout==true?
                                        <View style={{ paddingVertical: 10 }}>
                                                {
                                                    this.props.isShippingAddress == true ?
                                                        <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18 }}>Shipping addresses ( {this.state.existingAddress.jobsite_address.length} )</Text>
                                                        :
                                                        <Text style={{ fontFamily: "Montserrat-SemiBold", fontSize: 18 }}>Billing addresses ( {this.state.existingAddress.jobsite_address.length} )</Text>  
                                                }
                                            </View>
                                        :null
                                   }

                                    {
                                        this.state.existingAddress.jobsite_address.map((item, index) => {
                                            return (
                                                <View style={{ backgroundColor: "#fff", padding: 10, flexDirection: 'row', width: '100%', borderWidth: 1, marginVertical: 5, borderRadius: 3, borderColor: '#f2f2f2' }}>
                                                    <TouchableOpacity onPress={() => this.selectedAddress(item.id)} style={{ width: '70%' }}>
                                                        <Text style={{ fontFamily: "Montserrat", marginBottom: 5, fontSize: 16 }}>{item.job_site_name}</Text>
                                                        <Text style={{ fontFamily: "Montserrat", marginBottom: 5, fontSize: 12 }} >{item.address_line1}</Text>
                                                        {
                                                            item.address_line2 == null ? null :
                                                                <Text style={{ fontFamily: "Montserrat", marginBottom: 5, fontSize: 12 }} >{item.address_line2}</Text>
                                                        }
                                                        {
                                                          item.postcode=="-"?null:
                                                           <Text style={{ fontFamily: "Montserrat", marginBottom: 5, fontSize: 10 }} >Postcode : {item.postcode}</Text>
                                                        }
                                                    </TouchableOpacity>
                                                    {
                                                        this.props.isFromCheckout==false?
                                                        <View style={{ width: '30%', alignItems: 'center',justifyContent:'space-around' }}>
                                                            <View style={{flexDirection:'row',justifyContent:'space-around',width:'80%',marginBottom:5}}>
                                                                <TouchableOpacity onPress={() => this.editAddress(item.id)}>
                                                                    <Image source={images.edit} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                                                </TouchableOpacity>

                                                                <TouchableOpacity onPress={() => this.deleteAddress(item.id)}>
                                                                    <Image source={require('../images/garbage-can.png')} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                                                </TouchableOpacity>
                                                            </View>
                                                            <View>
                                                                {
                                                                item.primary ?
                                                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                                                        <Image source={images.available} style={{ width: 25, height: 25, resizeMode: 'contain' }} />
                                                                        <Text style={{color:'#0a0',fontSize:12,fontFamily:'Montserrat-SemiBold'}}>Primary</Text>
                                                                    </View>
                                                                    :
                                                                    <TouchableOpacity onPress={()=>this.setAsPrimary(item.id)} style={{borderWidth:0.5,paddingHorizontal:10,paddingVertical:5,borderRadius:2,borderColor:'lightgray'}}>
                                                                        <Text style={{fontSize:12,fontFamily:"Montserrat"}}>Set primary</Text>
                                                                    </TouchableOpacity>
                                                                }
                                                            </View>

                                                        </View>
                                                        :
                                                        <View style={{ width: '30%', alignItems:'flex-end',justifyContent:'center'}}>
                                                            <TouchableOpacity onPress={() => this.editAddress(item.id)}>
                                                                <Image source={images.edit} style={{ width: 20, height: 20, resizeMode: 'contain' }} />
                                                            </TouchableOpacity>
                                                            <Text style={{fontFamily:'Montserrat',paddingTop:3,fontSize:12}}>Edit</Text>
                                                        </View>
                                                    }

                                                </View>
                                            )
                                        })
                                    }

                                </View>
                        }
                    </ScrollView>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.deleteAddressModalShowing}
                        onRequestClose={() => this.setState({ removeProductModalVisible: false })}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(52,52,52,0.5)' }}>

                            <View style={{ width: '85%', backgroundColor: '#FFF', borderRadius: 5 }}>
                                <View>
                                    <Text style={{ fontFamily: "Montserrat", paddingHorizontal: 20, paddingVertical: 10, color: 'black' }}>Remove address</Text>
                                    <Text style={{ fontFamily: "Montserrat", paddingHorizontal: 20, fontSize: 12 }}>Are you sure you want to remove this address?</Text>
                                    <View style={{ borderWidth: 0.3, borderColor: 'gray', width: '100%', flexDirection: 'row', marginTop: 10 }}>
                                        <TouchableOpacity onPress={() => this.setState({ deleteAddressModalShowing: false })}
                                            style={{ alignItems: 'center', width: '50%', padding: 10, borderRightWidth: 0.3, borderRightColor: 'gray' }}>
                                            <Text style={{ fontFamily: 'Montserrat' }}>Cancel</Text>
                                        </TouchableOpacity>

                                        {
                                            this.state.deleteAddressLoading ?
                                                <TouchableOpacity style={{ alignItems: 'center', width: '50%', padding: 10 }}>
                                                    <ActivityIndicator color="red" />
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity onPress={() => this.remove()}
                                                    style={{ alignItems: 'center', width: '50%', padding: 10 }}>
                                                    <Text style={{ fontFamily: 'Montserrat', color: '#fa7153' }}>Remove</Text>
                                                </TouchableOpacity>
                                        }
                                    </View>
                                </View>
                            </View>
                        </View>
                    </Modal>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.setPrimaryLoading}
                        onRequestClose={() => this.setState({ removeProductModalVisible: false })}>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(52,52,52,0.5)' }}>
                            <BarIndicator color="#fff"/>
                        </View>
                    </Modal>
                </View>
                </View>
            );
        }

    }
}



function mapStateToProps(state) {
    return {
        userId: state.userId,
        addAddressRefershing: state.addAddressRefershing,
        isShippingAddress: state.isShippingAddress,
        isFromCheckout: state.isFromCheckout,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setAddressChanged: (value) => dispatch({ type: 'setAddressChanged', value }),
        setaddAddressRefershing: (value) => dispatch({ type: "setaddAddressRefershing", value }),
        setAddressId: (value) => dispatch({ type: "setAddressId", value }),
        setisFromCheckout: (value) => dispatch({ type: 'setisFromCheckout', value }),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ManageSiteAddress);


