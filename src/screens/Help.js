import React, { Component } from 'react';
import { View, Text,ScrollView,TouchableOpacity,Image,StyleSheet} from 'react-native';
import images from '../images/index'
import {HelpStyles} from './Styles/PrimaryStyle';
import {StyleContainer} from '../screens/Styles/CommonStyles'
import CustomNotification from './CustomNotification';


export default class helpScreen extends Component {
  
    state = {
        help:[{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false},{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false},{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false},{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false},{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false},{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false},{"title":"Lorem Ipsum is simply dummy text","content":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","isShow":false}]

    };
  
async changeIsShown(index)
    {
        
        var dummyArr=this.state.help
        dummyArr[index].isShow=!dummyArr[index].isShow
        await this.setState({
            help:dummyArr
        })
        
    }

  render() {
    return (
      <View style={HelpStyles.MainContainer}>

        <ScrollView showsVerticalScrollIndicator={false}>
          {
            this.state.help.map((item,index)=>{
              return(
                  <View style={HelpStyles.ItemMainView}>
                      <View style={HelpStyles.ItemHeadView}>
                        <View style={HelpStyles.ItemHeadTxtView}>
                            <Text style={{fontFamily:"Montserrat",fontSize:13,color:'black'}}>{index+1}. {item.title}</Text>
                        </View>
                        <View style={HelpStyles.ItemHeadImgView}>
                            
                            <TouchableOpacity style={HelpStyles.ItemHeadImgInnerView} onPress={()=>{this.changeIsShown(index)}}>
                            <Image source={images.downArrow} style={StyleContainer.ImageContainStyle}/>
                            </TouchableOpacity>
                            
                        </View>
                      </View>
                      
                      {
                          item.isShow==true?
                          <View style={HelpStyles.ItemHiddenView}>
                              <Text style={{fontFamily:"Montserrat",color:'#afafaf',lineHeight:19,fontSize:12,textAlign:'justify'}}>{item.content}</Text>
                          </View>:
                          null
                      }
                      
                  </View>
              )
            })
          }
        </ScrollView>
    <CustomNotification/>
  </View>
    );
  }
}



