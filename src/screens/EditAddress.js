import React, { Component } from 'react';
import { View, Text,Image,ScrollView,TouchableOpacity,TextInput,StyleSheet,ToastAndroid, ActivityIndicator } from 'react-native';
import {OverAllStyle,MyProfileStyle} from './Styles/PrimaryStyle';
import NetInfo from '@react-native-community/netinfo';
import ContentLoader from 'react-native-easy-content-loader';
import { StyleContainer } from './Styles/CommonStyles';
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import CustomNotification from './CustomNotification';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class EditAddress extends Component {
  
    state = {
        isEdit:false,
        networkFailed:false,
        jobSiteName:'',jobSiteNameError:'',
        jobSiteAddress1:'',jobSiteAddress1Error:'',
        jobSiteAddress2:'',jobSiteAddress2Error:'',
        contactPerson:'',contactPersonError:'',
        contactPersonNo:'',contactPersonNoError:'',
        jobSiteNotes:'',jobSiteNotesError:'',
        suburb:'',suburbError:'',
        response:[],
        loading:false,
        saveLoading:false,
        postCode:"",
        postCodeError:""
    };

  componentDidMount(){
      //console.log("address id"+this.props.addressId)
      this.getAddressDetail()
  }

  componentWillUnmount(){
     
  }

  getAddressDetail(){
   
        NetInfo.fetch().then(state => {
          //console.log("Connection type", state.type);
          if(state.isConnected==true){ 
                  this.setState({loading:true})
                  fetch(baseUrl+this.props.userId+'/address/'+this.props.addressId+'/edit')
                  .then((response) => response.json())
                   .then((responseJson) => {
                       console.log(responseJson)
                    this.setState({
                        postCode:responseJson.data.postcode,
                        jobSiteName:responseJson.data.job_site_name,
                        jobSiteAddress1:responseJson.data.address_line1,
                        jobSiteAddress2:responseJson.data.address_line2,
                        contactPerson:responseJson.data.contact_person,
                        contactPersonNo:responseJson.data.contact_person_no,
                        suburb:responseJson.data.suburb,
                        jobSiteNotes:responseJson.data.job_site_note,
                        loading:false
                    })
                    //console.log(this.state.postCode)
                  }).catch((err)=>{
                    //console.log(err)
                  })
          }
          else{
            this.setState({networkFailed:true})
            alert("Check network connection")
          }
        })            
    }

    changeIsEdit(){
        this.setState({isEdit:!this.state.isEdit})
    }
      
   async saveEdits(){

        if(this.state.jobSiteName==""){
            this.setState({jobSiteNameError:"Job site name field is required"})
        }else{
            this.setState({jobSiteNameError:""})
        }

        if(this.state.jobSiteAddress1==""){
            this.setState({jobSiteAddress1Error:"Site address line 1 is required"})
        }else{
            this.setState({jobSiteAddress1Error:""})
        }

        if(this.state.jobSiteAddress2==""){
            this.setState({jobSiteAddress2Error:"Site address line 1 is required"})
        }else{
            this.setState({jobSiteAddress2Error:""})
        }

        if(this.state.contactPerson==""){
            this.setState({contactPersonError:"Contact person name is required"})
        }else{
            this.setState({contactPersonError:""})
        }

        if(this.state.postCode==""){
            this.setState({postCodeError:"Postcode feild is required"})
        }else{
            this.setState({postCodeError:""})
        }

        if(this.state.suburb==""){
            this.setState({suburbError:"suburb feild is required"})
        }else{
            this.setState({suburbError:""})
        }

        if(this.state.contactPersonNo==""){
            this.setState({contactPersonNoError:"Contact person name is required"})
        }else{
            this.setState({contactPersonNoError:""})
        }

        // if(this.state.jobSiteNotes==""){
        //     this.setState({jobSiteNotesError:"Job site notes field is required"})
        // }else{
        //     this.setState({jobSiteNotesError:""})
        // }

        if(this.state.jobSiteName!='' && this.state.jobSiteAddress1!='' && this.state.jobSiteAddress2!=''&& this.state.postCode!='' &&
         this.state.suburb!='' && this.state.contactPerson!='' && this.state.contactPersonNo!=''){

        this.setState({saveLoading:true})
        await fetch(baseUrl+this.props.userId+"/address/"+this.props.addressId+"/edit", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "address_id": this.props.addressId,
                "postcode": this.state.postCode,
                "job_site_name": this.state.jobSiteName,
                "address_line1": this.state.jobSiteAddress1,
                "address_line2": this.state.jobSiteAddress2,
                "job_site_note": this.state.jobSiteNotes,
                "contact_person": this.state.contactPerson,
                "contact_person_no": this.state.contactPersonNo,
                "suburb":this.state.suburb
            })
    
            })
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({saveLoading:false})
              console.log(responseJson)
              if(responseJson.success==true){
                this.changeIsEdit()
                this.props.setaddAddressRefershing(true)
              }
            }) 
        }
    }

    render() {
        if(this.state.loading==true){
            return(
                <View style={{flex:1,padding:30}}>
                    <ContentLoader primaryColor='#FFF' active pRows={5} pWidth={['100%','100%','100%','100%','100%']} pHeight={[45,45,45,45,45]}/>
                </View>
            )
        }
    return (
        
            <View style={{flex:1,padding:30}}>
                {
                    this.state.isEdit==false?
                        <View style={{width:'100%'}}>
                        
                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                        <View style={MyProfileStyle.TextShowView}>
                                            <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.jobSiteName}</Text>
                                        </View>
                                </View>

                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                        <View style={MyProfileStyle.TextShowView}>
                                            <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.jobSiteAddress1}</Text>
                                        </View>
                                </View>

                                {
                                    this.state.jobSiteAddress2==""?null:
                                    <View style={OverAllStyle.TextInputWhiteMainView}>
                                            <View style={MyProfileStyle.TextShowView}>
                                            <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.jobSiteAddress2}</Text>
                                            </View>
                                    </View>
                                }

                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                        <View style={MyProfileStyle.TextShowView}>
                                        <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.postCode}</Text>
                                        </View>
                                </View>

                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                        <View style={MyProfileStyle.TextShowView}>
                                        <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.suburb}</Text>
                                        </View>
                                </View>

                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                        <View style={MyProfileStyle.TextShowView}>
                                        <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.contactPerson}</Text>
                                        </View>
                                </View>
                                <View style={OverAllStyle.TextInputWhiteMainView}>
                                        <View style={MyProfileStyle.TextShowView}>
                                        <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.contactPersonNo}</Text>
                                        </View>
                                </View>
                                
                                {
                                    this.state.jobSiteNotes==""?null:
                                    <View style={OverAllStyle.TextInputWhiteMainView}>
                                            <View style={MyProfileStyle.TextShowView}>
                                            <Text style={{fontFamily:"Montserrat",color:'#acacac'}} >{this.state.jobSiteNotes}</Text>
                                            </View>
                                    </View>
                                }   

                                <TouchableOpacity style={{width:'100%',marginTop:30}} onPress={()=>{this.changeIsEdit()}}>
                                        <View style={StyleContainer.ButtonBorder}>
                                            <Text style={{fontFamily:"Montserrat-SemiBold",color:'#000',fontSize:17}} >EDIT</Text>
                                        </View>

                                </TouchableOpacity>
                        </View>
                        :
                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                        <View style={{width:'100%'}}>
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                value={this.state.jobSiteName}
                                placeholder="Jobsite name"
                                ref={(input) => { this.firstNameRef = input; }}
                                onSubmitEditing={() => {
                                this.jobSiteAddress1.focus();}}
                                onChangeText={(value)=>this.setState({jobSiteName:value})} 
                                returnKeyType={"next"}
                                /> 
                                {
                                    this.state.jobSiteNameError==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.jobSiteNameError}</Text>
                                    </View> 
                                } 
                            </View>

                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                placeholder="Address line 1"
                                value={this.state.jobSiteAddress1}
                                ref={(input) => { this.jobSiteAddress1 = input; }}
                                onSubmitEditing={() => {
                                this.jobSiteAddress2.focus();}}
                                onChangeText={(value)=>this.setState({jobSiteAddress1:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.jobSiteAddress1Error==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.jobSiteAddress1Error}</Text>
                                    </View> 
                                } 
                            </View>

                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                placeholder="Address line 2"
                                value={this.state.jobSiteAddress2}
                                ref={(input) => { this.jobSiteAddress2 = input; }}
                                onSubmitEditing={() => {
                                this.postCode.focus();}}
                                onChangeText={(value)=>this.setState({jobSiteAddress2:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.jobSiteAddress2Error==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.jobSiteAddress2Error}</Text>
                                    </View> 
                                } 
                            </View>

                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                placeholder="Postcode"
                                value={this.state.postCode}
                                ref={(input) => { this.postCode = input; }}
                                onSubmitEditing={() => {
                                this.suburb.focus();}}
                                onChangeText={(value)=>this.setState({postCode:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.postCodeError==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.postCodeError}</Text>
                                    </View> 
                                } 
                            </View>

                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                placeholder="Suburb"
                                value={this.state.suburb}
                                ref={(input) => { this.suburb = input; }}
                                onSubmitEditing={() => {
                                this.contactPerson.focus();}}
                                onChangeText={(value)=>this.setState({suburb:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.suburbError==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.suburbError}</Text>
                                    </View> 
                                } 
                            </View>
                            
                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                placeholder="Contact person"
                                value={this.state.contactPerson}
                                ref={(input) => { this.contactPerson = input; }}
                                onSubmitEditing={() => {
                                this.contactPersonNo.focus();}}
                                onChangeText={(value)=>this.setState({contactPerson:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.contactPersonError==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.contactPersonError}</Text>
                                    </View> 
                                } 
                            </View>

                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                value={this.state.contactPersonNo}
                                placeholder="Contact number"
                                ref={(input) => { this.contactPersonNo = input; }}
                                onSubmitEditing={() => {
                                this.jobSiteNotes.focus();}}
                                onChangeText={(value)=>this.setState({contactPersonNo:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.contactPersonNoError==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.contactPersonNoError}</Text>
                                    </View> 
                                } 
                            </View>

                            <View style={OverAllStyle.TextInputWhiteMainView}>
                            <TextInput style={OverAllStyle.TextInputView} 
                                placeholder="Notes"
                                value={this.state.jobSiteNotes}
                                ref={(input) => { this.jobSiteNotes = input; }}
                                onSubmitEditing={() => this.saveEdits()}
                                onChangeText={(value)=>this.setState({jobSiteNotes:value})} 
                                returnKeyType={"next"}
                                />  
                                {
                                    this.state.jobSiteNotesError==''?null:
                                    <View style={{backgroundColor:'#f2f2f2'}}>
                                       <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.jobSiteNotesError}</Text>
                                    </View> 
                                } 
                            </View>

                                {
                                    this.state.saveLoading==true?
                                    <TouchableOpacity style={{width:'100%',marginTop:30,padding:15,backgroundColor:'#fa7153',borderRadius:5,alignSelf:'center'}}>
                                        <ActivityIndicator color="#fff"/>
                                    </TouchableOpacity>
                                    :
                                    <TouchableOpacity onPress={()=>this.saveEdits()} style={{width:'100%',marginTop:30,padding:15,backgroundColor:'#fa7153',borderRadius:5,alignSelf:'center'}}>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',textAlign:'center'}}>SAVE EDITS</Text>
                                    </TouchableOpacity>
                                }
                        </View>
                        </KeyboardAwareScrollView>
                }
            
                <CustomNotification/>
            
            </View>  
    );
  }
}


function mapStateToProps(state){
  return{
      userId:state.userId,
      addressId:state.addressId
  }
}

function mapDispatchToProps(dispatch){
  return{
    setaddAddressRefershing:(value)=>dispatch({type:"setaddAddressRefershing",value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(EditAddress);


