import { StyleSheet} from 'react-native';
import {Colors} from '../Styles/colors';

export const StyleContainer= StyleSheet.create(
    {
        MainContainer:{
            flex:1,
            backgroundColor:'#fff',
            padding:20,
            alignItems:'center',
            justifyContent:'center'
          },
          ButtonOrange:{
            width:'100%',
            paddingVertical:15,
            backgroundColor:Colors.themeColor,
            borderRadius:3,
            alignItems:'center',
            marginBottom:15,
            marginVertical:10,
          },
          ButtonBorder:{
            width:'100%',
            paddingVertical:15,
            borderWidth:0.5,
            borderRadius:3,
            alignItems:'center',
            marginVertical:10,
            borderColor:'gray'
          },
          //used for sign up ,sign in form 
          TextInputView:{
            borderRadius:3,
            //color:Colors.txtInputClr,
            color:'black',
            borderWidth:1,
            borderColor:Colors.txtInputClr,
            backgroundColor:Colors.whitebg,
            width:'100%',
            height:45,
            paddingHorizontal:20,
            fontSize:14,
            fontFamily:'Montserrat'
            },
        TextInputMainView:{
            width:'100%',
            marginBottom:10
        },
        ImageContainStyle:{
            borderRadius:7,
            width:undefined,
            height:undefined,
            flex:1,
            resizeMode:'contain'
        },
        ImageCoverStyle:{
            width:undefined,
            height:undefined,
            flex:1,
            resizeMode:'contain'
        },
        //RedIndicationTxtStyle used in validation and product delivery status in product vies
        RedIndicationTxtStyle:{
          fontSize:10,
          color:Colors.redIndicationClr,
          fontFamily:'Montserrat'
        }
      
    }
)



