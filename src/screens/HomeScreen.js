
import React, { Component } from 'react';
import { View, Text ,ScrollView,TouchableOpacity,BackHandler,TextInput,Modal,Image,ToastAndroid,SafeAreaView,ActivityIndicator,RefreshControl,KeyboardAvoidingView, Platform} from 'react-native';
import {connect} from 'react-redux'
import NetInfo from '@react-native-community/netinfo';
import  ContentLoader from 'react-native-easy-content-loader'
import {baseUrl} from '../Controller/baseUrl'
import images from '../images/index'
import {OverAllStyle,ProductViewStyle,HomeScreenStyle, CartScreenStyle,ModelStyle} from './Styles/PrimaryStyle';
import {StyleContainer} from '../screens/Styles/CommonStyles'
import { AndroidBackHandler } from "react-navigation-backhandler";
import CustomNotification from './CustomNotification';
import { Colors } from './Styles/colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import {Toast} from 'native-base'
import { Button, Snackbar } from 'react-native-paper';

const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        const paddingToBottom = 20;
        return layoutMeasurement.height + contentOffset.y >=
          contentSize.height - paddingToBottom;
      };

class HomeScreen extends Component {
    
    state = {
        loading:false,
        addTocartLoading:false,
        selectedProdIndex:null,
        modalVisible:false,
        selectedProduct:{},
        categories:[],
        popularProducts:[],
        dummyCategory:[{id:0},{id:0},{id:0},{id:0},{id:0},{id:0}],
        weightChoosed:true,
        colorChoosed:true,
        sizeChoosed:true ,
        variantPrice:null,
        variantQty:null,
        variantWeight:null,
        cart:[],
        variants:[],
        networkFailed:false,
        refreshing:false,
        count:0,
        page:1,
        nextPageAvailable:true,
        nextPageLoading:false,
        notificationCount:null,

        signleProductLoading:false,
        modalResponse:[],
        productOptions:[],
        productOptionCount:null,
        dummyArray:[],
        availableCombinations:[],
        combinationStatus:null,
        variantId:null,
        imageUrl:null,
        updateproductmodal:false,
        modalProductWeight:null,
        quantity:0,
        variantLength:null,
        addToCartStatus:false,
        nonBulkUrl:null,

        prodPrice:null,
        isediting:false,
        iseditingValue:null,
        snackBarVisible:false
  }

 async componentDidMount(){
   this.props.navigation.closeDrawer(); 
   await this.getUserid() 
   await this.getHomeScreenDetails() 
  }

  async componentDidUpdate(prevProps){
    if(this.props.productCountChanged==true){
      await this.props.setProductCountChanged(false)
     await this.getUserid() 
     await this.getHomeScreenDetails()
    }else if(this.props.homeScreenUpdate==true){
      await this.props.setHomeScreenUpdate(false)
      await this.getUserid() 
      console.log("home update using homescreenupdate")
      await NetInfo.fetch().then(state => {
        if(state.isConnected==true){
            //this.setState({loading:true})
            fetch(baseUrl+this.props.userId+'/home')
            .then((response) => response.json())
            .then((responseJson) => {
                    this.setState({    
                        nonBulkUrl:responseJson.non_bulk_category_url, 
                        categories:responseJson.categories,
                        popularProducts:responseJson.popular_products,
                        loading:false
                    })
                    // this.setState({loading:false,refreshing:false,networkFailed:false})
                    this.setState({refreshing:false,networkFailed:false})
                    this.props.setPostCode(responseJson.postcode)
                    this.props.setCartCount(responseJson.cart_count)
                    this.props.setDeliveryType(responseJson.delivery_type)
                    this.props.setNotificationCount(responseJson.notification_count)
                    //consle.log("update"+this.props.postCode)
            })
            .catch((err)=>{
                this.getHomeScreenDetails()
            })   
        }
        else{
          this.setState({networkFailed:true})
        }
      }) 
      await this.props.setSingleProductPageUpdate(true)
      await this.props.setCategoryProductCountChanged(true)
    }

  }

 async getUserid(){
  console.log("call getuserid")
  await this.setState({loading:true})
    await AsyncStorage.getItem('userId').then((value) => {
      if(value!=null){
        this.setUserID(value)
        //consle.log("user")
      //consle.log("customer"+ this.props.userId)
      }
    })
   await AsyncStorage.getItem('guestId').then((value) => {
      if(value!=null){
        this.setUserID(value)
        //consle.log("guest")
      //consle.log("guest"+ this.props.userId)
      }
    })
  }

 async setUserID(value){
   await this.props.setuserId(JSON.parse(value))
   //consle.log("user id "+this.props.userId)
  }

 async getHomeScreenDetails(){   
  console.log("get homedetails")
  await NetInfo.fetch().then(state => {
        //consle.log("Connection type", state.type);
        if(state.isConnected==true){
            //this.props.setFetchLoading(true)
            fetch(baseUrl+this.props.userId+'/home')
            .then((response) => response.json())
            .then((responseJson) => {
                    if(responseJson.is_load==true){
                      this.setState({nextPageAvailable:true})
                    }else{
                      this.setState({nextPageAvailable:false})
                    }
                    this.setState({  
                        nonBulkUrl:responseJson.non_bulk_category_url,  
                        categories:responseJson.categories,
                        popularProducts:responseJson.popular_products,
                        page:1,
                        // nextPageAvailable:true,
                    })

                    this.setState({loading:false,refreshing:false,networkFailed:false})
                    this.props.setPostCode(responseJson.postcode)
                    this.props.setCartCount(responseJson.cart_count)
                    this.props.setDeliveryType(responseJson.delivery_type)
                    this.props.setNotificationCount(responseJson.notification_count)
                    //consle.log(this.props.postCode)
            })
            .catch((err)=>{
                ////consle.log(err)
                //alert("Couldn't reach server , try later ")
            })   
        }
        else{
          this.setState({networkFailed:true,loading:false})
              //alert("Check network connection")
        }
      }) 
    ////consle.log("deliveryTyep"+this.props.deliveryType)
    
    await this.props.setCategoryProductCountChanged(true)
    await this.props.setOrderCountChanged(true)
    await this.props.setHomeScreenUpdate(false)
  }

  async navigateCategory(item){
      await this.props.setCategoryId(item.id)
      await this.props.setCategoryUrl(item.url)
      await this.props.setCategoryName(item.name)
      this.props.navigation.navigate("categoryList")
   }

async modalDecrement(){
  if(this.state.quantity==1){
   await this.setState({quantity:1,isediting:false,modalVisible:false,modalVisible:true})     
  }else{
     await this.setState({quantity:this.state.quantity-1,isediting:false,modalVisible:false,modalVisible:true})     
    }
}

async modalIncrement(selectedVariant){
 await this.setState({quantity:parseInt(this.state.quantity)+1,isediting:false,modalVisible:false,modalVisible:true})
}
 
async customQty(text){
  await this.setState({quantity:text,modalVisible:false,modalVisible:true,iseditingValue:text})
}

async changeTabIndex(index){
  const products=[...this.state.categories]
  await this.setState({selectedCategoryName:products[index].name})
  await this.props.setCategoryId(products[index].id)
  this.getTabDetail()
}

async selectedProduct(id,imageUrl){
  this.setState({modalVisible:true,signleProductLoading:true})
  await fetch(baseUrl+this.props.userId+'/product/'+id)
        .then((response) => response.json())
        .then((responseJson) => {
          //consle.log(responseJson)
          var dummyArray=new Array(responseJson.product_option_count)
          var variantOption=responseJson.product_options
         if(responseJson.product_variants.length!=0){
          variantOption.map((item,key)=>{            
              item.option.map((item,index)=>{
                //if(responseJson.default_variant_id==item.variant_id){
                  if(responseJson.product_variants[0].id==item.variant_id){
                  item.selected=true
                  dummyArray.splice(0,0,item.option_value_id)
                }
              })
          })
        }
          var filtered = dummyArray.filter(item=> {
            return item != null;
          });
          
          var reverced=filtered.reverse()
          
          if(responseJson.quantity==0){
            this.setState({quantity:1})
          }else{
            this.setState({quantity:responseJson.quantity})
          }
          
            this.setState({
                dummyArray:reverced,
                modalResponse:responseJson,
                productOptions:variantOption,
                productOptionCount:responseJson.product_option_count,
                availableCombinations:responseJson.product_variants,
                imageUrl:imageUrl,
                signleProductLoading:false
            })
            if(this.state.productOptions.length!=0){
              this.compareArrayValues()
            }else{
              this.setState({combinationStatus:true,variantId:this.state.availableCombinations[0].id,prodPrice:this.state.availableCombinations[0].price.toFixed(2)})
            }
            
        })  
}

changeProductVariant(key,indexPosition){

  var dummyArray=this.state.dummyArray
  var productOptions=this.state.productOptions
  productOptions.map((item,index)=>{
    if(index==key){
      item.option.map((item,index1)=>{
        item.selected=false
        if(index1==indexPosition){
          item.selected=true
          dummyArray[key]=item.option_value_id 
        }
      })
    }
  })
  this.setState({productOptions:productOptions,dummyArray:dummyArray})
  //console.warn(this.state.dummyArray)
  this.compareArrayValues()
}

compareArrayValues(){
  //console.warn("enter comparision")
  let array=this.state.availableCombinations

  for(var i=0;i<array.length;i++){
    if(this.state.productOptionCount==1){
     // if(this.state.dummyArray[0]==array[i].product_option_value_id1){
        if(this.state.dummyArray[0]==array[i].product_option_value_id1){
          // var modalresponse=this.state.modalResponse
          // modalresponse.default_variant_price=array[i].price
          // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
          this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
          //this.getModalProductDetails(this.state.variantId)
          break;
        // }else{
        //   this.setState({combinationStatus:false})
        //   break
        // }
      }
    }else if(this.state.productOptionCount==2){
      // if((this.state.dummyArray[0]==array[i].product_option_value_id1) || (this.state.dummyArray[1]==array[i].product_option_value_id2)){
        if((this.state.dummyArray[0]==array[i].product_option_value_id1) && (this.state.dummyArray[1]==array[i].product_option_value_id2)){
          // var modalresponse=this.state.modalResponse
          // modalresponse.default_variant_price=array[i].price
          // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
          this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
          break;
        }
        // else{
        //   this.setState({combinationStatus:false})
        //   break
        // } 
      //}
    }else if(this.state.productOptionCount==3){
      // if(this.state.dummyArray[0]==array[i].product_option_value_id1 || this.state.dummyArray[1]==array[i].product_option_value_id2 ||
      //   this.state.dummyArray[2]==array[i].product_option_value_id3){
          if(this.state.dummyArray[0]==array[i].product_option_value_id1 && this.state.dummyArray[1]==array[i].product_option_value_id2 &&
            this.state.dummyArray[2]==array[i].product_option_value_id3){
              // var modalresponse=this.state.modalResponse
              // modalresponse.default_variant_price=array[i].price
              // this.setState({modalResponse:modalresponse,variantId:array[i].id,combinationStatus:true})
              this.setState({prodPrice:array[i].price.toFixed(2),variantId:array[i].id,combinationStatus:true})
            break;
          // }else{
          //   this.setState({combinationStatus:false})
          //   break
          // }
        }
    }
  } 
}

async changeProductValue(indexValue){
////consle.log(this.state.selectedProduct)
let selectedProduct=this.state.selectedProduct
let products = [...this.state.popularProducts];
products[indexValue]=selectedProduct;
await this.setState({products:products})
}


async addToCartWithVariants(){
  if(this.state.quantity==0 || this.state.quantity==""){
    alert("Please add quantity!")
  }else{
  this.setState({addTocartLoading:true,isediting:false})
  await fetch(baseUrl+this.props.userId+'/addtocart', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      "customer_id":this.props.userId,
      "product_id": this.state.modalResponse.id,
      "product_variant_id" :this.state.variantId,  
      "quantity":this.state.quantity,
    })
  })
    .then((response) => response.json())
    .then((responseJson) => {
        //consle.log("response"+JSON.stringify(responseJson))
        if(responseJson.success==true){
        this.setState({addTocartLoading:false,modalVisible:false,snackBarVisible:true})
        this.props.setIncreaseCartCount()
        }                  
    })
    .catch((err)=>{
      //consle.log(err)
      this.setState({addTocartLoading:false})
  })  
    await this.props.setCategoryProductCountChanged(true)
    await this.props.setProductCountChanged(false)
  }
}

navigateSingleProductScreen(id){
  this.props.setProductId(id)
  this.props.navigation.navigate("singleProductScreen")
}

addFavourite(value,index){
  var type;
  if(this.state.popularProducts[index].is_favourite==true){
    let products=[...this.state.popularProducts]
    products[index].is_favourite=false
    type=1
    this.setState({products:products})
  }else{
    let products=[...this.state.popularProducts]
    products[index].is_favourite=true
    type=0
    this.setState({products:products})
  }
  ////consle.log(this.props.userId,this.state.popularProducts[index].id,this.state.popularProducts[index].variant_id,type)
  fetch(baseUrl+this.props.userId+'/wishlist', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
     "customer_id": this.props.userId,
     "product_id":this.state.popularProducts[index].id,
     "is_wishlist":type
    })
  })
 .then((response) => response.json())
 .then((responseJson) => {
     //consle.log(responseJson.data.message)
      // if(responseJson.data.message=="added"){
      //    Toast.show({text:"Added to favourites !",duration:1000,buttonText:"Done",textStyle:{fontSize:12}})
      // }else{
      //   Toast.show({text:"Removed from favourites !",duration:1000,buttonText:"Done",textStyle:{fontSize:12}})
      // }
})
}

_onRefresh = () => {
  ////consle.log("onrefress")
  this.setState({refreshing: true});
  this.getHomeScreenDetails()
}

retry(){
  ////consle.log("retry")
  this.setState({networkFailed:false})
  this.getHomeScreenDetails()
}

onBackButtonPressAndroid = () => {
  this.setState({count:this.state.count+1})
  setTimeout(() => {this.setState({count:this.state.count-1})}, 2000)
  if(this.state.count==0 || this.state.count==1){
  ToastAndroid.show("Tap again to exit", ToastAndroid.SHORT);
  }
  else{
    if(this.state.count==2){
    BackHandler.exitApp()
    }
  }
  return true;
};

async getNextPageDetails(){
  if(this.state.nextPageAvailable==true){
  await this.setState({page:this.state.page+1,nextPageLoading:true})
  await fetch(baseUrl+this.props.userId+'/home?page='+this.state.page)
  .then((response)=>response.json())
  .then((responseJson)=>{
    //console.warn(responseJson)
    this.setState({
      popularProducts:this.state.popularProducts.concat(responseJson.popular_products),
      nextPageAvailable:responseJson.is_load,
      nextPageLoading:false
    })
  })
}
}

async navigateToNonBulkProduct(){
  await this.props.setCategoryUrl(this.state.nonBulkUrl)
  console.log(this.state.nonBulkUrl)
  await this.props.setCategoryName("Non-bulk products")
  this.props.navigation.navigate("categoryList")
}


incdecEdit(){
  var qty=this.state.quantity.toString()
  //consle.log(qty)
  this.setState({iseditingValue:qty,isediting:true})
 }

  render() {
    var selectedVariant={};
       if(this.state.networkFailed==true){
         return(              
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Image source={images.networkFailed} style={{width:70,margin:8,height:100,resizeMode:'contain'}}/>
                    <Text style={{fontFamily:"Montserrat",fontSize:17,color:'black',paddingBottom:10}}>Connection Error</Text>
                    <Text style={{fontFamily:"Montserrat"}}>Please check your network and try again.</Text>
                    <TouchableOpacity onPress={()=>this.retry()} style={{marginVertical:20,borderColor:'#fa7153',borderWidth:1,paddingHorizontal:30,paddingVertical:5,borderRadius:5}}>
                      <Text style={{fontFamily:"Montserrat",color:'#fa7153',fontSize:16}}>Retry</Text>
                    </TouchableOpacity>
                </View>
         )
       }else
       if(this.state.loading==true){
           return(
            <View style={{flex:1}}>
               <ScrollView style={{flex:1}}>
                <View style={{width:'100%',paddingVertical:20,backgroundColor:'rgb(224, 224, 222)'}} >
                    <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',paddingHorizontal:20}}>
                    <ContentLoader active  pRows={0} containerStyles={{width:100,margin:8,height:30,marginVertical:10}}/>
                    <ContentLoader active  pRows={0} containerStyles={{width:100,height:30,marginVertical:10}}/>
                    <ContentLoader active  pRows={0} containerStyles={{width:100,margin:8,height:30,marginVertical:10}}/>
                    </View>
                </View>
          
                <View style={{marginTop:-20,width:'100%',justifyContent:'center',alignItems:'center'}}>
                    <View style={{width:'90%',backgroundColor:'rgb(240, 240, 240)',justifyContent:'space-between',flexDirection:'row',flexWrap:'wrap',borderRadius:10}}>
                    {
                                this.state.dummyCategory.map((item,index)=>{
                                    return(
                                        <View key={index} style={[HomeScreenStyle.CatecoryContainer,index % 2 == 0 ? HomeScreenStyle.CategoryBorderRight:null,index<=this.state.dummyCategory.length-3?HomeScreenStyle.CategoryBorderBottom:null]}>
                                            
                                                <ContentLoader active avatar aShape="square" pRows={0}  pHeight={'100%'} containerStyles={{padding:10,flexDirection:'column',alignItems:'center'}}/>                              
                                            
                                        </View>
                                    )
                                })
                                
                            }
                    </View>
                </View>
                <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30,marginHorizontal:20}}/>
                <ContentLoader active avatar aShape="square" pRows={2} containerStyles={{padding:30,marginHorizontal:20}}/>
            <View/>
            </ScrollView>
        </View>

           )
       }else{
        return (  
        <SafeAreaView style={OverAllStyle.MainContainerPad0}>
            <ScrollView style={HomeScreenStyle.SubContainer}
            refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh}/> }
            onScroll={({nativeEvent}) => {
              if (isCloseToBottom(nativeEvent)) {
                this.getNextPageDetails()
             }
            }}
            scrollEventThrottle={0}>
            <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
            
            <View style={{backgroundColor:'#fa7153',paddingBottom:10}}>
            <View style={{flexDirection:'row',justifyContent:'space-between',paddingVertical:15,paddingHorizontal:10}}>
                    <View style={{flexDirection:'row',width:'30%'}}>
                      <View style={HomeScreenStyle.LocationIcon}>
                        <Image source={require('../images/delivery_location.png')} style={HomeScreenStyle.LocationIcon}/>
                      </View>
                      <TouchableOpacity onPress={()=>this.props.navigation.navigate("changePostcode")}>
                        <Text ellipsizeMode='tail' numberOfLines={1} style={{fontFamily:"Montserrat-SemiBold",fontSize:8,color:'#FFF'}}>DELIVERY LOCATION</Text>
                        <View style={{flexDirection:'row'}}>
                            <Text ellipsizeMode='tail' numberOfLines={1} style={{fontFamily:"Montserrat-SemiBold",fontSize:8,color:'#FFF',}}>Postcode - {this.props.postCode}</Text>
                            <View style={{marginLeft:5}}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:8,color:'#FFF',textDecorationLine:'underline',}}>Change</Text>
                            </View>
                        </View>
                      </TouchableOpacity>
                    </View>
                    
                     {/* <Image source={images.whiteLogo} style={{flex:1,width:null,height:null,resizeMode:'contain'}}/> */}
                     <View style={{width:'20%',height:'130%'}}> 
                       <Image source={images.whiteLogo} style={{width:null,height:null,resizeMode:'contain',flex:1}}/>
                     </View>
                    
                    <View style={{width:'30%',alignItems:'flex-end'}}>
                     <View style={{flexDirection:'row'}}>
                       <TouchableOpacity onPress={()=>this.props.navigation.navigate("notification")} style={HomeScreenStyle.MenuIcon}>
                            {
                              this.props.notificationCount==0?
                                null
                                :
                                /* <View style={{width:15,height:15,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:10,left:10}}>
                                <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text>
                                </View>                             */
                                <View style={{width:7,height:7,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:12,left:12}}>
                                {/* <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text> */}
                                </View>                            
                            }
                            <Image source={images.bell} style={StyleContainer.ImageContainStyle}/>
                        </TouchableOpacity>

                       <TouchableOpacity onPress={()=>this.props.navigation.toggleDrawer()} style={HomeScreenStyle.MenuIcon}>
                              <Image source={images.menuIcon} style={StyleContainer.ImageContainStyle}/>
                       </TouchableOpacity>
                       </View>
                    </View>
                </View>

                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:12,color:'#fff',marginBottom:10,textAlign:'center',paddingBottom:10}}>Shop by categories</Text>
          </View>
            
            <View>
                
                <View style={HomeScreenStyle.CategoryMainView}>
                    <View style={HomeScreenStyle.CategorySubView}>
                        {
                            this.state.categories.map((item,index)=>{
                                return(
                                    <TouchableOpacity key={index} onPress={()=>this.navigateCategory(item)} 
                                    style={[HomeScreenStyle.CatecoryContainer,index % 2 == 0 ? HomeScreenStyle.CategoryBorderRight:null,index<=3?HomeScreenStyle.CategoryBorderBottom:null]}>
                                        <View style={HomeScreenStyle.CategoryImgView}>
                                            <Image source={{uri:item.image_url}} style={StyleContainer.ImageContainStyle}/>
                                        </View>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#919191',paddingTop:5,}}>{item.name}</Text>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </View>
                </View>
                <TouchableOpacity onPress={()=>this.navigateToNonBulkProduct()} 
                style={{backgroundColor:'#fa7153',paddingVertical:10,marginHorizontal:15,marginTop:-5,borderBottomLeftRadius:10,borderBottomRightRadius:10}}>
                <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',textAlign:'center',}}> Non-bulk products</Text>
                </TouchableOpacity>

                <View style={HomeScreenStyle.ContainerPading}>
            
                    <View style={HomeScreenStyle.PopularTxtView}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:17}}>Most popular</Text>
                    </View>    
                    <View>
                    {
                        this.state.popularProducts.map((item,index)=>{
                            return(
                                <View key={index} style={ProductViewStyle.ProductMainContainer}>
                                    

                                    <View style={ProductViewStyle.ProductView}>

                                        <View style={ProductViewStyle.ProductImageView}>
                                            <Image source={{uri:item.image_url}} style={ProductViewStyle.ImageCoverStyle} />
                                            <View style={ProductViewStyle.ZindexView}>
                                                <TouchableOpacity style={ProductViewStyle.HeartView}>
                                                {
                                                    item.is_favourite==true?
                                                    <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("remove",index)}>
                                                        <Image source={images.favouriteWithBackgroud} style={StyleContainer.ImageCoverStyle} />  
                                                    </TouchableOpacity>
                                                    :
                                                    <TouchableOpacity style={{flex:1}} onPress={()=>this.addFavourite("add",index)}>
                                                        <Image source={images.favouriteWithoutBackgroud} style={StyleContainer.ImageCoverStyle} />
                                                    </TouchableOpacity>
                                                }
                                                </TouchableOpacity>
                                            </View>
                                        </View>

                                        <TouchableOpacity onPress={()=>this.navigateSingleProductScreen(item.id)} 
                                           style={ProductViewStyle.ProductDetailView}>
                                                <Text numberOfLines={2}  ellipsizeMode='tail' style={{fontSize:14,marginBottom:3,fontFamily:"Montserrat-SemiBold",}}>{item.name} </Text>
                                                <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:13,marginBottom:3}}>$ {item.default_variant_price.toFixed(2)}</Text>
                                                {/* {
                                                    this.props.deliveryType==0?
                                                    <Text style={{fontFamily:"Montserrat",color:'#3bcf0a',fontSize:10}}>Delivery available</Text>:
                                                    <Text numberOfLines={2} ellipsizeMode='tail' style={StyleContainer.RedIndicationTxtStyle}>Sorry we do not delivery to this area. Please arrange pick-up in store.</Text>
                                                }      */}
                                        </TouchableOpacity>
                                    </View>

                                    {
                                        item.quantity==0?
                                        <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                            <TouchableOpacity style={{width:'100%',height:25,marginBottom:5}} 
                                            onPress={()=>{ this.selectedProduct(item.id,item.image_url) }} >
                                                <Image source={images.plusIcon} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}/>
                                            </TouchableOpacity>
                                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:10,textAlign:'center',color:'#fa7153'}}>ADD</Text>
                                        </View> 
                                        :
                                        <View style={{width:'25%',justifyContent:'center',alignItems:'center'}}>
                                            <View style={CartScreenStyle.IncDecMainContainer}>
                                                <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} onPress={()=>{this.decreaseQty(index,selectedVariant)}}>
                                                    <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                                </TouchableOpacity>
                                                
                                                <View style={CartScreenStyle.IncDecQtyContainer}>
                                                <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                                </View>
                                                
                                                
                                                <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} onPress={()=>{this.increaseQty(index)}}>
                                                    <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View> 
                                    }
                                </View>
                            )
                        })
                    }
                        
                    </View>
                </View>

            </View>
            </AndroidBackHandler>
            {
              this.state.nextPageLoading?
              <View style={{alignItems:'center',justifyContent:'center',paddingVertical:5}}>
                 <ActivityIndicator/>
              </View>
              :
              null
            }
            </ScrollView>
            
              
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={()=>this.setState({modalVisible:false})}>
                
                  <KeyboardAvoidingView keyboardVerticalOffset={20}  behavior={Platform.OS == "ios" ? "padding" : "height"} style={{flex:1}}>
                
                    <View style={ModelStyle.modelContainer}>
                        <TouchableOpacity onPress={()=>this.setState({iseditingValue:0,isediting:false,modalVisible:false})} 
                         style={{marginBottom:10}}>
                            <Image source={images.cancel} style={{width:25,height:25,resizeMode:'cover'}}/>
                        </TouchableOpacity>
                        <View style={{width:'100%',backgroundColor:'#FFF',padding:20,borderRadius:20,marginBottom:-15}}>
                         {
                           this.state.signleProductLoading==true?
                           <View style={{width:'100%',height:'50%',paddingVertical:20}} >
                              <ContentLoader active avatar aShape="square" pRows={2}/>

                              <View style={{marginTop:20,borderWidth:0.5,borderStyle:'dashed',paddingTop:10,marginBottom:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              <View style={{borderWidth:0.5,borderStyle:'dashed',paddingTop:10,borderRadius:5}}>
                                <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                <View style={{flexDirection:'row',paddingLeft:20,justifyContent:'space-around'}}>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  <ContentLoader active  pRows={0} containerStyles={{width:100,height:30}}/>
                                  </View>
                              </View>

                              {/* <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                </TouchableOpacity> */}
                            </View>
                           :
                           <ScrollView showsVerticalScrollIndicator={false}>
                           
                            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                               <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>ADD TO CART</Text>
                               {
                                 this.state.updateproductmodal==true?
                                 <ActivityIndicator/>
                                 :
                                 null
                               }
                            </View> 
                            
                             <View style={ModelStyle.modelProductContainer}>
                                <Image source={{uri:this.state.imageUrl}} style={{width:60,height:60,borderRadius:7}}></Image>
                                <View style={{paddingLeft:20,width:'80%'}}>
                                    <Text numberOfLines={2}  ellipsizeMode="tail" style={[ModelStyle.modelProductName,{fontFamily:"Montserrat-SemiBold"}]}>{this.state.modalResponse.name}</Text>
                                    <Text style={ModelStyle.modelProductPrice}>$ {this.state.prodPrice}</Text>
                                </View>
                            </View>
                              
                              {
                                this.state.productOptions.map((item,key)=>{
                                  return(
                                      <View style={{borderWidth:0.5,paddingBottom:5,borderColor:'gray',borderStyle:'dashed',borderRadius:5,paddingHorizontal:5,marginVertical:5,backgroundColor:'#fff'}}>
                                        <Text style={{marginBottom:3,paddingLeft:5,paddingTop:5}}>{item.type}</Text>
                                        <View style={{flexDirection:'row',justifyContent:'flex-start',flexWrap:'wrap'}}>
                                        {
                                          item.option.map((item,index)=>{
                                            
                                            if(item.selected==true){
                                              if(this.state.combinationStatus==true){
                                                return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:0.3,borderColor:'gray',borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#fa7153',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',color:'#fff',fontFamily:'MontSerrat-SemiBold'}}>{item.value}</Text>
                                                  </TouchableOpacity>     
                                                )
                                              }else{
                                                  return(
                                                  <TouchableOpacity style={{padding:7,borderWidth:1,borderRadius:20,borderColor:'#fa7153',minWidth:'25%',maxWidth:'50%',alignItems:'center',backgroundColor:'#f2f5f6',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                    <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                                  </TouchableOpacity>
                                                )
                                              }
                                            }else{
                                              return(
                                              <TouchableOpacity onPress={()=>this.changeProductVariant(key,index)} style={{padding:7,borderWidth:0.5,borderRadius:20,minWidth:'25%',maxWidth:'50%',alignItems:'center',borderStyle:'dashed',margin:5,justifyContent:'center'}}>
                                                <Text style={{textAlign:'center',fontFamily:'MontSerrat'}}>{item.value}</Text>
                                              </TouchableOpacity>
                                            )
                                            }
                                          })
                                        }
                                        </View>
                                      </View>
                                  )
                                })
                              }
                              {
                                this.state.combinationStatus==true?
                                null
                                :
                                <View style={{flexDirection:'row',alignItems:'center',paddingBottom:5}}> 
                                    <Image source={images.notAvailable} style={{width:20,height:20,resizeMode:'contain'}}/>
                                    <Text style={{color:'red',fontSize:12,fontFamily:'MontSerrat',paddingLeft:5}}>Selected combination not available</Text>
                                </View>
                              }
                            
                          
                          <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10,paddingVertical:15,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={{fontFamily:"Montserrat",fontSize:15}}>Price :  </Text>
                                    <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                </View>
                                <View style={{justifyContent:'center',alignItems:'flex-end'}}>
                                    <View style={CartScreenStyle.IncDecMainContainer}>
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>{this.modalDecrement()}}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                      </TouchableOpacity>
                                      
                                      
                                      {
                                        this.state.isediting==true?
                                        <TouchableOpacity style={{width:'30%',justifyContent:'center'}}>
                                        <TextInput
                                          autoFocus={this.state.isediting}
                                          style={{width:'100%',height:24,paddingVertical:0,fontSize:12,fontFamily:"Montserrat",textAlign:'center',color:Colors.themeColor}}
                                          onChangeText={(text)=>this.customQty(text)}
                                          value={this.state.iseditingValue}
                                          keyboardType={"phone-pad"}
                                        />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={()=>this.incdecEdit()} style={{width:'30%',alignItems:'center',justifyContent:'center'}}>
                                          <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                        </TouchableOpacity>
                                      }
                                      
                                      
                                      
                                      <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                      onPress={()=>this.modalIncrement()}>
                                          <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                      </TouchableOpacity>
                                  </View>
                                </View>
                            
                          </View>
                            {/* <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom: 10,borderTopWidth:0.5,borderBottomWidth:0.5,borderColor:'#c3c3c3'}}>
                                <View style={{width:'50%',borderRightWidth:0.5,borderColor:'#c3c3c3',paddingVertical:20}}>
                                    
                                     {
                                       this.state.modalResponse.weight==""?
                                       <View>
                                        <Text style={{fontFamily:"Montserrat"}}>Total Weight : </Text>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:15}}>NAN</Text>
                                       </View>
                                       :
                                       <View>
                                        <Text style={{fontFamily:"Montserrat"}}>Total Weight : </Text>
                                        <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fa7153',fontSize:15}}>{this.state.modalResponse.weight*this.state.quantity} {this.state.modalResponse.weight_unit}</Text>
                                       </View>
                                     }
                                      
                                    
                                </View>
                                <View style={{flexDirection:'row',width:"50%",justifyContent:'space-between',paddingVertical:20,paddingLeft:10}}>
                                    <View style={{width:'60%'}}>
                                        <Text style={{fontFamily:"Montserrat"}}>Price :</Text>
                                        <View style={{width:'100%'}}>
                                            <Text numberOfLines={2} ellipsizeMode="tail" style={{fontFamily:"Montserrat-SemiBold",color:"#fa7153",fontSize:15,}}>$ {(this.state.prodPrice*this.state.quantity).toFixed(2)}</Text>
                                          </View>
                                    </View>

                                    <View style={{justifyContent:'center',alignItems:'flex-end',flex:1,widht:'50%'}}>
                                       <View style={CartScreenStyle.IncDecMainContainer}>
                                          <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                          onPress={()=>{this.modalDecrement()}}>
                                              <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                          </TouchableOpacity>
                                          
                                          <View style={CartScreenStyle.IncDecQtyContainer}>
                                          <Text style={[OverAllStyle.QtyTxtStyle,{fontFamily:"Montserrat"}]}> {this.state.quantity} </Text>
                                          </View>
                                          
                                          <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                          onPress={()=>this.modalIncrement()}>
                                              <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                          </TouchableOpacity>
                                      </View>
                                    </View>
                                </View>
                            </View> */}

                            {
                              this.state.combinationStatus==true?
                              <View>
                                {
                                  this.state.addTocartLoading?
                                  <TouchableOpacity style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <ActivityIndicator color="#fff"  />
                                  </TouchableOpacity>
                                  :
                                  <TouchableOpacity onPress={()=>this.addToCartWithVariants()} 
                                  style={{backgroundColor:'#fa7153',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                    <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                                  </TouchableOpacity>
                                }
                              </View>
                              :
                              <View style={{backgroundColor:'#e3b1a6',paddingVertical:12,width:'100%',marginVertical:10,alignItems:'center',borderRadius:3}}>
                                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:18,}}> ADD TO CART</Text>
                              </View>
                            }
                            </ScrollView> 
                         }
                        </View>
                    </View>
                    </KeyboardAvoidingView>
            </Modal>    

            {/* <Modal 
                animationType="fade"
                transparent={true}
                visible={this.state.addToCartStatus}>
                <View style={{flex:1,justifyContent:'flex-end'}}>
                    <View style={{backgroundColor:'gray',width:'80%',alignSelf:'center',padding:8,marginBottom:"20%",borderRadius:5}}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',color:'#fff',fontSize:14,paddingBottom:5}}>Item added successfully </Text>
                    </View>   
                </View>
            </Modal>    */}

            {/* <CustomNotification/> */}
            
            <Snackbar
              visible={this.state.snackBarVisible}
              onDismiss={()=>this.setState({snackBarVisible:false})}
              duration={3000}
              action={{
                label: 'GO TO CART',
                onPress: () => {
                  this.props.navigation.navigate("drawer",{screen:"myCart"})
                },
              }}>
              Item added successfully!
            </Snackbar>
        </SafeAreaView>
     )
    }
  }
}
function mapStateToProps(state){
    return{
        userId:state.userId,
        productCountChanged:state.productCountChanged,
        categoryProductCountChange:state.categoryProductCountChange,
        postCode:state.postCode,
        deliveryType:state.deliveryType,
        cartCount:state.cartCount,
        homeScreenUpdate:state.homeScreenUpdate,
        notificationCount:state.notificationCount
    }
}

function mapDispatchToProps(dispatch){
    return{
        setProductId:(value)=>dispatch({type:"setProductId",value}),
        setuserId:(value)=>dispatch({type:'setuserId',value}),
        setCategoryId:(value)=>dispatch({type:"setCategoryId",value}),
        setCartCount:(value)=>dispatch({type:"setCartCount",value}),
        setIncreaseCartCount:()=>dispatch({type:"IncreaseCartCount"}),
        setDecreaseCartCount:()=>dispatch({type:"DecreaseCartCount"}),
        setPostCode:(value)=>dispatch({type:"setPostCode",value}),
        setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
        setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
        setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
        setOrderCountChanged:(value)=>dispatch({type:"setOrderCountChanged",value}),
        setHomeScreenUpdate:(value)=>dispatch({type:"setHomeScreenUpdate",value}),
        setSingleProductPageUpdate:(value)=>dispatch({type:"setSingleProductPageUpdate",value}),
        setNotificationCount:(value)=>dispatch({type:"setNotificationCount",value}),
        setCategoryUrl:(value)=>dispatch({type:"setCategoryUrl",value}),
        setCategoryName:(value)=>dispatch({type:"setCategoryName",value})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(HomeScreen);


