import React, { Component } from 'react';
import { View, Text,ActivityIndicator } from 'react-native';
import { baseUrl } from '../Controller/baseUrl';
import {connect} from 'react-redux'

class TrackOrder extends Component {
  
    state = {
      loading:false,
      array:[{step:"Placed On :",time:'10:00 AM',status:true},{step:"Packed :",time:'10:00 AM',status:true},{step:"Shipped :",time:'10:00 AM',status:false},{step:"Delivery :",time:'10:00 AM',status:false}]
    };
  
    componentDidMount(){
      this.getDetails()
    }

    getDetails(){
      this.setState({loading:true})
      fetch(baseUrl+this.props.userId+"/my-orders/"+this.props.orderId+"/order-tracking")
      .then((responce)=>responce.json())
      .then((response)=>{
        //console.warn(response)
        this.setState({array:response.order_tracking,loading:false})
      })
    }

  render() {
    if(this.state.loading==true){
      return(
      <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
        <ActivityIndicator color="#fa7153" size="large"/>
        <Text style={{paddingTop:10}}>Loading</Text>
      </View>
      )
    }else{
    return (
      <View style={{flex:1,backgroundColor:'#fff',padding:30}}>

        {
          this.state.array.map((item,index)=>{
            return(
              <View style={{flexDirection:'row'}}>
               
                <View style={{alignItems:'center'}}>   
                  <View style={{paddingHorizontal:10}}>
                     {
                       item.reached==true?
                       <View>
                         {
                           item.step=="Order Cancelled"?
                           <View style={{width:15,height:15,backgroundColor:'red',borderRadius:100}}/>
                           :
                           <View style={{width:15,height:15,backgroundColor:'#59e954',borderRadius:100}}/>
                         }
                       </View>
                       :
                       <View style={{width:15,height:15,backgroundColor:'#dadcdd',borderRadius:100}}/>
                     }
                  </View>
                  {
                    this.state.array.length-1>index?
                      <View style={{width:15,height:80,alignItems:'center'}}>
                       {
                         item.reached==true?
                         <View style={{borderWidth:1.5,borderColor:'#59e954',height:'100%'}}/>
                         :
                         <View style={{borderWidth:1.5,borderColor:'#dadcdd',height:'100%'}}/>
                       }
                      </View>
                      :
                      null
                  }
                  
                </View>

                <View style={{marginLeft:10}}> 
                    <Text style={{fontFamily:"Montserrat-SemiBold",bottom:3,fontSize:13,}}>{item.step} </Text>
                    <Text style={{fontFamily:"Montserrat",fontSize:11,color:'gray'}}>{item.time}</Text>
                </View>
            </View>
            )
          })
        }
      </View>
    );
   }
  }
}
function mapStateToProps(state){
  return{
      userId:state.userId,
      orderId:state.orderId
  }
}

function mapDispatchToProps(dispatch){
  return{
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(TrackOrder);




