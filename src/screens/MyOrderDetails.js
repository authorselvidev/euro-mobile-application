import React, { Component } from 'react';
import { View, Text,ScrollView,Image,StyleSheet, ActivityIndicator} from 'react-native';
import images from '../images/index'
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,MyOrderDetailStyle,CartScreenStyle,CompleteOrderStyle, MyOrders} from './Styles/PrimaryStyle';
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {Textarea, Toast} from 'native-base'

class MyOrderDetails extends Component {

  state = {
    loading:false,  
    orderDetail:[],
    item:[],
    address:{},
    orderStatus:null,
    showingActivityIndicator:true,
    showingConfimationAlert:true
  };

  componentDidMount(){
      this.getOrderDetails()
  }

  componentWillUnmount(){
      this.props.setisFromOrderScreen(false)
  }

  getOrderDetails(){
    this.setState({loading:true})  
    console.log(baseUrl+this.props.userId+"/my-orders/"+this.props.orderId)
    fetch(baseUrl+this.props.userId+"/my-orders/"+this.props.orderId)
    .then((response)=>response.json())
    .then((responseJson)=>{
        console.log(responseJson)
        this.setState({orderDetail:responseJson.orders,address:responseJson.orders.address,item:responseJson.orders.item,loading:false})  
    })
    setTimeout(()=>{
        this.setState({showingActivityIndicator:false})
     },2000)
  }

async navigateSignleProduct(id){
   await this.props.setProductId(id)  
    this.props.navigation.navigate("singleProductScreen")
  }

 async gobackFunction(){
    if(this.props.initialRoute=="myOrderDetails"){
        await this.props.setinitialRoute("homeScreen")
        this.props.navigation.navigate("homeScreen")
    }else{
        this.props.navigation.goBack()
    }
  }

  render() {
      console.error(this.props.initialRoute)
      var subAmount=(this.state.orderDetail.sub_total+0).toFixed(2)
      var totalAmount=(this.state.orderDetail.sub_total+this.state.orderDetail.delivery_charge).toFixed(2)
      
      if(this.state.loading==true){
          return(
              <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
                  <ActivityIndicator color="#fa7153" size="large"/>
                  <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
              </View>
          )
      }else{
        return (
        <View style={{flex:1}}>
        <View style={{flexDirection:'row',paddingVertical:15,justifyContent:'space-between',alignItems:'center',backgroundColor:'#fa7150',paddingHorizontal:20}}>
            <TouchableOpacity onPress={()=>this.gobackFunction()} style={{width:'100%',height:20}}>
                <Image source={images.backArrow} style={{width:25,height:25}}/>
            </TouchableOpacity>

            <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>Order details</Text>
            <Text/>
        </View>
        <ScrollView style={OverAllStyle.MainContainerPad0}>
            {/* {
                this.state.orderDetail.order_request==0?
                <View>
                    {
                        this.state.showingConfimationAlert==true?
                            <View style={{width:'95%',alignSelf:'center',backgroundColor:'#fff',padding:10,margin:10,borderRadius:5,borderWidth:0.5,borderStyle:'dashed'}}>
                            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                                <Text style={{fontFamily:'Montserrat-SemiBold',fontSize:16}}>Confirming order with store</Text> 
                                
                                {
                                this.state.showingActivityIndicator==true?
                                <ActivityIndicator/>
                                :
                                <TouchableOpacity onPress={()=>this.setState({showingConfimationAlert:false})}>
                                    <Text style={{color:'red'}}>close</Text>
                                </TouchableOpacity>
                                }
                                
                            </View>
                            
                            <Text style={{fontFamily:'Montserrat',fontSize:14,paddingLeft:8,paddingTop:10}}>Please wait...your order may take up to 5mins to confirm.</Text>
                            </View>
                            :
                            null
                    }
                </View>
                :
                null
            } */}

            {
              this.state.orderDetail.order_request==1 || this.state.orderDetail.order_request==2?
                <View style={MyOrders.completed}>
                    <View style={MyOrderDetailStyle.InsideProgress}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {this.state.orderDetail.id}</Text>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{this.state.orderDetail.order_status_text}</Text>
                    </View>
                </View>
                :
                null
            }

             {
              this.state.orderDetail.order_request==0?
                <View style={MyOrders.InProgressBanner}>
                    <View style={MyOrderDetailStyle.InsideProgress}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {this.state.orderDetail.id}</Text>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{this.state.orderDetail.order_status_text}</Text>
                    </View>
                </View>
                :
                null
            }

            {
              this.state.orderDetail.order_request==4?
                <View style={MyOrders.CanceledBanner}>
                    <View style={MyOrderDetailStyle.InsideProgress}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {this.state.orderDetail.id}</Text>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{this.state.orderDetail.order_status_text}</Text>
                    </View>
                </View>
                :
                null
            }

            {
              this.state.orderDetail.order_request==3?
                <View style={MyOrders.completedWithBlackBg}>
                    <View style={MyOrderDetailStyle.InsideProgress}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {this.state.orderDetail.id}</Text>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{this.state.orderDetail.order_status_text}</Text>
                    </View>
                </View>
                :
                null
            }

            {
              this.state.orderDetail.order_request==5?
                <View style={MyOrders.ReturnOrderBanner}>
                    <View style={MyOrderDetailStyle.InsideProgress}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}}>Order ID : {this.state.orderDetail.id}</Text>
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#FFF',}} >{this.state.orderDetail.order_status_text}</Text>
                    </View>
                </View>
                :
                null
            }
            
            
            <View onPress={()=>this.props.navigation.navigate("trackOrder")} style={{flexDirection:'row',padding:20,backgroundColor:'#FFF',borderBottomColor:'#EEE',borderBottomWidth:1,justifyContent:'space-between'}}>
                <View style={{flexDirection:'row'}}>
                <View style={{paddingHorizontal:10}}>
                    <View style={{width:15,height:15,backgroundColor:'#59e954',borderRadius:100}}/>
                   
                        <View>
                         {
                             this.state.orderDetail.order_request==4?
                             <View>
                                <View style={{width:15,height:46,alignItems:'center'}}>
                                <View style={{borderWidth:1.5,borderColor:'#59e954',height:'100%'}}/>
                                </View>
                                <View style={{width:15,height:15,backgroundColor:'red',borderRadius:100}}/>
                            </View>
                            :
                            <View>
                              {
                                this.state.orderDetail.order_request==3?
                                <View>
                                    <View style={{width:15,height:46,alignItems:'center'}}>
                                    <View style={{borderWidth:1.5,borderColor:'#59e954',height:'100%'}}/>
                                    </View>
                                    <View style={{width:15,height:15,backgroundColor:'#59e954',borderRadius:100}}/>
                                </View>
                                :
                                <View>
                                    <View>
                                        <View style={{width:15,height:46,alignItems:'center'}}>
                                        <View style={{borderWidth:1.5,borderColor:'#59e954',height:'100%'}}/>
                                        </View>
                                        <View style={{width:15,height:15,backgroundColor:'#f3c800',borderRadius:100}}/>
                                    </View>
                                </View>
                              }
                            </View>
                            }
                        </View>
                </View>

                <View style={{marginLeft:10}}>
                    
                    <View style={{marginBottom:25}}>
                        <Text style={{fontFamily:"Montserrat-SemiBold",bottom:3,fontSize:13,}}>Placed on </Text>
                        <Text style={{fontFamily:"Montserrat",fontSize:11,color:'gray'}}>{this.state.orderDetail.placed_on}</Text>
                    </View>

                    <View>

                       {
                         this.state.orderDetail.order_request==0?
                            <View style={{bottom:-5}}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>Waiting for store confirmation  </Text>
                            </View>
                            :
                            null
                        }

                       {
                         this.state.orderDetail.order_request==1?
                            <View style={{bottom:-5}}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text}  </Text>
                            </View>
                            :
                            null
                        }

                         {
                         this.state.orderDetail.order_request==2?
                            <View style={{bottom:-5}}>
                                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text} </Text>
                            </View>
                            :
                            null
                        }

                        {
                         this.state.orderDetail.order_request==3 ?
                        <View>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text}  </Text>
                            <Text style={{fontFamily:"Montserrat",fontSize:11,color:'gray'}}>{this.state.orderDetail.delivery_date}</Text>
                        </View>
                        :
                        null
                        }

                        {/* {
                        this.state.orderDetail.order_request==1?
                        <View style={{paddingTop:5}}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text} </Text>
                        </View>
                        :
                        null
                        } */}

                        {/* {
                        this.state.orderDetail.order_request==3?
                        <View style={{paddingTop:5}}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text} </Text>
                        </View>
                        :
                        null
                        } */}

                        {
                        this.state.orderDetail.order_request==4?
                        <View style={{paddingTop:5}}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text} </Text>
                        </View>
                        :
                        null
                        }

                        {
                        this.state.orderDetail.order_request==5?
                        <View style={{paddingTop:5}}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,}}>{this.state.orderDetail.order_status_text} </Text>
                        </View>
                        :
                        null
                        }
                        
                    </View>

                </View>
                </View>    
                {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate("trackOrder")} style={{flex:1,justifyContent:'flex-end'}}>
                    <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fa7153',fontSize:12}}>Track status</Text>
                </TouchableOpacity> */}
            </View>


            <View style={MyOrderDetailStyle.DeliveryAddressMainView}>
                    {
                        this.state.orderDetail.delivery_type==0?
                        <View>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16,marginBottom:10}}>Delivery address</Text>
                            {/* <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15,marginBottom:15}}>Job site address</Text> */}
                        </View>
                        :
                        <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16,marginVertical:5}}>Pickup address</Text>
                    }
                    

                    <View style={MyOrderDetailStyle.DelverAddressSubView}>
                        
                        <View style={MyOrderDetailStyle.locationIconView}>                        
                            <Image source={images.location} style={StyleContainer.ImageContainStyle} />
                        </View>

                       {
                           this.state.orderDetail.delivery_type==0?
                           <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5,}}>{this.state.address.job_site_name}</Text>
                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5,}} >{this.state.address.address}</Text>
                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5,}} >Suburb : {this.state.address.suburb} <Text style={{fontSize:10,fontFamily:"Montserrat"}}>( postcode : {this.state.address.postcode} ) </Text> </Text>
                            </View>
                            :
                            <View style={MyOrderDetailStyle.JobSiteDetailView}>
                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginVertical:5,}}>EURO ABRACIVES</Text>
                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5,}} >Sydney Hardware & Buildings Supplies</Text>
                                <Text style={{fontFamily:"Montserrat",fontSize:14,marginBottom:5,}} >90-114 Princes HWY Peters NSW 2044</Text>
                            </View>
                       }

                </View>

            </View>


            <View style={MyOrderDetailStyle.ProductDisplayMainView}>
            
                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15,marginBottom:15}}>ITEMS:</Text>

                <View>
                {
                    this.state.item.map((item,index)=>{
                        var productPrice=(item.quantity * item.price).toFixed(2)
                    return(
                        <TouchableOpacity onPress={()=>this.navigateSignleProduct(item.id)} style={MyOrderDetailStyle.ProductDisplayView}>
                            
                            <View style={MyOrderDetailStyle.ProductView}>
                                <View style={MyOrderDetailStyle.ProductImageView}>
                                    <Image source={{uri:item.image}} style={MyOrderDetailStyle.ImgContainerStyle}/>
                                </View>
                                <View style={MyOrderDetailStyle.ProductDetailView}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{fontFamily:'Montserrat',fontSize:12}} >{item.name}</Text>
                                    <Text style={{fontFamily:'Montserrat',fontSize:10,color:'gray'}}>{item.product_variants}</Text>
                                </View>
                            </View>
                           
                            <View style={{width:'10%',justifyContent:'center',alignItems:'center'}}>
                               <Text style={{fontFamily:'Montserrat',fontSize:12}}>x {item.quantity}</Text>
                            </View>
                            
                            <View style={MyOrderDetailStyle.PriceView}>
                               <Text style={{fontFamily:"Montserrat",fontSize:12,}}>$ {productPrice}</Text>
                            </View>

                        </TouchableOpacity>
                    )
                    })
                }
                </View>

            </View>


            <View style={MyOrderDetailStyle.BillDetailView}>
                {
                    this.state.orderDetail.total_weight==0?null :

                    <View style={MyOrderDetailStyle.InsideBillView}>
                      <Text style={{fontFamily:"Montserrat",fontSize:13,}}>Total weight</Text>
                      <Text style={{fontFamily:"Montserrat",fontSize:13,}}>{this.state.orderDetail.total_weight} ton</Text>
                    </View>
                }

                <View style={MyOrderDetailStyle.InsideBillView}>
                    <Text style={{fontFamily:"Montserrat",fontSize:13}}>Sub total</Text>
                    <Text style={{fontFamily:"Montserrat",fontSize:13}}>$ {subAmount}</Text>
                </View>

                {
                    this.state.orderDetail.delivery_type==0?
                    <View style={{flexDirection:'row',justifyContent:'space-between',borderTopWidth:1,borderTopColor:'#dedede',paddingTop:10,marginTop:5,marginHorizontal:-20,paddingHorizontal:20}}>
                        <Text style={{fontFamily:"Montserrat",fontSize:13,color:'#f91900',}}>Delivery charges</Text>
                        <Text style={{fontFamily:"Montserrat",fontSize:13,color:'#f91900',}}>$ {(this.state.orderDetail.delivery_charge+0).toFixed(2)}</Text>
                    </View>
                    :null
                }
            </View>


            {/* <View style={MyOrderDetailStyle.BillDetailView}>
                <View style={MyOrderDetailStyle.InsideBillView}>
                    <Text style={{fontFamily:"Montserrat",fontSize:14,}}>Total amount</Text>
                    <Text style={{fontFamily:"Montserrat",fontSize:14,}}>$ {totalAmount}</Text>
                </View>

            </View> */}


            {/* <View style={MyOrderDetailStyle.BillDetailView}>
            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:14,marginBottom:5}}>Apply coupon</Text>
                <View style={MyOrderDetailStyle.InsideBillView}>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#59e954'}}>Offers Discount</Text>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13,color:'#59e954'}}>$4.04</Text>
                </View>

            </View> */}

            <View style={MyOrderDetailStyle.BillDetailView}>
            
                <View style={MyOrderDetailStyle.InsideBillView}>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>Grant total</Text>
                    <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:18,}}>$ {totalAmount}</Text>
                </View>

            </View>


            <View style={MyOrderDetailStyle.BillDetailView}>
            
                <View style={MyOrderDetailStyle.InsideBillView}>
                    <Text style={{fontFamily:"Montserrat",fontSize:14,}}>Payment method:</Text>
                    <Text style={{fontFamily:"Montserrat",fontSize:14,}}>{this.state.orderDetail.payment_type}</Text>
                </View>

                <View style={MyOrderDetailStyle.InsideBillView}>
                    <Text style={{fontFamily:"Montserrat",fontSize:14,}}>Refrence no:</Text>
                    {
                        this.state.orderDetail.transaction_id==null?
                        <Text style={{textAlign:'center'}}>-</Text>
                        :
                        <Text style={{fontFamily:"Montserrat",fontSize:14,}}>{this.state.orderDetail.transaction_id}</Text>
                    }
                    
                </View>

            </View>
            
            {
                this.state.orderDetail.comments==""?null:
                    <View style={CartScreenStyle.WhiteContainer} >
                        <View style={CartScreenStyle.WhiteSubContainer}>
                            <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15}}>Comments</Text>
                            <View style={CompleteOrderStyle.TxtAreaView}>
                                <Textarea numberOfLines={5} style={{width:'100%',height:'100%',fontFamily:'Montserrat',fontSize:12}}
                                value={this.state.orderDetail.comments}
                                placeholderTextColor="#bfbfbf"
                                editable={false}
                                // onChangeText={(value)=>{this.changeDescriptionData(value)}}
                                />
                            </View>
                        </View>
                    </View>
            }
        </ScrollView>
        </View>
        );
     }
  }
}

function mapStateToProps(state){
    return{
        userId:state.userId,
        orderId:state.orderId,
        orderPlacedTime:state.orderPlacedTime,
        isFromOrderScreen:state.isFromOrderScreen,
        initialRoute:state.initialRoute
    }
  }
  
  function mapDispatchToProps(dispatch){
    return{
        setorderId:(value)=>dispatch({type:"setorderId",value}),
        setProductId:(value)=>dispatch({type:"setProductId",value}),
        setisFromOrderScreen:(value)=>dispatch({type:"setisFromOrderScreen",value}),
        setinitialRoute:(value)=>dispatch({type:"setinitialRoute",value})
    }
  }
  export default connect(mapStateToProps,mapDispatchToProps)(MyOrderDetails);
  



