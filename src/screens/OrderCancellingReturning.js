import React, { Component } from 'react';
import { View, Text, ActivityIndicator,TouchableOpacity,Image,Modal, ScrollView } from 'react-native';
import {connect} from 'react-redux'
import { baseUrl } from '../Controller/baseUrl';
import { MyOrderDetailStyle, HelpStyles } from './Styles/PrimaryStyle';
import { StyleContainer } from './Styles/CommonStyles';
import images from '../images';
import { TextInput } from 'react-native-gesture-handler';
import CustomNotification from './CustomNotification';
import { Toast } from 'native-base'
import { Button, Snackbar } from 'react-native-paper';

class OrderCancellingReturning extends Component {
  constructor(props) {
    super(props);
    this.state = {
       loading:false,  
       item:[],
       reason:[],
       selectReason:false,
       reasonSelected:"",
       reasonId:null,
       comments:'',
       moreDetails:'',
       moreDetailsError:'',
       reasonIdError:'',
       cancelButtonLoader:false,
       snackBarCancellingVisible:false,
       snackBarRetrunVisible:false,
    };
  }

  componentDidMount(){
    //console.log(this.props.orderReturn)
    this.getOrderDetails()
    this.getReason()
}

getOrderDetails(){
  this.setState({loading:true})  
  fetch(baseUrl+this.props.userId+"/my-orders/"+this.props.orderId)
  .then((response)=>response.json())
  .then((responseJson)=>{
      this.setState({item:responseJson.orders.item,loading:false})  
      //console.log(this.state.address)
  })
}

getReason(){
  fetch(baseUrl+this.props.userId+"/my-orders/"+this.props.orderId+"/cancel-or-return")
  .then((response)=>response.json())
  .then((responseJson)=>{
    this.setState({reason:responseJson.cancel_reasons})
    //console.warn(this.state.reason)
  })
}

async navigateSignleProduct(id){
 await this.props.setProductId(id)  
  this.props.navigation.navigate("singleProductScreen")
}

async confirmToCancellingReturning(){
  var orderType=0
  if(this.props.orderReturn==true){
    orderType=2
  }else{
    orderType=1
  }

  if(this.state.reasonId!=null && this.state.moreDetails!=""){
  this.setState({cancelButtonLoader:true})
  console.log(orderType,this.state.reasonId,this.state.moreDetails,this.state.comments)
  await fetch(baseUrl+this.props.userId+"/my-orders/"+this.props.orderId+"/cancel-or-return",{
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        "order_type": orderType,
        "reason": this.state.reasonId ,
        "more_details": this.state.moreDetails,
        "comments":this.state.comments
    })
  })
  .then((response)=>response.json())
  .then((responseJson)=>{
     console.log(responseJson)
     this.props.setOrderCountChanged(true)
     this.setState({cancelButtonLoader:true})
     if(orderType==1){
       this.setState({snackBarCancellingVisible:true})
      // Toast.show({
      //   text: "Your order has been cancelled",
      //   buttonText: "Okay",
      //   position: "bottom",
      //   textStyle: { fontFamily: 'Lato-Regular' },
      //   buttonTextStyle: { fontFamily: 'Lato-Bold' }
      // })
     }else{
      this.setState({snackBarRetrunVisible:true})
      // Toast.show({
      //   text: "Your order has been request for return",
      //   buttonText: "Okay",
      //   position: "bottom",
      //   textStyle: { fontFamily: 'Lato-Regular' },
      //   buttonTextStyle: { fontFamily: 'Lato-Bold' }
      // })
     }
     this.props.navigation.navigate("drawer",{screen:'myOrder'})
  })
 }else{
   if(this.state.reasonId==null){
     this.setState({reasonIdError:"Please select a valid reason"})
   }else{
    this.setState({reasonIdError:""})
   }
   if(this.state.moreDetails==""){
     this.setState({moreDetailsError:"Make sure fill order cancelling reason in words"})
   }else{
    this.setState({moreDetailsError:""})
   }
 }
}

  render() {
    if(this.state.loading==true){
      return(
        <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
          <ActivityIndicator color="#fa7153" size="large"/>
          <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
        </View>
      )
    }else{
      return (
        <View style={{flex:1}}>
          <ScrollView>
           <View style={MyOrderDetailStyle.ProductDisplayMainView}>
                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15,marginBottom:15}}>ITEMS:</Text>
                <View>
                {
                    this.state.item.map((item,index)=>{
                    return(
                        <TouchableOpacity onPress={()=>this.navigateSignleProduct(item.id)} style={MyOrderDetailStyle.ProductDisplayView}>
                            
                            <View style={MyOrderDetailStyle.ProductView}>
                                <View style={MyOrderDetailStyle.ProductImageView}>
                                    <Image source={{uri:item.image}} style={MyOrderDetailStyle.ImgContainerStyle}/>
                                </View>
                                <View style={MyOrderDetailStyle.ProductDetailView}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{fontFamily:'Montserrat',fontSize:12}} >{item.name}</Text>
                                    <Text style={{fontFamily:'Montserrat',fontSize:12}}>x  {item.quantity}</Text>
                                </View>
                            </View>

                            <View style={MyOrderDetailStyle.PriceView}>
                               <Text style={{fontFamily:"Montserrat",fontSize:12,}}>${item.quantity*item.price}</Text>
                            </View>

                        </TouchableOpacity>
                    )
                    })
                }
                </View>
            </View>
            
            {
              this.state.reasonSelected!=""?
              <View>
                    
                <View style={{backgroundColor:'#fff',padding:20,marginTop:10}}>
                      <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:15,color:'black',paddingBottom:10}}>Reason for cancelling</Text>
                      <TouchableOpacity onPress={()=>this.setState({selectReason:!this.state.selectReason})} style={{flexDirection:'row',justifyContent:'space-between'}}>
                          <Text style={{fontFamily:"Montserrat",fontSize:13,width:'90%'}} numberOfLines={3}>  {this.state.reasonSelected}</Text>      
                          {
                            this.state.selectReason==true?
                            <Image source={images.downArrow} style={{width:15,height:15,resizeMode:'contain',transform: [{ rotate: "270deg" }] }}/>
                            :
                            <Image source={images.downArrow} style={{width:15,height:15,resizeMode:'contain'}}/>
                          }
                        </TouchableOpacity>
                      

                </View>
              </View>  
              :
              <TouchableOpacity onPress={()=>this.setState({selectReason:!this.state.selectReason})} style={{flexDirection:'row',justifyContent:'space-between',backgroundColor:'#fff',padding:20,marginTop:10}}>
                    {
                      this.props.orderReturn==false?
                      <Text style={{fontFamily:"Montserrat",fontSize:15,color:'black'}}>Reason for cancelling</Text>      
                      :
                      <Text style={{fontFamily:"Montserrat",fontSize:15,color:'black'}}>Reason for Returning</Text>      
                    }
                    
                    
                      {
                        this.state.selectReason==true?
                        <Image source={images.downArrow} style={{width:15,height:15,resizeMode:'contain'}}/>
                        :
                        <Image source={images.downArrow} style={{width:15,height:15,resizeMode:'contain',transform: [{ rotate: "270deg" }] }}/>
                      }
              </TouchableOpacity>
            }

            {
              this.state.selectReason==true?
                <View style={{backgroundColor:'#fff',paddingHorizontal:20}}>
                  {
                    this.state.reason.map((item,index)=>{
                      var selectedreson=null;
                      return(
                        <View>
                          <TouchableOpacity onPress={()=>this.setState({reasonSelected:item.reason,reasonId:item.id,selectReason:!this.state.selectReason,reasonIdError:''})} style={{paddingVertical:15,paddingLeft:10,borderBottomWidth:1,borderBottomColor:'#dedede'}}>
                              <Text style={{fontFamily:"Montserrat",fontSize:13}}>{item.reason}</Text>
                          </TouchableOpacity>
                        </View>
                      )
                    })
                  }     
                </View>
                :
                null
            }
            
             {
                this.state.reasonIdError=='' ?null:
                <View style={{width:'100%',marginLeft:20,marginTop:5}}>
                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.reasonIdError}</Text>
                </View>
              }

            <View style={{backgroundColor:'#fff',marginTop:10,padding:20}}>
              <Text>More details</Text>
              <TextInput 
                style={{ height: 40, borderColor: 'gray', borderBottomWidth:1 }}
                onChangeText={(text)=>this.setState({moreDetails:text})}
                value={this.state.moreDetails}
              />
               <Text style={{fontFamily:"Montserrat",paddingVertical:10,color:'gray',fontSize:12}}>please share futher information</Text>
               
               {
                this.state.moreDetailsError==''?null:
                <View style={{width:'100%'}}>
                <Text style={StyleContainer.RedIndicationTxtStyle}>{this.state.moreDetailsError}</Text>
                </View>
              }
            </View>

            {/* <View style={{backgroundColor:'#fff',marginTop:10,padding:20}}>
              <Text>Comments</Text>
              <TextInput 
                style={{ height: 40, borderColor: 'gray', borderBottomWidth:1 }}
                onChangeText={(text)=>this.setState({comments:text})}
                value={this.state.comments}
              />
              
            </View> */}

            {
              this.state.cancelButtonLoader==true?
              <TouchableOpacity style={{backgroundColor:'#fa7153',width:'90%',padding:15,alignSelf:'center',borderRadius:5,marginVertical:20}}>
                <ActivityIndicator color="#FFF"/>
              </TouchableOpacity>
              :
              <TouchableOpacity onPress={()=>this.confirmToCancellingReturning()} style={{backgroundColor:'#fa7153',width:'90%',padding:15,alignSelf:'center',borderRadius:5,marginVertical:20}}>
                {
                  this.props.orderReturn==true?
                  <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16,color:'#fff',textAlign:'center'}}>CONFIRM TO RETURNING</Text>
                  :
                  <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:16,color:'#fff',textAlign:'center'}}>CONFIRM TO CANCELLATION</Text>
                }
              </TouchableOpacity>
            }
           </ScrollView>

           <CustomNotification/>

           {/* <Snackbar
              visible={this.state.snackBarCancellingVisible}
              onDismiss={()=>this.setState({snackBarCancellingVisible:false})}
              duration={3000}
              action={{
                label: 'Close',
                onPress: () => {
                  this.setState({snackBarCancellingVisible:false})
                },
              }}>
              Your order has been cancelled
            </Snackbar> */}

            {/* <Snackbar
              visible={this.state.snackBarRetrunVisible}
              onDismiss={()=>this.setState({snackBarRetrunVisible:false})}
              duration={3000}
              action={{
                label: 'Close',
                onPress: () => {
                  this.setState({snackBarRetrunVisible:false})
                },
              }}>
              Your order has been return
            </Snackbar> */}

          </View>

      );
    }
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
      orderId:state.orderId,
      orderReturn:state.orderReturn
  }
}

function mapDispatchToProps(dispatch){
  return{
      setorderId:(value)=>dispatch({type:"setorderId",value}),
      setProductId:(value)=>dispatch({type:"setProductId",value}),
      setOrderCountChanged:(value)=>dispatch({type:"setOrderCountChanged",value})
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(OrderCancellingReturning);

