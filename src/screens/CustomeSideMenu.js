import React, { Component } from 'react';
import { View, Text,TouchableOpacity,Image,ScrollView,Modal,SafeAreaView } from 'react-native';
import images from '../images/index'
import {connect} from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage';

class CustomeSideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalvisible:false,
      isGuest:false
    };
  }

  componentDidMount(){
    AsyncStorage.getItem('guestId').then((value) => {
      if(value!=null){
        this.setState({isGuest:true})
      }else{
        this.setState({isGuest:false})
      }
    })
    console.log("isguest"+this.state.isGuest)
    this.props.setguestToUserChanged(false)
  }

  componentDidUpdate(prevProps){
     if(this.props.guestToUserChanged==true){
       console.log("update")
      AsyncStorage.getItem('guestId').then((value) => {
        if(value!=null){
          this.setState({isGuest:true})
        }else{
          this.setState({isGuest:false})
        }
      })
      this.props.setguestToUserChanged(false)
     }
  }

  async createAddress(){
    await this.props.setisFromCheckout(false)
    await this.props.setisShippingAddress(false)
    this.closeToggle()
    console.warn(this.props.isFromCheckout)
    this.props.navigation.navigate("manageSiteAddress")
  }

 async logout(){
   await AsyncStorage.removeItem('userId');
   await AsyncStorage.removeItem('guestId');
   await this.props.setProductCountChanged(true)
   await this.props.setCategoryProductCountChanged(true)
   await this.props.setOrderCountChanged(true)
   await this.props.setisFromCheckout(false)
   await this.props.setisShippingAddress(true)
   await this.props.setCartCount(0)
    this.setState({modalvisible:false})
    this.closeToggle()
    this.props.navigation.navigate("signin")
  }

  signin(){
    this.closeToggle()
    this.props.navigation.navigate("signin")
  }

  closeToggle(){
    this.props.navigation.closeDrawer()
  }

  render() {
    return ( 
      <SafeAreaView style={{flex:1,backgroundColor:'#fff'}}>
        <View style={{paddingLeft:15}}>
           <Image source={images.logo} style={{width:150,height:60,resizeMode:'contain',marginVertical:20,alignSelf:'flex-start'}}/>
        </View>
        <View style={{marginHorizontal:20}}>
          <Text style={{fontFamily:"Montserrat-SemiBold"}}>DELIVERY LOCATION</Text>
          <View style={{flexDirection:'row'}}>
            <Text style={{fontFamily:"Montserrat",fontSize:12}}>Postcode - {this.props.postCode} </Text>
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("changePostcode"),this.closeToggle()}} style={{borderBottomWidth:0.5,borderColor:'#fa7153'}}>
              <Text style={{fontFamily:"Montserrat",color:'#fa7153',fontSize:12}}>change</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          
          {
            this.state.isGuest==true?
            <TouchableOpacity onPress={()=>this.signin()} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,borderTopWidth:0.5,borderTopColor:'gray',paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray',marginTop:15}}>
              <Image source={images.login} style={{width:25,height:25,resizeMode:'contain'}}/>
              <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Signin / Signup</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("myProfile"),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,borderTopWidth:0.5,borderTopColor:'gray',paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray',marginTop:15}}>
              <Image source={images.profile} style={{width:25,height:25,resizeMode:'contain'}}/>
              <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>My profile</Text>
            </TouchableOpacity>
          }
         
         {
          this.state.isGuest==true?null:
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("drawer",{screen:"myOrder"}),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
              <Image source={images.ordersIconWithoutColor} style={{width:25,height:25,resizeMode:'contain'}}/>
              <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>My orders</Text>
            </TouchableOpacity>
         }
         
          <TouchableOpacity onPress={()=>this.createAddress()} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
            <Image source={images.menuBarLocation} style={{width:25,height:25,resizeMode:'contain'}}/>
            <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Manage site address</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>{this.props.navigation.navigate("shopByCategory"),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
            <Image source={images.list} style={{width:25,height:25,resizeMode:'contain'}}/>
            <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Shop by category</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={()=>{this.props.navigation.navigate("wishList"),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
            <Image source={images.favourite} style={{width:25,height:25,resizeMode:'contain'}}/>
            <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>My wishlist</Text>
          </TouchableOpacity>

          {
            this.state.isGuest==true?
            null
            :
            <TouchableOpacity onPress={()=>{this.props.navigation.navigate("changePassword"),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
              <Image source={images.lock} style={{width:25,height:25,resizeMode:'contain'}}/>
              <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Change password</Text>
            </TouchableOpacity>
          }
          

          <TouchableOpacity onPress={()=>{this.props.navigation.navigate("notification"),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
            <Image source={images.grayBell} style={{width:25,height:25,resizeMode:'contain'}}/>
            <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Notifications</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity onPress={()=>{this.props.navigation.navigate("help"),this.closeToggle()}} style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
            <Image source={images.question} style={{width:25,height:25,resizeMode:'contain'}}/>
            <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Help</Text>
          </TouchableOpacity> */}

          {
            this.state.isGuest==true?
            null
            :
            <TouchableOpacity onPress={()=>this.setState({modalvisible:!this.state.modalvisible})}  style={{flexDirection:'row',alignItems:'center',paddingVertical:20,paddingLeft:15,borderBottomWidth:0.5,borderBottomColor:'gray'}}>
              <Image source={images.logout} style={{width:25,height:25,resizeMode:'contain'}}/>
              <Text style={{fontFamily:"Montserrat",fontSize:16,paddingLeft:12}}>Logout</Text>
            </TouchableOpacity>
          }
        </ScrollView>
        
        <Modal
             animationType="slide"
             transparent={true}
             visible={this.state.modalvisible}
             onRequestClose={()=>this.setState({modalvisible:false})}>
              <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.5)'}}>
                  
                <View style={{width:300,height:150,backgroundColor:'#FFF',padding:20,borderRadius:20}}>
                  <View>
                    <Text style={{fontFamily:"Montserrat",color:'black',fontSize:18}}>Logout confirmation?</Text>
                    <Text lineBreakMode='clip' numberOfLines={4} style={{paddingVertical:20,color:'black'}}>Are you sure you want to logout now?</Text>
                    <View style={{flexDirection:'row',alignItems:'flex-end',alignSelf:'flex-end'}}>
                      <TouchableOpacity onPress={()=>this.setState({modalvisible:!this.state.modalvisible})}>
                        <Text style={{fontFamily:"Montserrat",color:'#ae0000',fontSize:17,paddingRight:20}}>NO</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={()=>this.logout()}>
                        <Text style={{fontFamily:"Montserrat",color:'#ae0000',fontSize:17}}>YES</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
        </Modal>
      </SafeAreaView>
    );
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
      postCode:state.postCode,
      guestToUserChanged:state.guestToUserChanged,
      isFromCheckout:state.isFromCheckout,
  }
}

function mapDispatchToProps(dispatch){
  return{
      setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
      setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
      setOrderCountChanged:(value)=>dispatch({type:"setOrderCountChanged",value}),
      setCartCount:(value)=>dispatch({type:"setCartCount",value}),
      setguestToUserChanged:(value)=>dispatch({type:"setguestToUserChanged",value}),
      setisFromCheckout:(value)=>dispatch({type:'setisFromCheckout',value}),
      setisShippingAddress:(value)=>dispatch({type:"setisShippingAddress",value}),
      setisFromCheckout:(value)=>dispatch({type:'setisFromCheckout',value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(CustomeSideMenu);

