
import React, { Component } from 'react';
import { View, Text,ToastAndroid,BackHandler, TouchableOpacity,Linking,Image,TextInput,ScrollView,Modal,ActivityIndicator,Platform } from 'react-native';
import images from '../images/index'
import base64 from 'react-native-base64'

import NetInfo from '@react-native-community/netinfo'
import AuthenticationService from '../Controller/ServiceClass';
import { SigninModel, SigninResponseModel, GuestLoginModelResponse, GuestLoginModel } from '../DataModel/ModelClass';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {connect} from 'react-redux'
import { Colors } from './Styles/colors';
import { AndroidBackHandler } from "react-navigation-backhandler";
import CustomNotification from './CustomNotification';
import { baseUrl } from '../Controller/baseUrl';
import AsyncStorage from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';

class Signin extends Component {
   state={
     loading:false,
     emailError:'', 
     email:'',
     password:'',
     passwordError:'',
     deviceType:'',
     fcmToken:null,
     LoginStatus:null,
     rememberLogin:true,
     guestLoginLoading:false,
     gusetId:null,
     permissionRejected:false,
     modalVisible:false,
     hidePassword:true,
     count:0
   }  

 async componentDidMount(){  
  console.log("signin mount")
 await AsyncStorage.getItem("guestId").then((value)=>{
    //console.log("guest"+value)
    if(value!=null){
      this.setState({gusetId:value})
    }
  }) 
 await AsyncStorage.getItem("@email").then((value)=>{
    console.log(value)
    if(value!=null){
      this.setState({email:JSON.parse(value)})
    }
  }) 
  this.checkPermission()  
  this.deviceType()
   }

   componentDidUpdate(prevProps){
     if(this.props.guestLogin!==prevProps.guestLogin){
      AsyncStorage.getItem("guestId").then((value)=>{
        //console.log("guest"+value)
        if(value!=null){
          this.setState({gusetId:value})
        }
      }) 
      this.checkPermission()  
      this.deviceType()
     }
   }

   async checkPermission() {
        const authStatus=await messaging().requestPermission();
       const enabled=
         authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
         authStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if(enabled){
        console.log('Authorization status : ', authStatus);
        }
        this.getToken()
  }

  async getToken() {
      
    if (!this.state.fcmToken) {
      // Register the device with FCM
      await messaging().registerDeviceForRemoteMessages();

      // Get the token
      const token = await messaging().getToken();

      console.log(token)
      await this.setState({fcmToken:token})
    }
  }

  async requestPermission() {
    // try {
    //   await firebase.messaging().requestPermission();
    //     this.getToken();
    // } catch (error) {
    //   // User has rejected permissions
    //   //alert("permission rejected")
    //   await this.setState({permissionRejected:true,modalVisible:true})
    //   //console.log(this.state.permissionRejected)
    // }
  }


  async deviceType(){
   if(Platform.OS==="ios"){
     await this.setState({deviceType:"ios"})
    }else{
     await this.setState({deviceType:"android"})
    }
  }

 checkEmail(){
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
    if(this.state.email=='')
    {
        this.setState({emailError:'The email field is required'}) 
    }
    else{
        this.setState({emailError:''})
        if(reg.test(this.state.email) === true)
        {
            this.setState({emailError:''})
        }
        else{
            this.setState({emailError:'check the email format'})
        }
    }

    
  }

 async signin(){
   //this.checkPermission()
   await this.checkEmail();
  
   if(this.state.password==''){
    this.setState({passwordError:"The password field is required"})
  }else{
    this.setState({passwordError:""})
   }

    SigninModel.email=this.state.email
    SigninModel.password=this.state.password
    SigninModel.deviceType=this.state.deviceType
    SigninModel.deviceToken=this.state.fcmToken

   if(this.state.emailError=='' && this.state.passwordError==''){ 
      
      await this.setState({loading:true})
      await AuthenticationService.signin()
      

      if(SigninResponseModel.status){
        if(this.state.rememberLogin==true){
          await AsyncStorage.setItem('@email',JSON.stringify(this.state.email))
        }
       
        //this.props.navigation.navigate('homeScreen')
       await AsyncStorage.getItem('guestId').then((value) => {
          if(value==null){
              AsyncStorage.setItem('userId',JSON.stringify(SigninResponseModel.userId))
              this.props.setProductCountChanged(true)
              this.props.setguestToUserChanged(true)
              this.props.setinitialRoute("homeScreen")
              this.props.navigation.navigate("homeScreen")
          }else{
              AsyncStorage.removeItem('guestId');
              AsyncStorage.setItem('userId',JSON.stringify(SigninResponseModel.userId))  
              // if(this.props.cartCount<1){
                this.props.setProductCountChanged(true)
                this.props.setguestToUserChanged(true)
                this.props.setAddressChanged(true)
                this.props.setinitialRoute("homeScreen")
                this.props.navigation.navigate("homeScreen")
              // }else{
              //   this.props.setProductCountChanged(true)
              //   this.props.setAddressChanged(true)
              //   this.props.setguestToUserChanged(true)
              //   this.props.navigation.navigate("completeOrder")
              // }
          }
          })

        SigninResponseModel.status=false
      }else{
        this.setState({emailError:SigninResponseModel.errorMessage})
      }
      await this.setState({loading:false})
    }      
   }

  async guestLogin() {
    this.setState({emailError:'',passwordError:''})
    //await this.setState({guestLoginLoading:true})
    await NetInfo.fetch().then(state => {
      //console.log("Connection type", state.type);
      if(state.isConnected==true){
        this.setState({guestLoginLoading:true})
        fetch(baseUrl+'guest', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
           "user_type":"guest",
           "device_type":this.state.deviceType,
           "device_token":this.state.fcmToken
            })
          })
        .then((response) => response.json())
         .then((responseJson) => {
             AsyncStorage.setItem('guestId',JSON.stringify(responseJson.data.id))
             this.props.setProductCountChanged(true)
             this.props.setguestToUserChanged(true)
             this.props.navigation.navigate("postCode")
             this.setState({guestLoginLoading:false})
         }).catch((err)=>{
          //console.log(err)
      })  
      }else{
        alert("Network connection failed")
      }
    })
    //await this.setState({guestLoginLoading:false})
  }

  onBackButtonPressAndroid = () => {
      this.setState({count:this.state.count+1})
      setTimeout(() => {this.setState({count:this.state.count-1})}, 2000)
      if(this.state.count==0 || this.state.count==1){
      ToastAndroid.show("Tap again to exit", ToastAndroid.SHORT);
      }
      else{
        if(this.state.count==2){
        BackHandler.exitApp()
        }
      }
      return true;
  };

  render() {
    return (
            <View style={StyleContainer.MainContainer}>
             <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
              <View style={{width:'50%',height:'15%'}}>        
                <Image source={images.logo} style={{width:undefined,height:undefined,resizeMode:'contain',flex:1}}/>
              </View>

              <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',fontSize:23,marginTop:20,marginBottom:15,letterSpacing:1}}>Existing customer</Text>
        
                  <View style={StyleContainer.TextInputMainView}>
                    <TextInput style={StyleContainer.TextInputView}
                     
                      placeholder="Email Address"
                      ref={(input) => { this.emaildRef = input; }}
                      onSubmitEditing={() => {this.checkEmail(),this.passwordRef.focus()}}
                      onChangeText={(value)=>{this.setState({email:value})}} 
                      returnKeyType={"next"}
                      keyboardType="default"
                      value={this.state.email}
                     />
                     
                      {
                       this.state.emailError==''?null
                       :
                         <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.emailError}</Text>
                     }
                     
                    </View> 

                    <View style={{flexDirection:'row',width:'100%',marginVertical:10,alignItems:'center',borderWidth:1,borderColor:Colors.txtInputClr,borderRadius:3}}>
                    <TextInput 
                    style={{backgroundColor:Colors.whiteBg,width:'90%',height:45,paddingHorizontal:10,paddingLeft:20,
                          fontSize:14,fontFamily:'Montserrat'}} 
                      secureTextEntry={this.state.hidePassword}
                       placeholder="Password"
                       ref={(input) => { this.passwordRef = input; }}                         
                       onChangeText={(value)=>{this.setState({password:base64.encode(value)})}} 
                       onSubmitEditing={() => {this.signin()}}
                       returnKeyType={"done"}        
                       textContentType="oneTimeCode"        
                        />
                        <TouchableOpacity onPress={()=>this.setState({hidePassword:!this.state.hidePassword})}>
                           {
                             this.state.hidePassword==true?
                             <Image source={images.hidePassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                             :
                             <Image source={images.showPassword} style={{width:20,height:20,resizeMode:'contain'}}/>
                           }
                           
                        </TouchableOpacity>
                    </View>
                    <View style={{alignSelf:'flex-start',marginTop:-8}}>
                    {
                     this.state.passwordError==''?
                      null
                      :
                      <Text style={{fontFamily:"Montserrat",color:'red',fontSize:12,marginBottom:5}}>{this.state.passwordError}</Text>
                     } 
                    </View>
            <View style={{width:'100%',flexDirection:'row',justifyContent:'space-between',paddingVertical:5,marginBottom:10}}>
                <View style={{flexDirection:'row',alignItems:'center'}}>
                  
                  {
                    this.state.rememberLogin?
                    <TouchableOpacity onPress={()=>this.setState({rememberLogin:!this.state.rememberLogin})}>
                      <Image source={require('../images/rememberLogin.png')} style={{width:20,height:20,resizeMode:'contain'}}/>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity onPress={()=>this.setState({rememberLogin:!this.state.rememberLogin})}>
                    <View style={{width:20,height:20,borderWidth:1.5,borderColor:'gray'}}/>
                    </TouchableOpacity>
                  }
                  
                   <Text style={{fontFamily:"Montserrat",paddingLeft:10,fontSize:13}}>Remember my login</Text>
                </View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("forgetPassword")}>
                  <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13}}>Forgot password?</Text>
                </TouchableOpacity>
            </View>

            {
              this.state.loading?
              <TouchableOpacity style={StyleContainer.ButtonOrange}>
                <ActivityIndicator color="#fff"/>
              </TouchableOpacity>
              :
              <TouchableOpacity onPress={()=>this.signin()} style={StyleContainer.ButtonOrange}>
                <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff',fontSize:17}}>SIGN IN</Text>
              </TouchableOpacity>
            }

            <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginBottom:15}}>
                <Text style={{fontFamily:"Montserrat-SemiBold",fontSize:13}}>Don't have an account?</Text>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("signup")}>
                  <Text style={{fontFamily:"Montserrat",color:'#fa7153',paddingLeft:5,fontSize:13}}>Create account</Text>
                </TouchableOpacity>
            </View>

            {
              this.state.gusetId==null?
              <View style={{width:'100%'}}>
                {
                this.state.guestLoginLoading==true?
                <TouchableOpacity style={StyleContainer.ButtonBorder}>
                  <ActivityIndicator color="#fa7153"/>
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={()=>this.guestLogin()} style={StyleContainer.ButtonBorder}>
                  <Text style={{fontFamily:"Montserrat",fontSize:17}}>GUEST CHECK-OUT</Text>
                </TouchableOpacity>
              }
              </View>
              :
              null
            }            
            
            {
              this.state.permissionRejected==true?
              <View>
              {
                Platform.OS=='ios'?
                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.modalVisible}
                  onRequestClose={()=>this.setState({modalVisible:false})}>
                    
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.5)'}}>
                        <TouchableOpacity onPress={()=>this.setState({modalVisible:!this.state.modalVisible})} 
                         style={{marginBottom:10}}>
                            <Image source={images.cancel} style={{width:25,height:25,resizeMode:'cover'}}/>
                        </TouchableOpacity>
                        <View style={{width:'80%',backgroundColor:'#FFF',padding:20,borderRadius:10,justifyContent:'center',alignItems:'center'}}>
                            <Image source={images.grayBell} style={{width:50,height:50,resizeMode:'contain'}}/>
                            <Text style={{fontFamily:"Montserrat-SemiBold",paddingTop:30}}>Enable Reminder</Text>
                            <Text style={{fontFamily:"Montserrat",fontSize:12,color:'gray',paddingTop:10}}>Tap "Allow" so we can</Text>
                            <Text style={{fontFamily:"Montserrat",fontSize:12,color:'gray'}}>recommend better experience</Text>

                            <TouchableOpacity onPress={()=>Linking.openSettings('app-setting')} 
                              style={{width:'70%',paddingVertical:13,backgroundColor:'#fa7153',borderWidth:0.5,borderColor:'gray',borderRadius:30,marginVertical:20}}>
                              <Text style={{fontFamily:"Montserrat-SemiBold",textAlign:'center',color:'#fff',fontSize:18}}>Set Permission</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
                :
                null
            }
            </View>
            :null
            }
           </AndroidBackHandler>

           <CustomNotification/>
      </View>
      
    );
  }
}

function mapStateToProps(state){
  return{
      userId:state.userId,
      guestLogin:state.guestLogin,
      cartCount:state.cartCount
  }
}

function mapDispatchToProps(dispatch){
  return{
    setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
    setAddressChanged:(value)=>dispatch({type:'setAddressChanged',value}),
    setguestToUserChanged:(value)=>dispatch({type:"setguestToUserChanged",value}),
    setinitialRoute:(value)=>dispatch({type:"setinitialRoute",value})
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(Signin);
