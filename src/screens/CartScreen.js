
import React, { Component } from 'react';
import { View, Text,SafeAreaView,Image,TouchableOpacity,Modal,ScrollView,Keyboard, TextInput,ActivityIndicator, Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {connect} from 'react-redux'
import {baseUrl} from '../Controller/baseUrl'
import images from '../images/index';
import {StyleContainer} from '../screens/Styles/CommonStyles';
import {OverAllStyle,CartScreenStyle, HomeScreenStyle} from './Styles/PrimaryStyle';
import CustomNotification from './CustomNotification';
import { Toast } from 'native-base';
import { Colors } from './Styles/colors';
import { Snackbar } from 'react-native-paper';

class CartScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false,
      orderDetails:[],
      orderType:["Delivey","Pick-up"],
      initialTypeIndex:0,
      totalAmount:0,
      totalWeight:null,
      deliveryCharge:0,
      selectedProduct:{},
      networkFailed:false,
      deleteProductStatus:false,
      deliveryType:true,
      cartLoading:false,
      selectedCartItem:null,
      IncOrDecProductName:null,
      IncOrDecProductCountChanged:false,
      removeProduct:false,
      removeProductModalVisible:false,
      removeCartProductId:null,
      removeCartProductIndex:null,
      modalVisble:false,

      isediting:false,
      iseditingValue:null,
      selectedItemForEditing:null,
      iskebordVisible:false,
      arrayIndexValue:null,
      itemRowId:null,
      snackbarVisible:false
    };
  }

  async componentDidMount(){
   await this.getCartDetail()
  }

  UNSAFE_componentWillMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow)
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide)
  }
    
  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
    
  _keyboardDidShow = ()=> {
    this.setState({iskebordVisible:true})
    console.log(this.state.iskebordVisible)
    
  }
    
  _keyboardDidHide = () =>{
    this.setState({iskebordVisible:false})
    console.log(this.state.iskebordVisible)
    if(this.state.itemRowId!=null){
      this.addToCart(this.state.arrayIndexValue,this.state.itemRowId)
    }
  }

 async componentDidUpdate(){
    if(this.props.categoryProductCountChange==true){
      await this.props.setCategoryProductCountChanged(false)    
      await NetInfo.fetch().then(state => {
        if(state.isConnected==true){
                fetch(baseUrl+this.props.userId+'/cart')
                .then((response) => response.json())
                 .then((responseJson) => {
                      this.props.setOrderDetails(responseJson.data.cart_products),
                      this.props.setTotalAmount(responseJson.data.sub_total.toFixed(2))
                      if(responseJson.data.total_weight==0){
                        this.props.setTotalWeight(0)
                      }else{
                        this.props.setTotalWeight(responseJson.data.total_weight)
                      }
                })   .catch((err)=>{
              })  
              } 
        else{
          this.setState({networkFailed:true})
            alert("Check network connection")
        }
      })        
      //this.setState({loading:false,deleteProductStatus:false})
    }
  }

  async getCartDetail(){  
   await NetInfo.fetch().then(state => {
      if(state.isConnected==true){
              this.setState({loading:true})
              fetch(baseUrl+this.props.userId+'/cart')
              .then((response) => response.json())
               .then((responseJson) => {
                   //consle.log(responseJson)
                    if(responseJson.data.cart_products.length!==0){
                      this.props.setOrderDetails(responseJson.data.cart_products),
                      this.props.setTotalAmount(responseJson.data.sub_total.toFixed(2))
                      if(responseJson.data.total_weight==0){
                        this.props.setTotalWeight(0)
                      }else{
                        this.props.setTotalWeight(responseJson.data.total_weight)
                      }
                      this.setState({loading:false})
                    }else{
                      this.setState({loading:false})
                    }
                    
                    
                    ////////consle.log(this.props.totalAmount,this.props.deliveryCharge)
              })   .catch((err)=>{
                //consle.log(err)
            })
            //////consle.log( "tw"+this.props.totalWeight)  
            } 
      else{
        this.setState({networkFailed:true})
          alert("Check network connection")
      }
    }) 
    await this.props.setCategoryProductCountChanged(false)           
}

// async addToCart(indexValue,itemName){
//     await this.setState({totalAmount:0,totalWeight:0,cartLoading:true,selectedCartItem:indexValue})
//     await this.props.orderDetails.map((item,index)=>{
//          this.setState({totalAmount:this.state.totalAmount+(item.price*item.quantity),
//               totalWeight:this.state.totalWeight+(item.weight*item.quantity)
//              })
//     })
//  await this.props.setTotalAmount(this.state.totalAmount.toFixed(2))
//  if(this.state.totalWeight==0){
//    await this.props.setTotalWeight(0)
//  }else{
//   await this.props.setTotalWeight(this.state.totalWeight.toFixed(2))
//  }
// this.setState({cartLoading:false})

// ////consle.log(this.props.userId,this.props.selectedProduct.id,this.props.selectedProduct.variant_id,this.props.selectedProduct.quantity,)
//  await fetch(baseUrl+this.props.userId+'/addtocart', {
//          method: 'POST',
//          headers: {
//          'Content-Type': 'application/json',
//          },
//          body: JSON.stringify({
//             "customer_id":this.props.userId,
//             "product_id": this.props.selectedProduct.id,
//             "product_variant_id" :this.props.selectedProduct.variant_id,    
//             "quantity":this.props.selectedProduct.quantity,
//          })
//        })
//      .then((response) => response.json())
//      .then((responseJson) => {
//           //////consle.log(responseJson)
//      }).catch((err)=>{
//       //////consle.log(err)
//   })  
//   await this.props.setCategoryProductCountChanged(true)
//  // await this.props.setProductCountChanged(true)
//   this.setState({cartLoading:false,IncOrDecProductName:itemName,IncOrDecProductCountChanged:true})
//   setTimeout(()=>{
//     this.setState({IncOrDecProductCountChanged:false})
//    },1000)
//  }

async customQty(text,indexValue,item){
  console.warn(indexValue)
  var products = [...this.props.orderDetails];
  await this.setState({arrayIndexValue:indexValue,itemRowId:item.row_id,selectedCartItem:indexValue})
  
  if(text==""){
    products[indexValue].quantity=0;
    await this.setState({iseditingValue:""})
  }else{
  products[indexValue].quantity=text;
  await this.setState({iseditingValue:products[indexValue].quantity})
  await this.props.setOrderDetails(products)
  await this.props.setSelectProduct(this.props.orderDetails[indexValue])
  }
}

  async increaseQty(indexValue,item){
      await this.setState({isediting:false})
      let products = [...this.props.orderDetails];
      products[indexValue].quantity=parseInt(products[indexValue].quantity)+1
      await this.props.setOrderDetails(products)
      await this.props.setSelectProduct(this.props.orderDetails[indexValue])
      await this.addToCart(indexValue,item.row_id)
 }

async decreaseQty(indexValue,item){
     await this.setState({isediting:false})
     let products = [...this.props.orderDetails];
     products[indexValue].quantity-=1;
     
    if(products[indexValue].quantity<1){
      products[indexValue].quantity=1
      this.removeProduct(item.row_id,indexValue)
    }else{
    await this.props.setOrderDetails(products)
    await this.props.setSelectProduct(this.props.orderDetails[indexValue])
    await this.addToCart(indexValue,item.row_id)
    }
  }

  async addToCart(indexValue,rowid){
    // console.error(this.props.selectedProduct.quantity, rowid)
    await this.setState({totalAmount:0,totalWeight:0,cartLoading:true,selectedCartItem:indexValue})
    await this.props.orderDetails.map((item,index)=>{
         this.setState({totalAmount:this.state.totalAmount+(item.price*item.quantity),
              totalWeight:this.state.totalWeight+(item.weight*item.quantity)
             })
    })
 await this.props.setTotalAmount(this.state.totalAmount.toFixed(2))
//  console.warn(this.state.totalWeight.toFixed(2))
 if(this.state.totalWeight==0){
   await this.props.setTotalWeight(0)
 }else{
  await this.props.setTotalWeight(this.state.totalWeight.toFixed(2))
 }

////consle.log(this.props.userId,this.props.selectedProduct.id,this.props.selectedProduct.variant_id,this.props.selectedProduct.quantity,)
 await fetch(baseUrl+this.props.userId+'/updatecart', {
         method: 'POST',
         headers: {
         'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            "row_id"   :rowid,
            "quantity":this.props.selectedProduct.quantity,
         })
       })
     .then((response) => response.json())
     .then((responseJson) => {
          consle.log(responseJson)
     }).catch((err)=>{
      ////consle.log(err)
  })  
  await this.props.setCategoryProductCountChanged(true)
  await this.props.setProductCountChanged(true)
  this.setState({cartLoading:false,isediting:false,itemRowId:null,IncOrDecProductName:"test",IncOrDecProductCountChanged:true,snackbarVisible:true})
  setTimeout(()=>{
    this.setState({IncOrDecProductCountChanged:false})
   },500)
   
   //Toast.show({text:`You've changed '{this.state.IncOrDecProductName}' QUANTITY to '{this.props.selectedProduct.quantity}'`,duration:1000,textStyle:{fontSize:14}})
 }

async incdecEdit(item,index){
  await this.setState({selectedItemForEditing:item.id,selectedCartItem:index})
  var qty=item.quantity.toString()
  //consle.log(qty)
  this.setState({iseditingValue:qty,isediting:true})
 }

 async remove(){
    await this.setState({removeProduct:true})
     this.removeProduct(this.state.removeCartProductId,this.state.removeCartProductIndex)
  }

async removeProduct(cartItemId,index){
   await this.setState({removeCartProductId:cartItemId,removeCartProductIndex:index,removeProductModalVisible:true})
  if(this.state.removeProduct==true){
    await this.setState({removeProductModalVisible:false,deleteProductStatus:true})

    await fetch(baseUrl+this.props.userId+'/removecart', {
      method: 'POST',
      headers: {
      'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "row_id":cartItemId
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      ////consle.log(responseJson)
      this.setState({deleteProductStatus:false,removeProduct:false})
    }).catch((err)=>{
      this.setState({deleteProductStatus:false,removeProduct:false})
    })
    this.setState({deleteProductStatus:false,removeProduct:false})
      let products = [...this.props.orderDetails];
      products.pop(this.state.removeCartProductIndex)
      await this.props.setOrderDetails(products)

    await this.props.setProductCountChanged(true)
    await this.props.setCategoryProductCountChanged(true)
  }
  this.setState({deleteProductStatus:false})
  //console.warn(this.state.deleteProductStatus)
}

async navigateCompleteOrder(){
  await this.props.setSingleProductId(null)
  if(this.props.deliveryType==0){
      await this.props.setorderDeliverType(0)
      this.props.navigation.navigate("completeOrder")
 }else{
  await this.props.setorderDeliverType(1)
  this.props.navigation.navigate("completeOrder")
 }
}

 async setDeliveryType(value){
  if(value==0){
    await this.setState({deliveryType:true})
    await this.props.setorderDeliverType(0)
  }else{
    await this.setState({deliveryType:false})
    await this.props.setorderDeliverType(1)
  }
}

async navigateSingleProductScreen(id){
  await this.props.setProductId(id)  
  this.props.navigation.navigate("singleProductScreen")
}

async continueToPickup(){
   await this.setState({modalVisble:false})
   await this.props.setorderDeliverType(1)
   this.props.navigation.navigate("completeOrder")
 }

  render() {
   const totalAmount=this.props.totalAmount
   const totalWeight=this.props.totalWeight
   if(this.state.loading==true){
     return(
       <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#fff'}}>
         <ActivityIndicator size="large" color="#fa7153"/>
         <Text style={{fontFamily:"Montserrat",paddingTop:10}}>Loading</Text>
       </View>
     )
   }else {
    return (
      <View style={OverAllStyle.MainContainerPad0}>
          <SafeAreaView style={OverAllStyle.HeaderMainView}>

            <View style={OverAllStyle.HeaderStyle}>

                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:"home"})} style={OverAllStyle.HeaderIcon}>
                  <Image source={images.backArrow} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>

                  <Text style={{fontFamily:'Montserrat-SemiBold',color: '#FFF',fontSize:17}}>My cart</Text>

                <TouchableOpacity style={{justifyContent:'center'}} onPress={()=>this.props.navigation.navigate("notification")} style={HomeScreenStyle.MenuIcon}>
                    {
                      this.props.notificationCount==0?
                        null
                        :
                        /* <View style={{width:15,height:15,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:10,left:10}}>
                        <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text>
                        </View>                             */
                        <View style={{width:7,height:7,backgroundColor:'#fff',position:'absolute',alignSelf:'flex-end',borderRadius:10,alignItems:'center',justifyContent:'center',bottom:12,left:12}}>
                        {/* <Text style={{fontSize:8,fontFamily:'Montserrat',textAlign:'center'}}>{this.props.notificationCount}</Text> */}
                        </View>                            
                    }
                  <Image source={images.bell} style={StyleContainer.ImageContainStyle}/>
                </TouchableOpacity>
            
            </View>
            
        </SafeAreaView>
        {
          this.props.orderDetails.length==0?
        
            <View style={{width:'100%',height:'100%',justifyContent:'center',alignItems:'center'}}>
              <View style={{width:'80%',height:200}}>
                <Image source={images.emptyCart} style={{width:undefined,height:undefined,flex:1,resizeMode:'contain'}}></Image>
              </View>          
                <Text style={{fontFamily:"Montserrat",textAlign:'center'}}>Your cart is empty!</Text>
            
                <Text style={{fontFamily:"Montserrat",paddingVertical:10,fontSize:12}}>Add items to it now.</Text>
            <View style={{width:'80%'}}>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("drawer",{screen:'home'})}
                  style={StyleContainer.ButtonOrange}>
                  <Text style={{fontFamily:"Montserrat-SemiBold",color:'#fff'}}>Shop now</Text>
                </TouchableOpacity>
              </View>
            </View>
            :
      <ScrollView>
        
            <View>
      
              <View style={CartScreenStyle.WhiteContainer} >
              {
                this.props.orderDetails.map((item,index)=>{
                  var productPrice=(item.price * item.quantity).toFixed(2)
                  return(
                    
                    <View key={item.cartItem_id} style={CartScreenStyle.ProductMainContainer}>
                      <View style={CartScreenStyle.ProductContainer}>
                              
                              <TouchableOpacity style={CartScreenStyle.DeleteImgContainer}
                              onPress={()=>{this.removeProduct(item.row_id,index)}}>
                                  <View style={CartScreenStyle.DeleteImgSubContainer}>
                                  <Image source={images.delete} style={StyleContainer.ImageCoverStyle}/>
                                  </View>
                              </TouchableOpacity>

                              
                                <View style={[CartScreenStyle.ProductImgContainer,{justifyContent:'center'}]}>
                                  <Image source={{uri:item.image_url}} style={[StyleContainer.ImageCoverStyle,{borderRadius:5,marginRight:5}]}/>
                                </View>                             
                              

                              <TouchableOpacity onPress={()=>this.navigateSingleProductScreen(item.id)}  style={CartScreenStyle.ProductNameContainer}>
                                <Text style={CartScreenStyle.prodctTxtStyle} numberOfLines={2} ellipsizeMode="middle" >{item.name}</Text>
                                  {/* {
                                    item.option_text==null || item.option_value==null?null:
                                    <Text style={{fontSize:10,color:'gray',fontFamily:'Montserrat'}}>{item.option_text} : {item.option_value} {item.option_unit}</Text>
                                  } */}
                                  {
                                    item.product_variants==""?null:
                                    <Text numberOfLines={3} ellipsizeMode="tail" style={{fontSize:10,color:'gray',fontFamily:'Montserrat',flexShrink: 1}}>{item.product_variants}</Text>
                                  }
                              </TouchableOpacity>
                      </View>


                      <View style={[CartScreenStyle.ProductQtyPriceContainer,{flex:1}]}>

                        {/* <View style={{width:'20%',alignItems:'center'}}>
                           {
                             item.weight==0?
                             null
                             :
                             <Text style={{fontFamily:'Montserrat',textAlign:'center',fontSize:8}}>{item.weight*item.quantity} ton</Text>
                           }
                           
                        </View> */}

                        <View style={{alignItems:'center'}}>                        
                        {
                          index==this.state.selectedCartItem?
                            <View style={{alignItems:'center',flex:1,justifyContent:'center',alignSelf:'center'}}>
                             {
                               this.state.cartLoading?
                                <View style={CartScreenStyle.IncDecMainContainer}>
                                    <TouchableOpacity style={CartScreenStyle.OrangeIncBtn}>
                                        <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                    </TouchableOpacity>
                                    
                                    <View style={CartScreenStyle.IncDecQtyContainer}>
                                      <ActivityIndicator color="#fa7153"/>
                                    </View>
                                    
                                    <TouchableOpacity style={CartScreenStyle.OrangeIncBtn}>
                                        <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                    </TouchableOpacity>
                                </View>
                               :
                               <View style={CartScreenStyle.IncDecMainContainer}>
                                  <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                                  onPress={()=>{this.decreaseQty(index,item)}}>
                                      <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                                  </TouchableOpacity>
                                  
                                  <TouchableOpacity onPress={()=>this.incdecEdit(item,index)} style={CartScreenStyle.IncDecQtyContainer}>
                                    {
                                      this.state.isediting==true && item.id==this.state.selectedItemForEditing?
                                        <TextInput
                                          autoFocus={this.state.isediting}
                                          style={{width:'100%',height:24,paddingVertical:0,fontFamily:'Montserrat',fontSize:12,textAlign:'center',color:Colors.themeColor}}
                                          onChangeText={(text)=>this.customQty(text,index,item)}
                                          value={this.state.iseditingValue}
                                          // onSubmitEditing={()=>}
                                          keyboardType={"phone-pad"}
                                          // onEndEditing={(value)=>console.warn(value)}
                                        />
                                        :
                                        <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                    }
                                  </TouchableOpacity>
                                  
                                  <TouchableOpacity style={CartScreenStyle.OrangeIncBtn}
                                    onPress={()=>{this.increaseQty(index,item)}}>
                                      <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                                  </TouchableOpacity>
                              </View>                              
                             }
                            </View>
                          :
                          <View style={CartScreenStyle.IncDecMainContainer}>
                              <TouchableOpacity style={CartScreenStyle.OrangeIncBtn} 
                              onPress={()=>{this.decreaseQty(index,item)}}>
                                  <Text style={OverAllStyle.IncDecTxtStyle}> - </Text>
                              </TouchableOpacity>
                              
                              <TouchableOpacity onPress={()=>this.incdecEdit(item,index)} style={CartScreenStyle.IncDecQtyContainer}>
                                {
                                  this.state.isediting==true && item.id==this.state.selectedItemForEditing?
                                    <TextInput
                                      autoFocus={this.state.isediting}
                                      style={{width:'100%',height:24,paddingVertical:0,fontFamily:'Montserrat',fontSize:12,textAlign:'center',color:Colors.themeColor}}
                                      onChangeText={(text)=>this.customQty(text,index,item)}
                                      value={this.state.iseditingValue}
                                      keyboardType={"phone-pad"}
                                      // onEndEditing={(value)=>console.warn(value)}
                                    />
                                    :
                                    <Text style={OverAllStyle.QtyTxtStyle}>{item.quantity}</Text>
                                }
                              </TouchableOpacity>
                              
                              
                              <TouchableOpacity style={CartScreenStyle.OrangeIncBtn}
                              onPress={()=>{this.increaseQty(index,item)}}>
                                  <Text style={OverAllStyle.IncDecTxtStyle}> + </Text>
                              </TouchableOpacity>
                          </View>
                        }
                      </View>

                        <View style={{width:'50%',alignItems:'flex-start'}}>
                        <Text numberOfLines={1} ellipsizeMode="tail"  style={{fontFamily:'Montserrat-SemiBold',fontSize:10}}>${productPrice}</Text>
                        </View>
                      </View>

                    </View>
                  )
                })
              }

              <View style={CartScreenStyle.WhiteSubContainer}>
                  
                  {
                    totalWeight==0?null:
                    <View style={CartScreenStyle.SpacebtwnContainer}>
                      <Text style={CartScreenStyle.blackBoldHeadTxtStyle}>Total weight (approx..)</Text>
                      <Text style={CartScreenStyle.blackBoldHeadTxtStyle}>{totalWeight} Ton</Text>
                    </View>
                  }

                  {/* <View style={CartScreenStyle.SpacebtwnContainer}>
                    <Text style={CartScreenStyle.blackBoldHeadTxtStyle}>Total amount</Text>
                    <Text style={CartScreenStyle.blackBoldHeadTxtStyle}>$ {totalAmount}</Text>
                  </View> */}

              </View>
            

          </View>
              

              

          {/* <View style={CartScreenStyle.WhiteContainer} >
            <View style={CartScreenStyle.WhiteSubContainer}>
                <View style={CartScreenStyle.SpacebtwnContainer}>

                <View style={CartScreenStyle.OfferIconMainView}>
                    <View style={CartScreenStyle.OfferIconSubView}>
                        <Image source={images.offers} style={StyleContainer.ImageContainStyle}/>
                    </View>
                </View>

                <View style={CartScreenStyle.TxtInputMainView}>
                    <TextInput placeholder="Enter coupon code" style={CartScreenStyle.TxtInputView}/>
                </View>

              <View style={CartScreenStyle.ApplyBtnMainView}>
                  <TouchableOpacity style={CartScreenStyle.ApplyBtnSubView}>
                    <Text style={CartScreenStyle.applyTxtStyle}>APPLY</Text>
                  </TouchableOpacity>
              </View>
                
                
                </View>
                
            </View>
          </View> */}


          {
            this.props.deliveryType==0?
              <View style={CartScreenStyle.WhiteContainer} >
                <View style={CartScreenStyle.WhiteSubContainer}>
                  <View style={{marginBottom:10}}>
                  <Text style={CartScreenStyle.blackBoldHeadTxtStyle}>Order type</Text>
                  </View>
                      <TouchableOpacity style={{flexDirection:'row'}}>
                        <View style={{width:18,height:18,borderRadius:10,borderWidth:1,borderColor:'#EEE',justifyContent:'center',alignItems:'center',marginRight:10}}>
                          <View style={{width:10,height:10,borderRadius:10,backgroundColor:'#fd7153'}} />
                        </View>
                        <Text style={{fontFamily:"Montserrat"}}>Delivery</Text>
                      </TouchableOpacity>
                </View>
              </View>
              :
              null
          }

          <View style={CartScreenStyle.WhiteContainer} >
              {/* <View style={CartScreenStyle.WhiteSubBtmBdrContainer}>
                  <View style={CartScreenStyle.SpacebtwnContainer}>
                    <Text style={{fontFamily:'Montserrat-SemiBold'}}>Item total amount</Text>
                    <Text style={{fontFamily:'Montserrat-SemiBold'}}>$ {totalAmount}</Text>
                  </View>
              </View> */}
              

              <View style={CartScreenStyle.WhiteSubContainer}>
                  <View style={CartScreenStyle.SpacebtwnContainer}>
                     <Text style={CartScreenStyle.PayTxtStyle}>Total amount</Text>
                     <Text style={CartScreenStyle.PayTxtStyle}>$ {totalAmount}</Text>
                  </View>
              </View>
          </View>

        </View>
        
       </ScrollView>
   }
     
        <TouchableOpacity onPress={()=>this.navigateCompleteOrder()} style={CartScreenStyle.nextButtonContainer}>
          <View>
            <Text style={{fontFamily:'Montserrat-SemiBold',color:'#fff',fontSize:15}}>Next</Text>
            <Text style={{fontFamily:'Montserrat',color:'#fff',fontSize:12}}>Checkout</Text>
          </View>
          <View style={{justifyContent: 'center',}}>
            <Image source={images.backArrow} style={{width:25,height:25,transform:[{rotate:'180deg'}]}}/>
          </View>
        </TouchableOpacity>
       
     
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.deleteProductStatus}
        onRequestClose={()=>this.setState({deleteProductStatus:false})}
        >                    
          
          <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.3)'}}>
              <ActivityIndicator color="#fa7153" size="large"/>
          </TouchableOpacity>

      </Modal>

      {/* <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.loading}>                    
          <TouchableOpacity style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.2)'}}>
              <ActivityIndicator color="#fa7153" size="large"/>
          </TouchableOpacity>
      </Modal> */}

      {/* <Modal 
            animationType="fade"
            transparent={true}
            visible={this.state.IncOrDecProductCountChanged}
            onRequestClose={()=>this.setState({IncOrDecProductCountChanged:false})}>
              <View style={{flex:1,justifyContent:'flex-end'}}>
                  <View style={{backgroundColor:'#f2f5f6',width:'80%',alignSelf:'center',padding:5,marginBottom:30,borderRadius:5}}>
                      <Text style={{fontFamily:"Montserrat",textAlign:'center',fontSize:13,paddingBottom:5}}>You've changed '{this.state.IncOrDecProductName}' QUANTITY to '{this.props.selectedProduct.quantity}'</Text>
                  </View>   
          </View>
      </Modal>      */}

      <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.removeProductModalVisible}
          onRequestClose={()=>this.setState({removeProductModalVisible:false})}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.5)'}}>
              
              <View style={{width:'85%',backgroundColor:'#FFF',borderRadius:5}}>
              <View>
                  <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,paddingVertical:10,color:'black'}}>Remove Item</Text>
                  <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,fontSize:12}}>Are you sure you want to remove this item?</Text>
                  <View style={{borderWidth:0.3,borderColor:'gray',width:'100%',flexDirection:'row',marginTop:10}}>
                    <TouchableOpacity onPress={()=>this.setState({removeProductModalVisible:false})}
                    style={{alignItems:'center',width:'50%',padding:10,borderRightWidth:0.3,borderRightColor:'gray'}}>
                      <Text style={{fontFamily:'Montserrat'}}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.remove()}
                    style={{alignItems:'center',width:'50%',padding:10}}>
                      <Text style={{fontFamily:'Montserrat',color:'#fa7153'}}>Remove</Text>
                    </TouchableOpacity>
                  </View>
              </View>
              </View>
          </View>
      </Modal>

      <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisble}
          onRequestClose={()=>this.setState({modalVisble:false})}>
          <View style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'rgba(52,52,52,0.5)'}}>
              
              <View style={{width:'85%',backgroundColor:'#FFF',borderRadius:5}}>
              <View>
                  <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,paddingVertical:10,color:'black'}}>Check availability </Text>
                  <Text style={{fontFamily:"Montserrat",paddingHorizontal:20,fontSize:12}}>Your order not accommodate for delivery, please choose pick-up.</Text>
                  <View style={{borderWidth:0.3,borderColor:'gray',width:'100%',flexDirection:'row',marginTop:10}}>
                    <TouchableOpacity onPress={()=>this.setState({modalVisble:false})}
                    style={{alignItems:'center',width:'50%',padding:10,borderRightWidth:0.3,borderRightColor:'gray'}}>
                      <Text style={{fontFamily:'Montserrat'}}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.continueToPickup()}
                    style={{alignItems:'center',width:'50%',padding:10}}>
                      <Text style={{fontFamily:'Montserrat',color:'#fa7153'}}>Continue</Text>
                    </TouchableOpacity>
                  </View>
              </View>
              </View>
          </View>
      </Modal>

      <Snackbar
        visible={this.state.snackbarVisible}
        onDismiss={()=>this.setState({snackbarVisible:false})}
        duration={3000}
        action={{
          label: 'close',
          onPress: () => {
            this.setState({snackbarVisible:false})
          },
        }}>
        Item updated successfully!
      </Snackbar>
    </View>
    );
   }
  }
}
function mapStateToProps(state){
  return{
      userId:state.userId,
      orderDetails:state.orderDetails,
      selectedProduct:state.selectedProduct,
      totalAmount:state.totalAmount,
      totalWeight:state.totalWeight,
      productCountChanged:state.productCountChanged,
      categoryProductCountChange:state.categoryProductCountChange,
      singleProductId:state.singleProductId,
      deliveryType:state.deliveryType,
      choosedPickupMethod:state.choosedPickupMethod,
      notificationCount:state.notificationCount,
      orderDeliverType:state.orderDeliverType
  }
}

function mapDispatchToProps(dispatch){
  return{
      setOrderDetails:(value)=>dispatch({type:"setOrderDetails",value}),
      setSelectProduct:(value)=>dispatch({type:"setSelectProduct",value}),
      setTotalAmount:(value)=>dispatch({type:"setTotalAmount",value}),
      setTotalWeight:(value)=>dispatch({type:"setTotalWeight",value}),
      setProductCountChanged:(value)=>dispatch({type:"setProductCountChanged",value}),
      setCategoryProductCountChanged:(value)=>dispatch({type:"setCategoryProductCountChange",value}),
      setGuestLogin:(value)=>dispatch({type:"setGuestLogin",value}),
      setSingleProductId:(value)=>dispatch({type:"setSingleProductId",value}),   
      setDeliveryType:(value)=>dispatch({type:"setDeliveryType",value}),
      setChoosedPickupMethod:(value)=>dispatch({type:"setChoosedPickupMethod",value}),
      setProductId:(value)=>dispatch({type:"setProductId",value}),
      setorderDeliverType:(value)=>dispatch({type:'setorderDeliverType',value}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(CartScreen);